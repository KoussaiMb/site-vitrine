<?php include_once('inc/bootstrap.php'); ?>
<?php
if (strtolower($_SERVER['HTTP_STAGE']) !== 'local' && (!isset($_GET['note'])))
    App::setFlashAndRedirect('danger', 'Un problème est survenu, n\'hésitez pas à nous en informer', '/');
?>
<?php include_once('inc/header_start.php'); ?>
<meta name="google" content="nositelinkssearchbox">
<title>Nobo - Satisfaction</title>
<link rel="stylesheet" href="css/public_style.css">
<?php include_once('inc/header_start.php'); ?>
<?php include_once('inc/navbar.php'); ?>
<?php include_once('inc/navbar_phone.php'); ?>

<section class="first-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 text-center spaceTopBottom">
                <div class="col-xs-12">
                    <img class="img-responsive" src="img/nobo/sticker/thumbs-up.svg" alt="hotel-paris-dessin" style="margin: auto">
                </div>
                <div class="col-xs-12">
                    <h2>Merci de votre retour</h2>
                    <p>
                        Nous avons à cœur de vous satisfaire à chaque prestation et suivons avec rigueur vos notations 
                        après chacun de nos passages.
                    </p>
                    <a href="/"><button class="btn btn-blue" style="margin-top: 20px">Retourner sur le site</button></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once('inc/footer_start.php'); ?>
<?php include_once('inc/analyticstracking.php'); ?>
<?php include_once('inc/footer_end.php'); ?>