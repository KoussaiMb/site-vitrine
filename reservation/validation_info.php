<?php
include_once('../inc/bootstrap.php');
$subForm = Session::getInstance()->read('subForm');

if (!$subForm || $subForm && !isset($subForm->token)) {
    Session::getInstance()->setFlash('info', 'Commençez par renseigner votre besoin.');
    App::redirect('besoin');
}

if (empty($_POST['address'])){
    $_POST['address'] = $_POST['autocomplete'];
}
Session::getInstance()->update('recapInfo', (object)$_POST);
$recap = Session::getInstance()->read('recap');
$all = (array)$recap + $_POST + (array)$subForm;
$all['payLater'] = isset($all['payLater']) ? 1 : 0;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // No apiToken, apiToken expired ? -> Guest
    if (empty(Session::getInstance()->read('apiToken')) ||
        !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken')))
        ApiCaller::addGuestApiTokenToSession('besoin');

    // Subscriber exists?
    $apiConnector = ApiCaller::retrieveSubscriber($all['token']);
    if ($apiConnector->getHttpCode() !== 200)
        App::setFlashAndRedirect('danger',
            'Commençez par renseigner votre besoin.', 'besoin');

    // Update subscriber
    $apiConnector = ApiCaller::updateSubscriberInfo($all);
    if ($apiConnector->getHttpCode() !== 200) {
        App::setFlashArrayAndRedirect('danger',
            $apiConnector->getResponse()->message, 'info');
    }

    // Save needed data for next step
    $subForm = (object)[
        'token' => $all['token'],
        'paymentStep' => true,
        'firstname' => $all['firstname'],
        'lastname' => $all['lastname'],
        'email' => $all['email'],
        'price' => $all['price'],
        'date' => $all['date'],
        'recurrence_id' => $all['recurrence_id'],
        'hour' => $all['hour'],
        'surface' => $all['surface'],
        'workTime' => $all['workTime'],
        'workTimeChosen' => $all['workTimeChosen']
    ];
    Session::getInstance()->update('subForm', $subForm);

    // Mail new customer
    $all['status'] = 'Nouveau client';
    $all['paiement'] = 'Non';
    ApiCaller::mailJetNewCustomer($all);                        // TODO : if sending mail failed -> notify / alert
    App::redirect('paiement?payment=sepa');
}
App::setFlashArrayAndRedirect('danger', 'Un problème est survenu, veuillez réessayer', 'info');
