<?php
include_once('../inc/bootstrap.php');

$subForm = Session::getInstance()->read('subForm');
$recap = Session::getInstance()->read('recap');

if (empty($subForm))
{
    Session::getInstance()->delAll();
    $subForm = (object)['token' => my_crypt::genStandardToken(), 'paymentStep' => false];
    Session::getInstance()->write('subForm', $subForm);
}
if (!empty($recap) && isset($recap->recurrence_id)){
    $recPos = App::getListManagement()->getPositionById($recap->recurrence_id) - 1;
    $recField = App::getListManagement()->getFieldById($recap->recurrence_id);
}

include_once('../inc/header_start.php');
?>
<!-- title -->
<title>Nobo - Réservation - besoin</title>
<!-- Google -->
<meta name="description" content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie pour le ménage et le repassage, partout à Paris. Réservation en ligne, sans engagement ni frais d'inscription">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- Facebook -->
<meta property="og:url"           content="https://nobo.life/femme-de-menage/paris"/>
<meta property="og:type"           content="website"/>
<meta property="og:title"           content="Nobo - Femme de menage haut de gamme a Paris"/>
<meta property="og:description"       content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie pour le ménage et le repassage, partout à Paris. Réservation en ligne, sans engagement ni frais d'inscription"/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="img/fb.png"/>
<!-- CSS -->
<meta name="google" content="nositelinkssearchbox">
<link rel="stylesheet" href="/css/public_style.css">
<link rel="stylesheet" href="/css/lib/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/lib/bootstrap-slider.min.css">
<?php include_once("../inc/analyticstracking.php"); ?>
<script>
    /* loadCSS. [c]2017 Filament Group, Inc. MIT License */
    /* Standalone workflow */
    (function( w ){
        "use strict";
        if( !w.loadCSS ){
            w.loadCSS = function(){};
        }
        var rp = loadCSS.relpreload = {};
        rp.support = (function(){
            var ret;
            try {
                ret = w.document.createElement( "link" ).relList.supports( "preload" );
            } catch (e) {
                ret = false;
            }
            return function(){
                return ret;
            };
        })();

        rp.bindMediaToggle = function( link ){
            var finalMedia = link.media || "all";

            function enableStylesheet(){
                link.media = finalMedia;
            }
            if( link.addEventListener ){
                link.addEventListener( "load", enableStylesheet );
            } else if( link.attachEvent ){
                link.attachEvent( "onload", enableStylesheet );
            }
            setTimeout(function(){
                link.rel = "stylesheet";
                link.media = "only x";
            });
            setTimeout( enableStylesheet, 3000 );
        };

        rp.poly = function(){
            if( rp.support() ){return;}
            var links = w.document.getElementsByTagName( "link" );
            for( var i = 0; i < links.length; i++ ){
                var link = links[i];
                if( link.rel === "preload" && link.getAttribute( "as" ) === "style" && !link.getAttribute( "data-loadcss" ) ){
                    link.setAttribute( "data-loadcss", true );
                    rp.bindMediaToggle( link );
                }
            }
        };
        if( !rp.support() ){
            rp.poly();

            var run = w.setInterval( rp.poly, 500 );
            if( w.addEventListener ){
                w.addEventListener( "load", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            } else if( w.attachEvent ){
                w.attachEvent( "onload", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            }
        }
        if( typeof exports !== "undefined" ){
            exports.loadCSS = loadCSS;
        }
        else {
            w.loadCSS = loadCSS;
        }
    }( typeof global !== "undefined" ? global : this ) );
</script>
</head>
<body class="sub-body">

<div class="sub-head">
    <div class="reservation-ban-1"></div>
    <div class="reservation-ban-2"><img src="/img/nobo/logo/nobo_logo_baseline_400x240.png"></div>
    <div class="reservation-ban-3"></div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
            <!-- head -->
            <div class="col-xs-12 sub-bg pricing-head">
                <h4 class="active"><span class="glyphicon glyphicon-briefcase"></span> Votre besoin</h4>
                <h4 class="hidden-xs"><span class="glyphicon glyphicon-user"></span> Informations personnelles</h4>
                <h4 class="hidden-xs"><span class="glyphicon glyphicon-lock"></span> Paiement</h4>
            </div>

            <!-- body -->
            <div class="col-xs-12 col-sm-8 sub-bg">
                <div>
                    <?php include('../inc/print_flash_helper.php'); ?>
                </div>
                <form novalidate autocomplete="off" action="validation_besoin" method="post" data-toggle="validator" id="subFormNeed">
                    <!--new one -->
                    <div class="group-pricing">
                        <div class="col-xs-12 col-sm-8">
                            <div class="pricing-container">
                                <div class="cnrflash-reservation">
                                    <div class="cnrflash-inner">
                                        <span class="cnrflash-label">Populaire</span>
                                    </div>
                                </div>
                                <div class="pricing-title">
                                    besoin récurrent
                                </div>
                                <div class="print-price rec-price">
                                    <p>28€/h</p>
                                    <p class="price-detail">Soit <strong>14€/h</strong> après crédit d'impôt</p>
                                </div>
                                <div class="flex-row flex-center-stretch">
                                    <div class="col-xs-6 flex-col flex-between pricing-choice">
                                        <p>Plusieurs fois par semaine</p>
                                        <button type="button" class="flex-row flex-center btn-rec <?= !isset($recPos) ? 'cur' : ''; ?><?= isset($recPos) && $recPos == 0 ? 'cur' : ''; ?>" name="rec0" value="0"><span class="glyphicon glyphicon-ok"></span></button>
                                    </div>
                                    <div class="col-xs-6 flex-col flex-between pricing-choice">
                                        <p>Une fois par semaine</p>
                                        <button type="button" class="flex-row flex-center btn-rec <?= isset($recPos) && $recPos == 1 ? 'cur' : ''; ?>" name="rec1" value="1"><span class="glyphicon glyphicon-ok"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="pricing-container">
                                <div class="ponctuel-title">
                                    Ponctuel
                                </div>
                                <div class="print-price ponctuel-price">
                                    <p>32€/h</p>
                                    <p class="price-detail">Soit <strong>16€/h</strong> après crédit d'impôt</p>
                                </div>
                                <div class="flex-row flex-center-stretch">
                                    <div class="col-xs-6 flex-col flex-between pricing-choice">
                                        <p>Deux fois par mois</p>
                                        <button type="button" class="flex-row flex-center btn-rec <?= isset($recPos) && $recPos == 2 ? 'cur' : ''; ?>" name="rec2" value="2"><span class="glyphicon glyphicon-ok"></span></button>
                                    </div>
                                    <div class="col-xs-6 flex-col flex-between pricing-choice">
                                        <p>Une fois</p>
                                        <button type="button" class="flex-row flex-center btn-rec <?= isset($recPos) && $recPos == 3 ? 'cur' : ''; ?>" name="rec3" value="3" id="rec3"><span class="glyphicon glyphicon-ok"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="sub-hr"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <h3>votre domicile</h3>
                        </div>
                        <div class="col-xs-12 workTime-wrap-outer">
                            <div class="col-xs-12 col-md-6 workTime-wrap-inner">
                                <div>
                                    <h4>Superficie</h4>
                                    <input type="text" name="surface" value="<?= isset($recap->surface) ? htmlspecialchars($recap->surface) : 15; ?>" data-slider-min="15" data-slider-max="300" data-slider-step="5" data-slider-value="<?= isset($recap->surface) ? htmlspecialchars($recap->surface) : '15'; ?>" id="slider"/>
                                    <label for="slider"><span id="sliderValLabel" class="gold"><span id="sliderVal"><?= isset($recap->surface) ? htmlspecialchars($recap->surface) : '15'; ?> </span><span id="sliderVal2"> m2</span></span></label>
                                </div>
                                <div>
                                    <!-- img slide -->
                                    <div class="col-sm-12 spaceTopBottom hidden-xs">
                                        <img src="/img/nobo/gallery/tarif-nobo-studio-menage-paris.jpg" class="img-responsive" id="img-slider">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 workTime-wrap-inner">
                                <div class="">
                                    <p class="text-success" id="warranty">Suivant vos critères, voici le temps recommandé. Vous restez cependant libre de l'ajuster à votre guise !</p>
                                    <div class="col-xs-12 workTtime">
                                        <div class="workTime-part valign workTime-minus">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </div>
                                        <div class="workTime-part workTime-box valign">
                                            <input type="hidden" name="workTimeChosen" value="<?= isset($recap->workTimeChosen) ? $recap->workTimeChosen : '3:00'; ?>" id="workTimeChosen">
                                            <input type="hidden" name="workTime" value="<?= isset($recap->workTime) ? $recap->workTime : '3:00'; ?>" id="workTimeReal">
                                            <span id="workTime"><?= isset($recap->workTimeChosen) ? join('h', explode(':', $recap->workTimeChosen)) : '3h00'; ?></span>
                                        </div>
                                        <div class="workTime-part valign workTime-plus">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="sub-hr"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <h3>Date de la 1ère intervention</h3>
                        </div>
                        <div class="col-xs-12 col-sm-6 spaceTopBottom">
                            <label for="date">Date</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="calendar"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input type="text" class="form-control pickDate input-sm" name="date" value="<?= isset($recap->date) ? htmlspecialchars($recap->date) : null; ?>" placeholder="Début du service" aria-describedby="calendar" id="date"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 spaceTopBottom">
                            <label for="hour">Heure</label>
                            <div class="input-group">
                                <select class="form-control input-sm" name="hour" id="hour" required>
                                    <option value="00:00:00" <?= isset($recap->hour) && $recap->hour == "08:00:00" ? 'selected' : ''; ?> <?= !isset($recap->hour) ? 'selected' : ''; ?>>Cela m'est égal</option>
                                    <option value="08:00:00" <?= isset($recap->hour) && $recap->hour == "08:00:00" ? 'selected' : ''; ?>>08h00</option>
                                    <option value="09:00:00" <?= isset($recap->hour) && $recap->hour == "09:00:00" ? 'selected' : ''; ?>>09h00</option>
                                    <option value="10:00:00" <?= isset($recap->hour) && $recap->hour == "10:00:00" ? 'selected' : ''; ?>>10h00</option>
                                    <option value="11:00:00" <?= isset($recap->hour) && $recap->hour == "11:00:00" ? 'selected' : ''; ?>>11h00</option>
                                    <option value="12:00:00" <?= isset($recap->hour) && $recap->hour == "12:00:00" ? 'selected' : ''; ?>>12h00</option>
                                    <option value="13:00:00" <?= isset($recap->hour) && $recap->hour == "13:00:00" ? 'selected' : ''; ?>>13h00</option>
                                    <option value="14:00:00" <?= isset($recap->hour) && $recap->hour == "14:00:00" ? 'selected' : ''; ?>>14h00</option>
                                    <option value="15:00:00" <?= isset($recap->hour) && $recap->hour == "15:00:00" ? 'selected' : ''; ?>>15h00</option>
                                    <option value="16:00:00" <?= isset($recap->hour) && $recap->hour == "16:00:00" ? 'selected' : ''; ?>>16h00</option>
                                    <option value="17:00:00" <?= isset($recap->hour) && $recap->hour == "17:00:00" ? 'selected' : ''; ?>>17h00</option>
                                    <option value="18:00:00" <?= isset($recap->hour) && $recap->hour == "18:00:00" ? 'selected' : ''; ?>>18h00</option>
                                    <option value="19:00:00" <?= isset($recap->hour) && $recap->hour == "19:00:00" ? 'selected' : ''; ?>>19h00</option>
                                    <option value="20:00:00" <?= isset($recap->hour) && $recap->hour == "20:00:00" ? 'selected' : ''; ?>>20h00</option>
                                </select>
                                <span class="input-group-addon" id="clock"><span class="glyphicon glyphicon-time"></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-md-offset-3 spaceTopBottom">
                        <input type="hidden" name="rec" value="<?= isset($recPos) ? $recPos : 0; ?>" id="rec" required>
                        <button type="submit" class="btn btn-blue" id="nextStepButton">confirmer</button>
                    </div>
                </form>
            </div>

            <!-- float -->
            <div class="col-xs-12 col-sm-4 floater">
                <div class="col-xs-12 sub-bg">
                    <div class="recap clear-center">
                        <div>
                            <h4>récapitulatif:</h4>
                            <p>Date <span id="recapDate"><?= isset($recap->date) ? htmlspecialchars($recap->date) : '-'; ?></span></p>
                            <p>Heure <span id="recapHour"><?= !isset($recap->hour) || isset($recap) && htmlspecialchars($recap->hour) == "00:00" ? 'Cela m\'est égal' : htmlspecialchars($recap->hour); ?></span></p>
                            <p>Superficie <span id="recapSurface"><?= isset($recap->surface) ? htmlspecialchars($recap->surface) : '15'; ?> m²</span></p>
                            <p>Récurrence <span  id="recapRec"><?= isset($recField) ? $recField : 'Plusieurs fois par semaine'; ?></span></p>
                        </div>
                        <div class="sub-hr"></div>
                        <div>
                            <p><span id="recapWorktime"><?= isset($recap->workTimeChosen) ? join('h', explode(':', htmlspecialchars($recap->workTimeChosen))) : '3h00'; ?></span></p>
                            <?php if (!isset($recap->warranty) || isset($recap->warranty) && htmlspecialchars($recap->warranty) == 1): ?>
                                <p class="sub-detail clear-center text-success" id="recapWarranty">résultat garanti</p>
                            <?php else: ?>
                                <p class="recap-detail text-danger" id="recapWarranty">résultat <strong>NON</strong> garanti</p>
                            <?php endif; ?>
                        </div>
                        <div class="sub-hr"></div>
                        <div>
                            <p>Tarif <span><span  id="recapPriceBrut"><?= isset($recap->price) ? htmlspecialchars($recap->price) : '56'; ?></span> euros</span></p>
                            <p class="bold">Tarif réél* <span><span id="recapPriceNet"><?= isset($recap->price) ? htmlspecialchars($recap->price) / 2 : '28'; ?></span> euros</span></p>
                        </div>
                    </div>
                </div>
                <div class="sub-detail">*TTC, après <a href="/faq#ci" target="_blank">crédit d'impôt de 50%</a></div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery -->
<script src="/js/lib/jquery-3.1.1.min.js"></script>
<!-- Bootstrap JS -->
<script src="/js/bootstrap.min.js"></script>
<!-- JS vrac -->
<script src="/js/lib/bootstrap-datepicker.min.js"></script>
<script src="/js/locale/bootstrap-datepicker.fr.min.js"></script>
<script src="/js/lib/jquery.validate.min.js"></script>
<script src="/js/lib/jquery.validate.add.min.js"></script>
<script src="/js/lib/bootstrap-slider.min.js"></script>
<script src="/js/reservation_script.js"></script>
<script src="/js/js.cookie.js"></script>

<script>
    $(document).ready(function(){
        var recap = <?= isset($recap) ? '1;' : '0;'; ?>
        var slider = $('#slider');
        var workTime = $('#workTime');
        var recapRec = [
            'Plusieurs fois par semaine',
            'Une fois par semaine',
            'Deux fois par mois',
            'Ponctuel'
        ];

        //Init functions
        $(".pickDate").datepicker({
            language : "fr",
            startDate: "tomorrow",
            autoclose: 'true',
            format: "yyyy-mm-dd",
            disableTouchKeyboard: true,
            zIndexOffset: 1150
        });

        slider.slider({
            min: 15,
            max: 300,
            scale: 'logarithmic',
            step: 5
        });

        if (recap == 0)
            upWorktimeSubForm(parseInt(slider.val()), parseInt($('.cur').val()));
        checkWorktime(
            $('#workTimeChosen').val().split(':').join('h'),
            HMtoTime($('#workTimeReal').val().split(':').join('h')),
            parseInt($('.cur').val())
        );

        //Listeners

        $(".workTime-part").on('click', function () {
            var ret;
            var neededTime;
            var rec;

            rec = $('.cur').val();
            if ($(this).hasClass('workTime-plus'))
                ret = setWorktime(workTime.text(), 1);
            else if ($(this).hasClass('workTime-minus'))
                ret = setWorktime(workTime.text(), -1);
            if (ret) {
                neededTime = calculTime(parseInt(slider.val()), parseInt(rec));

                workTime.text(ret);
                $('#workTimeChosen').val(ret.split('h').join(':'));
                $('#workTimeReal').val(timeToHM(neededTime).split('h').join(':'));
                checkWorktime(ret, neededTime, parseInt(rec));
            }
        });

        $('.btn-rec').on('click', function () {
            $('.cur').removeClass('cur');
            $(this).addClass('cur');
            upWorktimeSubForm(parseInt(slider.val()), parseInt($(this).val()));
            $('#recapRec').text(recapRec[parseInt($(this).val())]);
            $('#rec').val(parseInt($(this).val()));
        });

        slider.on('change', function () {
            $("#sliderVal").text($(this).val());
            $('#recapSurface').text($(this).val());
        });
        slider.on("slideStop", function() {
            upWorktimeSubForm(parseInt($(this).val()), parseInt($('.cur').val()));
        });

        $('#date').on('changeDate', function () {
            $('#recapDate').text($(this).val());
        });

        $('#hour').on('change', function () {
            $('#recapHour').text($(this).children(':selected').text());
        });

        $('#subFormNeed').validate( {
            ignore: ":not(:visible)",
            rules: {
                date: {
                    required: true,
                    pattern: "(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"
                }
            },
            messages: {
                date: {
                    required: "Vous avez oublié la date de votre prestation",
                    pattern: "Entrez une date valide (aaaa-mm-jj)"
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `help-block` class to the error element
                error.addClass( "help-block red" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                } else {
                    error.insertAfter( element.parent() );
                }
            },
            highlight: function ( element ) {
                $( element ).parents( ".form-control" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element) {
                $( element ).parents( ".form-control" ).addClass( "has-success" ).removeClass( "has-error" );
            }
        });
    });

</script>
<?php include_once ('../inc/exit_popup_phone.php'); ?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <!-- Hotjar Tracking Code for https://www.nobo.life -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:666727,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
<?php endif; ?>
</body>
</html>