<?php
include_once('../inc/bootstrap.php');

$subForm = Session::getInstance()->read('subForm');
$recap = Session::getInstance()->read('recap');
$status = Session::getInstance()->read('status');


Session::getInstance()->delete('subForm');
Session::getInstance()->delete('recap');

include_once('../inc/header_start.php');
?>
<meta name="google" content="nositelinkssearchbox">
<title>Nobo - Confirmation de la réservation</title>
<link rel="stylesheet" href="/css/public_style.css">
<?php include_once("../inc/analyticstracking.php"); ?>
    <script>
        /* loadCSS. [c]2017 Filament Group, Inc. MIT License */
        /* Standalone workflow */
        (function( w ){
            "use strict";
            if( !w.loadCSS ){
                w.loadCSS = function(){};
            }
            var rp = loadCSS.relpreload = {};
            rp.support = (function(){
                var ret;
                try {
                    ret = w.document.createElement( "link" ).relList.supports( "preload" );
                } catch (e) {
                    ret = false;
                }
                return function(){
                    return ret;
                };
            })();

            rp.bindMediaToggle = function( link ){
                var finalMedia = link.media || "all";

                function enableStylesheet(){
                    link.media = finalMedia;
                }
                if( link.addEventListener ){
                    link.addEventListener( "load", enableStylesheet );
                } else if( link.attachEvent ){
                    link.attachEvent( "onload", enableStylesheet );
                }
                setTimeout(function(){
                    link.rel = "stylesheet";
                    link.media = "only x";
                });
                setTimeout( enableStylesheet, 3000 );
            };

            rp.poly = function(){
                if( rp.support() ){return;}
                var links = w.document.getElementsByTagName( "link" );
                for( var i = 0; i < links.length; i++ ){
                    var link = links[i];
                    if( link.rel === "preload" && link.getAttribute( "as" ) === "style" && !link.getAttribute( "data-loadcss" ) ){
                        link.setAttribute( "data-loadcss", true );
                        rp.bindMediaToggle( link );
                    }
                }
            };
            if( !rp.support() ){
                rp.poly();

                var run = w.setInterval( rp.poly, 500 );
                if( w.addEventListener ){
                    w.addEventListener( "load", function(){
                        rp.poly();
                        w.clearInterval( run );
                    } );
                } else if( w.attachEvent ){
                    w.attachEvent( "onload", function(){
                        rp.poly();
                        w.clearInterval( run );
                    } );
                }
            }
            if( typeof exports !== "undefined" ){
                exports.loadCSS = loadCSS;
            }
            else {
                w.loadCSS = loadCSS;
            }
        }( typeof global !== "undefined" ? global : this ) );
    </script>
</head>
<body class="sub-body">
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 text-center spaceTopBottom">
                <div class="col-xs-12">
                    <img class="img-responsive" src="/img/nobo/sticker/inscription-hotel-gros-finalisation.png" alt="hotel-paris-dessin" style="margin: auto">
                </div>
                <div class="col-xs-12">
                    <h2>Bienvenue chez Nobo</h2>
                    <p>
                        Votre réservation a bien été prise en compte, l'équipe nobo vous rappelle dans les
                        plus brefs délais.
                    </p>
                    <a href="/"><button class="btn btn-blue">retourner sur le site</button></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once('../inc/footer_start.php'); ?>
</body>
</html>
