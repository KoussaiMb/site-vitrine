<section>
    <div class="reservation-helper">
        <div class="margin-constraint">
            <div class="no-content-width">
                <div class="row">
                    <div class="col s12">
                        <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 1</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 2</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 3</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>