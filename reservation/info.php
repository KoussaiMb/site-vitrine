<?php
include_once('../inc/bootstrap.php');

$subForm = Session::getInstance()->read('subForm');
if (!$subForm) {
    Session::getInstance()->setFlash('info', 'Renseignez votre besoin');
    App::redirect('besoin');
}

$recap = Session::getInstance()->read('recap');
$recapInfo = Session::getInstance()->read('recapInfo');
$recValue = Session::getInstance()->read('recValue');

include_once('../inc/header_start.php');
?>

<title>Nobo - Réservation info</title>
<meta name="google" content="nositelinkssearchbox">
<link rel="stylesheet" href="/css/public_style.css">
<?php include_once("../inc/analyticstracking.php"); ?>
<script>
    /* loadCSS. [c]2017 Filament Group, Inc. MIT License */
    /* Standalone workflow */
    (function( w ){
        "use strict";
        if( !w.loadCSS ){
            w.loadCSS = function(){};
        }
        var rp = loadCSS.relpreload = {};
        rp.support = (function(){
            var ret;
            try {
                ret = w.document.createElement( "link" ).relList.supports( "preload" );
            } catch (e) {
                ret = false;
            }
            return function(){
                return ret;
            };
        })();

        rp.bindMediaToggle = function( link ){
            var finalMedia = link.media || "all";

            function enableStylesheet(){
                link.media = finalMedia;
            }
            if( link.addEventListener ){
                link.addEventListener( "load", enableStylesheet );
            } else if( link.attachEvent ){
                link.attachEvent( "onload", enableStylesheet );
            }
            setTimeout(function(){
                link.rel = "stylesheet";
                link.media = "only x";
            });
            setTimeout( enableStylesheet, 3000 );
        };

        rp.poly = function(){
            if( rp.support() ){return;}
            var links = w.document.getElementsByTagName( "link" );
            for( var i = 0; i < links.length; i++ ){
                var link = links[i];
                if( link.rel === "preload" && link.getAttribute( "as" ) === "style" && !link.getAttribute( "data-loadcss" ) ){
                    link.setAttribute( "data-loadcss", true );
                    rp.bindMediaToggle( link );
                }
            }
        };
        if( !rp.support() ){
            rp.poly();

            var run = w.setInterval( rp.poly, 500 );
            if( w.addEventListener ){
                w.addEventListener( "load", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            } else if( w.attachEvent ){
                w.attachEvent( "onload", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            }
        }
        if( typeof exports !== "undefined" ){
            exports.loadCSS = loadCSS;
        }
        else {
            w.loadCSS = loadCSS;
        }
    }( typeof global !== "undefined" ? global : this ) );
</script>
</head>
<body class="sub-body" onload="initGoogleAutocomplete();">

<div class="sub-head">
    <div class="reservation-ban-1"></div>
    <div class="reservation-ban-2"><img src="/img/nobo/logo/nobo_logo_baseline_400x240.png"></div>
    <div class="reservation-ban-3"></div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
            <!-- head -->
            <div class="col-xs-12 sub-bg pricing-head">
                <h4 class="hidden-xs"><span class="glyphicon glyphicon-briefcase"></span> Votre besoin</h4>
                <h4 class="active"><span class="glyphicon glyphicon-user"></span> Informations personnelles</h4>
                <h4 class="hidden-xs"><span class="glyphicon glyphicon-lock"></span> Paiement</h4>
            </div>
            <!-- body -->
            <div class="col-xs-12 col-sm-8 sub-bg">
                <div>
                    <?php include('../inc/print_flash_helper.php'); ?>
                </div>
                <form novalidate action="validation_info" method="post" data-toggle="validator" id="subFormInfo">
                    <div class="sub-content spaceTopBottom">
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="firstname">Entrez votre prénom:</label>
                            <input type="text" class="form-control" name="firstname" value="<?= isset($recapInfo) && isset($recapInfo->firstname) ? htmlspecialchars($recapInfo->firstname) : null; ?>" placeholder="john" id="firstname"/>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="lastname">Entrez votre nom:</label>
                            <input type="text" class="form-control" name="lastname" value="<?= isset($recapInfo) && isset($recapInfo->lastname) ? htmlspecialchars($recapInfo->lastname) : null; ?>" placeholder="doe" id="lastname"/>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="phone">Entrez votre numéro de téléphone:</label>
                            <input type="tel" class="form-control" name="phone" value="<?= isset($recapInfo) && isset($recapInfo->phone) ? htmlspecialchars($recapInfo->phone) : null; ?>" placeholder="+33612345678" id="phone"/>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="email">Entrez votre email:</label>
                            <input type="email" class="form-control" name="email" value="<?= isset($recapInfo) && isset($recapInfo->email) ? htmlspecialchars($recapInfo->email) : null; ?>" placeholder="johnDoe@mail.com" id="email"/>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="address">Entrez votre adresse:</label>
                            <input type="text" class="form-control" placeholder="3 rue de la paix" name="autocomplete" value="<?= isset($recapInfo) && isset($recapInfo->autocomplete) ? htmlspecialchars($recapInfo->autocomplete) : null; ?>" id="autocomplete"/>
                            <input type="hidden" name="lat" value="<?= isset($recapInfo) && isset($recapInfo->lat) ? htmlspecialchars($recapInfo->lat) : null; ?>" id="lat">
                            <input type="hidden" name="lng" value="<?= isset($recapInfo) && isset($recapInfo->lng) ? htmlspecialchars($recapInfo->lng) : null; ?>" id="lng">
                            <input type="hidden" name="street_number" value="<?= isset($recapInfo) && isset($recapInfo->street_number) ? htmlspecialchars($recapInfo->street_number) : null; ?>" id="street_number">
                            <input type="hidden" name="route" value="<?= isset($recapInfo) && isset($recapInfo->route) ? htmlspecialchars($recapInfo->route) : null; ?>" id="route">
                            <input type="hidden" name="address" value="<?= isset($recapInfo) && isset($recapInfo->address) ? htmlspecialchars($recapInfo->address) : null; ?>" id="Address">
                            <input type="hidden" name="address_ext" value="<?= isset($recapInfo) && isset($recapInfo->address_ext) ? htmlspecialchars($recapInfo->address_ext) : null; ?>" id="administrative_area_level_1">
                            <input type="hidden" name="zipcode" value="<?= isset($recapInfo) && isset($recapInfo->zipcode) ? htmlspecialchars($recapInfo->zipcode) : null; ?>" id="postal_code">
                            <input type="hidden" name="city" value="<?= isset($recapInfo) && isset($recapInfo->city) ? htmlspecialchars($recapInfo->city) : null; ?>" id="locality">
                            <input type="hidden" name="country" value="<?= isset($recapInfo) && isset($recapInfo->country) ? htmlspecialchars($recapInfo->country) : null; ?>" id="country">
                        </div>
                    </div>
                    <div class="col-xs-12 spaceTopBottom text-center">
                        <a href="besoin"><button type="button" class="btn btn-default">Revenir en arrière</button></a>
                        <button type="submit" class="btn btn-blue" name="action" value="payNow" id="infoValidation">confirmer</button>
                        <!--<button type="submit" class="btn-link-v2" name="action" value="payLater" id="payLater">payer plus tard</button>-->
                    </div>
                </form>
            </div>

            <!-- float -->
            <div class="col-xs-12 col-sm-4 floater">
                <div class="col-xs-12 sub-bg">
                    <div class="recap clear-center">
                        <div>
                            <h4>récapitulatif:</h4>
                            <p>Date <span><?= htmlspecialchars($recap->date); ?></span></p>
                            <?php if ($recap->hour == '00:00'): ?>
                                <p>Heure <span>Cela m'est égal</span></p>
                            <?php else: ?>
                                <p>Heure <span><?= htmlspecialchars($recap->hour); ?></span></p>
                            <?php endif; ?>
                            <p>Superficie <span><?= htmlspecialchars($recap->surface); ?> m²</span></p>
                            <p>Récurrence <span><?= App::getListManagement()->getFieldById(htmlspecialchars($recap->recurrence_id)); ?></span></p>
                        </div>
                        <div class="sub-hr"></div>
                        <div>
                            <p><span id="recapWorktime"><?= join('h', explode(':', htmlspecialchars($recap->workTimeChosen))); ?></span></p>
                            <?php if (htmlspecialchars($recap->warranty) == 1): ?>
                                <p class="recap-detail text-success">résultat garanti</p>
                            <?php else: ?>
                                <p class="recap-detail text-danger">résultat <strong>NON</strong> garanti</p>
                            <?php endif; ?>
                        </div>
                        <div class="sub-hr"></div>
                        <div>
                            <p>Tarif <span><?= htmlspecialchars($recap->price); ?> euros</span></p>
                            <p class="bold">Tarif réél* <span><?= htmlspecialchars($recap->price) / 2; ?> euros</span></p>
                        </div>
                    </div>
                </div>
                <div class="sub-detail">*TTC, après <a href="/faq#ci" target="_blank">crédit d'impôt de 50%</a></div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery -->
<script src="/js/lib/jquery-3.1.1.min.js"></script>
<!-- Bootstrap JS -->
<script src="/js/bootstrap.min.js"></script>
<!-- JS vrac -->
<script src="/js/lib/jquery.validate.min.js"></script>
<script src="/js/lib/jquery.validate.add.min.js"></script>
<script src="/js/js.cookie.js"></script>
<!-- google go -->
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBeMne5-vmGGRYsdcCxDb-PYs3McbjVmCg&libraries=places" async defer></script>


<script>
    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function    fillInAddr() {
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
        }
        if (typeof(place.address_components) !== 'undefined') {
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
            $('#lat').val(place.geometry.location.lat());
            $('#lng').val(place.geometry.location.lng());
            $('#Address').val($('#street_number').val() + ' ' + $('#route').val());
        }
    }
    
    $('#subFormInfo').on('submit', function () {
        validateForm();
    });

    $('#autocomplete').keypress(function(e) {
        if (e.which == 13) {
            google.maps.event.trigger(autocomplete, 'place_changed');
            return false;
        }
    });

    function    initGoogleAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types: ['geocode'], componentRestrictions: {country: 'fr'}});
        autocomplete.addListener('place_changed', fillInAddr);
    }

    // Block submit on return key
    document.getElementById("subFormInfo").onkeypress = function(e) {
        var key = e.charCode || e.keyCode || 0;
        if (key === 13) {
            e.preventDefault();
        }
    };

    function validateForm() {
        $('#subFormInfo').validate( {
            ignore: "",
            rules: {
                zipcode: {
                    required: true
                },
                firstname: {
                    required: true,
                    pattern: "^[a-zA-Z -]*$"
                },
                lastname: {
                    required: true,
                    pattern: "^[a-zA-Z -]*$"
                },
                email: {
                    required: true,
                    email: true
                },
//                autocomplete: {
//                    required: true
//                },
                phone: {
                    required: true,
                    pattern: "^[0-9\+]*$",
                    minlength: 10,
                    maxlength: 13
                    //pattern: "^(0|\+33)([1-9])([0-9]{8})$"
                }
            },
            messages: {
                zipcode: {
                    required: "Veuillez sélectionner votre adresse dans la liste des choix proposés"
                },
                firstname: {
                    required: "Vous avez oublié votre prénom",
                    pattern: "Veuillez saisir un prénom correct"
                },
                lastname: {
                    required: "Vous avez oublié votre nom",
                    pattern: "Veuillez saisir un nom correct"
                },
                email: {
                    required: "Vous avez oublié votre email",
                    email: "Entrez un email valide"
                },
                phone: {
                    required: "Vous avez oublié votre numéro de téléphone",
                    pattern: "Entrez un numéro de téléphone valide",
                    minlength: "Entrez un numéro de téléphone valide",
                    maxlength: "Entrez un numéro de téléphone valide"
                }
//                autocomplete: "Entrez une adresse"
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `help-block` class to the error element
                error.addClass( "help-block red" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element ) {
                $( element ).parents( ".form-control" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element) {
                $( element ).parents( ".form-control" ).addClass( "has-success" ).removeClass( "has-error" );
            }
        });
    }

    $(document).ready(function(){
        validateForm();
    });

</script>
<?php include_once ('../inc/exit_popup_phone.php'); ?>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <!-- Hotjar Tracking Code for https://www.nobo.life -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:666727,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
<?php endif; ?>
</body>
</html>