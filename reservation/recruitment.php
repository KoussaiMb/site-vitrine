<?php
require ("../inc/bootstrap.php");

$recruitment_pages = [
    'emploi-femme-de-menage'
];

if (!isset($_POST['origine']))
    include('../error/404.php');

$url = explode('?', $_POST['origine'])[0];
$origine_last = substr($url, strrpos($url, '/') + 1);

if (!in_array($origine_last, $recruitment_pages))
    include('../error/404.php');

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken'))) {
    ApiCaller::addGuestApiTokenToSession('/', false);
}
if (!empty($_POST['action']) && $_POST['action'] === 'post')
{
    $_POST['ip'] = App::get_user_ip();
    $apiConnector = ApiCaller::mailJetRecruitment($_POST);
    if ($apiConnector->getHttpCode() === 200)
    {
        Session::getInstance()->setFlash('warning', "Nous avons bien pris en compte votre demande, l'équipe nobo étudiera votre CV dans les plus brefs délais");
        App::redirect('/');
    }
    else if($_SERVER['HTTP_STAGE'] == "local"){
        var_dump($apiConnector->getResponse());
    }
}
Session::getInstance()->setFlash('danger', "Désolé ! Un problème est survenu... Contactez-nous directement !");
App::redirect(htmlspecialchars($_POST['origine']));