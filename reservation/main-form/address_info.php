<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 10:55
 */
include_once('../../inc/bootstrap.php');
$cur_step = 4;
$reservation = Session::getInstance()->read('reservation');

if ($_SERVER['REQUEST_METHOD'] === 'POST'
    && isset($_POST['password'])
    && $reservation['appointment_type'] === 'mission') {
    $account = mainForm::parse_form_account($_POST);
    unset($_POST['password']);
    unset($_POST['c_password']);
    $reservation['account'] = $_POST;
    Session::getInstance()->update('reservation', $reservation);

    if ($account === null)
        header('HTTP/1.1 500 Internal Server Error');

    $apiConnector = ApiCaller::updateReservationAccount($account);
    mainForm::print_code($apiConnector->getHttpCode());
}

$reservation['current_step'] = $cur_step;
$reservation['max_step'] = max($reservation['max_step'], $cur_step);
Session::getInstance()->update('reservation', $reservation);
$addressAccess = isset($reservation['address_access']) ? $reservation['address_access'] : null;
?>

<section class="container mt-3 mb-3">
    <div class="row justify-content-center">
        <div class="col col-xs-12 col-sm-6 text-center">
            <div class="card reservation-card">
                <div class="card-header text-center justify-content-center">
                    <span class="material-icons stepBack align-bottom">chevron_left</span>
                    <span>Informations d'accès</span>
                </div>
                <div class="card-body">
                    <form id="form-address-info">
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="batiment">Bâtiment</label>
                                <input type="text" class="form-control" name="batiment" value="<?= !empty($addressAccess['batiment']) ? $addressAccess['batiment'] : null; ?>" placeholder="ex: A" id="batiment">
                            </div>
                            <div class="form-group col">
                                <label for="floor">Etage</label>
                                <input type="text" class="form-control" name="floor" value="<?= !empty($addressAccess['floor']) ? $addressAccess['floor'] : null; ?>" placeholder="ex: 2" id="floor">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="digicode">digicode</label>
                                <input type="text" class="form-control" name="digicode" value="<?= !empty($addressAccess['digicode']) ? $addressAccess['digicode'] : null; ?>" placeholder="ex: A1234B" id="digicode">
                            </div>
                            <div class="form-group col">
                                <label for="digicode_2">2ème digicode</label>
                                <input type="text" class="form-control" name="digicode_2" value="<?= !empty($addressAccess['digicode_2']) ? $addressAccess['digicode_2'] : null; ?>" placeholder="ex: A1234B" id="digicode_2">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="doorBell">interphone</label>
                                <input type="text" class="form-control" name="doorBell" value="<?= !empty($addressAccess['doorBell']) ? $addressAccess['doorBell'] : null; ?>" placeholder="ex: Ludo Martin" id="doorBell">
                            </div>
                            <div class="form-group col">
                                <label for="door">Porte</label>
                                <input type="text" class="form-control" name="door" value="<?= !empty($addressAccess['door']) ? $addressAccess['door'] : null; ?>" placeholder="ex: Au fond à droite" id="door">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="description">Détails</label>
                                <textarea class="form-control" name="description" placeholder="Un détail à ajouter ?" id="description"><?= !empty($addressAccess['description']) ? $addressAccess['description'] : null; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" id="validateAddressInfo">Validez</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="reservation-helper">
        <div class="margin-constraint">
            <div class="no-content-width">
                <div class="row">
                    <div class="col s12">
                        <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 1</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 2</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 3</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        let slotChoice = $('#slotChoice>div>p>span');
        let form = $('#form-address-info');

        $('#date').on('change', function () {
            $('#validateMission').addClass('deactivate');
        });

        $('#slot').on('change', function () {
            slotChoice.toggleClass('material-underline-like');
        });

        form.on('submit', function () {
            if (form.valid() === true) {
                //TODO: do the call then click if 200
                $('#validateAddressInfo')
                    .prop('type', 'button')
                    .prop('name', 'action')
                    .prop('value', 'validateAddressInfo')
                    .trigger('click');
            }
            return false;
        });

        form.validate({
            rules: {
                batiment: {regex: "^[a-zA-Z1-9]*$"},
                floor: {},
                digicode: {},
                digicodeBis: {},
                doorBell: {regex: "^[a-zA-Z1-9 \-]*$"},
                door: {},
                description: {}
            },
            messages: {
                batiment: "Le bâtiment est invalide",
                floor: "L'étage est invalide",
                digicode: "Le digicode est invalide",
                digicodeBis: "Le digicode est invalide",
                doorBell: "L'interphone est invalide",
                door: "La porte est invalide",
                description: "La description est invalide"
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                error.insertAfter(element);
            },
            highlight: function (element) {
                $(element).removeClass('is-valid').addClass('is-invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
            }
        });
    });
</script>
