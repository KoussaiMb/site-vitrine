<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 10:55
 */
include_once('../../inc/bootstrap.php');
$cur_step = 2;
$reservation = Session::getInstance()->read('reservation');

if (isset($_POST['appointment_type']))
    $reservation['appointment_type'] = $_POST['appointment_type'];
if ($reservation === null || !isset($reservation['appointment_type']))
    header('HTTP/1.1 500 Internal Server Error');

$reservation['current_step'] = $cur_step;

Session::getInstance()->update('reservation', $reservation);
/*
$google_calendar = new googleCalendar();
$test = $google_calendar->test();
debug::data_r($test);
*/
?>


<?php
if ($reservation['appointment_type'] === 'phone'): ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-sm-12 col-lg-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header text-center justify-content-center">
                        <span class="material-icons stepBack align-bottom">chevron_left</span>
                        <span>Rendez-vous téléphonique</span>
                    </div>
                    <div class="card-body">
                        <div class="col">
                            <p class="text-right"><button class="btn-link-v2" type="button" id="switchAppointmentType">Programmez votre prestation</button></p>
                        </div>
                        <div class="row">
                            <div class="col">
                                <table class="table table-responsive table-fixed">
                                    <thead>
                                    <tr>
                                        <th class="subtractWeek noselect"><i class="material-icons">chevron_left</i></th>
                                        <th class="weekDiv">Lun.<div></div></th>
                                        <th class="weekDiv">Mar.<div></div></th>
                                        <th class="weekDiv">Mer.<div></div></th>
                                        <th class="weekDiv">Jeu.<div></div></th>
                                        <th class="weekDiv">Ven.<div></div></th>
                                        <th class="weekDiv">Sam.<div></div></th>
                                        <th class="addWeek noselect"><i class="material-icons">chevron_right</i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $creneaux = ['00', '15', '30', '45'];
                                    $start = 8;
                                    $end = 20;
                                    while($start < $end) {
                                        foreach($creneaux as $creneau) {
                                            echo "<tr>";
                                            echo "<td class=''></td>";
                                            echo "<td class='hourDiv'>".$start.":".$creneau."</td>";
                                            echo "<td class='hourDiv'>".$start.":".$creneau."</td>";
                                            echo "<td class='hourDiv'>".$start.":".$creneau."</td>";
                                            echo "<td class='hourDiv'>".$start.":".$creneau."</td>";
                                            echo "<td class='hourDiv'>".$start.":".$creneau."</td>";
                                            echo "<td class='hourDiv'>".$start.":".$creneau."</td>";
                                            echo "<td class=''></td>";
                                            echo "</tr>";
                                        }
                                        $start++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <div class="hidden-lg">
                                    <div class="row">
                                        <div class="col" id="datePickerDiv">
                                            <input type="hidden" name="datePicker" id="datePicker">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col hour-display-xs mt-1">
                                            <table class="table">
                                                <tbody>
                                                <?php
                                                $creneaux = ['00', '15', '30', '45'];
                                                $start = 8;
                                                $end = 20;
                                                while($start < $end) {
                                                    echo "<tr>";
                                                    foreach($creneaux as $creneau)
                                                        echo "<td class='hourDiv'>".$start.":".$creneau."</td>";
                                                    echo "</tr>";
                                                    $start++;
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col">
                                <button type="button" class="v-hidden btn btn-success" name="action" value="validatePhone" id="validatePhone">Validez</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
    <div class="reservation-helper">
        <div class="margin-constraint">
            <div class="no-content-width">
                <div class="row">
                    <div class="col s12">
                        <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 1</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 2</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 3</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <script>
        $(document).ready(function () {
            let weekDiv = $('.weekDiv');
            let hourDiv = $('.hourDiv');
            let curDate = moment();

            displayWeek(curDate);

            $('.addWeek').on('click', function () {
                curDate.add('7', 'days');
                displayWeek(curDate);
            });

            $('.subtractWeek').on('click', function () {
                if (curDate.isBefore(moment()) === false)
                    curDate.subtract('7', 'days');
                displayWeek(curDate);
            });

            weekDiv.on('click', function () {
                if ($(this).hasClass('deactivate') === false) {
                    weekDiv.removeClass('active');
                    $(this).addClass('active');
                }
            });

            hourDiv.on('click', function () {
                hourDiv.removeClass('active');
                $(this).addClass('active');
                displayValidationButton();
            });

            function displayWeek() {
                let today = moment();
                let dayEl = [1, 2, 3, 4, 5, 6];
                let dateToInsert;
                let i = 0;

                weekDiv.removeClass('deactivate');
                while (dayEl[i]) {
                    dateToInsert = moment(curDate).day(dayEl[i]);
                    weekDiv.eq(i).children('div').text(dateToInsert.format("DD/MM"));
                    if (dateToInsert.isSameOrAfter(today, "day") === false) {
                        weekDiv.eq(i).addClass('deactivate').removeClass('active');
                    }
                    i++;
                }
            }

            function displayValidationButton() {
                $('#validatePhone').addClass('v-hidden deactivate');
            }
        });

        $(document).ready(function () {
            let datePickerDiv = $('#datePicker');

            datePickerDiv.datetimepicker({
                format: 'L',
                inline: true,
                minDate: moment(),
                locale: moment.locale('fr')
            }).done(function () {
                calendar_with_material_icons();
            });

            datePickerDiv.on("change.datetimepicker", function (e) {
                $('#datePicker').val(e.date.format("YYYY-MM-DD"));
            });

        });
    </script>
<?php elseif($reservation['appointment_type'] === 'visit'): ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-xs-12 col-sm-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header text-center justify-content-center">
                        <span class="material-icons stepBack align-bottom">chevron_left</span>
                        <span>Réservez un créneau</span>
                    </div>
                    <div class="card-body">
                        <div class="col">
                            <p class="text-right"><button class="btn-link-v2" type="button" id="switchAppointmentType">rendez-vous téléphonique</button></p>
                        </div>
                        <div class="row">
                            <div class="col-9">
                                <h6>Première visite</h6>
                            </div>
                            <div class="col">
                                Mes horaires
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="reservation-helper">
            <div class="margin-constraint">
                <div class="no-content-width">
                    <div class="row">
                        <div class="col s12">
                            <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 1</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 2</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 3</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {

        });
    </script>
<?php elseif ($reservation['appointment_type'] === 'video'): ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-xs-12 col-sm-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header text-center justify-content-center">
                        <span class="material-icons stepBack align-bottom">chevron_left</span>
                        <span>Réservez un créneau</span>
                    </div>
                    <div class="card-body">
                        <div class="col">
                            <p class="text-right"><button class="btn-link-v2" type="button" id="switchAppointmentType">rendez-vous téléphonique</button></p>
                        </div>
                        <div class="row">
                            <div class="col-9">
                                <h6>Première visite</h6>
                            </div>
                            <div class="col">
                                Mes horaires
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="reservation-helper">
            <div class="margin-constraint">
                <div class="no-content-width">
                    <div class="row">
                        <div class="col s12">
                            <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 1</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 2</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 3</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {

        });
    </script>
<?php else: ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-xs-12 col-sm-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header text-center justify-content-center">
                        <span class="material-icons stepBack align-bottom">chevron_left</span>
                        <span>Date désirée de la prestation</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <p class="text-right"><button class="btn-link-v2" type="button" id="switchAppointmentType">rendez-vous téléphonique</button></p>
                            </div>
                        </div>
                        <input type="hidden" name="date" id="date">
                        <div class="row">
                            <div class="col">
                                <label for="datePicker" class="sr-only"></label>
                                <div class="mt-4 input-group" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#datePicker" name="datePicker" placeholder="Choisissez votre date" min="<?= date_create()->format('Y-m-d'); ?>" id="datePicker" onkeydown="return false;">
                                    <div class="input-group-append" data-target="#datePicker" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="material-icons">date_range</i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5 justify-content-center" id="slotChoice">
                            <div class="col-5 text-right">
                                <p><span class="material-underline-like">matin</span></p>
                            </div>
                            <div class="col-2 text-center">
                                <div class="md-form">
                                    <div class="material-switch">
                                        <input name="slot" value="morning" type="checkbox" class="d-hidden" checked>
                                        <input id="slot" name="slot" value="afternoon" type="checkbox">
                                        <label for="slot" class="nobo-background-main"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5 text-left">
                                <p><span>après-midi</span></p>
                            </div>
                        </div>
                        <div class="mt-5">
                            <button type="button" class="v-hidden btn btn-success" name="action" value="validateMission" id="validateMission">Validez</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="reservation-helper">
            <div class="margin-constraint">
                <div class="no-content-width">
                    <div class="row">
                        <div class="col s12">
                            <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 1</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 2</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 3</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            let slotChoice = $('#slotChoice>div>p>span');
            let inputDatePicker = $('#datePicker');
            let inputDate = $('#date');

            $('#slot').on('change', function () {
                slotChoice.toggleClass('material-underline-like');
            });

            inputDatePicker.datetimepicker({
                format: 'L',
                minDate: inputDatePicker.prop('min'),
                locale: moment.locale('fr')
            });

            inputDatePicker.on("change.datetimepicker", function (e) {
                updateDateInput(e.date.format("YYYY-MM-DD"));
            });

            function updateDateInput(date) {
                inputDate.val(date);
                $('#validateMission').addClass('deactivate');
            }
        });
    </script>
<?php endif; ?>
<script>
    function calendar_with_material_icons() {
        $('.fa-chevron-left')
            .removeClass('fa fa-chevron-left')
            .addClass('material-icons')
            .text('chevron_left');
        $('.fa-chevron-right')
            .removeClass('fa fa-chevron-right')
            .addClass('material-icons')
            .text('chevron_right');
    }
</script>
