<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 10:55
 */
include_once('../../inc/bootstrap.php');
$reservation = Session::getInstance()->read('reservation');
if ($reservation === null)
    header('HTTP/1.1 500 Internal Server Error');
if (isset($_POST['stripeToken'])) {
    //$apiConnector = ApiCaller::updateReservationAccount($account);TODO: add cart

    if ($apiConnector->getHttpCode() !== 200)
        header('HTTP/1.1 500 Internal Server Error');
} else if (isset($_POST['stripeSource'])) {
    //TODO implement add iban
}
Session::getInstance()->delete('reservation');
?>

<section class="container mt-3 mb-3">
    <div class="row justify-content-center">
        <div class="col col-xs-12 col-sm-6 text-center">
            <div class="card reservation-card">
                <div class="card-header text-center justify-content-center">
                    <span class="material-icons stepBack align-bottom">chevron_left</span>
                    <span>L'équipe nobo vous souhaite la bienvenue</span>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <p>Exit Page</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="reservation-helper">
        <div class="margin-constraint">
            <div class="no-content-width">
                <div class="row">
                    <div class="col s12">
                        <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 1</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 2</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 3</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        let slotChoice = $('#slotChoice>div>p>span');

        $('#date').on('change', function () {
            $('#validateMission').addClass('deactivate');
        });

        $('#slot').on('change', function () {
            slotChoice.toggleClass('material-underline-like');
        });
    });
</script>
