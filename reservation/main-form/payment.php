<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 10:55
 */
include_once('../../inc/bootstrap.php');
$cur_step = 5;
$reservation = Session::getInstance()->read('reservation');


if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['batiment'])) {
    $reservation['address_access'] = $_POST;
    $apiConnector = ApiCaller::updateReservationAddressInfo($_POST);

    mainForm::print_code($apiConnector->getHttpCode());
} else if (!isset($reservation['address_access']))
    mainForm::print_code(403);

if (!isset($reservation['stripe_key'])) {
    $ans = ApiCaller::retrieveStripeKey('public');
    mainForm::print_code($ans->getHttpCode());
    $reservation['stripe_key'] = $ans->getResponse()->data->value;
}

$reservation['current_step'] = $cur_step;
$reservation['max_step'] = max($reservation['max_step'], $cur_step);
Session::getInstance()->update('reservation', $reservation);
$payment_type = isset($_POST['payment_type']) ? $_POST['payment_type'] : 'iban';
?>

<section class="container mt-3 mb-3">
    <div class="row justify-content-center">
        <div class="col col-xs-12 col-sm-6 text-center">
            <div class="card reservation-card">
                <div class="card-header text-center justify-content-center">
                    <span class="material-icons stepBack align-bottom">chevron_left</span>
                    <span>Votre moyen de paiement</span>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <form action="/charge" method="post" id="payment-form">
                                <div class="form-group">
                                    <input type="hidden" name="payment_type" value="<?= $payment_type; ?>" id="payment_type">
                                    <p class="text-right">
                                        <button class="btn-link-v2" type="button" id="switchPaymentType">
                                            <?= $payment_type === 'iban' ? "Payer par carte" : "Payer par IBAN"; ?>
                                        </button>
                                    </p>
                                    <label for="stripe-element">
                                        <?= $payment_type === 'iban' ? "Paiement par IBAN" : "Paiement par carte"; ?>
                                    </label>
                                    <div id="stripe-element">
                                        <!-- A Stripe Element will be inserted here. -->
                                    </div>

                                    <!-- Used to display Element errors. -->
                                    <div id="error-message" role="alert"></div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default" id="validatePayment">Valider</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once ('../main-form-helper/address_check_helper.php'); ?>

<script>
    $(document).ready(function () {
        let stripe = Stripe('<?= $reservation['stripe_key']; ?>');
        let form = document.getElementById('payment-form');
        let error_element = document.getElementById('error-message');
        let elements = stripe.elements();
        let options = {};
        let style = {
            base: {
                fontSize: '16px',
                color: "#32325d",
            }
        };

        <?php if ($payment_type === 'iban'): ?>
        options = {
            style: style,
            supportedCountries: ['SEPA'],
            placeholderCountry: 'FR',
            classes: {base:'form-control input-sm'},
        };
        <?php else: ?>
        options = {
            style: style,
            classes: {base:'form-control input-sm'},
        };
        <?php endif; ?>
        let stripe_element = elements.create('<?= $payment_type; ?>', options);
        stripe_element.mount('#stripe-element');

        stripe_element.on('change', function(event) {
            error_element.textContent = event.error ? event.error.message : '';
        });

        form.addEventListener('submit', function(event) {
            event.preventDefault();

            <?php if ($payment_type === 'iban'): ?>
            let sourceData = {
                type: 'sepa_debit',
                currency: 'eur',
                owner: {
                    name: document.querySelector('input[name="name"]').value,
                    email: document.querySelector('input[name="email"]').value,
                },
                mandate: {
                    notification_method: 'email',
                },
            };

            stripe.createSource(stripe_element, sourceData).then(function(result) {
                if (result.error) {
                    error_element.textContent = result.error.message;
                } else {
                    stripeHandler(result.source);
                }
            });
            <?php else: ?>
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    error_element.textContent = result.error.message;
                } else {
                    stripeHandler(result.token);
                }
            });
            <?php endif; ?>
        });

        function stripeHandler(token) {
            let hiddenInput = document.createElement('input');
            let button = document.getElementById('validatePayment');

            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', '<?= $payment_type === 'iban' ? 'stripeSource' : 'stripeToken'; ?>');
            hiddenInput.setAttribute('value', token.id);
            hiddenInput.setAttribute('id', 'stripe_token');
            form.appendChild(hiddenInput);
            button.prop('name', 'action');
            button.prop('value', 'validatePayment');
            button.prop('type', 'button');
            button.trigger('click');
        }
    });
</script>
