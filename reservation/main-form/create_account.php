<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 10:55
 */
include_once('../../inc/bootstrap.php');
$cur_step = 3;
$reservation = Session::getInstance()->read('reservation');

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['date'])) {
    $_POST['appointment_date'] = date_create($_POST['date'])->format('Y-m-d H:i:s');
    unset($_POST['date']);

    $lead_appointment_exist = ApiCaller::isLeadAppointmentExist();
    $http_code = $lead_appointment_exist->getHttpCode();

    if ($http_code === 404)
        $apiConnector = ApiCaller::addReservationAppointment($_POST);
    else if ($http_code === 200)
        $apiConnector = ApiCaller::updateReservationAppointment($_POST);
    else
        mainForm::print_code($http_code);

    mainForm::print_code($apiConnector->getHttpCode());
}

$reservation['current_step'] = $cur_step;
$reservation['max_step'] = max($reservation['max_step'], $cur_step);
Session::getInstance()->update('reservation', $reservation);
$account = isset($reservation['account']) ? $reservation['account'] : null;
?>

<section class="container mt-3 mb-3">
    <div class="row justify-content-center">
        <div class="col col-xs-12 col-sm-6 text-center">
            <div class="card reservation-card">
                <div class="card-header text-center justify-content-center">
                    <span class="material-icons stepBack align-bottom">chevron_left</span>
                    <span>Création de votre compte</span>
                </div>
                <div class="card-body">
                    <form id="create_account_form">
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="first_name">Prénom</label>
                                <input type="text" class="form-control" name="first_name" value="<?= !empty($account['first_name']) ? $account['first_name'] : null; ?>" placeholder="ex: Ludo" id="first_name" required>
                            </div>
                            <div class="form-group col">
                                <label for="last_name">Nom</label>
                                <input type="text" class="form-control" name="last_name" value="<?= !empty($account['last_name']) ? $account['last_name'] : null; ?>" placeholder="ex: Martin" id="last_name" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="phone">Téléphone</label>
                                <input type="tel" class="form-control" name="phone" value="<?= !empty($account['phone']) ? $account['phone'] : null; ?>" placeholder="ex: 0606060606" id="phone" required>
                            </div>
                            <div class="form-group col">
                                <label for="email">email</label>
                                <input type="email" class="form-control" name="email" value="<?= !empty($account['email']) ? $account['email'] : null; ?>" placeholder="ex: ludo@nobo.life" id="email" required>
                            </div>
                        </div>
                        <label for="password">Mot de passe</label>
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="password" class="form-control" placeholder="Mot de passe" name="password" id="password" required>
                            </div>
                            <div class="form-group col">
                                <input type="password" class="form-control" placeholder="Confirmez-le" name="c_password" id="c_password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <small class="form-text text-muted">Votre mot de passe servira pour toutes nos applications</small>
                            <small class="form-text text-muted">Minimum 8 caractères</small>
                            <small class="form-text text-muted">Minimum une minuscule, une majuscule et un chiffre</small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" id="validateAccount">Validez</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="reservation-helper">
        <div class="margin-constraint">
            <div class="no-content-width">
                <div class="row">
                    <div class="col s12">
                        <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 1</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 2</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Aide 3</span>
                                <p>Voici ce que je vous propose</p>
                            </div>
                            <div class="card-action">
                                <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        let slotChoice = $('#slotChoice>div>p>span');
        let form = $("#create_account_form");

        $('#date').on('change', function () {
            $('#validateMission').addClass('deactivate');
        });

        $('#slot').on('change', function () {
            slotChoice.toggleClass('material-underline-like');
        });

        form.on('submit', function () {
            if (form.valid() === true) {
                $('#validateAccount')
                    .prop('type', 'button')
                    .prop('name', 'action')
                    .prop('value', 'validateAccount')
                    .trigger('click');
            }
            return false;
        });

        form.validate({
            rules: {
                first_name: {required: true, minlength: 2, regex: "^[a-zA-Z -]*$"},
                last_name: {required: true, minlength: 2, regex: "^[a-zA-Z -]*$"},
                email: {required: true, email: true},
                phone: {required: true, minlength: 10, maxlength: 18, regex: "^[0-9\+ ]*$"},
                password: {required: true, minlength: 8, regex: "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$"},
                c_password: {required: true, equalTo: "#password"}
            },
            messages: {
                first_name: "Votre prénom est invalide",
                last_name: "Votre nom est invalide",
                email: "Votre email est invalide",
                phone: "Votre numéro est invalide",
                password: "Votre mot de passe est invalide",
                c_password: {required: "Confirmez votre mot de passe", equalTo: "La confirmation est incorrecte"}
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                error.insertAfter(element);
            },
            highlight: function (element) {
                $(element).removeClass('is-valid').addClass('is-invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
            }
        });
});
</script>
