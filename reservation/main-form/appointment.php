<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 10:55
 */
include_once('../../inc/bootstrap.php');
$cur_step = 1;
$reservation = Session::getInstance()->read('reservation');

if ($reservation === null)
    header('HTTP/1.1 500 Internal Server Error');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $parsedInfos = mainForm::parse_form_address_check_to_api($_POST);
    $reservation['need'] = $parsedInfos;

    if ($parsedInfos === null)
        header('HTTP/1.1 500 Internal Server Error');

    $lead_need_exist = ApiCaller::isLeadNeedExist();
    $http_code = $lead_need_exist->getHttpCode();

    if ($http_code === 404)
        $apiConnector = ApiCaller::addReservationNeed($parsedInfos);
    else if ($http_code === 200)
        $apiConnector = ApiCaller::updateReservationNeed($parsedInfos);
    else
        mainForm::print_code($http_code);
    mainForm::print_code($apiConnector->getHttpCode());
}
$reservation['current_step'] = $cur_step;
$reservation['max_step'] = max($reservation['max_step'], $cur_step);
Session::getInstance()->update('reservation', $reservation);
?>

<?php if ($reservation['need']['recTime'] > 1): ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-xs-12 col-sm-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header text-center justify-content-center">
                        <span class="material-icons stepBack align-bottom">chevron_left</span>
                        <span>Votre rendez-vous</span>
                    </div>
                    <div class="card-body">
                        <div class="mt-5 text-left ml-2">
                            <h5>Encore des questions ?</h5>
                            <small class="text-muted form-text">Contactez la réception</small>
                        </div>
                        <div class="mt-3">
                            <button type="button" class="btn btn-secondary" name="action" value="AppointmentPhone">Rendez-vous téléphonique</button>
                        </div>
                        <div class="mt-5" id="">
                            <div class="mt-4 text-left ml-2">
                                <h5>Pour avancer</h5>
                                <small class="text-muted form-text">Programmez un premier rendez-vous à votre domicile</small>
                            </div>
                            <div class="mt-3">
                                <button type="button" class="btn btn-secondary" name="action" value="AppointmentVisit">Programmez une visite</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="reservation-helper">
            <div class="margin-constraint">
                <div class="no-content-width">
                    <div class="row">
                        <div class="col s12">
                            <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 1</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 2</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 3</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php elseif ($reservation['need']['recTime'] === 0): ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-xs-12 col-sm-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header text-center">
                        <span class="material-icons stepBack align-bottom">chevron_left</span>
                        <span>Votre rendez-vous</span>
                    </div>
                    <div class="card-body">
                        <div class="mt-5 text-left ml-2">
                            <h5>Encore des questions ?</h5>
                            <small class="text-muted form-text">Contactez la réception</small>
                        </div>
                        <div class="mt-3">
                            <button type="button" class="btn btn-secondary" name="action" value="AppointmentPhone">Rendez-vous téléphonique</button>
                        </div>
                        <div class="mt-5 text-left ml-2">
                            <h5>Date d'intervention</h5>
                            <small class="text-muted form-text">Choisissez le créneau parmis nos disponibilités</small>
                        </div>
                        <div class="mt-3">
                            <button type="button" class="btn btn-secondary" name="action" value="AppointmentMission">Programmer une prestation</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="reservation-helper">
            <div class="margin-constraint">
                <div class="no-content-width">
                    <div class="row">
                        <div class="col s12">
                            <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 1</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 2</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 3</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php else: ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-xs-12 col-sm-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header text-center">
                        <span class="material-icons stepBack align-bottom">chevron_left</span>
                        <span>Votre rendez-vous</span>
                    </div>
                    <div class="card-body">
                        <div class="mt-5 text-left ml-2">
                            <h5>Encore des questions ?</h5>
                            <small class="text-muted form-text">Contactez la réception</small>
                        </div>
                        <div class="mt-3">
                            <button type="button" class="btn btn-secondary" name="action" value="AppointmentPhone">Rendez-vous téléphonique</button>
                        </div>
                        <div class="mt-5 text-left ml-2">
                            <h5>Date d'intervention</h5>
                            <small class="text-muted form-text">Choisissez le créneau parmis nos disponibilités</small>
                        </div>
                        <div class="mt-3">
                            <button type="button" class="btn btn-secondary" name="action" value="AppointmentVideo">Programmer un videoCall</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="reservation-helper">
            <div class="margin-constraint">
                <div class="no-content-width">
                    <div class="row">
                        <div class="col s12">
                            <h4><i class="material-icons">live_help</i> Besoin d'aide ?</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 1</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 2</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4">
                            <div class="card blue-grey darken-1">
                                <div class="card-content white-text">
                                    <span class="card-title">Aide 3</span>
                                    <p>Voici ce que je vous propose</p>
                                </div>
                                <div class="card-action">
                                    <a href="#" target="_blank">Un lien vers une aide plus structurée</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<script>
    $(document).ready(function () {
    });
</script>