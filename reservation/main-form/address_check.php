<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 10:55
 */
include_once('../../inc/bootstrap.php');
$cur_step = 0;
$reservation = Session::getInstance()->read('reservation');

if ( empty($reservation['address_check']) || empty($reservation['address_info']))
    header('HTTP/1.1 500 Internal Server Error');

$lead_exist = ApiCaller::isLeadExist();
$http_code = $lead_exist->getHttpCode();

if ($http_code === 404)
    $apiConnector = ApiCaller::addReservation($reservation['address_info']);
else if ($http_code === 200)
    $apiConnector = ApiCaller::updateReservation($reservation['address_info']);
else
    header('HTTP/1.1 500 Internal Server Error');
mainForm::print_code($apiConnector->getHttpCode());
$reservation['current_step'] = $cur_step;

if (!isset($reservation['max_step'])) {
    $reservation['max_step'] = $cur_step;
}
Session::getInstance()->update('reservation', $reservation);
Session::getInstance()->read('reservation');
?>

<?php if ($reservation['address_check'] === 'true'): ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-s-12 col-sm-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header">
                        <small class="text-center mb-0 text-muted">Vous êtes éligible ! :)</small>
                        <p class="text-center mb-0"><?= $reservation['address']; ?></p>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-end justify-content-center" id="cleaningDiv">
                            <div class="col-12">
                                <small class="text-muted">Ménage</small>
                            </div>
                            <div class="col-xs-6 col-sm-8 col-sm-offset-2 mt-2">
                                <p>
                                    <input type="hidden" name="cleaning_time" value="3" id="cleaning_time">
                                    <i class="material-icons noselect removeTime">remove_circle_outline</i>
                                    <span class="h4 material-underline-like time-text">3h00</span>
                                    <i class="material-icons noselect addTime">add_circle_outline</i>
                                </p>
                            </div>
                        </div>
                        <div class="row mt-2 align-items-end justify-content-center" id="ironingDiv">
                            <div class="col-12">
                                <small class="text-muted">Repassage</small>
                            </div>
                            <div class="col-xs-6 col-sm-8 col-sm-offset-2 mt-2">
                                <p>
                                    <input type="hidden" name="ironing_time" value="0" id="ironing_time">
                                    <i class="material-icons noselect removeTime fancy-position">remove_circle_outline</i>
                                    <span class="h4 material-underline-like time-text v-hidden"></span>
                                    <i class="material-icons noselect addTime fancy-position">add_circle_outline</i>
                                </p>
                            </div>
                        </div>
                        <div class="row mt-5 justify-content-center" id="askRec">
                            <div class="col-5 text-right">
                                <p><span class="material-underline-like">Ponctuel</span></p>
                            </div>
                            <div class="col-2 text-center">
                                <div class="md-form">
                                    <div class="material-switch">
                                        <input name="recurrenceChoice" value="punctual" type="checkbox" class="d-hidden" checked>
                                        <input id="recurrenceChoice" name="recurrenceChoice" value="recurrent" type="checkbox">
                                        <label for="recurrenceChoice" class="nobo-background-main"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5 text-left">
                                <p><span>Récurrent</span></p>
                            </div>
                        </div>
                        <div class="text-center recChoice mt-3" id="punctualChoice">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="punctual" value="rent" autocomplete="off"> Loc. saisonnière
                                </label>
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="punctual" value="move" autocomplete="off"> Emménagement
                                </label>
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="punctual" value="other" autocomplete="off"> Autre
                                </label>
                            </div>
                        </div>
                        <div class="text-center recChoice d-hidden mt-3" id="recurrentChoice">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="recurrent" value="multiple" autocomplete="off"> >1/semaine
                                </label>
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="recurrent" value="1" autocomplete="off"> 1/semaine
                                </label>
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="recurrent" value="0.5" autocomplete="off"> 2/mois
                                </label>
                            </div>
                        </div>
                        <div class="d-hidden mt-4" id="recTimeChoice">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="recTime" value="2" autocomplete="off"> 2
                                </label>
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="recTime" value="3" autocomplete="off"> 3
                                </label>
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="recTime" value="4" autocomplete="off"> 4
                                </label>
                                <label class="btn btn-outline-secondary">
                                    <input type="radio" name="recTime" value="5" autocomplete="off"> 5
                                </label>
                            </div>
                        </div>
                        <div class="v-hidden mt-4" id="recurrenceValidation">
                            <button type="button" class="btn btn-success" name="action" value="validateAddressCheck" id="validateStep1">Validez</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include_once ('../main-form-helper/address_check_helper.php'); ?>
<?php else: ?>
    <section class="container mt-3 mb-3">
        <div class="row justify-content-center">
            <div class="col col-6 text-center">
                <div class="card reservation-card">
                    <div class="card-header">
                        <small class="text-center mb-0 text-muted">Vous n'êtes pas éligible :(</small>
                        <p class="text-center mb-0"><?= $reservation['address']; ?></p>
                    </div>
                    <div class="card-body">
                        <p class="text-center mt-4">Soyez la première personne au courant lorsque Nobo viendra dans votre secteur !</p>
                        <div class="mt-5">
                            <label for="email" class="sr-only">Votre Email</label>
                            <input type="email" class="form-control" placeholder="Votre email" name="email" id="email">
                            <small class="form-text text-muted"></small>
                        </div>
                        <button type="button" class="btn btn-success mt-3" name="action" value="validateNewAddress" id="validateNewAddress">Tenez-vous informé !</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<script>
    $(document).ready(function () {
        printTime();
        $('.addTime').on('click', function () {
            let timeInput = getCurrentTimeInput($(this));
            let time = parseFloat(timeInput.val());

            if (time < 12)
                time -= -0.5;
            timeInput.val(time);
            printTime();
        });

        $('.removeTime').on('click', function () {
            let timeInput = getCurrentTimeInput($(this));
            let time = parseFloat(timeInput.val());

            if (timeInput.prop('id') !== "cleaning_time" &&  time > 0)
                time -= 0.5;
            else if (time > 2)
                time -= 0.5;
            timeInput.val(time);
            printTime();
        });

        function printTime() {
            let inputs = $('#cleaning_time, #ironing_time');
            let ironingDiv = $('#ironingDiv');
            let ironingCursor =  ironingDiv.find('.addTime, .removeTime');
            let ironing_timeText =  ironingDiv.find('.time-text');
            let readableTime;

            inputs.each(function (i, e) {
                readableTime = timeDecimalToReadable(e.value);
                $(e).parent().children('.time-text').text(readableTime);
            });

            if (inputs.last().val() === '0') {
                ironingCursor.addClass('fancy-position');
                ironing_timeText.removeClass('deactivate')
            }
            else {
                ironingCursor.removeClass('fancy-position');
                ironing_timeText.addClass('deactivate')
            }
        }

        function getCurrentTimeInput(el) {
            return el.parent().children('input');
        }

        function timeDecimalToReadable(time) {
            let hour = Math.floor(time);
            let minute;

            if (Math.ceil(time) !== parseFloat(time))
                minute = '30';
            else
                minute = '00';

            return(hour + 'h' + minute);
        }
    });
    $(document).ready(function () {
        let validationButton = $('#recurrenceValidation');
        let recTimeDiv = $('#recTimeChoice');
        let recDiv = $('#recurrentChoice');
        let punctualDiv = $('#punctualChoice');
        let recTypeChoice = $('#recurrenceChoice');
        let recTypeText = $('#askRec>div>p>span');

        //Recurrence Type buttons listener
        recTypeChoice.on('change', function () {
            if ($(this).prop('checked') === true) {
                showRecButtons();
                resetPunctualButtons();
                recTypeText.first().addClass()
            }
            else {
                showPunctualButton();
                resetRecButtons();
            }
            recTypeText.toggleClass('material-underline-like');
        });

        //Recurrence buttons listener
        $('input[name="recurrent"]').on('change', function () {
            if ($(this).val() === 'multiple') {
                showRecTime();
                resetValidationButton();
            } else {
                resetRecTime();
                showValidationButton();
            }
        });

        //recTime buttons listener
        $('input[name="recTime"]').on('change', function () {
            showValidationButton();
        });

        //Punctual buttons listener
        $('input[name="punctual"]').on('change', function () {
            showValidationButton();
        });

        function resetValidationButton() {
            validationButton.removeClass('deactivate');
        }

        function showValidationButton() {
            validationButton.addClass('deactivate');
        }

        function resetRecTime() {
            recTimeDiv.addClass('d-hidden')
                .find('.active')
                .removeClass('active')
                .children()
                .prop('checked', false);
            resetValidationButton();
        }

        function showRecTime() {
            recTimeDiv.removeClass('d-hidden');
        }

        function resetRecButtons() {
            recDiv.addClass('d-hidden')
                .find('.active')
                .removeClass('active')
                .children()
                .prop('checked', false);
            resetRecTime();
        }
        function showRecButtons() {
            recDiv.removeClass('d-hidden');
        }

        function resetPunctualButtons() {
            punctualDiv.addClass('d-hidden')
                .find('.active')
                .removeClass('active')
                .children()
                .prop('checked', false);
            resetValidationButton();
        }

        function showPunctualButton() {
            punctualDiv.removeClass('d-hidden');
        }
    });
</script>