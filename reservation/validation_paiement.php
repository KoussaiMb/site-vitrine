<?php
include_once('../inc/bootstrap.php');
//require_once ("../vendor/autoload.php");
include_once('../stripe/init.php');

if ($_POST['code'] == 'stop'){
    Session::getInstance()->delAll();
    App::redirect('besoin');
}
$redirect_payment = "paiement";
if (!empty($_POST['payment_method']))
    $redirect_payment .= "?payment=" . $_POST['payment_method'];

$subForm = Session::getInstance()->read('subForm');
$recap = Session::getInstance()->read('recap');
$recapInfo = $recap = Session::getInstance()->read('recapInfo');

if (!$subForm || !$recap || $subForm && !isset($subForm->token)) {
    Session::getInstance()->setFlash('info', 'Commençez par renseigner votre besoin.');
    App::redirect('besoin');
}
else if ($subForm && $subForm->paymentStep == false) {
    Session::getInstance()->setFlash('info', 'Renseignez vos coordonnées d\'abord.');
    App::redirect('info');
}

$all = (array)$recap + $_POST + (array)$subForm + (array)$recapInfo;
$all['who'] = 'customer';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // No apiToken ? -> Guest
    if (empty(Session::getInstance()->read('apiToken')) ||
        !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken')))
        ApiCaller::addGuestApiTokenToSession('besoin');

    // Subscriber exists?
    $apiConnector = ApiCaller::retrieveSubscriber($all['token']);
    if ($apiConnector->getHttpCode() !== 200)
        App::setFlashAndRedirect('danger',
            'Commençez par renseigner votre besoin.', 'besoin');

    // New user insertion
    $all['user_token'] = my_crypt::genStandardToken();
    $apiConnector = ApiCaller::addRegistration($all);
    if ($apiConnector->getHttpCode() !== 200)
        App::setFlashArrayAndRedirect('danger',
            $apiConnector->getResponse()->message, 'besoin');

    $price = $all['price'];

    // Used promocode ?
    if (!empty($_POST) && !empty($_POST['code'])){
        $apiConnector = ApiCaller::retrievePromocode($_POST['code']);
        // Wrong code or server issue
        if ($apiConnector->getHttpCode() !== 200)
            App::setFlashArrayAndRedirect('danger', $apiConnector->getResponse()->message, $redirect_payment);

        // Success
        $price = $all['price'] -
            ($all['price'] * $apiConnector->getResponse()->data->percentage / 100) -
            $apiConnector->getResponse()->data->euro;
        $codeLog = [
            'user_token' => $all['user_token'],
            'oldPrice' => $all['price'],
            'newPrice' => $price,
            'promocode_id' => $apiConnector->getResponse()->data->id
        ];
        ApiCaller::addPromocodeLog($codeLog);
        $all['promocode'] = $_POST['code'];
    }
    else {
        $_POST['code'] = false;
    }

    // MailJet pre-requirements
    $all['source'] = $all['stripeToken'];
    $apiConnector = ApiCaller::addCustomerStripe($all);
    ApiCaller::paymentFunctionChecker($apiConnector);

    $all['paymentAccount'] = $apiConnector->getResponse()->data->paymentAccount;

    $apiConnector = ApiCaller::updateUserPaymentAccount($all);
    ApiCaller::paymentFunctionChecker($apiConnector);

    // Mails
    $all['status'] = 'Carte de paiement enregistrée';
    $all['paiement'] = 'Oui';
    ApiCaller::mailJetNewCustomer($all);                     // TODO : if sending mail failed -> notify / alert
    ApiCaller::mailJetWelcomeCustomerSuspendedPayment($all);    // TODO : if sending mail failed -> notify / alert

    ApiCaller::addWaitingList(['user_token' => $all['user_token']]);
    if ($apiConnector->getHttpCode() !== 200)
        App::setFlashArrayAndRedirect('danger', $apiConnector->getResponse()->message, 'paiement');
    Session::getInstance()->write('status', 'payed');
    App::redirect('confirmation');

}
App::setFlashArrayAndRedirect('danger', 'Un problème est survenu, veuillez réessayer', $redirect_payment);