<?php
require ("../inc/bootstrap.php");

$adwords_pages = [
    'femme-de-menage-haut-de-gamme',
    'femme-de-menage-paris-15',
    'femme-de-menage-paris'
];

if (!isset($_POST['origine']))
    include('../error/404.php');

$url = explode('?', $_POST['origine'])[0];
$origine_last = substr($url, strrpos($url, '/') + 1);

if (!in_array($origine_last, $adwords_pages))
    include('../error/404.php');

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken'))) {
    ApiCaller::addGuestApiTokenToSession('/', false);
}
if (!empty($_POST['action']) && $_POST['action'] === 'post')
{
    $_POST['ip'] = App::get_user_ip();
    $apiConnector = ApiCaller::mailJetAdwordsReservation($_POST);
    if ($apiConnector->getHttpCode() === 200)
    {
        Session::getInstance()->setFlash('warning', "Nous avons bien pris en compte votre demande, l'équipe nobo vous rappelle dans les plus brefs délais");
        App::redirect('/' . "?cpc=" . $origine_last);
    }
    else if($_SERVER['HTTP_STAGE'] == "local"){
        var_dump($apiConnector->getResponse());
    }
}
Session::getInstance()->setFlash('danger', "Désolé ! Un problème est survenu... Contactez-nous directement !");
App::redirect(htmlspecialchars($_POST['origine']));