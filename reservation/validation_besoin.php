<?php
include_once('../inc/bootstrap.php');
$subForm = Session::getInstance()->read('subForm');

if (!$subForm || $subForm && !isset($subForm->token)) {
    Session::getInstance()->setFlash('info', 'Commençez par renseigner votre besoin.');
    App::redirect('besoin');
}

$_POST['token'] = $subForm->token;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // No apiToken, apiToken expired ? -> Guest
    if (empty(Session::getInstance()->read('apiToken')) ||
        !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken')))
        ApiCaller::addGuestApiTokenToSession('besoin');
    // Does subscriber exist ?
    $apiConnector = ApiCaller::retrieveSubscriber($subForm->token);
    switch ($apiConnector->getHttpCode()) {
        // Subscriber exists
        case 200:
            $apiConnector = ApiCaller::updateSubscriberNeeds($_POST);
            if ($apiConnector->getHttpCode() !== 200)
                // Failure case
                App::setFlashArrayAndRedirect('danger', $apiConnector->getResponse()->message, 'besoin');
            $respData = $apiConnector->getResponse()->data;
            break ;

        // Subscriber doesn't exist
        case 404:
            $apiConnector = ApiCaller::addSubscriberNeeds($_POST);
            if ($apiConnector->getHttpCode() !== 200)
                // Failure case
                App::setFlashArrayAndRedirect('danger', $apiConnector->getResponse()->message, 'besoin');
            $respData = $apiConnector->getResponse()->data;
            break ;

        // Api call encountered an issue
        default:
            // Failure case
            $respData = null;
            App::setFlashArrayAndRedirect('danger', $apiConnector->getResponse()->message, 'besoin');
            break ;
    }
    Session::getInstance()->write('recValue', $_POST['rec']);
    Session::getInstance()->write('recap', $respData);
    App::redirect('info');
}
App::setFlashArrayAndRedirect('danger', 'Un problème est survenu, veuillez réessayer', 'besoin');