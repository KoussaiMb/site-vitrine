<?php
include_once('../inc/bootstrap.php');

header('Access-Control-Allow-Origin: *');//TODO:only stripe

$subForm = Session::getInstance()->read('subForm');
if (!$subForm) {
    Session::getInstance()->setFlash('info', 'Renseignez votre besoin');
    App::redirect('besoin');
}

$recap = Session::getInstance()->read('recap');

$apiConnector = ApiCaller::retrieveStripeKey('public');
if ($apiConnector->getHttpCode() !== 200)
    Session::getInstance()->setErr();
$pk_test = $apiConnector->getResponse()->data->value;

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken')))
    ApiCaller::addGuestApiTokenToSession('besoin');

$recValue = Session::getInstance()->read('recValue');
$recapInfo = Session::getInstance()->read('recapInfo');

include_once('../inc/header_start.php');
?>

<title>Nobo - Réservation paiement</title>
<meta name="google" content="nositelinkssearchbox">
<link rel="stylesheet" href="/css/public_style.css">
<?php include_once("../inc/analyticstracking.php"); ?>
<script>
    /* loadCSS. [c]2017 Filament Group, Inc. MIT License */
    /* Standalone workflow */
    (function( w ){
        "use strict";
        if( !w.loadCSS ){
            w.loadCSS = function(){};
        }
        var rp = loadCSS.relpreload = {};
        rp.support = (function(){
            var ret;
            try {
                ret = w.document.createElement( "link" ).relList.supports( "preload" );
            } catch (e) {
                ret = false;
            }
            return function(){
                return ret;
            };
        })();

        rp.bindMediaToggle = function( link ){
            var finalMedia = link.media || "all";

            function enableStylesheet(){
                link.media = finalMedia;
            }
            if( link.addEventListener ){
                link.addEventListener( "load", enableStylesheet );
            } else if( link.attachEvent ){
                link.attachEvent( "onload", enableStylesheet );
            }
            setTimeout(function(){
                link.rel = "stylesheet";
                link.media = "only x";
            });
            setTimeout( enableStylesheet, 3000 );
        };

        rp.poly = function(){
            if( rp.support() ){return;}
            var links = w.document.getElementsByTagName( "link" );
            for( var i = 0; i < links.length; i++ ){
                var link = links[i];
                if( link.rel === "preload" && link.getAttribute( "as" ) === "style" && !link.getAttribute( "data-loadcss" ) ){
                    link.setAttribute( "data-loadcss", true );
                    rp.bindMediaToggle( link );
                }
            }
        };
        if( !rp.support() ){
            rp.poly();

            var run = w.setInterval( rp.poly, 500 );
            if( w.addEventListener ){
                w.addEventListener( "load", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            } else if( w.attachEvent ){
                w.attachEvent( "onload", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            }
        }
        if( typeof exports !== "undefined" ){
            exports.loadCSS = loadCSS;
        }
        else {
            w.loadCSS = loadCSS;
        }
    }( typeof global !== "undefined" ? global : this ) );
</script>
</head>
<body class="sub-body">

<div class="spinner-container hidden" id="spinner">
    <div class="canvas-container">
        <div class="canvas canvas6">
            <div class="spinner6 p1"></div>
            <div class="spinner6 p2"></div>
            <div class="spinner6 p3"></div>
            <div class="spinner6 p4"></div>
        </div>
    </div>
</div>

<div class="sub-head">
    <div class="reservation-ban-1"></div>
    <div class="reservation-ban-2"><img src="/img/nobo/logo/nobo_logo_baseline_400x240.png"></div>
    <div class="reservation-ban-3"></div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
            <!-- head -->
            <div class="col-xs-12 sub-bg pricing-head">
                <h4 class="hidden-xs"><span class="glyphicon glyphicon-briefcase"></span> Votre besoin</h4>
                <h4 class="hidden-xs"><span class="glyphicon glyphicon-user"></span> Informations personnelles</h4>
                <h4 class="active"><span class="glyphicon glyphicon-lock"></span> Paiement</h4>
            </div>
            <!-- body -->
            <div class="col-xs-12 col-sm-8 sub-bg">
                <div>
                    <?php include('../inc/print_flash_helper.php'); ?>
                </div>
                <?php if(isset($_GET['payment']) && $_GET['payment'] == 'sepa'): ?>
                    <div class="col-xs-12">
                        <a href="paiement" class="btn-switch-payment"><span class="glyphicon glyphicon-credit-card"></span>  Payer par carte</a>
                    </div>
                    <form novalidate autocomplete="off" action="validation_paiement" method="post" data-toggle="validator" id="subFormPayment">
                        <input type="hidden" name="payment_method" value="sepa">
                        <div class="spaceTopBottom">
                            <div class="form-group col-xs-12 paymentGroup">
                                <label for="name">Titulaire du compte</label>
                                <input id="name" name="name" class="input-sm form-control" value="<?= $recapInfo->firstname . ' ' . $recapInfo->lastname ?>" placeholder="Jenny Rosen" required>
                            </div>
                            <input id="email" name="email" type="hidden" class="input-sm form-control" value="<?= $recapInfo->email; ?>" placeholder="jenny.rosen@example.com" required>
                            <div class="form-group col-xs-12 paymentGroup">
                                <label for="iban-element">IBAN</label>
                                <div id="iban-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>
                            </div>
                            <div id="bank-name"></div>

                            <!-- Used to display form errors. -->
                            <div id="error-message" role="alert"></div>
                            <div class="col-xs-12">
                                <a href="https://stripe.com/" target="_blank"><img src="../img/nobo/logo/stripe/stripe-outline-dark.svg" alt="logo-stripe-outline-dark"></a>
                            </div>
                            <!-- Display mandate acceptance text. -->
                            <!--
                            <div id="mandate-acceptance">
                                By providing your IBAN and confirming this payment, you are
                                authorizing Rocketship Inc. and Stripe, our payment service
                                provider, to send instructions to your bank to debit your account and
                                your bank to debit your account in accordance with those instructions.
                                You are entitled to a refund from your bank under the terms and
                                conditions of your agreement with your bank. A refund must be claimed
                                within 8 weeks starting from the date on which your account was debited.
                            </div>
                            -->
                            <div class="form-group col-xs-12 col-sm-6 spaceTopBottom clear-center">
                                <input type="checkbox" class="" name="cgv" value="1" id="cgv" required>
                                <label for="cgv">J'accepte les <a href="/mentions-legales" target="_blank">CGV</a>.</label>
                                <em class="error-sub hidden" id="error-cgv">Veuillez valider les cgv.</em>
                            </div>
                            <div class="col-xs-12">
                                <div class="sub-hr"></div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
                                <label for="code" class="control-label">Code de promotion</label>
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control" name="code" value="<?= isset($recapInfo) && isset($recapInfo->code) ? htmlspecialchars($recapInfo->code) : null ?>" placeholder="Mon code ici" id="code">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-sm" id="codeValidation">Valider</button>
                                    </div>
                                </div>
                                <em class="error-sub hidden" id="error-code">Le code promo n'est pas valide.</em>
                                <div class="promo-detail promo-detail-success hidden">Le code a été validée</div>
                                <div class="promo-detail promo-detail-fail hidden">Le code est erroné</div>
                            </div>
                            <div class="col-xs-12 text-center">
                                <p class="text-success">
                                    <span class="glyphicon glyphicon glyphicon-info-sign"></span>
                                    Un Guest Relation vous contactera sous 24 heures, rien ne sera débité avant cet appel
                                </p>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3" style="margin-bottom: 1em">
                                <div class="col-xs-12">
                                    <a href="info"><button type="button" class="btn btn-default">Revenir en arrière</button></a>
                                    <button type="submit" class="btn btn-blue" id="payNow" onclick="return gtag_report_conversion()">CONFIRMER</button>
                                </div>
                            </div>
                        </div>
                    </form>
                <!--
                <style>
                    input,
                    .StripeElement {
                        height: 40px;
                        padding: 10px 12px;

                        color: #32325d;
                        background-color: white;
                        border: 1px solid transparent;
                        border-radius: 4px;

                        box-shadow: 0 1px 3px 0 #e6ebf1;
                        -webkit-transition: box-shadow 150ms ease;
                        transition: box-shadow 150ms ease;
                    }

                    input:focus,
                    .StripeElement--focus {
                        box-shadow: 0 1px 3px 0 #cfd7df;
                    }

                    .StripeElement--invalid {
                        border-color: #fa755a;
                    }

                    .StripeElement--webkit-autofill {
                        background-color: #fefde5 !important;
                    }
                </style>
                -->
                <?php else: ?>
                <div class="col-xs-12">
                    <a href="paiement?payment=sepa" class="btn-switch-payment"><span class="glyphicon glyphicon-transfer"></span>  Payer par virement</a>
                </div>
                <form novalidate autocomplete="off" action="validation_paiement" method="post" data-toggle="validator" id="subFormPayment">
                    <div class="spaceTopBottom">
                        <div class="form-group col-xs-12 paymentGroup">
                            <label for="cc-number" class="control-label">Numéro de carte</label>
                            <input id="cc-number" type="tel" class="input-sm form-control cc-number" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 paymentGroup">
                            <label for="cc-exp" class="control-label">Expiration</label>
                            <input id="cc-exp" type="tel" class="input-sm form-control cc-exp" autocomplete="cc-exp" placeholder="•• / ••" required>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 paymentGroup">
                            <label for="cc-cvc" class="control-label">CVC</label>
                            <input id="cc-cvc" type="tel" class="input-sm form-control cc-cvc" placeholder="•••" required>
                        </div>
                        <div id="payment-error"></div>
                        <div class="col-xs-12">
                            <a href="https://stripe.com/" target="_blank"><img src="../img/nobo/logo/stripe/stripe-outline-dark.svg" alt="logo-stripe-outline-dark"></a>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 spaceTopBottom clear-center">
                            <input type="checkbox" class="" name="cgv" value="1" id="cgv" required>
                            <label for="cgv">J'accepte les <a href="/mentions-legales" target="_blank">CGV</a>.</label>
                            <em class="error-sub hidden" id="error-cgv">Veuillez valider les cgv.</em>
                        </div>
                        <div class="col-xs-12">
                            <div class="sub-hr"></div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
                            <label for="code" class="control-label">Code de promotion</label>
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" name="code" value="<?= isset($recapInfo) && isset($recapInfo->code) ? htmlspecialchars($recapInfo->code) : null ?>" placeholder="Mon code ici" id="code">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-sm" id="codeValidation">Valider</button>
                                </div>
                            </div>
                            <em class="error-sub hidden" id="error-code">Le code promo n'est pas valide.</em>
                            <div class="promo-detail promo-detail-success hidden">Le code a été validée</div>
                            <div class="promo-detail promo-detail-fail hidden">Le code est erroné</div>
                        </div>
                        <div class="col-xs-12 text-center">
                            <p class="text-success">
                                <span class="glyphicon glyphicon glyphicon-info-sign"></span>
                                Un Guest Relation vous contactera sous 24 heures, rien ne sera débité avant cet appel
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-6 col-md-offset-3" style="margin-bottom: 1em">
                            <div class="col-xs-12">
                                <a href="info"><button type="button" class="btn btn-default">Revenir en arrière</button></a>
                                <button type="submit" class="btn btn-blue" id="payNow" onclick="return gtag_report_conversion()">CONFIRMER</button>
                            </div>
                        </div>
                    </div>
                </form>
                <?php endif; ?>
            </div>
            <!-- float -->
            <div class="col-xs-12 col-sm-4 floater">
                <div class="col-xs-12 sub-bg">
                    <div class="recap clear-center">
                        <div>
                            <h4>récapitulatif:</h4>
                            <p>Date <span><?= htmlspecialchars($recap->date); ?></span></p>
                            <?php if ($recap->hour == '00:00'): ?>
                                <p>Heure <span>Cela m'est égal</span></p>
                            <?php else: ?>
                                <p>Heure <span><?= htmlspecialchars($recap->hour); ?></span></p>
                            <?php endif; ?>
                            <p>Surperficie <span><?= htmlspecialchars($recap->surface); ?> m²</span></p>
                            <p>Récurrence <span><?= App::getListManagement()->getFieldById(htmlspecialchars($recap->recurrence_id)); ?></span></p>
                        </div>
                        <div class="sub-hr"></div>
                        <div>
                            <p><span id="recapWorktime"><?=  join('h', explode(':', htmlspecialchars($recap->workTimeChosen))); ?></span></p>
                            <?php if (htmlspecialchars($recap->warranty) == 1): ?>
                                <p class="recap-detail text-success">résultat garanti</p>
                            <?php else: ?>
                                <p class="recap-detail text-danger">résultat <strong>NON</strong> garanti</p>
                            <?php endif; ?>
                        </div>
                        <div class="sub-hr"></div>
                        <div>
                            <p>Tarif <span><?= htmlspecialchars($recap->price); ?> €</span></p>
                            <p class="blue recap_promotion little-italic hidden">Réduction <span class="pull-right" id="recap_reduction"></span></p>
                            <p class="recap_promotion hidden">Tarif après réduction <span class="pull-right" id="price_promotion"></span></p>
                            <p class="bold">Tarif réél* <span id="priceReel"><?= htmlspecialchars($recap->price) / 2; ?> €</span></p>
                        </div>
                    </div>
                </div>
                <div class="sub-detail">*TTC, après <a href="/faq#ci" target="_blank">crédit d'impôt de 50%</a></div>
            </div>
        </div>
    </div>
</div>
<style>
    .spinner6 {
        background: white;/*dot colors*/
        border-radius: 50%;
        height: 1em;
        margin: .1em;
        width: 1em;
    }
    .p1 {animation: fall 1s linear .3s infinite;}
    .p2 {animation: fall 1s linear .2s infinite;}
    .p3 {animation: fall 1s linear .1s infinite;}
    .p4 {animation: fall 1s linear infinite;}
    @keyframes fall {
        0% {transform: translateY(-15px);}
        25%, 75% {transform: translateY(0);}
        100% {transform: translateY(-15px);}
    }
    .canvas-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
        margin: 0 auto;
        max-width: 650px;
        min-width: 200px;
    }
    .canvas {
        align-items: center;
        background: rgba(0,0,0,0.3);/*background dots*/
        border-radius: 50%;
        box-shadow: 0 5px 20px rgba(0,0,0,0.2);
        display: flex;
        height: 10em;
        justify-content: center;
        margin: 1em 1em 2em 1em;
        width: 10em;
    }
    .spinner-container {
        position: fixed;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.3);/*background spinner*/
        z-index: 30;
        left:0;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-content: stretch;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .btn-switch-payment {
        text-decoration: none;
        color: #3E3E3E;
        padding: 10px 3px 3px 3px;
        float: right;
    }
    .btn-switch-payment:hover {
        text-decoration: none;
    }
</style>

<!-- Jquery -->
<script src="/js/lib/jquery-3.1.1.min.js"></script>
<!-- Bootstrap JS -->
<script src="/js/bootstrap.min.js"></script>
<!-- payement library -->
<?php if(isset($_GET['payment']) && $_GET['payment'] == 'sepa'): ?>
    <script src="https://js.stripe.com/v3/"></script>
<?php else: ?>
    <script src="/js/lib/jquery-payement.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<?php endif; ?>
<script>
    function showLoading() {
        $("#spinner").removeClass('hidden');
    }
    function stopLoading() {
        $("#spinner").addClass('hidden');
    }
    $(document).ready(function(){
        let recap_reduction = $('#recap_reduction'),
            price_promotion = $('#price_promotion'),
            cgv = $('#cgv'),
            error_cgv = $('#error-cgv'),
            code = $('#code'),
            error_code = $('#error-code'),
            form = $('#subFormPayment'),
            price = parseInt(<?php echo $recap->price; ?>),
            valide;

        <?php if(isset($_GET['payment']) && $_GET['payment'] == 'sepa'): ?>

// Create a Stripe client.
// Note: this merchant has been set up for demo purposes.
        var stripe = Stripe("<?php echo $pk_test; ?>");

// Create an instance of Elements.
        var elements = stripe.elements();

// Create an instance of the iban Element.
        var iban = elements.create('iban', {
            classes: {base:'form-control input-sm'},
            supportedCountries: ['SEPA'],
        });

// Add an instance of the iban Element into the `iban-element` <div>.
        iban.mount('#iban-element');

        var errorMessage = document.getElementById('error-message');
        var bankName = document.getElementById('bank-name');

        iban.on('change', function(event) {
            // Handle real-time validation errors from the iban Element.
            if (event.error) {
                errorMessage.textContent = event.error.message;
                errorMessage.classList.add('visible');
            } else {
                errorMessage.classList.remove('visible');
            }

            // Display bank name corresponding to IBAN, if available.
            if (event.bankName) {
                bankName.textContent = event.bankName;
                bankName.classList.add('visible');
            } else {
                bankName.classList.remove('visible');
            }
        });

// Handle form submission.
        form.on('submit', function(event) {
            let error = false;
            valide = false;

            event.preventDefault();
            showLoading();

            if (cgv.is(':checked') === true && (!code.val() || code.val().match(/^[0-9a-zA-Z ]*$/))) {
                error_cgv.addClass('hidden');
                error_code.addClass('hidden');
                valide = true;
            }
            else {
                cgv.is(':checked') ? error_cgv.addClass('hidden') : error_cgv.removeClass('hidden');
                !code.val() || code.val().match(/^[0-9a-zA-Z ]*$/) ? error_code.addClass('hidden') : error_code.removeClass('hidden');
            }

            if (error === false && valide === true){
                let sourceData = {
                    type: 'sepa_debit',
                    currency: 'eur',
                    owner: {
                        name: document.querySelector('input[name="name"]').value,
                        email: document.querySelector('input[name="email"]').value,
                    },
                    mandate: {
                        // Automatically send a mandate notification email to your customer
                        // once the source is charged.
                        notification_method: 'email',
                    }
                };

                // Call `stripe.createSource` with the iban Element and additional options.
                stripe.createSource(iban, sourceData).then(function(result) {
                    if (result.error) {
                        // Inform the customer that there was an error.
                        errorMessage.textContent = result.error.message;
                        errorMessage.classList.add('visible');
                        stopLoading();
                    } else {
                        // Send the Source to your server to create a charge.
                        errorMessage.classList.remove('visible');
                        stripeSourceHandler(result.source);
                    }
                });
            } else {
                stopLoading();
            }

            function stripeSourceHandler(source) {
                  if (source.length === 0 || source.status !== 'chargeable') {
                      stopLoading();
                      reportError('Une erreur est survenue lors de la prise en charge de votre IBAN');
                 }else{
                      let token = source.id;

                      form.append('<input type="hidden" name="stripeToken" value="' + token + '" />');
                      form.get(0).submit();
                  }
            }
            return false;
        });
        <?php else: ?>
        let ccNum = $('.cc-number'),
            ccExp = $('.cc-exp'),
            ccCvc = $('.cc-cvc');

        //stripe validation
        Stripe.setPublishableKey("<?php echo $pk_test; ?>");
        ccNum.payment('formatCardNumber');
        ccExp.payment('formatCardExpiry');
        ccCvc.payment('formatCardCVC');
        $.fn.toggleInputError = function(failed) {
            this.parent('.paymentGroup').toggleClass('has-error', failed);
            if (failed)
                error = true;
            return this;
        };
        form.submit(function(e) {
            error = false;
            valide = false;
            showLoading();

            e.preventDefault();
            let exp = ccExp.payment('cardExpiryVal'),
                cardType = $.payment.cardType($('.cc-number').val());

            ccNum.toggleInputError(!$.payment.validateCardNumber(ccNum.val()));
            ccExp.toggleInputError(!$.payment.validateCardExpiry(exp));
            ccCvc.toggleInputError(!$.payment.validateCardCVC(ccCvc.val(), cardType));
            if (cgv.is(':checked') === true && (!code.val() || code.val().match(/^[0-9a-zA-Z ]*$/))) {
                error_cgv.addClass('hidden');
                error_code.addClass('hidden');
                valide = true;
            }
            else {
                cgv.is(':checked') ? error_cgv.addClass('hidden') : error_cgv.removeClass('hidden');
                !code.val() || code.val().match(/^[0-9a-zA-Z ]*$/) ? error_code.addClass('hidden') : error_code.removeClass('hidden');
            }
            if (error === false && valide === true){
                Stripe.card.createToken({
                    number: ccNum.val(),
                    cvc: ccCvc.val(),
                    exp_month: exp.month,
                    exp_year: exp.year
                }, stripeResponseHandler);
            }
            stopLoading();
            return false;
        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                reportError(response.error.message);
                stopLoading();
            }else{
                let token = response.id;

                form.append('<input type="hidden" name="stripeToken" value="' + token + '" />');
                form.get(0).submit();
            }
        }
        //end stripe validation
        <?php endif; ?>

        function gtag_report_conversion(url) {
            let callback = function () {
                if (typeof(url) !== 'undefined') {
                    window.location = url;
                }
            };
            gtag('event', 'conversion', {
                'send_to': 'AW-873879215/XD0ZCJq5iXEQr63ZoAM',
                'transaction_id': '',
                'event_callback': callback
            });
            return false;
        }

        function reportError(msg) {
            $('#payment-error').text(msg).addClass('bg-danger');
            btnSubmit.attr("disabled", false);
            return false;
        }

        // With API call
        $('#codeValidation').on('click', function () {
            $.ajax({
                beforeSend: function(request) {
                    var apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/promocodes/' ?>" + code.val(),
                type: 'GET',
                dataType: 'json',
                success: function (ans) {
                    var percentage = ans.data.percentage;
                    var euro = ans.data.euro;
                    var temp = Math.round(price - (price * parseInt(percentage) / 100) - parseInt(euro));
                    var reduction = '';

                    code.removeClass('code-error');
                    code.addClass('code-valide');
                    $('.promo-detail').addClass('hidden');

                    if (percentage && percentage != 0)
                        reduction += 'Réduction ' + percentage + '%';
                    else if (euro && euro != 0)
                        reduction += 'Réduction ' + euro + '€';
                    if (reduction.length > 0) {
                        recap_reduction.text(reduction);
                        price_promotion.text(temp + ' euros');
                        $('.recap_promotion').removeClass('hidden');
                        $('#priceReel').text(temp / 2 + ' euros').addClass('gold');
                        $('.promo-detail-success').removeClass('hidden');
                    }
                    else {
                        $('.recap_promotion').addClass('hidden');
                        $('#priceReel').removeClass('gold');
                        $('.promo-detail-fail').removeClass('hidden');
                    }
                },
                error: function () {
                    code.val('');
                    $('.recap_promotion').addClass('hidden');
                    code.removeClass('code-valide');
                    code.addClass('code-error');
                    $('.promo-detail-fail').removeClass('hidden');
                    $('.promo-detail').addClass('hidden');
                    $('#price').text(price + ' euros').removeClass('gold');
                    $('#priceReel').text(price / 2 + ' euros').removeClass('gold');
                }
            });
        });
    });
</script>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <!-- Hotjar Tracking Code for https://www.nobo.life -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:666727,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
<?php endif; ?>
</body>
</html>