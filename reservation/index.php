<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 11:56
 */
/**
 * reservation session structure
 *
 * max_step
 * current_step
 * form_init: '',
 * data [
 * cleaningTime: '',
 * ironingTime: '',
 * recurrenceType: '',
 * punctualType: '',
 * recurrentType: '',
 * recTime: '',
 * appointment : {type: '', day : '', date:'', slot: ''},
 * account: {first_name: '', last_name: '', email: '', phone: ''},
 * address: {batiment: '', floor: '', digicode: '', digicodeBis: '', doorBell: '', comment: ''}
 * ]
 *
 */
include_once('../inc/bootstrap.php');

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken'))) {
    ApiCaller::addGuestApiTokenToSession('/', false);
}

$reservation = Session::getInstance()->read('reservation');
$phone_reception = isset($_SERVER['HTTP_PHONE_NOBO']) ? $_SERVER['HTTP_PHONE_NOBO'] : '0146746297';
$lead_exist = ApiCaller::isLeadExist();
//If Post, it's a new address. If no POST && No reservation, it's been shunted.
if ($_SERVER['REQUEST_METHOD'] === 'POST'
    && !(isset($reservation['address_info']['lat']) && $_POST['lat'] === $reservation['address_info']['lat'])
    && !(isset($reservation['address_info']['lng']) && $_POST['lng'] === $reservation['address_info']['lng']))
{
    $address = new googleAddress($_POST);
    if ($address->is_valid() === null)
        App::setFlashAndRedirect(
            'warning',
            'Une erreur est survenue lors de la réservation',
            '/');
    $new_address['address'] =  $address->getFullAddress();
    $new_address['address_info'] = $address->get_all_information();
    $new_address['address_check'] = $address->is_valid();
    Session::getInstance()->update('reservation', $new_address);
}
else if ($reservation === null || $reservation !== null && $lead_exist->getHttpCode() === 404)
{
    Session::getInstance()->delete('reservation');
    unset($_COOKIE['reservation']);
    setcookie('reservation', null, -1, '/');
    App::setFlashAndRedirect(
        'success',
        'Veuillez d\'abord entrer votre adresse avant de réserver',
        '/');
}

//Generate the page
$reservation = Session::getInstance()->read('reservation');
$title = 'Nobo - reservation';
$description = 'Première page de réservation';
$header = new generate_header($title, $description);
$header->setBasicHeader();
$header->setCssFile('/css/app.css');
$header->setCssFile('/css/reservation.css');
$header->setCssFile('https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css');
$header->generate();
?>
<div id="view"></div>
<?php
$footer = new generate_footer();
$footer->setBasicFooter();
$footer->setScriptFile('/js/reservation.js');
$footer->setScriptFile("https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js");
$footer->setScriptFile("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js");
$footer->setScriptFile("https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js");
$footer->setScriptFile("https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js");
$footer->setScriptFile("https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js");
$footer->setScriptFile("https://js.stripe.com/v3/");
$footer->setScript("var urgenceCall = '".$phone_reception."';");
$footer->generate();