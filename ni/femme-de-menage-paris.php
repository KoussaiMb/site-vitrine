<?php include_once('../inc/bootstrap.php'); ?>
<?php
if (Session::getInstance()->hasFlash())
$notif = Session::getInstance()->getFlash();
?>
<?php include_once('../inc/header_start.php'); ?>
<!-- title -->
<title>Femme de ménage dans tous les arrondissements de Paris</title>
<!-- CSS -->
<!--<link rel="stylesheet" href="/css/ad_style.css">-->
<link rel="preload" href="/css/localization_style.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<link rel="preload" href="/css/lib/bootstrap-datepicker.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="/css/localization_style.css"></noscript>
<noscript><link rel="stylesheet" href="/css/lib/bootstrap-datepicker.min.css"></noscript>
<?php include_once('../inc/header_end.php'); ?>
<?php include_once('../inc/navbar_phone.php'); ?>
<?php include_once('../inc/navbar_ad.php'); ?>
<style>
    .numberAd {
        padding-right: 100px!important;
    }
    .numberAd span {
        color: #27374D;
        text-shadow: none;
        padding: 5px;
        font-size: 1.4em;
    }
    #navbar-fixed  {
        margin: 0!important;
        background-color: rgba(255, 123, 106, 0.7);
    }
    #ad-paris-15 {
        padding-top: 70px;
        background-color: #122b40;
        height: 70%;
    }
    #ad-paris {
        padding-top: 70px;
        height: 80%;
        width: 100%;
        background: url('/img/paris/ad/appartement-paris-menage-nobo.jpg') center center no-repeat;
        min-width: 100%;
        background-size: cover;
    }
    .background-black {
        background-color: #27374d;
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 2px;
    }
    a.navbar-brand {
        padding: 0 0 0 10px;
    }
    .integration-photo-header > img,
    .integration-photo-header-80 > img {
        height: 360px;
    }
    .integration-photo-header {
        position: absolute;
        bottom: 30%;
        left: 7%;
    }
    .integration-photo-header-80 {
        position: absolute;
        bottom: 20%;
        left: 7%;
    }
    .ad-title {
        font-size: 1.7em;
        font-weight: bold;
        color: #eeeeee;
    }
    .navbar-fixed-top li a:hover:after {
        background: #27374D;
    }
    .navbar {
        border-bottom: none;
    }
    .btn-ad-reservation {
        box-shadow: none;
        background-color: inherit;
        border: none;
        color: #FFF;
        font-size: 1.2em;
        border-bottom: 1px solid white;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    .btn-ad-reservation:hover {
        font-style: italic;
        border-color: rgb(255, 123, 106);
    }
    .italic {
        font-style: italic;
    }
    .nav .navbar-text {
        color: #27374D;
    }
    @media only screen and (max-width: 767px) {
        #ad-paris-15 {
            height: 90%;
            padding-top: 70px!important;
        }
        #ad-paris {
            height: 90%;
        }
        .btn-stage {
            padding: 8px;
        }
        a.navbar-brand {
            transform: translateX(-50%);
            left: 50%;
            position: relative;
        }
    }
    @media only screen and (min-width: 768px) {
        .ad-form-padding {
            padding-left: 60px;
            padding-right: 60px;
        }
        .navbar.navbar-transparent p {
            padding-top: 2px;
            padding-bottom: 2px;
        }
        .navbar-nav p {
            font-size: 1.4em;
            padding-top: 11px;
            padding-bottom: 11px;
            transition: all 0.3s;
        }
        .ad-stage-wrapper {
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-justify-content: space-around;
            -ms-flex-pack: distribute;
            justify-content: space-around;
            -webkit-align-content: stretch;
            -ms-flex-line-pack: stretch;
            align-content: stretch;
            -webkit-align-items: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
        }
    }
    @media only screen and (max-width: 1500px) {
        .hidden-1500 {
            display: none;
        }
    }
</style>

<section id="ad-paris" class="flex-col flex-center-stretch">
    <div class="integration-photo-header-80 hidden-1500">
        <img src="/img/nobo/gallery/soraya-femme-de-chambre-avec-logo.png" alt="femme de chambre de chez nobo">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 ad-form-padding">
                <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 background-black">
                    <div class="text-center">
                        <h1 class="ad-title">
                            Femme de ménage dans tous les arrondissements de Paris !
                        </h1>
                    </div>
                    <form name="sentMessage" id="contactForm" action="/reservation/envoie_mail" method="post">
                        <input type="hidden" name="origine" value="<?= htmlspecialchars($_SERVER['REQUEST_URI']); ?>">
                        <div class="col-xs-12 marge-inner-form">
                            <p class="center form-title hidden-xs">
                                Reçevez votre devis en <span class="gold bold">un clic</span> !
                            </p>
                            <div class="form-group">
                                <input type="text" name="firstname" class="form-control input-sm" placeholder="Prénom" id="firstname">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="text" name="lastname" class="form-control input-sm" placeholder="Nom" id="lastname">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <select class="form-control input-sm" name="recurrence" id="recurrence" required>
                                    <option value="" selected disabled>Combien de fois par semaine ?</option>
                                    <option value="Ponctuel">Ponctuel</option>
                                    <option value="2/mois">2/mois</option>
                                    <option value="1/semaine">1/semaine</option>
                                    <option value="2/semaine">2/semaine</option>
                                    <option value="3/semaine">3/semaine</option>
                                    <option value="4/semaine">4/semaine</option>
                                    <option value="5/semaine">5/semaine</option>
                                </select>
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" name="cp" class="form-control input-sm" placeholder="Code Postal" id="cp">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" name="tel" class="form-control input-sm" placeholder="Votre numéro de téléphone *" id="tel" required>
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 text-center">
                                <div id="success"></div>
                                <button id="devis" type="submit" class="btn-stage" name="action" value="post" style="margin-top: 0!important;">Obtenir mon devis</button>
                                <p class="white hidden-xs" style="margin-top: 0.8em">ou</p>
                                <button id="reserver" type="button" class="btn-ad-reservation hidden-xs" onclick="window.location = '/reservation/besoin';">Réserver maintenant</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Un service de ménage de qualité, à l'heure et sans engagement partout à Paris !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/nobo/sticker/star-one-blue.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/nobo/sticker/eye-blue.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/nobo/sticker/service-plus-blue.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Un service de qualité</h3>
                        <p class="nomarge">
                            Notre brigade est composée de
                            <strong>personnel issu de l’hôtellerie 4 et 5 étoiles</strong>.
                            Tous nos intervenants sont embauchés en <strong>CDI à temps plein</strong>,
                            vous n’êtes plus l’employeur !
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Un suivi minutieux</h3>
                        <p class="nomarge">
                            Comme dans un hôtel,
                            <strong>un(e) gouvernant(e) manage une petite équipe
                                d’intervenants et suit la qualité de leur travail</strong>.
                            Cette personne sera votre interface direct avec le service et
                            <strong>réalisera des contrôles
                                qualité chez vous de manière régulière</strong>.
                            Vous aurez son <strong>numéro de téléphone direct afin de pouvoir la contacter</strong>
                            en cas de besoin.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Une conciergerie du domicile</h3>
                        <p class="nomarge">
                            Afin de vous affranchir de <strong>toutes les tâches domestiques</strong>,
                            nous <strong>synchronisons gratuitement</strong> des services annexes comme
                            le <strong>pressing à domicile</strong>,
                            la <strong>livraison récurrente de fleurs</strong>,
                            <strong>la livraison de vos courses</strong>
                            ou encore <strong>la réservation d’un massage à domicile !</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1. Réservez en ligne en quelques clics</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2. La réception vous joint pour discuter de vos besoins</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                    </div>
                    <p class="color-grey">3. Un guest relation vous rencontre à domicile</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                    </div>
                    <p class="color-grey">4. Votre première prestation est réalisée</p>
                </div>
            </div>
            <p class="hidden-xs text-center margeTopBottom-1">
                <span class="italic">Ce processus prend entre 2 et 15 jours selon votre besoin</span>
            </p>
        </div>
</section>

<section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Vos voisins font appel à nous dans tous les arrondissements de Paris !</h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Une vraie aide ! Le ménage est parfait et les petites attentions font qu’on
                                    se sent vraiment traité comme à l’hôtel, c’est top bravo!"
                                </p>
                                <p>Emmanuelle, Beaugrenelle, <strong>Paris 16 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo d'Emmanuelle cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-emmanuelle.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Nettoyage toujours aussi bien après plusieurs mois de service,
                                    ce qui est très rare malheureusement.
                                    Je recommande, surtout pour les allergiques à la poussière!.."
                                </p>
                                <p>Margaux, Convention, <strong>Paris 9 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Margaux cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-margaux.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Très bon service de repassage, facilement joignable
                                    au téléphone + un réel suivi. Je recommande !"
                                </p>
                                <p>Xavier, Commerce, <strong>Paris 15 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Xavier client menage paris 15" src="/img/paris/avis/femme-de-menage-paris-15-avis-xavier.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once('../inc/footer_start.php'); ?>
<?php include_once ('../inc/exit_popup.php'); ?>
<?php include_once('../inc/analyticstracking.php'); ?>
<div>
    <div class="col-xs-12 text-center">
        <p>Nobo © <?= date_create()->format('Y'); ?></p>
    </div>
</div>
<script>
    $(document).ready(function () {
        let notifs = <?= empty($notif) ? json_encode(null) : json_encode($notif); ?>;

        if (notifs) {
            let keys = Object.keys(notifs);
            let values = Object.values(notifs);

            for (let i = 0, len = keys.length; i < len; i++) {
                $.notify({message: values[i]},{type: keys[i]});
            }
        }
    });

</script>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <!-- Hotjar Tracking Code for https://www.nobo.life -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:666727,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

        $('#contactForm').on('submit', function () {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Form',
                eventAction: 'Send',
                eventLabel: 'Paris',
                eventValue: '1',
            });
        })
    </script>
<?php endif; ?>
</body>
</html>