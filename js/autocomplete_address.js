var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('autocomplete')),
        {
            types: ['geocode'],
            componentRestrictions: {country: 'fr'}
        });
    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    let place = autocomplete.getPlace();
    let len =  place.address_components.length;
    let form = document.getElementById('check_address_form');
    let lat = place.geometry.location.lat();
    let lng = place.geometry.location.lng();

    if (lat === null || lat === undefined || lat === '')
        console.log('No lat/lng');

    for (let i = 0; i < len; i++) {
        let addressType = place.address_components[i].types[0];

        if (componentForm[addressType]) {
            let val = place.address_components[i][componentForm[addressType]];
            appendInput(addressType, val, form);
        }
    }

    appendInput('lat', lat, form);
    appendInput('lng', lng, form);
    appendInput('address_check', address_is_valid(lat, lng), form);
    Cookies.remove('reservation');
    form.submit();
}

function appendInput(name, value, el) {
    let input = document.createElement("input");

    input.type = 'hidden';
    input.name = name;
    input.value = value;
    el.appendChild(input);
}

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            let geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            let circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function address_is_valid(lat, lng) {
    let addressLatLng = new google.maps.LatLng(lat, lng);
    let my_constraint = new google.maps.Polygon({
        paths: [
            new google.maps.LatLng(48.830140, 2.222102),
            new google.maps.LatLng(48.807760, 2.239607),
            new google.maps.LatLng(48.806404, 2.352507),
            new google.maps.LatLng(48.832852, 2.447566),
            new google.maps.LatLng(48.889093, 2.443105),
            new google.maps.LatLng(48.913242, 2.398492),
            new google.maps.LatLng(48.919559, 2.293873),
            new google.maps.LatLng(48.878031, 2.183327)
        ]
    });

    return google.maps.geometry.poly.containsLocation(addressLatLng, my_constraint);
}