function imageTaille(taille)
{
    if (taille <= 20) {
        document.getElementById("slider").src="../paris/nobo/gallery/tarif-nobo-studio-menage-paris.jpg";
    } else if (taille <= 40) {
        document.getElementById("slider").src="../paris/nobo/gallery/tarif-nobo-grand-studio-paris-menage.jpg";
    } else if (taille <= 90) {
        document.getElementById("slider").src="../paris/nobo/gallery/tarif-nobo-appartement-appart-paris.jpg";
    } else {
        document.getElementById("slider").src="../paris/nobo/gallery/tarif-nobo-loft-menage-paris.jpg";
    }
}

function timeToHM(time)
{
    heures = Math.trunc(time / 60);
    minutes = Math.round(time - heures * 60);

    if (minutes <= 0.1){
        minutes = "00";
    } else if (minutes <= 30) {
        minutes = 30;
    } else {
        minutes = "00";
        heures += 1;
    }

    return (heures + "h" + minutes);
}

function HMtoTime(HM){
    var arr;
    var time;

    arr = HM.split('h');
    time = parseInt(arr[0]) * 60 + parseInt(arr[1]);
    return time;
}

function calculTime (taille, rec)
{
    var ratioRec = [1, 1.1, 1.5, 1.7];

    if (taille <= 20)
        temps = 140 + taille;
    else if (taille <= 40)
        temps = 145 + (taille - 20) * 1.5;
    else
        temps = 150 + (taille - 40);

    return temps * ratioRec[rec];
}

function calculPrice(rec, time) {
    var price;

    price = (rec == 3 || rec == 2 ? 32 : 28);
    return Math.round(price * time / 60);
}

function upWorktimeSubForm(taille, rec)
{
    var temps;
    var HM;

    temps = calculTime(taille, rec);
    HM = timeToHM(temps);
    imageTaille(taille);
    checkWorktime(HM, HM, rec);

    $('#workTime').text(HM);
    $('#workTimeReal').val(HM.split('h').join(':'));
    $('#workTimeChosen').val(HM.split('h').join(':'));
}

function checkWorktime (curTime, neededTime, rec) {
    var time;
    var price;

    time = HMtoTime(curTime);
    price = calculPrice(rec, HMtoTime(timeToHM(time)));

    if (time < neededTime){
        $('#warranty')
            .removeClass('text-success')
            .addClass('text-danger')
            .text('Nous ne pouvons vous garantir le résultat. Nous estimons à ' + timeToHM(neededTime) + ' le temps nécessaire pour garantir un résultat parfait.');
        $('#recapWarranty')
            .removeClass('text-success')
            .addClass('text-danger')
            .text('résultat NON garanti');
    }
    else {
        $('#warranty')
            .removeClass('text-danger')
            .addClass('text-success')
            .text("Suivant vos critères, voici le temps recommandé. Vous restez cependant libre de l'ajuster à votre guise !");
        $('#recapWarranty')
            .removeClass('text-danger')
            .addClass('text-success')
            .text('résultat garanti');
    }

    $('#recapPriceBrut').text(price);
    $('#recapPriceNet').text(price / 2);
    $('#recapWorktime').text(curTime);
}

function setWorktime (curTime, sens) {
    var arr;
    var time;

    if (!curTime)
        return 'error';
    if (curTime == '10h00' && sens == 1 || curTime == '3h00' && sens == -1)
        return curTime;
    arr = curTime.split('h');
    time = parseInt(arr[0]) * 60 + parseInt(arr[1]);
    return timeToHM(time + sens * 30);
}