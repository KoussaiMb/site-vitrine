/**
 * reservation session structure
 *
 * max_step
 * current_step
 * data [
 *          recurrenceType = punctual/recurrent
 *          punctualType = location/move/other
 *          recurrentType = multiple/one/half
 *          recTime = 1/2/3/4/5
 * ]
 *
 */
$(document).ready(function () {
    let calKey = "LNALFKICKHA45M2LPACNXOUQZUXFGIUK";
    let view = $('#view');
    let stepLoader = {
        0 : function () {loadAddressCheck()},
        1 : function() {loadAppointment()},
        2 : function(e) {loadCalendar(e)},
        3 : function() {loadCreateAccount()},
        4 : function() {loadAddressInfo()},
        5 : function() {loadPayment()},
        6 : function() {loadExitPage()}
    };
    let step = {
        validateAddressCheck : function () {validateAddressCheck();},
        validateNewAddress : function () {validateNewAddress();},
        AppointmentMission : function () {validateAppointmentType('mission')},
        AppointmentPhone : function () {validateAppointmentType('phone')},
        AppointmentVisit : function () {validateAppointmentType('visit')},
        AppointmentVideo : function () {validateAppointmentType('video')},
        validateMission : function () {validateMission()},
        validateVisit : function () {validateVisit()},
        validatePhone : function () {validatePhone()},
        validateAccount : function () {validateAccount()},
        validateAddressInfo : function () {validateAddressInfo()},
        validatePayment : function () {validatePayment()}
    };
    let reservation;

    getReservationFromCookie();
    load_step(reservation.current_step);

    function load_step(step) {
        let alert = getAlertFromCookie();

        if (alert !== undefined && alert !== null) {
            $.notify({message: alert['message']}, {type: alert['type']});
            removeAlertFromCookie();
        }
        reservation.current_step = step;
        setReservationInCookie();
        stepLoader[step]();
    }

    function goBack() {
        load_step(reservation.current_step - 1);
    }

    $(document).on('click', 'button[name="action"]', function () {
        step[$(this).val()]();
    });
    $(document).on('click', '.stepBack', function () {
        goBack();
    });
    $(document).on('click', '#switchAppointmentType', function () {
        let aType = reservation.appointment_type;
        let recType = reservation.recurrenceType;

        if (aType === 'phone' && recType === 'punctual') {
            validateAppointmentType('mission');
        } else if (aType === 'phone' && recType === 'recurrent') {
            validateAppointmentType('visit');
        } else {
            validateAppointmentType('phone');
        }
    });
    $(document).on('click', '#switchPaymentType', function () {
        let payment_type = $('#payment_type').val();

        loadPayment({payment_type: payment_type === 'iban' ? 'card' : 'iban'});
    });

    /*
     *  Validation functions
     */
    function validateAddressCheck() {
        let json_data = {
            cleaning_time : $('#cleaning_time').val(),
            ironing_time : $('#ironing_time').val(),
            recurrenceType : $('input[name="recurrenceChoice"]:checked').last().val(),
            punctualType : $('input[name="punctual"]:checked').val(),
            recurrentType : $('input[name="recurrent"]:checked').val(),
            recTime : $('input[name="recTime"]:checked').val()
        };

        reservation.recurrenceType = json_data.recurrenceType;
        loadAppointment(json_data);
    }

    function validateNewAddress() {
        console.log('Bdd new lead');
        console.log('Email to send');
    }

    function validateAppointmentType(appointment_type) {
        reservation.appointment_type = appointment_type;

        loadCalendar(appointment_type);
    }

    function validateMission() {
        let json_data = {
            appointment_type: reservation.appointment_type,
            date : $('#date').val(),
            slot : $('input[name="slot"]:checked').val()
        };

        loadCreateAccount(json_data);
    }

    function validateVisit() {
        let json_data = {
            appointment_type: reservation.appointment_type,
            date : $('#date').val(),
            slot : null
        };

        loadCreateAccount(json_data);
    }

    function validatePhone() {
        let json_data = {
            appointment_type: reservation.appointment_type,
            date : $('#date').val(),
            slot : null
        };

        loadCreateAccount(json_data);
    }

    function validateAccount() {
        let json_data = {
            first_name : $('#first_name').val(),
            last_name : $('#last_name').val(),
            email : $('#email').val(),
            phone : $('#phone').val(),
            password : $('#password').val(),
            c_password : $('#c_password').val()
        };

        if (reservation.appointment_type === 'phone')
            loadExitPage(json_data);
        else
            loadAddressInfo(json_data);
    }

    function validateAddressInfo() {
        let json_data = {
            batiment : $('#batiment').val(),
            floor : $('#floor').val(),
            digicode : $('#digicode').val(),
            digicode_2 : $('#digicode_2').val(),
            door : $('#door').val(),
            doorBell : $('#doorBell').val(),
            description : $('#description').val()
        };

        loadPayment(json_data);
    }

    function validatePayment() {
         loadExitPage();
    }

    /*
     * Loading functions
     */

    function loadAddressInfo(json_data) {
        setReservationStep(4);
        setReservationInCookie();

        ajax_call_html(
            '/reservation/main-form/address_info',
            'POST',
            json_data,
            function (e) {
                    view.html(e);
            },
            function (e, textStatus, xhr) {
                if (xhr.status === 500 || xhr.status === 404)
                    backupFail(true);
                else {
                    setStandardAlertInCookie();
                    load_step(3);
                }
            }
        );
    }

    function loadExitPage(json_data) {
        setReservationStep(6);
        setReservationInCookie();

        ajax_call_html(
            '/reservation/main-form/exit_page',
            'POST',
            json_data,
            function (e) {
                view.html(e);
            },
            function (e, textStatus, xhr) {
                if (xhr.status === 500 || xhr.status === 404)
                    backupFail(true);
                else {
                    setStandardAlertInCookie();
                    load_step(5);
                }
            }
        );
        removeReservationCookie();
    }

    function loadPayment(json_data) {
        setReservationStep(5);
        setReservationInCookie();

        ajax_call_html(
            '/reservation/main-form/payment',
            'POST',
            json_data,
            function (e) {
                view.html(e);
            },
            function (e, textStatus, xhr) {
                if (xhr.status === 500 || xhr.status === 404)
                    backupFail(true);
                else {
                    setStandardAlertInCookie();
                    load_step(4);
                }
            }
        );
    }

    function loadCreateAccount(json_data) {
        setReservationStep(3);
        setReservationInCookie();

        ajax_call_html(
            '/reservation/main-form/create_account',
            'POST',
            json_data,
            function (e) {
                view.html(e);
            },
            function (e, textStatus, xhr) {
                if (xhr.status === 500 || xhr.status === 404)
                    backupFail(true);
                else {
                    setStandardAlertInCookie();
                    load_step(2);
                }
            }
        );
    }

    function loadCalendar() {
        setReservationStep(2);
        setReservationInCookie();

        ajax_call_html(
            '/reservation/main-form/calendar',
            'POST',
            {appointment_type: reservation.appointment_type},
            function (e) {
                view.html(e);
            },
            function () {backupFail(true);}
        );
    }

    function loadCurrentStep() {
        getReservationFromCookie();
        stepLoader[reservation.current_step]();
    }

    function loadAddressCheck() {
        setReservationInCookie();

        ajax_call_html(
            '/reservation/main-form/address_check',
            'POST',
            null,
            function (e) {
                view.html(e);
            },
            function () {backupFail();}
        );
    }

    function loadAppointment(json_data) {
        setReservationStep(1);
        setReservationInCookie();

        ajax_call_html(
            '/reservation/main-form/appointment',
            json_data === undefined ? 'GET' : 'POST',
            json_data,
            function (e) {
                view.html(e);
            },
            function (e, textStatus, xhr) {
                if (xhr.status === 500 || xhr.status === 404)
                    backupFail(true);
                else {
                    setStandardAlertInCookie();
                    load_step(0);
                }
            }
        );
    }

    /*
     * Core Reception functions
     */

    function backupFail(stepBack = false) {
        let callDiv = createCallDiv(stepBack);

        view.html(callDiv);
    }

    function setReservationStep(step) {
        reservation.max_step = Math.max(reservation.max_step, step);
        reservation.current_step = step;
    }

    function getReservationFromCookie() {
        let res_temp = Cookies.getJSON('reservation');

        if (res_temp === undefined) {
            reservation = {
                max_step: 0,
                current_step: 0,
                appointment_type: null,
                recurrenceType: ''
             }
        }
        else
            reservation = res_temp;
    }

    function setReservationInCookie() {
        Cookies.set('reservation', JSON.stringify(reservation));
    }

    function getAlertFromCookie() {
        return Cookies.getJSON('alert');
    }

    function setStandardAlertInCookie() {
        setAlertInCookie('Les informations sont invalides', 'warning')
    }

    function setAlertInCookie(message, type) {
        Cookies.set('alert', JSON.stringify({message: message, type: type}));
    }

    function removeAlertFromCookie() {
        Cookies.remove('alert');
    }

    function removeReservationCookie() {
        Cookies.remove('reservation');
    }

    function createCallDiv(stepBack = false) {
        return "<section class=\"container mt-3 mb-3\">" +
            "<div class=\"row justify-content-center\">" +
            "<div class=\"col col-6 text-center\">" +
            "<div class=\"card reservation-card\">" +
            "<div class=\"card-header justify-content-center text-center alert-danger\">" +
            (stepBack === true
            ? "<span class=\"material-icons stepBack align-bottom\">chevron_left</span>"
            : "") +
            "<span>Un problème est survenu !</span>" +
            "</div>" +
            "<div class=\"card-body\">" +
            "<div class=\"mt-5\">" +
            "<p class='text-alert'>Désolé pour la gêne occasionnée :(</p>" +
            "<p>Contactez nous par téléphone au <a href='tel:"+urgenceCall+"'>"+readablePhone(urgenceCall)+"</a></p>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</section>";
    }

    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );
});
