function scrollEvent(method, cookie, trigger, bounce, scroll_down) {
    let st = $(window).scrollTop();

    if (scroll_down === false && st >= trigger)
        return true;
    if (scroll_down === true && st <= bounce)
        setCookieAndDropModalAndEvent(method, cookie);
    return scroll_down || false;
}

function setCookieAndDropModalAndEvent(method, cookie) {
    $('.lightbox').slideDown();
    Cookies.set(cookie, "1", 3);
    $(document).off(method);
}

$(document).ready(function () {
//CLOSE DESKTOP POPUP
    $('.close-popup').on('click', function () {
        $('.lightbox').slideUp();
    });
    $(document).on('click', function () {
        $('.lightbox').slideUp();
    });
    $(".popup-box").on('click', function (evt) {
        evt.stopPropagation();
    });
});