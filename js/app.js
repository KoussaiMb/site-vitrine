/**
 *  Ajax
 */

function ajax_log(error, file, func){
    let fn = func === undefined ? "ajax_call" : func;

    return $.ajax({
        url: '/api/log',
        data: {
            error : error,
            function : fn,
            file : file
        },
        dataType: 'json',
        type: 'POST',
        success: function(ret) {
            notify(ret.message, ret.code === '200' ? 'info' : 'danger');
        },
        error: function () {
            notify("An error system occured, call an admin", 'danger')
        }
    });
}

function ajax_call(url, type, data, SuccessCallBack, errorCallBack){
    SuccessCallBack = (typeof SuccessCallBack !== 'undefined') ? SuccessCallBack : false;

    if (typeof errorCallBack !== 'undefined')
        return $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            type: type,
            success: SuccessCallBack,
            error: errorCallBack
        });
    else
        return $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            type: type,
            success: SuccessCallBack,
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.url);
                console.log(thrownError);
            }
        })
}

function ajax_call_html(url, type, data, SuccessCallBack, errorCallBack){
    SuccessCallBack = (typeof SuccessCallBack !== 'undefined') ? SuccessCallBack : false;

    if (typeof errorCallBack !== 'undefined')
        return $.ajax({
            url: url,
            data: data,
            dataType: 'html',
            type: type,
            success: SuccessCallBack,
            error: errorCallBack
        });
    else
        return $.ajax({
            url: url,
            data: data,
            dataType: 'html',
            type: type,
            success: SuccessCallBack,
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.url);
                console.log(thrownError);
            }
        })
}

/**
 *  Notifications
 */

function notify(message, type) {
    $.notify({
        // options
        message: message
    }, {
        // settings
        element: 'body',
        type: type,
        allow_dismiss: true,
        newest_on_top: true,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        //offset: 200,
        //spacing: 10,
        z_index: 1031,
        delay: 1000,
        timer: 2800,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}

/*
Timer picker modifier for materialize
 */

function cleanTimePickerHours(hour_to_hide) {
    let hours = $('.timepicker-hours>.timepicker-tick');

    $.each(hour_to_hide, function (i, el) {
        hours.each(function (j, hour) {
            if (hour.textContent === el) {
                hour.remove();
                return false;
            }
        });
    })
}

function cleanTimePickerMinutes(minute_to_hide) {
    let minutes = $('.timepicker-minutes>.timepicker-tick');

    $.each(minute_to_hide, function (i, el) {
        minutes.each(function (j, minute) {
            if (minute.textContent === el) {
                minute.remove();
                return false;
            }
        });
    })
}

function hideAmPmTimePicker() {
    $('.timepicker-span-am-pm').remove();
}

function readablePhone(phone) {
    let len = phone.length;
    let readablePhone = "";
    let i = 0;

    while (42) {
        readablePhone += phone[i] + phone[i + 1];
        i += 2;
        if (i >= len)
            break;
        readablePhone += " ";
    }

    return readablePhone;
}