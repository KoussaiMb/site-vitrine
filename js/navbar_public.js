$(function() {
    let offsetScroll = window.innerHeight;
    let height = $(document).height();

    if ($(window).scrollTop() >= 20) {
        $(".navbar").removeClass("navbar-transparent");
    } else {
        $(".navbar").addClass("navbar-transparent");
    }

    $(window).on("scroll", function() {
        let curScroll = $(window).scrollTop();

        if (curScroll >= 20) {
            $(".navbar").removeClass("navbar-transparent");
        } else {
            $(".navbar").addClass("navbar-transparent");
        }
        if (curScroll >= offsetScroll) {
            if (curScroll >= height - offsetScroll * 2)
                $('#phone-xs div').stop().fadeOut(200);
            else
                $('#phone-xs div').fadeIn(200);
        } else {
            $('#phone-xs div').stop().fadeOut(200);
        }
    });
});