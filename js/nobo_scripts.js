/**
 *  App
 */

function parseDate(input) {
    var parts = input.match(/(\d+)/g); // parse a date in yyyy-mm-dd format
    return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
}

/**
 *  Time functions
 */

function addTime(time1, time2) {
    var date1 = new Date("7/13/2010 " + time1);
    var date2 = new Date("7/13/2010 " + time2);
    var dateRef = new Date("7/13/2010");
    var t = diffDate(dateRef, date1) + diffDate(dateRef, date2);
    return msToTime(t);
}

function multiplyTime(i, time) {
    var date = new Date("7/13/2010 " + time);
    var dateRef = new Date("7/13/2010");
    var t = i * diffDate(dateRef, date);
    return msToTime(t);
}

function diffDate(date1, date2){
    return Math.abs(date1.getTime() - date2.getTime());
}

function msToTime(ms){
    var h = Math.floor(ms/(1000 * 3600));
    var m = Math.floor(ms/(1000 * 60) % 60);
    return ((h.toString().length == 1 ? '0' + h : h) + ':' + (m.toString().length == 1 ? '0' + m : m));
}

/**
 *  Google API functions
 */

function LoadGoogleMap(map_id, Lat, Lng) {
    var myCenter = new google.maps.LatLng(Lat, Lng);
    var mapCanvas = document.getElementById(map_id);
    var mapOptions = {center: myCenter, zoom: 15};
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({position:myCenter});
    marker.setMap(map);
}
function initGoogleAutocomplete_addr(input_id) {
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById(input_id)),
        {types: ['geocode'], componentRestrictions: {country: 'fr'}});
    autocomplete.addListener('place_changed', fillInAddr_id);
}
function initGoogleAutocomplete_home(input_id) {
    autocomplete_home = new google.maps.places.Autocomplete(
        (document.getElementById(input_id)),
        {types: ['geocode'], componentRestrictions: {country: 'fr'}});
    autocomplete_home.addListener('place_changed', fillInAddr_home);
}

/*function initGoogleAutocomplete(input_id) {
    var ID = input_id.substr(input_id.indexOf('_') + 1);
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById(input_id)),
        {types: ['geocode']});
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            document.getElementById(component+ '_' + ID).value = '';
        }
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType + '_' + ID).value = val;
            }
        }
        $('#lat_' + ID).val(place.geometry.location.lat());
        $('#lng_' + ID).val(place.geometry.location.lng());
        $('#Address_' + ID).val($('#street_number_' + ID).val() + ' ' + $('#route_' + ID).val());
    })
}*/
function Googlegeolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

/**
* App function
*/

function rand() {
    return Math.random().toString(36).substr(2);
}