#!/bin/bash
cd mon-espace
pwd
curDirName=${PWD##*/}          # to assign to a variable
if [ "$curDirName" == "mon-espace" ]
then
    composer install
    npm install
    ./node_modules/.bin/encore production
    php bin/console cache:clear --env=prod --no-debug
    php bin/console cache:warmup --env=prod --no-debug
fi
