<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 10/11/2016
 * Time: 15:51
 */
class parseJson
{
    static function getKey($data)
    {
        return implode(", ", array_keys($data));
    }

    static function getVal($data)
    {
        return implode("', '", array_values($data));
    }

    private function createMeta($code, $msg = null, $success = null)
    {
        if ($msg) {
            if ($success) {
                return (object)['code' => $code, 'success_message' => $msg];
            } else {
                return (object)['code' => $code, 'error_message' => $msg];
            }
        }
        return (object)['code' => $code];
    }

    static function error($msg = 'Your request is weird !')
    {
        return json_encode(['meta' => self::createMeta(400, $msg, 0)], JSON_PRETTY_PRINT);
    }

    static function cleanString($string) {
        $string = strtolower($string);
        $string = preg_replace("/[^a-z0-9_'\s-]/", "", $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        $string = preg_replace("/[\s_]/", " ", $string);
        return $string;
    }
}