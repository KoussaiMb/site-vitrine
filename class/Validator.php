<?php
/**
 * Created by PhpStorm.
 * User: damie
 * Date: 04/11/2016
 * Time: 10:42
 */
class Validator
{
    private $data;
    private $errors = [];

    public function __construct($data){
        $this->data = $data;
    }
    /*
     * Private functions
     */
    private function getField($field){
        if (!isset($this->data[$field])) {
            return null;
        }
        return $this->data[$field];
    }
    private function is_empty($field){
        return $this->getField($field) == null;
    }
    /*
     * Public functions
     */
    public function is_uniq($field, $db, $table, $errMsg){
        $record = $db->query("SELECT id FROM `$table` WHERE `$field` = ?", [$this->getField($field)])->fetch();
        if($record){
            $this->errors[$field] = $errMsg;
        }
    }
    public function exist($table = 'user', $db, $field = 'id'){
        $record = $db->getRow($table, $db, $this->getField($field));
        if ($record){return true;}
        return false;
    }
    public function is_confirmed($db, $field){
        $record = $db->getRow('pending', 'USER', $field, $this->getField($field));
        if ($record && !$record->pending){return true;}
        return false;
    }
    public function date_isok($db){
        $record = $db->getRow('asked_at', 'USER', 'email', $this->getField('email'));
        $d1 = $record->asked_at;
        $d2 = new DateTime("now");
        if ($record->asked_at && date_diff($d1, $d2) < 1){return false;}
        return true;
    }
    public function can_confirm($db){
        $record = $db->getRow('pending', 'USER', 'id', $this->getField('id'));
        if ($record && $record->pending == 1){return true;}
        return false;
    }
    public function is_alpha($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("/^[a-zA-Z ]*$/", $this->getField($field))){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_alphanum($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("/^[0-9a-zA-Z ]*$/", $this->getField($field))){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_digicode($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("/^[0-9a-zA-Z]*$/", $this->getField($field)) || strlen($this->getField($field)) > 8){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_cp($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("/^[0-9]*$/", $this->getField($field)) || strlen($this->getField($field)) != 5){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_range($field, $errValid = null, $range){
        if ($this->getField($field) < $range[0] || $this->getField($field) > $range[1]){
            if ($errValid == null)
                $this->errors[$field] = 'Le format n\'est pas valide';
            else
                $this->errors[$field] = $errValid;
        }
    }

    public function is_password($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errEmpty[$field] = $errEmpty;
        }
        elseif(!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/", $this->getField($field))){
            $this->errValid[$field] = $errValid;
        }
    }

    public function is_num($field, $errValid = null, $errEmpty = null, $nb_digit = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif($nb_digit && strlen($this->getField($field)) > $nb_digit){
            $this->errors[$field] = "Le nombre entré est trop grand";
        }
        elseif(!preg_match("/^[0-9 ]*$/", $this->getField($field))){
            if($errValid == null){
                $this->errors[$field] = 'Le format n\'est pas valide';
            }else {
                $this->errors[$field] = $errValid;
            }
        }
    }
    public function is_pack_duedate($field, $errEmpty = null)
    {
        if ($errEmpty && $this->is_empty($field)) {
            $this->errors[$field] = $errEmpty;
        } elseif (!preg_match("/^[0-9 ]*$/", $this->getField($field))) {
            if ($errEmpty == null) {
                $this->errors[$field] = 'Le format n\'est pas valide';
            } elseif ($this->getField($field) < 1 || $this->getField($field) > 24) {
                $this->errors[$field] = 'La date limite du pack doit se situer entre 1 et 24 mois';
            }
        }
    }
    public function is_siret($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("/^[0-9 ]*$/", $this->getField($field)) && strlen($this->getField($field)) != 14){
            $this->errors[$field] = $errValid;
        }
    }
    /*
     $fichier = strtr($fichier,
     'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
     'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
    //On remplace les lettres accentutées par les non accentuées dans $fichier.
    //Et on récupère le résultat dans fichier
    //En dessous, il y a l'expression régulière qui remplace tout ce qui n'est pas une lettre non accentuées ou un chiffre
    //dans $fichier par un tiret "-" et qui place le résultat dans $fichier.
    $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
     */
    public function is_file($file, $destination, $maxsize = false, $extensions = false){
        if (!isset($_FILES[$file]) OR $_FILES[$file]['error'] > 0) {
            $this->errors['file'] = "Le document suivant: $file est manquant ou corrompu";
        }
        elseif ($maxsize !== false AND $_FILES[$file]['size'] > $maxsize){
            $this->errors['file'] = 'La taille de ' . $_FILES[$file]['name'] . ' est trop importante';
        }
        elseif ($extensions !== false AND is_array($extensions) AND !in_array(substr(strrchr($_FILES[$file]['name'],'.'),1), $extensions)){
            $this->errors['file'] = 'Veuillez upload un fichier au format pdf';
        }
        elseif (!move_uploaded_file($_FILES[$file]['tmp_name'], $destination)){
            $this->errors['file'] = 'Le fichier n\'a pas pu être upload';
        }
    }
    public function is_photo_profile($file, $maxsize = false, $extensions = false){
        if (!isset($_FILES[$file]) OR $_FILES[$file]['error'] > 0) {
            $this->errors['file'] = "Le document suivant: $file est manquant ou corrompu";
        }
        elseif ($maxsize !== false AND $_FILES[$file]['size'] > $maxsize){
            $this->errors['file'] = 'La taille de ' . $_FILES[$file]['name'] . ' est trop importante';
        }
        elseif ($extensions !== false AND is_array($extensions) AND !in_array(substr(strrchr($_FILES[$file]['name'],'.'),1), $extensions)){
            $this->errors['file'] = 'Veuillez upload un fichier au format jpg ou png';
        }
    }
    public function is_email($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!filter_var($this->getField($field), FILTER_VALIDATE_EMAIL)){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_stripeToken($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("/(tok_)([a-zA-Z0-9]*)$/", $this->getField($field))){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_pw($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,20}$/", $this->getField($field))){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_same($field1, $field2, $errMsg){
        if($this->getField($field1) !== $this->getField($field2)){
            $this->errors[$field2] = $errMsg;
        }
    }
    public function is_phone($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!preg_match("#^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$#", $this->getField($field))){
            $this->errors[$field] = $errValid;
        }
    }
    //(0|(\+33)|(0033))[1-9][0-9]{8}
    public function is_date($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!$this->valid_date($this->getField($field), 'd-m-Y') && !$this->valid_date($this->getField($field), 'd/m/Y') && !$this->valid_date($this->getField($field), 'd,m,Y')){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_date_bo($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        elseif(!$this->valid_date($this->getField($field), 'Y-m-d')){
            $this->errors[$field] = $errValid;
        }
    }
    private function valid_date($date, $format = 'd-m-Y'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    ///^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
    public function is_time($field, $errValid){
        if (!preg_match("/^(24:00)|(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]*$/", $this->getField($field))){
            $this->errors[$field] = $errValid;
        }
    }
    public function is_valid(){
        return empty($this->errors);
    }
    public function getErrors(){
        return $this->errors;
    }

    public function getData(){
        return $this->data;
    }
}