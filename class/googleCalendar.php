<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 06/09/2018
 * Time: 14:09
 */

require __DIR__ .'/../vendor/autoload.php';
//putenv('GOOGLE_APPLICATION_CREDENTIALS=c:/wamp64/www/nobo/nobo-vitrine/reservation/main-form/credentials.json');
//putenv('GOOGLE_APPLICATION_CREDENTIALS=C:\Users\jb\Downloads\nobo-calendar-46bb909cb1e5.json');
putenv('GOOGLE_APPLICATION_CREDENTIALS=C:\Users\jb\Downloads\Nobo-b68addef8ea2.json');

class googleCalendar
{
    private $client;
    private $calendars_id =
        [
            'phone_available' => '',
            'video_available' => '',
            'physic_available' => '',
            'phone_event' => '',
            'video_event' => '',
            'physic_event' => ''
        ];

    function __construct()
    {
        $this->getClient();
    }

    private function getClient()
    {
        $this->client = new Google_Client();
        $this->client->useApplicationDefaultCredentials();
        $this->client->setScopes(Google_Service_Calendar::CALENDAR_READONLY);
        /*
        $client->setHttpClient(new \GuzzleHttp\Client(array(
            'verify' => 'C:\wamp64\ssl\ca-bundle.crt',
        )));
        */
        //$client->setApplicationName('test');
        //$client->setSubject('dev.nobo@gmail.com');
    }

    public function test2() {
        $service = new Google_Service_Calendar($this->client);
        $calendarId = 'al0jjisio9mr7fgns7788tflhg@group.calendar.google.com';
        //$calendarId = 'dev.nobo@gmail.com';
        $optParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date('c'),
        );

        try {
            $results = $service->events->listEvents($calendarId, $optParams);
            debug::data_r($results);
        } catch(Exception $e) {
            debug::data_r($e->getMessage());
            exit();
        }
    }


    public function test() {
        $service = new Google_Service_Calendar($this->client);
        // Print the next 10 events on the user's calendar.
        $calendarId = 'al0jjisio9mr7fgns7788tflhg@group.calendar.google.com';
        //$calendarId = 'dev.nobo@gmail.com';
        $optParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date('c'),
        );
        try {
            $results = $service->events->listEvents($calendarId, $optParams);
            $events = $results->getItems();
        } catch(Exception $e) {
            debug::data_r($e->getMessage());
            exit();
            //$this->google_exception($e->getMessage());
        }
        if (empty($events)) {
            return ["events" => array()];
        } else {
            $events = [];

            foreach ($events as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                 $events[] = $start;
            }
            return ["events" => $events];
        }
    }

    function getUserFromToken($token) {
        $ticket = $this->client->verifyIdToken($token);
        if ($ticket) {
            $data = $ticket->getAttributes();
            return $data['payload']['sub']; // user ID
        }
        return false;
    }

    private function google_exception($exception) {
        debug::data_r($exception);
        $json = json_decode($exception);

        if (json_last_error() == JSON_ERROR_NONE)
            $error = $json->error;
        else
            $error = $exception;
        if (isset($error->code) && $error->code === 404)
            echo 'L\'id du calendrier est invalide ou vous n\'avez pas l\'accès...';
        else
            echo "error occured";
    }

    private function getEvent($events, $service){

        while(true) {
            foreach ($events->getItems() as $event) {
                echo $event->getSummary();
            }
            $pageToken = $events->getNextPageToken();
            if ($pageToken) {
                $optParams = array('pageToken' => $pageToken);
                $events = $service->events->listEvents('primary', $optParams);
            } else {
                break;
            }
        }
    }
}