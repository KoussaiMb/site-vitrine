<?php
/**
 * host = nobolifesknobo.mysql.db
 * dbname = nobolifesknobo
 * username = nobolifesknobo
 * password = Booster121
 */
/**
 * host = vps334537.ovh.net
 * dbname = nobo
 * username = root
 * password = ElRico1337
 */

require_once(realpath($_SERVER["DOCUMENT_ROOT"]) .'/vendor/autoload.php');

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

class App
{
    static $db = null;
    static $list = null;

    static function get_user_info(){
        DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_NONE);

        $dd = new DeviceDetector($_SERVER['HTTP_USER_AGENT']);

        $dd->parse();

        $userInfo = [];
        $userInfo['browser'] = $dd->getClient()['name'];
        $userInfo['os'] = $dd->getOs()['name'];
        $userInfo['device'] = $dd->getDevice();
        $userInfo['brand'] = $dd->getBrandName();
        $userInfo['model'] = $dd->getModel();
        $userInfo['ipAddress'] = App::get_user_ip();

        return ($userInfo);
    }

    static function getDB($database = 'nobo'){
        if (!self::$db){
            self::$db = new DB($_SERVER['HTTP_MYSQL_HOST'], $_SERVER['HTTP_MYSQL_DBNAME'], $_SERVER['HTTP_MYSQL_USERNAME'], $_SERVER['HTTP_MYSQL_PASSWORD']);
        }
        return self::$db;
    }
    static function getAuth(){
        return new Auth(Session::getInstance(), self::getDB(), [
            'restrictMsg' => "Sorry, la page à laquelle vous tentez d'accéder n'est pas disponible",
        ]);
    }
    static function getApi($secret_key){
        if ($secret_key !== 'JFnepz0364cw#jj&d=RDXe$fEc%few<sz')
            return false;
        return new api(self::getDB());
    }
    static function getCalendar(){
        return new calendar(self::getDB());
    }
    static function getFromList(){
        if (!self::$list){
            self::$list = self::getDB()->query("SELECT * FROM `List`")->fetchAll();
        }
        return new FromList(self::$list, self::getDB());
    }
    static function redirect($url){
        header("Location: $url");
        exit(0);
    }
    static function redirect_js($url){
        echo "<script>location ='". $url . "';</script>";
        exit(0);
    }
    static function console_log($data){
        echo "<script>console.log(". $data . ");</script>";
    }
    static function object_to_array($obj) {
        $arr= null;
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? App::object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }
    static function logPDOException(PDOException $e, $deep = 1)
    {
        $values = [];
        $values[] = debug_backtrace()[$deep]['function'];
        $values[] = $e->getMessage();
        $values[] = NULL;
        $values[] = ParseStr::after_trailing_slash(debug_backtrace()[$deep]['file']);

        try{
            self::getDB()->query("INSERT INTO `log_bo`(`function`, `error`, `user_id`, `file`, `date`) VALUES(?, ?, ?, ?, NOW())",
                $values);
        } catch (PDOException $e){
            Session::getInstance()->setFlash("danger", "erreur critique , contacter un admin");
        }
    }
    static function array_without_null($arr){
        $new_arr = null;
        foreach ($arr as $k => $v){
            if ($v){
                $new_arr = [$k => $v];
            }
        }
        return $new_arr;
    }
    static function getListManagement(){
        return new ListManagement(self::getDB());
    }
    static function getMail(){
        return new mjMail(self::getApi('JFnepz0364cw#jj&d=RDXe$fEc%few<sz'));
    }

    static function getPreviousProvider(){
        return new PreviousProvider(self::getDB());
    }

    static function NewLog($value, $user_id){
        self::getDB()->query("INSERT INTO `Log_User` SET `Date` = now(), `Value` = ?, `USER_ID` = ?", [$value, $user_id]);
    }

    static function getDispo($provider_id){
        $dispo = self::getDB()->query("SELECT *, D.ID as id FROM DISPO D WHERE D.USER_ID = ? AND D.start > NOW() ORDER BY D.start ASC", [$provider_id])->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($dispo);
    }
    static function returnJson($status, $msg){
        echo json_encode(array('status' => $status, 'message' => $msg));
        exit();
    }
    static function setFlashAndRedirect($flashType, $flashMessage, $redirection) {
        Session::getInstance()->setFlash($flashType, $flashMessage);
        self::redirect($redirection);
    }
    static function setFlashArrayAndRedirect($flashType, $flashMessageArray, $redirection) {
        $flashMessage = '';
        foreach ($flashMessageArray as $curMessage) {
            if (empty($flashMessage))
                $flashMessage .= $curMessage;
            else
                $flashMessage .= '<br>' . $curMessage;
        }
        Session::getInstance()->setFlash($flashType, $flashMessage);
        self::redirect($redirection);
    }
    static function upSuccess() {
        Session::getInstance()->setFlash('success', 'Les modifications ont été prises en compte.');
    }
    static function upFail() {
        Session::getInstance()->setFlash('danger', 'Les modifications n\'ont pas été prises en compte.');
    }
    static function unkownError() {
        Session::getInstance()->setFlash('danger', 'Une erreure inconnue est survenue, veuillez nous excuser.');
    }
    static function criticalError(){
        Session::getInstance()->setFlash('info', 'le site est en maintenance, réessayez plus tard.');
        //self::redirect($_SERVER['HTTP_URL_NOBO']);
    }
    static function errorRedirection(){
        return (object)[
            'red1' => '..',
            'content1' => 'Page d\'accueil',
            'red2' => '../reservation/besoin',
            'content2' => 'Effectuer sa première mission'
        ];
    }

    static function get_user_ip(){
        foreach ( array(
                      'HTTP_CLIENT_IP',
                      'HTTP_X_FORWARDED_FOR',
                      'HTTP_X_FORWARDED',
                      'HTTP_X_CLUSTER_CLIENT_IP',
                      'HTTP_FORWARDED_FOR',
                      'HTTP_FORWARDED',
                      'REMOTE_ADDR' ) as $key ) {
            if ( array_key_exists( $key, $_SERVER ) === true ) {
                foreach ( explode( ',', $_SERVER[ $key ] ) as $ip ) {
                    $ip = trim( $ip );
                    if ( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE ) !== false
                        && ( ( ip2long( $ip ) & 0xff000000 ) != 0x7f000000 ) ) {
                        return $ip;
                    }
                }
            }
        }
        return false;
    }

    static function parse_phone($needle = '', $phone) {
        $max = strlen($phone);
        $i = -1;

        while (++$i < $max) {
            if ($i != 0 && $i % 2 == 0)
                echo $needle;
            echo $phone[$i];
        }
    }

    static function parse_phone_v2($needle = '', $phone) {
        $max = strlen($phone);
        $i = -1;
        $res = "";

        while (++$i < $max) {
            if ($i != 0 && $i % 2 == 0)
                $res .= $needle;
            $res .= $phone[$i];
        }

        return $res;
    }

    static function setHtmlAndExit($message, $type) {
        echo "<div class='col text-center'><p class='alert alert-" .$type. "'>" .$message. "</p></div>";
        exit();
    }
}