<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 16:42
 */

class generate_header
{
    private $fonts = [];
    private $metas = [];
    private $cssFiles = [];
    private $scriptFiles = [];
    private $css = '';
    private $script = '';
    private $title;
    private $bodyClass = [];
    private $description;
    private $facebookInfo = [];
    private $googleInfo = [];

    /**
     * generate_header constructor.
     * @param $title
     * @param $description
     */
    function __construct($title, $description)
    {
        $this->setTitle($title);
        $this->setDescription($description);
        $this->facebookInfo['url'] = $this->getSelfUrl();
        $this->facebookInfo['description'] = $description;
        $this->facebookInfo['title'] = $title;
        $this->facebookInfo['type'] = 'website';
        $this->facebookInfo['app_id'] = '430915960574732';
        $this->facebookInfo['image'] = 'https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg';
        $this->googleInfo['verification'] = 'Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU';
    }

    /**
     * @return string
     */
    private function getSelfUrl() {
        $pageURL = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
        $url = $pageURL . $_SERVER['SERVER_NAME'] . strtok($_SERVER["REQUEST_URI"],'?');
        return $url;
    }

    /**
     * @param $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @param array $bodyClass
     */
    public function setBodyClass(array $bodyClass) {
        foreach ($bodyClass as $class)
            $this->bodyClass[] = $class;
    }

    /**
     * @return string
     */
    public function getBodyClass() {
        return implode(',', $this->bodyClass);
    }

    /**
     * @param $name
     * @param $content
     */
    public function setMeta($name, $content) {
        $this->metas[] = ['name' => $name, 'content' => $content];
    }

    /**
     * @param $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @param array $facebookInfo
     */
    public function setFacebookInfo(array $facebookInfo) {
        $info_available = ['url', 'type', 'title', 'description', 'app_id', 'image'];

        foreach ($facebookInfo as $info_type => $info)
            if (in_array($info_type, $info_available))
                $this->facebookInfo[$info_type] = $info;
    }

    /**
     * @param array $googleInfo
     */
    public function setGoogleInfo(array $googleInfo) {
        $info_available = ['verification'];

        foreach ($googleInfo as $info_type => $info)
            if (in_array($info_type, $info_available))
                $this->googleInfo[$info_type] = $info;
    }

    /**
     * @param $cssFile
     * @param null $integrity
     * @param null $crossorigin
     */
    public function setCssFile($cssFile, $integrity = null, $crossorigin = null) {
        $this->cssFiles[] = [
                'href' => $cssFile,
                'integrity' => $integrity,
                'crossorigin' => $crossorigin
            ];
    }

    /**
     * @param $scriptFile
     * @param null $integrity
     * @param null $crossorigin
     */
    public function setScriptFile($scriptFile, $integrity = null, $crossorigin = null) {
        $this->scriptFiles[] = [
            'src' =>$scriptFile,
            'integrity' => $integrity,
            'crossorigin' => $crossorigin
        ];
    }

    /**
     * @param $css
     */
    public function setCss($css) {
        $this->css .= $css;
    }

    /**
     * @param $script
     */
    public function setScript($script) {
        $this->script .= $script;
    }

    public function setBasicHeader() {
        $this->setCssFile('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
            "sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm",
            "anonymous");
        $this->setFont('https://fonts.googleapis.com/icon?family=Material+Icons');
        $this->setFont("https://fonts.googleapis.com/css?family=Montserrat:300,500,800i");
        $this->setFont("/fonts/GothamLight.ttf");
        $this->setFont("/fonts/GothamMedium.ttf");
    }

    /**
     * @param $font
     */
    public function setFont($font) {
        $this->fonts[] = $font;
    }

    public function printScriptFiles() {
        foreach ($this->scriptFiles as $scriptFile) {
            echo '<script type="text/javascript" src="'.$scriptFile['src'] .'"';
            if ($scriptFile['integrity'] !== null)
                echo ' integrity="'.$scriptFile['integrity'].'"';
            if ($scriptFile['crossorigin'] !== null)
                echo ' crossorigin="'.$scriptFile['crossorigin'].'"';
            echo '></script>';
        }
    }

    public function printCssFiles() {
        foreach ($this->cssFiles as $cssFile) {
            echo '<link rel="preload" href="'.$cssFile['href'].'" as="style" onload="this.onload=null;this.rel=\'stylesheet\'"';
            if ($cssFile['integrity'] !== null)
                echo ' integrity="'.$cssFile['integrity'].'"';
            if ($cssFile['crossorigin'] !== null)
                echo ' crossorigin="'.$cssFile['crossorigin'].'"';
            echo ">";
            echo '<noscript>';
            echo '<link rel="stylesheet" href="'.$cssFile['href'].'"';
            if ($cssFile['integrity'] !== null)
                echo ' integrity="'.$cssFile['integrity'].'"';
            if ($cssFile['crossorigin'] !== null)
                echo ' crossorigin="'.$cssFile['crossorigin'].'"';
            echo '</noscript>';
        }
    }

    public function generate() {
        echo '<!doctype html>';
        echo '<html>';
        echo '<head>';
        if($_SERVER['HTTP_STAGE'] != 'prod')
            echo '<meta name="robots" content="noindex, nofollow">';
        //encodage
        echo '<meta charset="utf-8" lang="fr">';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
        // mobile
        echo '<meta name="viewport" content="width=device-width, shrink-to-fit=no">';
        echo '<meta name="viewport" content="initial-scale=1.0, minimum-scale=0.5, maximum-scale=3.0">';
        // compatibility
        echo '<meta http-equiv="x-ua-compatible" content="ie=edge">';
        echo '<title>'.$this->title.'</title>';
        echo '<meta name="description" content="'.$this->description.'">';
        echo '<meta name="google-site-verification" content="'.$this->googleInfo['verification'].'"/>';
        echo '<meta property="og:url"                 content="'.$this->facebookInfo['url'].'"/>';
        echo '<meta property="og:type"                content="'.$this->facebookInfo['type'].'"/>';
        echo '<meta property="og:title"               content="'.$this->facebookInfo['title'].'"/>';
        echo '<meta property="og:description"         content="'.$this->facebookInfo['description'].'"/>';
        echo '<meta property="fb:app_id"              content="'.$this->facebookInfo['app_id'].'"/>';
        echo '<meta property="og:image"               content="'.$this->facebookInfo['image'].'"/>';
        foreach ($this->metas as $meta)
            echo '<meta name="'.$meta['name'].'" content="'.$meta['content'].'">';
        //font
        foreach ($this->fonts as $font) {
            echo '<link rel="preload" href="'.$font.'" as="style" onload="this.onload=null;this.rel=\'stylesheet\'">';
            echo '<noscript><link rel="stylesheet" href="'.$font.'"></noscript>';
        }
        $this->printCssFiles();
        $this->printScriptFiles();
        if (!empty($this->css))
            echo '<style>'.$this->css.'</style>';
        if (!empty($this->script))
            echo '<script>'.$this->script.'</script>';
        ?>
        <script>
            /* loadCSS. [c]2017 Filament Group, Inc. MIT License */
            /* Standalone workflow */
            (function( w ){
                "use strict";
                if( !w.loadCSS ){
                    w.loadCSS = function(){};
                }
                var rp = loadCSS.relpreload = {};
                rp.support = (function(){
                    var ret;
                    try {
                        ret = w.document.createElement( "link" ).relList.supports( "preload" );
                    } catch (e) {
                        ret = false;
                    }
                    return function(){
                        return ret;
                    };
                })();

                rp.bindMediaToggle = function( link ){
                    var finalMedia = link.media || "all";

                    function enableStylesheet(){
                        link.media = finalMedia;
                    }
                    if( link.addEventListener ){
                        link.addEventListener( "load", enableStylesheet );
                    } else if( link.attachEvent ){
                        link.attachEvent( "onload", enableStylesheet );
                    }
                    setTimeout(function(){
                        link.rel = "stylesheet";
                        link.media = "only x";
                    });
                    setTimeout( enableStylesheet, 3000 );
                };

                rp.poly = function(){
                    if( rp.support() ){return;}
                    var links = w.document.getElementsByTagName( "link" );
                    for( var i = 0; i < links.length; i++ ){
                        var link = links[i];
                        if( link.rel === "preload" && link.getAttribute( "as" ) === "style" && !link.getAttribute( "data-loadcss" ) ){
                            link.setAttribute( "data-loadcss", true );
                            rp.bindMediaToggle( link );
                        }
                    }
                };
                if( !rp.support() ){
                    rp.poly();

                    var run = w.setInterval( rp.poly, 500 );
                    if( w.addEventListener ){
                        w.addEventListener( "load", function(){
                            rp.poly();
                            w.clearInterval( run );
                        } );
                    } else if( w.attachEvent ){
                        w.attachEvent( "onload", function(){
                            rp.poly();
                            w.clearInterval( run );
                        } );
                    }
                }
                if( typeof exports !== "undefined" ){
                    exports.loadCSS = loadCSS;
                }
                else {
                    w.loadCSS = loadCSS;
                }
            }( typeof global !== "undefined" ? global : this ) );
        </script>
        <?php
        echo '</head>';
        echo '<body class="'.$this->getBodyClass().'">';
    }
}
