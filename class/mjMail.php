<?php
require_once '../mailjet/vendor/autoload.php';
use \Mailjet\Resources;

class mjMail
{
    private $mailjet;

    public function __construct($api)
    {
        $mailjet_keys = json_decode($api->getMailjetKeys());
        if (isset ($mailjet_keys) && $mailjet_keys->meta->code == '200') {
            $this->mailjet = new \Mailjet\Client($mailjet_keys->data->mailjet_public_key, $mailjet_keys->data->mailjet_private_key);
        }
    }

    public function newCustomer($status, $data)
    {
        $name = htmlspecialchars($data->firstname) . ' ' . htmlspecialchars($data->lastname);
        $rec = App::getListManagement()->getFieldById($data->recurrence_id);
        $date = 'Le' . ParseStr::date_to_str($data->date) . ' à ' . htmlspecialchars($data->hour);
        $surface = htmlspecialchars($data->surface);
        $workTime = htmlspecialchars($data->workTime);
        $price = htmlspecialchars($data->price);
        $phone = htmlspecialchars($data->phone);
        $address = htmlspecialchars($data->address);
        $email = htmlspecialchars($data->email);
        $body = [
            'Recipients' => [['Email' => $_SERVER['HTTP_MAIL_CONTACT']]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "Dring ! Un nouveau client",
            'Text-part' => "
                Un nouveau client est arrivé, status du paiement: $status
                $name
                $rec
                $date
                $surface
                $price
                ",
            'Html-part' => "
                <h3>Une nouveau client est arrivé</h3>
                <h4>Status du paiement: $status</h4>
                <br/> $name
                <br/> $rec, $workTime par prestation
                <br/> $date
                <br/> $surface m²
                <br/> $price euros
                <br/> $phone
                <br/> $address
                <br/> $email
                "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }

    public function welcomeCustomer($recap, $subForm)
    {
        $date = ParseStr::date_to_str($recap->date);
        $email = $subForm->email;
        $body = [
            'Recipients' => [['Email' => "$email"]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "Bienvenue chez Nobo",
            'Text-part' => "
                Bonjour,
    
                Votre compte client vient d'être créé et toute l'équipe Nobo vous souhaite la bienvenue !
                Votre réservation pour le $date a bien été prise en compte.
                
                Un majordome vous contactera pour préciser les détails concernant l'accès à votre domicile dans les jours à venir.
                
                Vous souhaitant une agréable journée,
                Nobo.
            ",
            'Html-part' => "
                Bonjour,
                <br/><br/>
                Votre compte client vient d'être créé, Nobo vous souhaite la bienvenue !
                <br/><br/>
                Votre réservation pour le $date a bien été prise en compte.
                <br/><br/>
                Un gouvernant vous contactera pour préciser les détails concernant l'accès à votre domicile dans les jours à venir.
                <br/><br/>
                Vous souhaitant une agréable journée,
                <br/>
                Nobo.
            "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }

    public function welcomeCustomerWithSuspendedPayment($recap, $subForm)
    {
        $date = ParseStr::date_to_str($recap->date);
        $email = $subForm->email;
        $body = [
            'Recipients' => [['Email' => "$email"]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "Bienvenue chez Nobo",
            'Text-part' => "
                Bonjour,
    
                Votre compte client vient d'être créé et toute l'équipe Nobo vous souhaite la bienvenue !
                Votre demande a bien été prise en compte pour une prestation le $date.
                
                Un Guest Relation prendra contacte avec vous dans un délai de 24 heures afin de cerner vos 
                besoins et vous indiquer si nous pouvons honorer cette prestation à la date et à 
                l'heure indiquées ou auquel cas, vous placer en liste d'attente. 
                
                En vous souhaitant une agréable journée,
                Nobo
            ",
            'Html-part' => "
                Bonjour,
                <br/><br/>
                Votre compte client vient d'être créé, Nobo vous souhaite la bienvenue !
                <br/><br/>
                Votre demande a bien été prise en compte pour une prestation le $date.
                <br/><br/>
                Un Guest Relation prendra contact avec vous dans un délai de 24 heures afin de cerner vos 
                besoins et vous indiquer si nous pouvons honorer cette prestation à la date et à 
                l'heure indiquer ou auquel cas vous placer en liste d'attente.
                <br/><br/>
                Vous souhaitant une agréable journée,
                <br/>
                Nobo
            "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }

    public function adwordsReservation($data)
    {
        $date = htmlspecialchars($data['date']);
        $firstname = htmlspecialchars($data['firstname']);
        $lastname = htmlspecialchars($data['lastname']);
        $tel = htmlspecialchars($data['tel']);
        $message = htmlspecialchars($data['message']);
        $email = htmlspecialchars($data['email']);
        $cp = htmlspecialchars($data['cp']);
        $code_promotion = htmlspecialchars($data['code_promotion']);
        $ip = $data['ip'];
        $origine = $data['origine'];
        $body = [
            'Recipients' => [['Email' => $_SERVER['HTTP_MAIL_CONTACT']]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "Reservation adwords",
            'Text-part' => "
                code de promotion: $code_promotion,
                date: $date,
                firstname: $firstname,
                lastname: $lastname,
                tel: $tel,
                message: $message,
                email: $email,
                cp: $cp,
                ip: $ip,
                page adwords: $origine
            ",
            'Html-part' => "
                <h3>Client adword</h3>
                <br>code de promotion: $code_promotion,
                <br>date: $date,
                <br>firstname: $firstname,
                <br>lastname: $lastname,
                <br>tel: $tel,
                <br>message: $message,
                <br>email: $email,
                <br>cp: $cp,
                <br>ip: $ip, 
                <br>page adwords: $origine 
            "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }
}
