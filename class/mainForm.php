<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/09/2018
 * Time: 16:59
 */

class mainForm
{
    static function parse_form_address_check_to_api($data) {
        if (!isset($data['cleaning_time']) || !isset($data['ironing_time'])
            || !isset($data['recurrenceType']))
            return null;
        $reservation['cleaning_time'] = ParseStr::decimalHoursToTime($data['cleaning_time']);
        $reservation['ironing_time'] = ParseStr::decimalHoursToTime($data['ironing_time']);
        $reservation['punctual_type'] = isset($data['punctualType']) ? $data['punctualType'] : null;

        if ($data['recurrenceType'] === 'punctual')
            $reservation['recTime'] = 0;
        else if ($data['recurrentType'] !== 'multiple')
            $reservation['recTime'] = $data['recurrentType'];
        else if (isset($data['recTime']))
            $reservation['recTime'] = $data['recTime'];
        else
            return null;
        return $reservation;
    }

    static function parse_form_account($data) {
        $reservation = $data;
        $validator = new Validator($data);
        $validator->is_password('password', 'Le mot de pass est invalide');
        $validator->is_same('password', 'c_password', 'La confirmation du mot de passe a échoué');
        if (!$validator->is_valid())
            return null;
        $reservation['password'] = my_crypt::genHash($_POST['password']);
        unset($reservation['c_password']);

        return $reservation;
    }

    static function print_code($code) {
        if ($code !== 200) {
            http_response_code($code);
            exit();
        }
    }
}