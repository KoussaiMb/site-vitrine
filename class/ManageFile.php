<?php

class ManageFile
{
    private $url_folder;

    public function __construct($user_id)
    {
        $upload_folder = "/home/nobo/website/private/uploads";
        $fd = opendir($upload_folder);
        while (($entry = readdir($fd)) != false){
            if (in_array($entry, $user_id)){
                $this->url_folder = $upload_folder . '/' . $entry;
                break;
            }
        }
        closedir($fd);
    }
    public function getFolder(){
        return $this->url_folder;
    }
    public function getFile($object)
    {
        $fd = opendir($this->url_folder);
        while (($entry = readdir($fd)) != false){
            if (in_array($entry, $object))
                return ($this->url_folder . '/' . $object);
        }
        closedir($fd);
        return false;
    }
    public function getAllFile()
    {
        $arr = [];
        $fd = opendir($this->url_folder);
        while (($entry = readdir($fd)) != false){
            array_push($arr, $this->url_folder . '/' . $entry);
        }
        closedir($fd);
        return $arr;
    }
}