<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 06/12/2016
 * Time: 12:32
 */
class ParseStr
{
    static function DatetimeToDate($Datetime, $format = null){
        return substr($Datetime, 8, 2) . '/' . substr($Datetime, 5, 2) . '/' . substr($Datetime, 0, 4);
    }
    static function DatetimeToTime($Datetime, $format = null){
        return substr($Datetime, 11, 2) . 'h' . substr($Datetime, 14, 2);
    }
    static function GetIntFromFloat($float){
        return substr($float, 0, strpos($float, '.'));
    }
    static function GetDecFromFloat($float){
        return substr($float, strpos($float, '.') + 1);
    }
    static function after_trailing_slash($str) {
        if (strstr($str, '/'))
            return substr($str, strrpos($str, '/') + 1);
        return substr($str, strrpos($str, '\\') + 1);
    }
    static function DatetimeToBeginIn($Datetime){
        $now = new DateTime();
        $to = new DateTime($Datetime);
        return ($now->diff($to)->format(('%d jours %h heures et %i minutes')));
    }
    static function FloatToTime($float){
        $int = self::GetIntFromFloat($float);
        $dec = self::GetDecFromFloat($float) * 6;
        if ($dec)
        {
            if ($dec == 0)
                return $int . 'h';
            if ($dec < 10)
                return $int . 'h' . '0' . $dec;
        }
        return $int . 'h';
    }
    static function timeToTime($time){
        if ($time[0] == 0)
            return substr($time, 1, 1) . 'h' . substr($time, 3, 2);
        return substr($time, 0, 2) . 'h' . substr($time, 3, 2);
    }
    static function parseDateTime ($date, $time){
        $hour = $time . ":00";
        $test = explode('/', $date);
        return '20' . $test[2]. '-' . $test[1] . '-' . $test[0] . " ". $hour;
    }
    static function GenRate($rate, $StarNumber = 5)
    {
        $i = 0;
        $int = self::GetIntFromFloat($rate);
        $dec = self::GetDecFromFloat($rate);
        $dec = strlen($dec) == 1 ? $dec * 10 : $dec;
        while ($i++ < $int) {
            echo "<span class=\"fa fa-star rate-on\"></span>";
        }
        if ($dec >= 75) {
            echo "<span class=\"fa fa-star rate-on\"></span>";
        }
        elseif($dec >= 25)
            echo "<span class=\"fa fa-star rate-half\"></span>";
        while (++$i < $StarNumber)
            echo "<span class=\"fa fa-star rate-off\"></span>";
    }
    static function GenPhotoName($SourceName, $token){
        $now = (new DateTime())->format('YmdHis');
        $ext = substr(strrchr($SourceName,'.'), 0);
        return $token . $now .$ext;
    }
    static function date_to_str($date, $format = 'y-m-d'){
        $mois = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
        $year_pos = strpos($format, 'y') / 2;
        $month_pos = strpos($format, 'm') / 2;
        $day_pos = strpos($format, 'd') / 2;
        $symbol = $format[1];
        $date_temp = explode($symbol, $date);
        return $date_temp[$day_pos] . ' ' .  $mois[intval($date_temp[$month_pos])  - 1] . ' ' . $date_temp[$year_pos];
    }
    static function decimalHoursToTime($hours) {
        if ($hours < 86400)
            return gmdate("H:i:s", $hours * 3600);
        return null;
    }
}