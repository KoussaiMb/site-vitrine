<?php
/**
 * Created by PhpStorm.
 * User: remy.machado
 * Date: 9/21/2017
 * Time: 3:00 PM
 */

class ApiConnector
{
    // Request part
    protected $ch;
    protected $url;
    protected $httpMethod;
    protected $requestData;
    protected $requestHeaders;

    // Response part
    protected $headers = [];
    protected $httpCode;
    public $response;

    protected $possibleMethods = [
        'GET',
        'POST',
        'PUT',
        'DELETE'
    ];


    public function __construct($initIt = false,
                                $url = '',
                                $httpMethod = 'GET',
                                $requestData = null,
                                $requestHeaders = [])
    {
        if ($initIt) {
            $this->setUrl($url);
            $this->setHttpMethod($httpMethod);
            $this->setRequestData($requestData);
            $this->setRequestHeaders($requestHeaders);
            $this->initCurlHandle();
            $this->setCurlOptions();
        }
    }

    public function execCurlHandle()
    {
        $this->response = json_decode(curl_exec($this->ch));
        $this->httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    }

    public function closeCurlHandle()
    {
        curl_close($this->ch);
    }

    public function debugResp()
    {
        echo 'HTTP CODE : ' . $this->httpCode . '<br>';
        echo curl_error($this->ch) ? 'CURL ERROR : ' . curl_error($this->ch) . '<br>' : '';
        echo "<pre>";
        print_r($this->response);
        echo "</pre>";
    }

    public function execClose()
    {
        $this->execCurlHandle();
        $this->closeCurlHandle();
    }

    public function execDebugClose()
    {
        $this->execCurlHandle();
        $this->debugResp();
        $this->closeCurlHandle();
    }

    public function initCurlHandle()
    {
        if (!$this->url)
            throw new Exception('Undefined curl URL');
        $this->ch = curl_init($this->url);
    }

    // GETTERS
    public function getCH() {
        return ($this->ch);
    }

    public function getHttpMethod() {
        return ($this->httpMethod);
    }

    public function getRequestData() {
        return ($this->requestData);
    }

    public function getUrl()
    {
        return ($this->url);
    }

    public function getHttpCode()
    {
        return ($this->httpCode);
    }

    public function getResponse()
    {
        return ($this->response);
    }

    // SETTERS
    public function setHttpMethod($httpMethod) {
        if (!in_array(strtoupper($httpMethod), $this->possibleMethods))
            throw new Exception('Unauthorized request method');
        $this->httpMethod = $httpMethod;
    }

    public function setRequestData($requestData) {
        $this->requestData = $requestData;
    }

    public function setRequestHeaders($requestHeaders) {
        if (!$requestHeaders) {
            $this->requestHeaders = [];
        }

        // Is associative array ?
        if (function ($requestHeaders) {
            if (array() === $requestHeaders)
                return (false);
            return (array_keys($requestHeaders) !== range(0, count($requestHeaders) - 1));
        }) {
            $this->requestHeaders = $requestHeaders;
        } else {
            throw new Exception('Badly formed headers');
        }
    }

    public function setUrl($url)
    {
        if ($_SERVER['HTTP_STAGE'] != 'local')
            if (!is_string($url) ||
                !filter_var($url, FILTER_VALIDATE_URL))
                throw new Exception('Badly formed url : [' . $url . ']');
        $this->url = $url;
    }

    protected function setCurlOptions()
    {
        if (!$this->ch)
            throw new Exception('Undefined curl handle');
        if (!$this->httpMethod)
            throw new Exception('Undefined http method');

        // This function is called by curl for each header received
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION,
            function($curl, $header)
            {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $name = strtolower(trim($header[0]));
                if (!array_key_exists($name, $this->headers))
                    $this->headers[$name] = [trim($header[1])];
                else
                    $this->headers[$name][] = trim($header[1]);

                return $len;
            }
        );

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $this->httpMethod);

        if ($this->requestData)
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($this->requestData));
        if ($this->requestHeaders) {
            $formattedHeaders = [];
            foreach ($this->requestHeaders as  $key => $value) {
                array_push($formattedHeaders, ($key . ': ' . $value));
            }
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $formattedHeaders);
        }
    }
}