<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 06/11/2016
 * Time: 00:33
 */
class my_crypt
{
    static function genToken($len = 16){
        $len /= 2;
        return bin2hex(openssl_random_pseudo_bytes($len));
    }
    static function genStandardToken($who = null){
        if ($who !== null)
            $who = $who . '_';
        return $who . bin2hex(openssl_random_pseudo_bytes(10));
    }
    static function genHash($password, $cost = 12){
        $options = [
            'cost' => $cost,
        ];
        return password_hash($password, PASSWORD_DEFAULT, $options);
    }
}