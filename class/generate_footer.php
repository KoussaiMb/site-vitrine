<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 17/08/2018
 * Time: 16:42
 */

class generate_footer
{
    private $fonts = [];
    private $cssFiles = [];
    private $scriptFiles = [];
    private $css = '';
    private $script = '';

    function __construct()
    {
    }

    public function setCssFile($cssFile) {
        $this->cssFiles[] = $cssFile;
    }

    public function setScriptFile($scriptFile, $integrity = null, $crossorigin = null) {
        $this->scriptFiles[] = [
            'src' =>$scriptFile,
            'integrity' => $integrity,
            'crossorigin' => $crossorigin
        ];
    }

    public function setCss($css) {
        $this->css .= $css;
    }

    public function setScript($script) {
        $this->script .= $script;
    }

    public function setBasicFooter() {
        $this->setScriptFile("https://code.jquery.com/jquery-3.3.1.min.js",
            "sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=",
            'anonymous');
        $this->setScriptFile('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
            'sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q',
            'anonymous');
        $this->setScriptFile('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
            'sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl',
            'anonymous');
        $this->setScriptFile('/js/app.js');
    }

    public function setFont($font) {
        $this->fonts[] = $font;
    }

    public function printScriptFiles() {
        foreach ($this->scriptFiles as $scriptFile) {
            echo '<script type="text/javascript" src="'.$scriptFile['src'] .'"';
            if ($scriptFile['integrity'] !== null)
                echo ' integrity="'.$scriptFile['integrity'].'"';
            if ($scriptFile['crossorigin'] !== null)
                echo ' crossorigin="'.$scriptFile['crossorigin'].'"';
            echo '></script>';
        }
    }

    public function printCssFiles() {
        foreach ($this->cssFiles as $cssFile) {
            echo '<link rel="preload" href="'.$cssFile.'" as="style" onload="this.onload=null;this.rel=\'stylesheet\'"';
            if ($cssFile['integrity'] !== null)
                echo ' integrity="'.$cssFile['integrity'].'"';
            if ($cssFile['crossorigin'] !== null)
                echo ' crossorigin="'.$cssFile['crossorigin'].'"';
            echo ">";
            echo '<noscript>';
            echo '<link rel="stylesheet" href="'.$cssFile.'"';
            if ($cssFile['integrity'] !== null)
                echo ' integrity="'.$cssFile['integrity'].'"';
            if ($cssFile['crossorigin'] !== null)
                echo ' crossorigin="'.$cssFile['crossorigin'].'"';
            echo '</noscript>';
        }
    }

    public function generate()
    {
        echo '<footer>';
        $this->printCssFiles();
        $this->printScriptFiles();
        if (!empty($this->css))
            echo '<style>'.$this->css.'</style>';
        if (!empty($this->script))
            echo '<script>'.$this->script.'</script>';
        if (strtolower($_SERVER['HTTP_STAGE']) === 'prod') {
            echo "<script>";
            echo "(function(h,o,t,j,a,r){";
            echo "h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};";
            echo "h._hjSettings={hjid:666727,hjsv:6};";
            echo "a=o.getElementsByTagName('head')[0];";
            echo "r=o.createElement('script');r.async=1;";
            echo "r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;";
            echo "a.appendChild(r);";
            echo "})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');";
            echo "</script>";
        }
        echo '</footer>';
        echo '</body>';
        echo '</html>';
    }
}
