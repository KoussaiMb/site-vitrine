<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 13/12/2016
 * Time: 10:35
 */
class ListManagement
{
    private $db;

    public function __construct($db){
        $this->db = $db;
    }
    public function getList($listName){
        return $this->db->query("SELECT * FROM `list` WHERE `listName` = ? AND `deleted` = 0 ORDER BY `position`", [$listName])->fetchAll();
    }
    public function inList($listName, $fieldId){
        return $this->db->query("SELECT 1 as exist FROM `list` WHERE `listName` = ? AND `deleted` = 0 AND `id` = ?", [$listName, $fieldId])->fetch()->exist;
    }
    public function getFieldById($id){
        return $this->db->query("SELECT `field` FROM `list` WHERE `id` = ?", [$id])->fetch()->field;
    }
    public function getPositionById($id){
        return $this->db->query("SELECT `position` FROM `list` WHERE `id` = ?", [$id])->fetch()->position;
    }
    public function getIdByPosition($listName, $position){
        return $this->db->query("SELECT `id` FROM `list` WHERE `listName` = ? AND `position` = ? LIMIT 0,1", [$listName, $position])->fetch()->id;
    }
    public function getAllList(){
        return $this->db->query("SELECT `listName` FROM `list` WHERE `deleted` = 0 GROUP BY `listName`")->fetchAll();
    }
    public function addInput($listName, $field, $position = null){
        try {
            $nbRow = $this->db->query("SELECT COUNT(position) FROM `list` WHERE `listName` = ? AND `deleted` = 0", [$listName])->fetch();
            $nbRow = array_values((array)$nbRow)[0];
        }catch (PDOException $e){
            echo $e->getMessage();
            $nbRow = 0;
        }
        if ($position == null || $position && $position > $nbRow){
            $position = $nbRow + 1;
        }
        else{
            $this->upPosition($listName, $position, 1);
        }
        $this->db->query("INSERT INTO list(`position`, `listName`, `field`) values(?, ?, ?)", [$position, $listName, $field]);
        return $this->db->lastInsertId();
    }
    public function getDeletedList($listName){
        return $this->db->query("SELECT * FROM `list` WHERE `listName` = ? AND `deleted` = 1 ORDER BY `position`",[$listName])->fetchAll();
    }
    public function getAllDeletedList(){
        return $this->db->query("SELECT `listName` FROM `list` WHERE `deleted` = 0 GROUP BY `listName`")->fetchAll();
    }
    public function callbackInput($listName, $id, $position = null){
        if ($id) {
            if ($position){
                $this->db->query("UPDATE `list` SET `position` = ? WHERE `id` = ?", [$position, $id]);
            }
            else {
                $position = $this->db->query("SELECT `position` FROM `list` WHERE `id` = ?", [$id])->fetch()->position;
            }
            $this->upPosition($listName, $position, 1);
            $this->db->query("UPDATE `list` SET `deleted` = 0 WHERE `id` = ?", [$id]);
            return true;
       } else {
            return false;
        }
    }
    public function delForm($listName){
        try {
            $this->db->query("DELETE FROM `list` WHERE `listName` = ?", [$listName]);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }
    public function delInput($id){
        $record = $this->db->query("SELECT `position`, `listName` FROM `list` WHERE `id` = ?", [$id])->fetch();
        try {
            $nbRow = $this->db->query("SELECT COUNT(position) FROM `list` WHERE `listName` = ?", [$record->listName])->fetch();
            $nbRow = array_values((array)$nbRow)[0];
        }catch(PDOException $e){
            echo $e->getMessage();
            $nbRow = 0;
        }
        $this->db->query("UPDATE `list` SET `deleted` = 1 WHERE `id` = ?", [$id]);
        if ($record->position <= $nbRow){
            $this->upPosition($record->listName, $record->position + 1, -1);
        }
    }
    public function upInput($id, $listName, $field, $value, $position = null){
        $nbRow = $this->db->nbRow('list', $listName);
        if ($position && $position > $nbRow){
            $position = $nbRow + 1;
        }
        else{
            $this->upPosition($listName, $position, 1);
        }
        $this->db->query("UPDATE `list` SET `position` = ?, `listName` = ?, `field` = ?, `value` = ? WHERE `id` = ?", [$position, $listName, $field, $value, $id]);
    }
    private function upPosition($listName, $position, $sens){
        try {
            $this->db->query("UPDATE `list` SET `position` = `position` + ? WHERE `listName`= ? AND `position` >= ? AND `deleted` = 0", [$sens, $listName, $position]);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}