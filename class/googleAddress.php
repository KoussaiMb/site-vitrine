<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 31/08/2018
 * Time: 16:55
 */

class googleAddress
{
    private $address = '';
    private $city = '';
    private $postal_code = '';
    private $fullname = '';
    private $lat = '';
    private $lng = '';
    private $address_ext = '';
    private $country = '';
    private $valid = false;

    function __construct(array $arr)
    {
        if (isset($arr['street_number']))
            $this->address .= $arr['street_number'] . ' ';
        if (isset($arr['route']))
            $this->address .= $arr['route'];
        if (isset($arr['locality']))
            $this->city .= $arr['locality'];
        if (isset($arr['postal_code']))
            $this->postal_code .= $arr['postal_code'];
        $this->fullname .=
            $this->address. ', '. $this->postal_code. ', '. $this->city;
        if (isset($arr['administrative_area_level_1']))
            $this->address_ext .= $arr['administrative_area_level_1'];
        if (isset($arr['country']))
            $this->country .= $arr['country'];
        if (isset($arr['lng']))
            $this->lng .= $arr['lng'];
        if (isset($arr['lat']))
            $this->lat .= $arr['lat'];
        $this->valid = isset($arr['address_check']) ? $arr['address_check'] : null;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getFullAddress() {
        return $this->fullname;
    }

    public function getPostalCode() {
        return $this->postal_code;
    }

    public function getcity() {
        return $this->city;
    }

    public function getlat() {
        return $this->lat;
    }
    public function getlng() {
        return $this->lng;
    }

    public function is_valid() {
        return $this->valid;
    }

    public function get_all_information() {
        return [
            'address' => $this->address,
            'address_ext' => $this->address_ext,
            'city' => $this->city,
            'zipcode' => $this->postal_code,
            'country' => $this->country,
            'lat' => $this->lat,
            'lng' => $this->lng
        ];
    }
}