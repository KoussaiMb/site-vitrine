<?php
/**
 * Created by PhpStorm.
 * User: remy.machado
 * Date: 9/26/2017
 * Time: 10:52 AM
 */

class ApiCaller {

    static function addGuestApiTokenToSession($errorRedirectUrl, $redirectUrl = true) {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/becomeGuest',
            'POST',
            App::get_user_info()
        );
        $apiConnector->execClose();
        if ($apiConnector->getHttpCode() !== 200) {
            if ($redirectUrl)
                App::setFlashArrayAndRedirect('danger', $apiConnector->getResponse()->message, $errorRedirectUrl);
            else {
                return ($apiConnector);
            }
        }
        Session::getInstance()->write('apiToken', isset($apiConnector->getResponse()->data->token) ? $apiConnector->getResponse()->data->token  : null);
        return ($apiConnector);
    }

    static function isApiTokenValid($apiToken)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/isValid/apiToken',
            'POST',
            null,
            ['apiToken' => $apiToken]
        );
        $apiConnector->execClose();
        return ($apiConnector->getHttpCode() === 200);
    }

    static function retrievePromocode($code)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/promocodes/' . $code,
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function retrieveStripeKey($type)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/stripe/keys/'. $type,
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function retrieveSubscriber($idOrToken) {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/subscribers/' . $idOrToken,
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function retrieveUser($user_token) {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/users/' . $user_token,
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function getIdByRecurrencePosition($position) {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/lists/recurrence/' . $position,
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addAddress($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/address',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addChargeFromCustomerAccount($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/stripe/customers/charges',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addCustomer($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/customers',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addCustomerStripe($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/stripe/customers',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addNewsletter($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/newsletter',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addPromocodeLog($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/promocodes/log',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addSubscriberNeeds($data) {
        if (!isset($data['rec']))
            App::setFlashAndRedirect('danger',
                'Un problème est survenu, veuillez réessayer', 'besoin');
        $apiConnector = ApiCaller::getIdByRecurrencePosition($data['rec'] + 1);
        if ($apiConnector->getHttpCode() !== 200)
            App::setFlashArrayAndRedirect('danger',
                $apiConnector->getResponse()->message, 'besoin');

        $data['recurrence_id'] = $apiConnector->getResponse()->data->id;
        $data['workTime'] = self::calculWorktime($data['rec'], $data['surface']);
        $warranty = strtotime($data['workTime']) <= strtotime($data['workTimeChosen']);
        $data['price'] = self::calculPrice($data['rec'], $data['workTimeChosen']);

        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/subscribers',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        if ($apiConnector->getHttpCode() === 200)
            $apiConnector->response->data->warranty = $warranty;
        return ($apiConnector);
    }

    static function addUser($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/users',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addRegistration($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/registration',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addWaitingList($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/waitingList',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateAddressesFromUser($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/users/'. $data['user_token'] . '/addresses',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateRegistration($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/registration/' . $data['user_token'],
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateSubscriberInfo($data) {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/subscribers/' . $data['token'] . '/info',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateSubscriberNeeds($data) {
        if (!isset($data['rec']))
            App::setFlashArrayAndRedirect('danger',
                'Un problème est survenu, veuillez réessayer', 'besoin');
        $apiConnector = ApiCaller::getIdByRecurrencePosition($data['rec'] + 1);
        if ($apiConnector->getHttpCode() !== 200)
            App::setFlashArrayAndRedirect('danger',
                $apiConnector->getResponse()->message, 'info');

        $data['recurrence_id'] = $apiConnector->getResponse()->data->id;
        $data['workTime'] = self::calculWorktime($data['rec'], $data['surface']);
        $warranty = strtotime($data['workTime']) <= strtotime($data['workTimeChosen']);
        $data['price'] = self::calculPrice($data['rec'], $data['workTimeChosen']);

        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/subscribers/' . $data['token'] . '/needs',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        if ($apiConnector->getHttpCode() === 200)
            $apiConnector->response->data->warranty = $warranty;
        return ($apiConnector);
    }

    static function updateSubscriberPayed($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/' . $data['token'] . '/payed',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateUser($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/users/' . $data['user_token'],
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateUserPaymentAccount($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/users/' . $data['user_token'] . '/paymentAccount',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function mailJetAdwordsReservation($data)
    {
        $optionalParams = ['origine', 'firstname', 'lastname', 'recurrence', 'cp', 'tel', 'ip'];

        foreach ($optionalParams as $param) {
            if (empty($data[$param])) {
                $data[$param] = 'Non renseigné';
            }
        }
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/adwordsReservation',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function mailJetRecruitment($data)
    {
        $optionalParams = ['origine', 'fullname', 'email', 'tel', 'cv'];

        foreach ($optionalParams as $param) {
            if (empty($data[$param])) {
                $data[$param] = 'Non renseigné';
            }
        }
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/recruitment',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function mailJetNewCustomer($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/newCustomer',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function mailJetWelcomeCustomer($data)
    {
        $data['date'] = ParseStr::date_to_str($data['date']);

        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/welcomeCustomer',
            'POST',
            ['date' => $data['date'], 'email' => $data['email']],
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function mailJetWelcomeCustomerSuspendedPayment($data)
    {
        $data['date'] = ParseStr::date_to_str($data['date']);

        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/welcomeCustomerSuspendedPayment',
            'POST',
            ['date' => $data['date'], 'email' => $data['email'], 'firstname' => $data['firstname'], 'hour' => $data['hour']],
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function paymentFunctionChecker($apiConnector)
    {
        switch ($apiConnector->getHttpCode())
        {
            // Success
            case 200:
                break ;

            // apiToken expired
            case 401:
                App::setFlashAndRedirect('danger',
                    'Votre session a expiré, veuillez réessayer', 'paiement');
                break ;

            default :
                App::setFlashAndRedirect('danger',
                    'Un problème est survenu lors du paiement. Réessayez, ou contactez nous au' . App::parse_phone('.', $_SERVER['HTTP_PHONE_NOBO']), 'paiement');
                break ;
        }
    }

    static function calculWorktime ($rec, $surface){
        $ratioRec = [1, 1.1, 1.5, 1.7];

        if ($surface <= 20)
            $time = 90 + $surface;
        else if ($surface <= 40)
            $time = 90 + ($surface - 20) * 1.5;
        else
            $time = 120 + ($surface - 40);

        return self::timeToMH($time * $ratioRec[$rec]);
    }

    static function calculPrice($rec, $time){
        if ($rec == 3)
            $hourPrice = 32;
        else
            $hourPrice = 28;
        $arr = explode (':', $time);
        return (60 * $arr[0] + $arr[1]) * $hourPrice / 60;
    }

    static function timeToMH($time) {
        $h = floor($time / 60);
        $m = round($time - $h * 60);

        if ($m <= 0.1){
            $m = "00";
        } else if ( $m <= 30) {
            $m = 30;
        } else {
            $m = "00";
            $h += 1;
        }
        return $h . ":" . $m;
    }

    /*
     * Reservation v2
     */

    static function addReservation($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateReservation($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function isLeadExist()
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/exist',
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function isLeadNeedExist()
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/need/exist',
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function isLeadAppointmentExist()
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/appointment/exist',
            'GET',
            null,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateReservationNeed($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/need',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addReservationNeed($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/need',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function addReservationAppointment($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/appointment',
            'POST',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateReservationAppointment($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/appointment',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateReservationAccount($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/account',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }

    static function updateReservationAddressInfo($data)
    {
        $apiConnector = new ApiConnector(
            true,
            $_SERVER['HTTP_URL_API_NOBO'] . '/v1/reservation/address/info',
            'PUT',
            $data,
            ['apiToken' => Session::getInstance()->read('apiToken')]
        );
        $apiConnector->execClose();
        return ($apiConnector);
    }
}
