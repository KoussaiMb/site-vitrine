<?php
include_once ('inc/bootstrap.php');
if (Session::getInstance()->hasFlash())
    $notif = Session::getInstance()->getFlash();

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken'))) {
    ApiCaller::addGuestApiTokenToSession('/', false);
}

include_once('inc/header_start.php');
?>
<!-- title -->
<title>Nobo - Femme de menage haut de gamme - Paris</title>
<!-- Google -->
<meta name="description" content="Réserver en ligne du personnel issu de l’hôtellerie pour votre ménage et repassage à domicile. Service disponible à Paris et en proche banlieue. 50% de crédit d’impôts">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- facebook preview -->
<meta property="og:url"                 content="https://nobo.life"/>
<meta property="og:type"                content="website"/>
<meta property="og:title"               content="Nobo - l'hôtel à la maison"/>
<meta property="og:description"         content="Nobo est la seule plateforme à proposer les services de personnel hôtelier à domicile. Entretien 5 étoiles garanti à chaque passage."/>
<meta property="fb:app_id"              content="430915960574732"/>
<meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
<!-- crisp chat -->
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="a8c900dc-bab0-47ce-80b0-4ccf2402e9ee";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
<?php endif; ?>
<!-- CSS -->
<!--<link rel="stylesheet" href="/css/public_style.css" media="all">-->
<link rel="stylesheet" type="text/css" href="/css/public_style.css">
<link rel="preload" href="/css/lib/magnific-popup.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="/css/lib/magnific-popup.css"></noscript>
<link rel="preload" href="/css/lib/t-scroll.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="/css/lib/t-scroll.min.css"></noscript>
<link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"></noscript>
<?php include_once ('inc/header_end.php'); ?>
<?php include_once ('inc/navbar.php'); ?>
<?php include_once ('inc/navbar_phone.php'); ?>
    <div id="stage">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 intro-box text-center">
                    <div class="col-xs-12 nopadding-xs">
                        <img class="img-stage" src="/img/nobo/logo/nobo-logo-typo-blanc-hardcrop.png">
                        <p class="p-stage">L'hôtel à la maison</p>
                        <h1 class="h1-stage">Femme de ménage haut de gamme à domicile</h1>
                    </div>
                </div>
            </div>
            <!--
            <div class="col-xs-12 text-center">
                <div class="fonctionnement-button col-sm-4 col-sm-offset-4 flex-col flex-between">
                    <form class="flew-row flex-center">
                        <div class="input-group">
                            <input type="tel" class="form-control" placeholder="Téléphone" name="phone" id="phone">
                            <span class="input-group-btn">
                    <button class="btn btn-callback callback" type="button">Rappelez-moi !</button>
                </span>
                        </div>
                    </form>
                    <p class="spaceTopBottom">ou</p>
                </div>
            </div>
            -->
            <div class="col-xs-12 text-center">
                <h4>Vérifiez l'éligibilité</h4>
                <div class="col-xs-6 col-xs-offset-3">
                    <form action="/reservation/" method="post" id="check_address_form">
                        <input class="form-control" type="text" name="address" id="autocomplete" placeholder="Entrez votre adresse" style="box-shadow: 0 0 1em #27374D; border-radius: 0">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <section id="mission">
        <div class="container">
            <div class="row">
                <div class="section">
                    <div class="landing-wrapper">
                        <h2>Notre mission</h2>
                        <p class="hidden-xs">Moderniser le service de ménage et repassage de votre appartement</p>
                    </div>
                    <div class="col-xs-12 col-sm-10 col-xs-offset-0 col-sm-offset-1 pad-top text-center all-mission-wrap">
                        <div class="col-xs-12 col-sm-4 fadeUp mission-wrap">
                            <div class="col-xs-4 col-sm-12">
                                <img class="mission-img" src="/img/nobo/sticker/star-one-blue.svg" alt="star-one-blue">
                            </div>
                            <div class="col-xs-8 col-sm-12">
                                <h3 class="gold">Qualité</h3>
                                <p class="below-sticker valign">
                                    Du personnel d'exception, issu de l'hôtellerie de luxe, recruté en CDI
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 fadeUp mission-wrap">
                            <div class="col-xs-4 col-sm-12">
                                <img class="mission-img" src="/img/nobo/sticker/clock-blue-150.svg" alt="clock-blue">
                            </div>
                            <div class="col-xs-8 col-sm-12">
                                <h3 class="gold">Flexibilité</h3>
                                <p class="below-sticker valign">
                                    Facturé à l'heure, sans engagement et sans gestion administrative
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 fadeUp mission-wrap">
                            <div class="col-xs-4 col-sm-12">
                                <img class=" mission-img" src="/img/nobo/sticker/eye-blue.svg" alt="star-one-blue">
                            </div>
                            <div class="col-xs-8 col-sm-12">
                                <h3 class="gold">Suivi</h3>
                                <p class="below-sticker valign">
                                    Travail supervisé par un(e) gouvernant(e) personnel(le)
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="visible-xs fadeLeft" id="inter-link">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a href="/menage-a-domicile/fonctionnement" class="spaceTop btn-important">
                        Notre fonctionnement<span class="glyphicon glyphicon-chevron-right pull-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="section-transition section-transition-white"><div></div></div>

    <section class="section-dark" id="team">
        <div class="container">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Notre équipe</h2>
                    <p class="hidden-xs">Du directeur des opérations aux femmes de menage</p>
                </div>
                <div class="col-xs-12 pad-top team-wrap-all">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-4 fadeUp">
                        <div class="col-xs-12 team-box team-box-main text-center">
                            <div class="col-xs-6 col-sm-12">
                                <img class="img-circle img-gouv" src="/img/nobo/gallery/directeur-des-operations-paris.jpg" alt="photo-COO-Sebastien">
                            </div>
                            <div class="col-xs-6 col-sm-12 spaceTop">
                                <h4>Sébastien</h4>
                                <span class="nopadding-xs-left-right seperate-name">-</span>
                                <a class="hidden" href="https://fr.linkedin.com/in/sébastien-mouton-9308605a" target="_blank"><img src="/img/nobo/sticker/linkedin-logo-circle-gold.png" alt="linkedin-circle-logo"></a>
                                <p class="bold nomarge">Gouvernant général <a class="hidden" href="https://fr.linkedin.com/in/sébastien-mouton-9308605a" target="_blank"><img src="/img/nobo/sticker/linkedin-logo-circle-gold.png" alt="linkedin-circle-logo"></a></p>
                            </div>
                            <div class="col-xs-12 spaceTop">
                                <p class="underline hidden-xs">Expérience</p>
                                <p class="col-xs-6">
                                    George V<br>
                                    Seabourn Sojourn
                                </p>
                                <p class="col-xs-6">
                                    Royal Monceau<br>
                                    Le Meurice
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 team-fdc">
                        <div class="col-xs-12 text-center fadeIn">
                            <p class="team-subtitle">Quelques unes de nos femmes de ménage</p>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="1">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Soraya</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">200</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>George V</p>
                            </div>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="2">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Sylvie</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">30</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>Château de la tour</p>
                            </div>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="3">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Anita</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">150</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>Sofitel</p>
                            </div>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="4">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Camelia</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">80</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>Hôtel de Bourbon</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden-xs section-transition section-transition-blue" style="margin-bottom: -1em;"><div></div></div>

    <section id="pricing">
        <div class="container">
            <div class="row">
                <div class="section">
                    <div class="landing-wrapper">
                        <h2>Nos tarifs</h2>
                    </div>
                    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                        <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                            <div class="cnrflash">
                                <div class="cnrflash-inner">
                                    <span class="cnrflash-label">Populaire</span>
                                </div>
                            </div>
                            <div class="n_t-top bg-gold">
                                <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                            </div>
                            <div class="n_t-bottom n_t-bottom-wrap">
                                <p class="n_t-detail-price gold">28 €/h</p>
                                <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                            </div>
                        </div>
                        <div class="col-sm-1 hidden-xs"><p></p></div>
                        <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                            <div class="n_t-top bg-blue-light">
                                <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                            </div>
                            <div class="n_t-bottom n_t-bottom-wrap">
                                <p class="n_t-detail-price blue">32 €/h</p>
                                <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center spaceTop">
                        <a href="reservation/besoin"><button class="btn-outline-blue">réserver</button></a>
                        <p>ou nous <a href="/contact">contacter</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="visible-xs border-transition-blue"></div>
    <div class="hidden-xs section-transition section-transition-white"><div></div></div>

    <section class="hidden-xs section-dark" id="fonctionnement">
        <div class="container">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2 class="fix-h2-small">Notre fonctionnement</h2>
                </div>
                <div class="col-xs-12 pad-top">
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/reservation-gold-120.svg" alt="phone-call-gold">
                        </div>
                        <p class="col-xs-12">Réservation en ligne sécurisée</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/phone-call-gold.svg" alt="phone-call-gold">
                        </div>
                        <p class="col-xs-12">Appel d'un réceptionniste pour établir vos besoins</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/gouvernant-premiere-visite-gold.svg" alt="gouvernant-gold">
                        </div>
                        <p class="col-xs-12">Visite de votre gouvernant(e) pour créer votre planning d'entretien</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/house-cleaned-gold.svg" alt="phone-call-gold">
                        </div>
                        <p class="col-xs-12">Première prestation de votre femme de ménage</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden-xs section-transition section-transition-blue"><div></div></div>

    <section id="voisin">
        <div class="container-fluid">
            <div class="row">
                <div class="section">
                    <div class="landing-wrapper">
                        <h2>Ils nous ont fait confiance</h2>
                    </div>
                    <div class="col-xs-12 voisin-section">
                        <div id="feedback-carousel-sm" class="carousel slide hidden-xs" data-ride="carousel">
                            <div class="carousel-inner feedback-carousel">
                                <div class="item item-space active">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                            "Toujours aussi bien après plusieurs mois de prestations,
                                            ce qui est très rare malheureusement.
                                            Je recommande pour leur sérieux et l’attention portée aux clients."
                                            </p>
                                            <p>Margaux, Convention, <strong>Paris 15</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo de Margaux cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-avis-margaux.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Nobo, c'est un service à la carte et on ne peut plus professionnel. Eric et son équipe sont formidables et mettront tout en œuvre pour vous satisfaire. L'hôtel à la maison, une promesse tenue!!"</p>
                                            <p>Marc, Ternes, <strong>Paris 17</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Marc client femme de menage et repassage paris 17" src="/img/paris/avis/femme-de-menage-paris-17-avis-marc.jpg" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item item-space">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Super service, le ménage est parfait et les petites attentions font qu’on
                                            se sent vraiment traité comme à l’hôtel, c’est top bravo!"</p>
                                            <p>Emmanuelle, Beaugrenelle, <strong>Paris 15</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo d'Emmanuelle cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-avis-emmanuelle.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Un service super innovant et une équipe très à l'écoute en cas de problème. On est fan de la première heure !"</p>
                                            <p>Justine, Saint-Augustin, <strong>Paris 8</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Justine cliente femme de menage Paris 8" src="/img/paris/avis/femme-de-menage-paris-8-avis-justine.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item item-space">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Un service de très grande qualité. J'ai pu tisser de vrais liens de confiance avec l'équipe et les recommande chaleureusement."</p>
                                            <p>Danièle, Jasmin, <strong>Paris 16</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Daniele cliente menage repassage paris 16" src="/img/paris/avis/femme-de-menage-paris-16-avis-daniele.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Bluffé par la qualité du ménage ! Digne d'un grand hôtel
                                            Hâte d'essayer leurs autres service !!"</p>
                                            <p>Jonathan, La Muette, <strong>Paris 16</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo de Jonathan client ménage nobo" src="/img/paris/avis/femme-de-menage-paris-16-avis-jonathan.jpg" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item item-space">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"J'utilise Nobo 4h par semaine depuis 1 an. J'ai pu garder la même <strong>femme de menage</strong> depuis le début et le travail est nickel."</p>
                                            <p>Carole, Villiers, <strong>Paris 17</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Carole cliente femme de ménage nobo paris 17" src="/img/paris/avis/femme-de-menage-paris-17-avis-carole.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                                "Très bon service, facilement joignable
                                                au téléphone + un réel suivi. Je recommande !"
                                            </p>
                                            <p>Xavier, Sèvres-Lecourbes, <strong>Paris 15</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Xavier client menage paris 15" src="/img/paris/avis/femme-de-menage-paris-avis-xavier.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a data-slide="prev" href="#feedback-carousel-sm" class="left carousel-control"><i class="glyphicon glyphicon-chevron-left"></i></a>
                            <a data-slide="next" href="#feedback-carousel-sm" class="right carousel-control"><i class="glyphicon glyphicon-chevron-right"></i></a>
                        </div>
                        <div id="feedback-carousel-xs" class="carousel slide visible-xs" data-ride="carousel">
                            <div class="carousel-inner feedback-carousel">
                                <div class="item active">
                                    <div class="voisin-wrap col-xs-12">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Le bonheur de rentrer chez soi.
                                                Tout est dans le détail et Nobo l'a bien compris !"</p>
                                            <p>Aurélia D.S. – Cliente depuis Aout 2016</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo-profil-avis-client-aureliaDS" src="/img/nobo/misc/avis-client-aureliaDS.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="voisin-wrap col-xs-12">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                                "Le service est de qualité et sur mesure. Il n'y a
                                                rien à ajouter, c'est parfait ! Merci Nobo."
                                            </p>
                                            <p>Alice R. – Cliente depuis Janvier 2017</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img src="/img/nobo/misc/avis-client-aliceR.png" alt="photo-profil-avis-client-aliceR" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-xs-12 voisin-wrap">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                                "Service impeccable. L'hôtel à la maison c'est
                                                vraiment le pied ! Je le recommande !"
                                            </p>
                                            <p>Hannah V. – Cliente depuis Décembre 2016</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo-profil-avis-client-hannahV" src="/img/nobo/misc/avis-client-hannahV.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-xs-12 voisin-wrap">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>Service excellent, de qualité et petite surprise à notre retour !</p>
                                            <p>Tino I. – Client depuis Septembre 2016</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo-profil-avis-client-tinoI" src="/img/nobo/misc/avis-client-tinoI.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a data-slide="prev" href="#feedback-carousel-xs" class="left carousel-control"><i class="glyphicon glyphicon-chevron-left"></i></a>
                            <a data-slide="next" href="#feedback-carousel-xs" class="right carousel-control"><i class="glyphicon glyphicon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-dark" id="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 section">
                    <div class="landing-wrapper landing-wrapper-dark">
                        <h2>tenez-vous informé</h2>
                        <p>Inscrivez-vous à notre newsletter !</p>
                        <form class="form-inline" id="form-newsletter">
                            <input type="text" class="form-control input-lg" name="name" placeholder="Nom, Prénom" id="name"/>
                            <input type="email" class="form-control input-lg" name="email" placeholder="Email" id="email"/>
                            <button type="button" class="btn btn-lg btn-gold" id="newsletterValidation">ENVOYER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include_once ('inc/footer_start.php'); ?>
<?php include_once ('inc/exit_popup.php'); ?>
<?php include_once('inc/analyticstracking.php'); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= $_SERVER['HTTP_GOOGLE_API']; ?>&libraries=places,geometry
&callback=initAutocomplete"
        async defer></script>
<script src="/js/lib/t-scroll.min.js"></script>
<script src="/js/autocomplete_address.js"></script>
<script>
    $(document).ready(function () {
        // API call newsletter
        $('#newsletterValidation').on('click', function () {
            var name = $('#name');
            var email = $('#email');

            $.ajax({
                beforeSend: function(request) {
                    var apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/newsletters'; ?>",
                type: 'POST',
                data: {name: name.val(), email: email.val()},
                dataType: 'json',
                success: function (ans) {
                    var messages = ans.message;

                    messages.forEach(function (message) {
                        $.notify({message: message},{type: 'success'});
                    });
                    name.val("");
                    email.val("");
                    Cookies.set("noboExitNewsletterPopUp", "1", 3);
                },
                error: function (jqXHR) {
                    var messages = $.parseJSON(jqXHR.responseText).message;

                    messages.forEach(function (message) {
                        $.notify({message: message},{type: 'danger'});
                    });
                }
            });
        });

        var notifs = <?= empty($notif) ? json_encode(null) : json_encode($notif); ?>;

        if (notifs) {
            var keys = Object.keys(notifs);
            var values = Object.values(notifs);

            for (var i = 0, len = keys.length; i < len; i++) {
                $.notify({message: values[i]},{type: keys[i]});
            }
        }
        $('#feedback-carousel-sm').carousel({
            pause: 'hover',
            interval: 10000
        });
        $('#feedback-carousel-xs').carousel({
            pause: 'hover',
            interval: 10000
        });

        //Listeners
        $("#scroller").click(function() {
            $('html,body').animate({scrollTop: $("#mission").offset().top}, 'slow');
        });
        Tu.tScroll({'t-element': '.fadeUp'});
        Tu.tScroll({'t-element': '.fadeIn'});
        Tu.tScroll({'t-element': '.fadeLeft'});
        Tu.tScroll({'t-element': '.fadeDown'});

        // API call "call me back"
        $('.callback').on('click', function () {
            $.ajax({
                beforeSend: function(request) {
                    let apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/callback'; ?>",
                type: 'POST',
                data: {phone: $('#phone').val()},
                dataType: 'json',
                success: function (ans) {
                    let messages = ans.message;
                    messages.forEach(function (message) {notify(message, 'success');});
                    $('#phone').val("");
                    Cookies.set("noboExitPhonePopUp", "1", 3);
                },
                error: function (jqXHR) {
                    let messages = $.parseJSON(jqXHR.responseText).message;
                    messages.forEach(function (message) {notify(message, 'danger');});
                }
            });
        });
    });
</script>
<?php include_once ('inc/footer_end.php'); ?>

