<?php include_once('inc/bootstrap.php'); ?>
<!doctype html>
<html>
<head>
    <?php if($_SERVER['HTTP_STAGE'] != 'prod'): ?>
        <meta name="robots" content="noindex, nofollow">
    <?php endif; ?>
    <!-- encodage -->
    <meta charset="utf-8" lang="fr">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- mobile -->
    <meta name="viewport" content="width=device-width, shrink-to-fit=no">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=0.5, maximum-scale=3.0">
    <!-- compatibility -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- font -->
    <link rel="preload" href="https://fonts.googleapis.com/css?family=Cardo|Josefin+Slab|Nunito" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cardo|Josefin+Slab|Nunito"></noscript>
    <!-- CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="preload" href="/css/bootstrap-theme.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="preload" href="/css/localization_style.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="preload" href="/css/nobo_app.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="preload" href="/css/nobo_navbar.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="/css/bootstrap-theme.min.css"></noscript>
    <noscript><link rel="stylesheet" href="/css/localization_style.css"></noscript>
    <noscript><link rel="stylesheet" href="/css/nobo_app.min.css"></noscript>
    <noscript><link rel="stylesheet" href="/css/nobo_navbar.min.css"></noscript>
    <!-- facebook preview -->
<meta property="og:url"                 content="https://nobo.life/emploi-femme-de-menage/"/>
<meta property="og:type"                content="website"/>
<meta property="og:title"               content="Emploi femme de ménage - CDI"/>
<meta property="og:description"         content="Vous cherchez un emploi de femme de ménage à Paris ? Un emploi en CDI comme aide ménagère vous attend dès maintenant !"/>
<meta property="fb:app_id"              content="430915960574732"/>
<meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
<!-- Google -->
<meta name="description" content="Vous cherchez un emploi de femme de ménage à Paris ? Un emploi en CDI comme aide ménagère vous attend dès maintenant !">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Emploi femme de ménage - CDI</title>
<!-- CSS -->
<?php include_once('inc/header_end.php'); ?>
<?php include_once('inc/navbar_recruit.php'); ?>
<?php include_once('inc/navbar_phone.php'); ?>
<style>
    .numberAd {
        padding-right: 100px!important;
    }
    .numberAd span {
        color: #27374D;
        text-shadow: none;
        padding: 5px;
        font-size: 1.4em;
    }
    #navbar-fixed  {
        margin: 0!important;
        background-color: rgba(255, 123, 106, 0.7);
    }
    #ad-paris-15 {
        padding-top: 70px;
        background-color: #122b40;
        height: 70%;
    }
    #ad-paris {
        padding-top: 70px;
        height: 80%;
        width: 100%;
        background: url('/img/paris/ad/appartement-paris-menage-nobo.jpg') center center no-repeat;
        min-width: 100%;
        background-size: cover;
    }
    .background-black {
        background-color: #27374d;
        background-color: rgba(0, 0, 0, 0.62);
        border-radius: 2px;
        padding-bottom: 10px;
    }
    a.navbar-brand {
        padding: 0 0 0 10px;
    }
    .integration-photo-header > img,
    .integration-photo-header-80 > img {
        height: 360px;
    }
    .integration-photo-header {
        position: absolute;
        bottom: 30%;
        left: 7%;
    }
    .integration-photo-header-80 {
        position: absolute;
        bottom: 20%;
        left: 7%;
    }
    .ad-title {
        font-size: 1.7em;
        font-weight: bold;
        color: #eeeeee;
    }
    .navbar-fixed-top li a:hover:after {
        background: #27374D;
    }
    .navbar {
        border-bottom: none;
    }
    .btn-ad-reservation {
        box-shadow: none;
        background-color: inherit;
        border: none;
        color: #FFF;
        font-size: 1.2em;
        border-bottom: 1px solid white;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    .btn-ad-reservation:hover {
        font-style: italic;
        border-color: rgb(255, 123, 106);
    }
    .italic {
        font-style: italic;
    }
    .nav .navbar-text {
        color: #27374D;
    }
    @media only screen and (max-width: 767px) {
        #ad-paris-15 {
            height: 90%;
            padding-top: 70px!important;
        }
        #ad-paris {
            height: 90%;
        }
        .btn-stage {
            padding: 8px;
        }
        a.navbar-brand {
            transform: translateX(-50%);
            left: 50%;
            position: relative;
        }
    }
    @media only screen and (min-width: 768px) {
        .ad-form-padding {
            padding-left: 60px;
            padding-right: 60px;
        }
        .navbar.navbar-transparent p {
            padding-top: 2px;
            padding-bottom: 2px;
        }
        .navbar-nav p {
            font-size: 1.4em;
            padding-top: 11px;
            padding-bottom: 11px;
            transition: all 0.3s;
        }
        .ad-stage-wrapper {
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-justify-content: space-around;
            /*-ms-flex-pack: distribute;*/
            justify-content: space-around;
            -webkit-align-content: stretch;
            -ms-flex-line-pack: stretch;
            align-content: stretch;
            -webkit-align-items: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
        }
        .nopadding-right {
            padding-right: 0;
        }
        .nopadding-right>input {
            text-align: right;
        }
        .nopadding-left {
            padding-left: 0;
        }
    }
    @media only screen and (max-width: 1500px) {
        .hidden-1500 {
            display: none;
        }
    }
    .pink-banner {
        background-color: rgba(255, 123, 106, 0.7);
        text-align: center;
    }
    .banner-announcement {
        border: 1px rgba(0, 0, 0, 0.2) dashed;
        background-color: rgba(255, 255, 255, 0.2);
        border-radius: 5px;
    }
    .logo-recruitment {
        padding-right: 10px
    }
    .info-recruitment {
        margin-top: 2.5em;
    }
    .info-recruitment {
        margin-top: 2.5em;
    }
    .info-recruitment img{
        height: 32px;
        width: 32px;
    }
    .info-recruitment p{
        font-size: 0.8em;
    }
</style>
<section id="ad-paris" class="flex-col flex-center-stretch">
    <div class="integration-photo-header-80 hidden-1500">
        <img src="/img/nobo/gallery/soraya-femme-de-chambre-avec-logo.png" alt="femme de chambre de chez nobo">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 ad-form-padding">
                <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 background-black">
                    <div class="text-center">
                        <h1 class="ad-title">
                            Emploi femme de ménage
                        </h1>
                    </div>
                    <div class="col-xs-12 text-center">
                        <p class="center form-title hidden-xs">
                            Vous cherchez des heures de ménage ? Postulez pour un CDI de femme de ménage chez Nobo !
                        </p>
                        <a class="typeform-share button btn-stage" href="https://nobo.typeform.com/to/QLJs49" data-mode="popup" data-hide-headers=true data-hide-footer=true data-submit-close-delay="2" target="_blank">
                            Je postule pour cet emploi
                        </a>
                        <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
                    </div>
                    <!--
                    <form name="recruitment" id="recruitment" action="/reservation/recruitment" method="post">
                        <input type="hidden" name="origine" value="<?= htmlspecialchars($_SERVER['REQUEST_URI']); ?>">
                        <div class="col-xs-12">
                            <p class="center form-title hidden-xs">
                                en <span class="gold bold">CDI</span> 35h !
                            </p>
                            <div class="form-group">
                                <input type="text" name="fullname" class="form-control input-sm text-center" placeholder="Nom, prénom" id="fullname">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 nopadding-left nopadding-xs">
                                <input type="tel" name="tel" class="form-control input-sm" placeholder="Téléphone" id="tel" required>
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 nopadding-right nopadding-xs">
                                <input type="email" name="email" class="form-control input-sm" placeholder="Email" id="email" required>
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        Votre CV&hellip;
                                        <input type="file" name="cv" style="display: none;" multiple>
                                    </span>
                                </label>
                                <input type="text" class="form-control" readonly>
                            </div>
                            <span class="help-block"></span>
                        </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 text-center">
                                <div id="success"></div>
                                <button id="apply" type="submit" class="btn-stage" name="action" value="post" style="margin-top: 0!important;">Postulez chez Nobo</button>
                            </div>
                        </div>
                    </form>
                    -->
                    <!--
                    <div class="typeform-widget" data-url="https://nobo.typeform.com/to/XzMRYH" data-transparency="30" style="width: 100%; height: 300px;"></div>
                    <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
                    -->
                </div>
            </div>
        </div>
    </div>
</section>

<div>
    <?php include_once('inc/breadcrumbs.php'); ?>
</div>

<section id="">
    <div class="container">
        <div class="row">
            <div class="recrutement-container">
                <div class="col-xs-12 col-sm-3 info-recruitment">
                    <h3>informations</h3>
                    <p><img class="logo-recruitment" src="/img/nobo/misc/recruitment/salaire.svg" alt="salaire monnaie"><strong>Salaire</strong> Hôtel 4, 5 étoiles</p>
                    <p><img class="logo-recruitment" src="/img/nobo/misc/recruitment/contrat-emploi.svg" alt="contrat emploi"><strong>CDI</strong> à temps plein</p>
                    <p><img class="logo-recruitment" src="/img/nobo/misc/recruitment/horloge-heure-de-menage.svg" alt="horloge heure de menage">Du Lundi au Vendredi de 9h à 17h</p>
                    <p><img class="logo-recruitment" src="/img/nobo/misc/recruitment/ballon-de-plage.svg" alt="ballon de plage">5 semaines de <strong>vacances</strong> par an</p>
                    <p><img class="logo-recruitment" src="/img/nobo/misc/recruitment/equipe.svg" alt="travail equipe">Travail en <strong>équipe</strong></p>
                    <p><img class="logo-recruitment" src="/img/nobo/misc/recruitment/pin-map.svg" alt="pin map">Paris</p>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <h2 class="h2-seo">Poste de femme / homme de ménage en CDI à Paris</h2>
                    <p>
                        Avec Nobo, obtenez enfin un vrai <strong>contrat de travail</strong> pour exercer votre métier de femme / homme de ménage à Paris.
                    </p>
                    <p>
                        Nobo vous offre un <strong>emploi</strong> en tant qu’aide ménagère dans des appartements Parisiens. Nous proposons un <span class="bold">service de luxe</span>, comme dans un hôtel 5 étoiles. Pour cela nous recrutons uniquement des hommes et femmes de ménage avec une <strong>expérience</strong> en tant que valet ou femme de chambre dans l’hôtellerie.
                    </p>
                    <p>
                        Vous n’aurez que <span class="bold">deux prestations</span> de ménage ou repassage <span class="bold">par jour</span>. Vous travaillerez du Lundi au Vendredi de 9h à 17h. Exceptionnellement, vous travaillerez le samedi matin si des heures de ménage sont décalées.
                    </p>
                    <p>
                        Votre <strong>travail</strong> inclura le ménage, le repassage, le dépoussiérage, le lavage du linge, le changement des draps et le rangement de l’appartement. Nous avons besoin de personnes expérimentées dans <strong>l’entretien d’un domicile</strong>, capables de prendre des initiatives pour fournir un travail de qualité (exemples: pas de poussière au-dessus des cadres, pas de trace sur les miroirs, pas de calcaire sur les robinets, bien essuyer les plinthes, etc…).
                    </p>
                    <p>
                        Vous travaillerez dans une petite équipe de 15 collègues avec qui vous passerez des moments privilégiés (gouters en équipe, formations, mission de ménage à plusieurs). Vous ne serez pas seul dans cet emploi !
                    </p>
                    <p>
                        Nobo a développé une <span class="bold">application mobile</span> sur laquelle vous verrez vos heures de ménage pour la semaine. Vous aurez le détail de chaque prestation et les consignes de vos clients (ménage seul, ménage et repassage, détails sur l’entretien). Enfin, l’application vous donnera le chemin d’un client à l’autre pour ne pas vous perdre !
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section pink-banner">
    <div class="container">
        <div class="row">
            <h3 class="bold" style="font-size: 1.2em">vous souhaitez gagner plus ?</h3>
            <p class="banner-announcement">
                Recommandez nous un ami, <span class="bold">nous vous offrons 100€</span> en cas de recrutement* !
            </p>
            <p>
                Envoyez nous simplement un email à
                <a href="mailto:recrutement@nobo.life?Subject=Je%20recommande%20pour%20Nobo" target="">recrutement@nobo.life</a>
                avec le nom, prénom et numéro de téléphone de vous et votre ami !
            </p>
            <p class="sub-detail">
                *une fois la période d’essai passée
            </p>
        </div>
    </div>
</section>
<!-- autres villes -->
<?php include_once ('inc/footer_start.php'); ?>
<?php include_once('inc/analyticstracking.php'); ?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>
<script>
    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
        let input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

            let input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        });
    });
</script>
</body>
</html>