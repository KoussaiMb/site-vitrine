<?php http_response_code(410); ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>410 - Page supprimée</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
</head>
<body>
<div class="col-xs-12 text-center">
    <div class="col-xs-2 col-xs-offset-5 text-center">
        <img class="SpaceTop img-responsive" src="/img/nobo/logo/nobo-logo-paris-blue-darker.png" alt="logo de nobo">
    </div>
    <div class="col-xs-12">
        <h1>Perdu ?</h1>
        <p>La page que vous recherchez n'existe plus</p>
        <a href="/" class="btn btn-primary">
            Page d'accueil
        </a>
        -
        <a href="javascript:history.back()" class="btn btn-default">
            Retour en arrière
        </a>
    </div>
</div>
</body>
</html>
<?php exit(); ?>