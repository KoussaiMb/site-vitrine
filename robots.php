<?php
include_once ('inc/bootstrap.php');
$url = $_SERVER['REQUEST_URI'];

if (preg_match("/robots$/", $url))
    include('error/404.php');

$stage = (strtolower($_SERVER['HTTP_STAGE']));
?>
<?php if ($stage == 'prod'): ?>
    User-Agent: *
    Disallow: /service-a-domicile
    Disallow: /scripts
    Disallow: /satisfaction.php
    Disallow: /satisfaction
    Disallow: /partenaires
    Disallow: /mon-espace
    Disallow: /ni
<?php else: ?>
    User-Agent: *
    Disallow: /
<?php endif; ?>