<?php include_once ('inc/bootstrap.php'); ?>
<?php include_once('inc/header_start.php'); ?>

<!-- facebook preview -->
<meta property="og:url"                 content="https://nobo.life/contact"/>
<meta property="og:type"                content="website"/>
<meta property="og:title"               content="Nobo - Nous contacter"/>
<meta property="og:description"         content="Nobo est la seule plateforme à proposer les services de personnel hôtelier à domicile. Entretien 5 étoiles garanti à chaque passage."/>
<meta property="fb:app_id"              content="430915960574732"/>
<meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
<!-- Google -->
<meta name="description" content="Ménage haut de gamme pour appartement exigeant.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Nobo - Nous contacter</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/public_style.css">

<?php include_once('inc/header_end.php'); ?>
<?php include_once ('inc/navbar.php'); ?>

    <section class="section-dark">
        <div class="container">
            <div class="col-xs-12 contact-header">
                <h1 class="gold">Nous contacer</h1>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 contact-content">
                <div class="col-xs-12 contact-stickers">
                    <div class="col-xs-6 col-md-3">
                        <a href="tel:<?= $_SERVER['HTTP_PHONE_NOBO'] ?>"><img src="img/nobo/sticker/contacter-nobo-phone-telephone-numero.png" alt="sticker-telephone-numero-nobo"></a>
                        <p><?= App::parse_phone('.', $_SERVER['HTTP_PHONE_NOBO']) ?></p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <a href="mailto:contact@nobo.life"><img src="img/nobo/sticker/contacter-email-nobo-paris.png" alt="sticker-email-nobo"></a>
                        <p>contact@nobo.life</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <a href="http://www.instagram.com/nobofrance"><img src="img/nobo/sticker/contacter-instagram-nobo-paris.png" alt="sticker-instagram-nobo"></a>
                        <p>#nobofrance</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <a href="http://www.facebook.com/nobofrance"><img src="img/nobo/sticker/contacter-facebook-nobo-paris.png" alt="sticker-facebook-nobo"></a>
                        <p>@nobofrance</p>
                    </div>
                </div>
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="contact-hr"></div>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 ask-content">
                    <form id="ask-form">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label for="firstname"></label>
                                <input type="text" class="form-control input-lg" name="firstname" id="firstname" placeholder="Prénom" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label for="lastname"></label>
                                <input type="text" class="form-control input-lg" name="lastname" id="lastname" placeholder="Nom" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label for="phone"></label>
                                <input type="tel" class="form-control input-lg" name="phone" id="phone" placeholder="Téléphone" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label for="email"></label>
                                <input type="email" class="form-control input-lg" name="email" id="email" placeholder="Email" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <label for="message"></label>
                                <textarea class="form-control input-lg" form="ask-form" maxlength="200" rows="4" name="message" id="message" placeholder="Message" required style="resize: none"></textarea>
                            </div>
                        </div>
                        <button type="button" class="btn btn-gold" id="contactValidation">envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include_once('inc/footer_start.php'); ?>
    <script>
        // API call contact
        $('#contactValidation').on('click', function () {
            $.ajax({
                beforeSend: function(request) {
                    var apiToken = "<?php echo Session::getInstance()->read('apiToken'); ; ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/contact'; ?>",
                type: 'POST',
                data: {
                    subject: 'Quelqu\'un nous contacte!',
                    firstname: $('#firstname').val(),
                    lastname: $('#lastname').val(),
                    email: $('#email').val(),
                    phone: $('#phone').val(),
                    message: $('#message').val()
                },
                dataType: 'json',
                success: function (ans) {
                    var messages = ans.message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'success'
                        });
                    });
                    $('#firstname').val("");
                    $('#lastname').val("");
                    $('#email').val("");
                    $('#phone').val("");
                    $('#message').val("");
                },
                error: function (jqXHR) {
                    var messages = $.parseJSON(jqXHR.responseText).message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'danger'
                        });
                    });
                }
            });
        });

    </script>

<?php include_once('inc/analyticstracking.php'); ?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>
<?php include_once('inc/footer_end.php'); ?>