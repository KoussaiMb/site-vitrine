<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/Paris-2-eme-75002"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Réservez une aide ménagère qualifiée en un clic dans le 2 ème arrondissement"/>
<meta property="og:description"     content="Avec Nobo, réservez un homme ou une femme de ménage issue de l'hôtellerie de luxe, dans le 2 ème arrondissement."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Réservez en ligne une aide ménagère issue de l'hôtellerie, dans le 2 ème arrondissement. Service haut de gamme | sans engagement | crédit d'impôts de 50%">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 2 ème arrondissement de Paris (75002) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-2eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 2ème <br>arrondissement de Paris (75002)</h1>
            <p class="hidden-xs color-grey">Le nettoyage c'est notre travail, plus le votre ! !</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Une femme de ménage ayant travaillé à l'hôtel, <br>pour votre ménage à domicile dans le 2ème à Paris !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Qualité</h3>
                        <p class="nomarge">
                            Avec Nobo réservez en ligne une <strong>femme de ménage à Paris 2</strong>.
                            Toutes nos aides ménagères sont issus de l'hôtellerie 4 et 5 étoiles afin d'assurer 
                            un travail de ménage à domicile qui répondra enfin à vos besoins.

                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Sur mesure</h3>
                        <p class="nomarge">
                            N'épluchez plus les petites annonces pour trouver la <strong>perle rare</strong> 
                            pour les services d'entretien de votre maison, 
                            <a class="link-gold underline" href="/reservation/besoin"> réservez </a>
                            en ligne en quelques clics une femme de ménage qui s'occupera du nettoyage de 
                            votre domicile avec les mêmes standards de qualité qu'à l'hôtel.
                            Convaincus de la qualité de notre service de ménage, réservez des heures 
                            <span class="bold">sans engagement</span> ! Alors si vous cherchez de <strong>l'aide
                            pour entretenir votre domicile dans le 1er arrondissement</strong>, optez pour Nobo ! 
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Une aide à 360° !..</h3>
                        <p class="nomarge">
                            En plus de vous apporter une <strong>femme de ménage</strong> compétente,
                            Nobo vous propose d'autres services à domicile: livraison
                            de fleurs pour égayer votre appartement, service de pressing ou
                            de cordonnerie à domicile, nous vous aidons à vous <strong>débarrasser de toutes 
                            les tâches ménagères</strong>. Nobo dépoussière le secteur avec une offre tout en un !
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservez votre service d'entretien en quelques clics</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.Nous vous appelons pour comprendre vos besoins</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Un(e) gouvernant(e) vient visiter votre domicile et créer votre planning d'entretien</p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Votre femme de ménage réalise une prestation de nettoyage test</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Vous avez des questions sur nos services d'entretien ? Rendez-vous sur notre rubrique
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Ménage à domicile - Paris 2 ème (75002)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans votre quartier</h3>
                <p class="text-justify">
                    Tout le quartier du <strong>2ème arrondissement de Paris</strong> est couvert par notre service d'entretien. 
                    D'<strong>Opéra</strong> au <strong>Musée des Arts & Métiers</strong> en passant 
                    par le <strong>Boulevard Hausmann</strong> qui connecte <strong>Richelieu-Drouot</strong>, 
                    <strong>Le Grand Rex</strong>, <strong>Bonne nouvelle</strong> et le <strong>Musée Grévin</strong>. 
                    On s'occupe de votre <strong>nettoyage de printemps</strong> jusqu'aux confins de la <strong>Galerie Vivienne</strong>.
                    <br>
                    Grâce à notre service d'entretien, vous aurez à nouveau le temps de flaner dans votre quartier. 
                    Profitez en pour aller visiter la <strong>Basilique Notre-Dame-des-Victoires</strong>,
                    allez voir une avant première au très select <strong>Silencio</strong> ou encore passer la soirée 
                    au <strong>Théâtre des Bouffes Parisiens</strong>.
                    <br>
                    Vous déménagez chez vos voisins ? Pas de problème, votre 
                    <a href="Paris-7-eme-75007">femme de ménage vous suit dans le 1 er arrondissement</a> !
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs de votre aide ménagère dans le 2 ème arrondissement (75002)</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo est disponible proche du 2 ème arrondissement à Paris :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage à Paris 1 (75001)</p></a>
                    <a href="Paris-7-eme-75007"><p>Femme de menage à Paris 7 (75007)</p></a>
                    <a href="8"><p>Femme de menage à Paris 8 (75008)</p></a>
                </div>
            </div>
        </div>
</section>
<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
