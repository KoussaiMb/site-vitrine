<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/Paris-5-eme-75005"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Femme de menage haut de gamme dans le 5 ème arrondissement"/>
<meta property="og:description"     content="Nobo, c'est un homme ou une femme de ménage issue de l'hôtellerie de luxe pour vous aider avec le ménage et le repassage, dans le 5 ème arrondissement."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Trouver une femme de ménage à Paris 5 ème en quelques clics grâce à Nobo. Unique service d'entretien de qualité hôtelière pour la maison !">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 5ème arrondissement de Paris (75005) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-5eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Réservez votre femme de menage dans le 5ème <br>arrondissement de Paris (75005)</h1>
            <p class="hidden-xs color-grey">Service d'entretien 5 étoiles à domicile à Paris 5!</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Aide ménagère haut de gamme, à l'heure et sans engagement dans le 5ème arrondissement de Paris!</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Qualité</h3>
                        <p class="nomarge">
                            Avec Nobo réservez en ligne une
                            <strong>femme de ménage</strong> issue de l’hôtellerie de luxe dans le 
                            <strong>5 ème arrondissement de Paris</strong>. Vous aurez enfin l'assurance 
                            de trouver une aide de confiance au savoir faire prouvé pour s'occuper du 
                            nettoyage de votre appartement du sol au plafond et pour répondre à vos besoins 
                            en terme de qualité de repassage !
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Flexibilité</h3>
                        <p class="nomarge">
                            <span class="bold">Facturé à l'heure, sans engagement</span> ni frais
                            d'inscription. C'est la liberté totale ! Choisissez le nombre d'heures de service 
                            dont vous avez besoin ainsi que la fréquence des prestations de votre <strong>femme de ménage</strong>.
                            <a class="link-gold underline" href="/reservation/besoin">Réservez</a> dès à présent en quelques 
                            clics une <strong>femme de ménage à Paris 5</strong>.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Une aide tout inclus</h3>
                        <p class="nomarge">
                            Toutes les <span class="bold">démarches administratives</span> liées à l’embauche de votre 
                            <strong>femme de ménage</strong> sont balayées d'un coup de main avec Nobo.
                            Bénéficiez de surcroît d'un
                            <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">
                            crédit d'impôts de 50% </a> sur tous nos services d'entretien du domicile et du linge.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservez votre aide en ligne (ou par téléphone)</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.La réception vous contact pour fixer un rendez-vous à domicile</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Rencontrez votre Guest Relation pour partager vos besoins </p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Testez votre future femme de ménage lors d'un premier nettoyage</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Vous avez d'autres questions avant de nous confier votre maison ?! Rendez-vous sur la page 
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez votre femme de menage dans le 5 ème arrondissement de Paris (75005)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans votre quartier</h3>
                <p class="text-justify">
                    Le <strong>5ème arrondissement</strong> est connu pour son 
                    <strong>musée d’Histoire Naturelle</strong>, la 
                    <span class="bold">grande mosquée de Paris</span> mais également pour 
                    son grand espace vert, le <strong>Jardin des Plantes</strong> l’un des mieux 
                    <strong>entretenu</strong> de <strong>Paris</strong> mais l’un des moins 
                    <strong>ménagé</strong>. Et bien rassurez vous, notre service d'entretien est 
                    disponible dans tous le 5 ème !
                    <br>
                    Vous souhaitez changer d'air ? Sachez que Nobo vous proposera également une 
                    <a href="Paris-6-eme-75006">femme de ménage dans le 6 ème arrondissement de Paris</a> si vous 
                    en aviez besoin !
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre femme de menage à Paris 5 (75005)</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo est disponible proche du 5 ème arrondissement à Paris :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage à Paris 1 (75001)</p></a>
                    <a href="Paris-6-eme-75006"><p>Femme de menage à Paris 6 (75006)</p></a>
                    <a href="15"><p>Femme de menage à Paris 15 (75015)</p></a>

                </div>
            </div>
        </div>
</section>

<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
