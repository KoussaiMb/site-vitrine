<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

    <meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/17"/>
    <meta property="og:type"            content="website"/>
    <meta property="og:title"           content="Nobo - Femme de menage haut de gamme dans le 17 ème arrondissement"/>
    <meta property="og:description"     content="Premier service de ménage et repassage haut de gamme à domicile. Personnel hôtelier. Sans engagement. Crédit d'impôts de 50%. Paris 17"/>
    <meta property="fb:app_id"          content="430915960574732"/>
    <meta property="og:image"           content="/img/paris/fb.png"/>
    <!-- Google -->
    <meta name="description" content="Premier service de ménage et repassage haut de gamme à domicile. Personnel hôtelier. Sans engagement. Crédit d'impôts de 50%. Paris 17"/>
    <meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
    <!-- title -->
    <title>Femme de menage dans le 17 ème arrondissement de Paris (75017) | Nobo</title>
    <link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-17eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 17ème arrondissement de Paris (75017)</h1>
            <p class="hidden-xs">Femme de ménage haut de gamme, flexible et sans engagements à Paris 17 ème !</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Une <strong>femme de ménage</strong> de confiance à Paris 17 !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Personnel hôtelier</h3>
                        <p class="nomarge">
                            Nobo est le leader des <span class="bold">services haut de gamme à domicile</span> 
                            dans le <strong>17</strong> ème arrondissement de <strong>Paris</strong>.
                            <br>
                            <br>
                            Les <strong>femmes de ménage </strong> Nobo ont toutes eu une expérience de femme 
                            de chambre dans <span class="bold">l'hôtellerie 4 et 5 étoiles</span>.
                            Elles sont également embauchées en CDI pour vous garantir de garder la même 
                            <strong>femme de ménage</strong> durant notre collaboration.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">À la demande</h3>
                        <p class="nomarge">
                            <a class="link-gold underline" href="/reservation/besoin"> Réservez </a> en ligne de manière sécurisée une
                            <strong>femme de ménage</strong> pour vos besoins récurrents ou ponctuels.
                            L'accès à votre <strong>femme de ménage </strong> ne nécessite 
                            <span class="bold">aucun frais de dossier ni engagement de durée</span>. Vous êtes libéré(e) du 
                            <strong>ménage</strong> et du <strong>repassage</strong> sans pour autant vous engager !
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Accompagnement personnalisé</h3>
                        <p class="nomarge">
                            Toutes les <span class="bold">démarches administratives</span> liées à l’embauche de votre 
                            <strong>femme de ménage</strong> sont effectuées par nos soins.
                            Bénéficiez de surcroît d'un
                            <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">
                                crédit d'impôts de 50%
                            </a> sur toutes nos prestations de manière automatique.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </div>
</section>

<section class="section-dark" id="fonctionnement">
        <div class="container">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2 class="fix-h2-small">Comment réserver votre <strong>femme de ménage</strong> Nobo ?</h2>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                        </div>
                        <p class="col-xs-12 text-sticker">1.Réservez la date souhaitée pour l'intervention de votre <strong>femme de ménage</strong></p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="Appel de votre gouvernant pour détailler vos envies">
                        </div>
                        <p class="col-xs-12 text-sticker">2.Votre gouvernant(e) détaille vos envies et attentes au téléphone</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="Rencontrez votre gouvernant et votre femme de ménage">
                        </div>
                        <p class="col-xs-12 text-sticker">3.Rencontrez votre gouvernant(e) à domicile. Il vous présente votre <strong>femme de ménage</strong></p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="1er essai de votre femme de ménage">
                        </div>
                        <p class="col-xs-12 text-sticker">4.Votre <strong>femme de ménage</strong> réalise alors un 1e essai</p>
                    </div>
                </div>
            </div>
             <p class="hidden-xs text-center" style="margin-top: 20px;">Pour trouver des réponses à toutes vos questions, visitez la page 
                <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes"> questions fréquentes</a> de notre site !
        </div>
    </section>

<section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Nos clients parlent de nous - <strong>Paris 17</strong></h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"J'utilise Nobo 4h par semaine depuis 1 an. J'ai pu garder la même <strong>femme de menage</strong> depuis le début et le travail est nickel."</p>
                                <p>Carole, Villiers, Paris 17</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Carole cliente femme de ménage nobo paris 17" src="/img/paris/avis/femme-de-menage-paris-17-avis-carole.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Une équipe très pro, assez rare de nos jours pour le souligner !.."</p>
                                <p>Dimitri, Pereire, Paris 17</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Dimitri client femme de ménage nobo paris 17" src="/img/paris/avis/femme-de-menage-paris-17-avis-dimitri.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Nobo, c'est un service à la carte et on ne peut plus professionnel. Eric et son équipe sont formidables et mettront tout en œuvre pour vous satisfaire. L'hôtel à la maison, une promesse tenue!!"</p>
                                <p>Marc, Ternes, Paris 17</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Marc client femme de menage et repassage paris 17" src="/img/paris/avis/femme-de-menage-paris-17-avis-marc.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Dès 14€/h, une <strong>femme de menage</strong> issue de l'hôtellerie - <strong>Paris 17</strong></h2>
                    <p>Profitez automatiquement de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots"> crédit d'impôts</a> sur vos prestations avec Nobo !</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou nous <a class="link-gold underline" href="/contact">contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Pourquoi choisir une <strong>femme de menage</strong> Nobo à <strong>Paris 17</strong> ème ?</h2>
                </div>

                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h3 class="titre-seo">Parce que nous gérons absolument tout pour vous !</h3>          
                    <p class="text-justify">
                        Nobo est le seul acteur à proposer une offre entièrement intégrée.
                    </p>
                    <ul>
                        <li>Nous recrutons nos <strong>femmes de ménage</strong>, puis les formons à nos standards qualité</li>
                        <li>Nous évaluons vos envies pour vous proposer la <strong>femme de menage</strong> réellement adaptée à votre recherche</li>
                        <li>Votre gouvernant(e) crée avec vous votre carnet d'entretien personnalisé à domicile</li>
                        <li>Votre gouvernant(e) vous présente votre future <strong>femme de ménage </strong> qui réalise un premier test</li>
                        <li>Votre gouvernant(e) fait le point sur votre satisfaction après ce premier <strong>menage</strong></li>
                        <li>Votre <strong>femme de ménage</strong> récupère un double de vos clés si vous le souhaitez</li>
                        <li>Votre gouvernant(e) procède à des inspections pour vérifier le travail de votre <strong>femme de ménage</strong></li>
                        <li>Si besoin, il peut également la former sur des tâches spécifiques à votre appartement</li>
                        <li>Nous assurons votre <strong>femme de menage</strong> à hauteur de trois cents mille euros par sinistre</li>
                        <li>Nous nous chargeons de tout l'administratif en tant qu’employeur direct de votre  <strong>femme de ménage</strong> </li>
                        <li>Nous vous transmettons votre attestation fiscale afin de bénéficier de 50% de crédit d'impôts </li>
                    </ul>
                    <p class="text-justify">
                        Vous n'avez plus qu'à profiter de la vie !
                    </p>
                    <h3 class="titre-seo">Parce que nous sommes <span class="bold">FLEXIBLES !</span> </h3>
                    <p class="text-justify">
                        Notre société évolue vers une économie "à la demande", alors pourquoi ne pas en faire autant pour votre <strong>femme de ménage</strong> ?
                        <br>
                        Chez Nobo, nous avons la réelle volonté de bouleverser l'accès aux <strong>services à domicile</strong> notamment en proposant une offre 
                        de <strong>femme de ménage</strong> sans engagement et à la demande.
                        <br>
                        Vous pouvez suspendre les prestations de votre <strong>femme de menage</strong> ponctuellement lors de vos congés ou bien définitivement 
                        pour n'importe quelle raison. Votre seule obligation est de nous prévenir 72 heures avant la prochaine intervention de votre 
                        <strong>femme de menage</strong> pour bénéficier d'une modification ou annulation gratuite.
                        <br>
                        Vous pouvez également opter pour une utilisation entièrement ponctuelle, si vos besoin ne nécessitent pas l'intervention régulière d'une 
                        <strong>femme de menage</strong>.
                        <br>
                        <br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark hidden-xs" id="info">
    <div class="container">
        <div class="row margeTopBottom-2">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2>Pour compléter les prestations de votre <strong>femme de ménage</strong> Nobo, d'autres services 
                utiles dans votre quartier de <strong>Paris 17</strong>:</h2>
            </div>
            <div class="col-xs-12 info-content">
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo KOL livreur de bonheur" src="/img/paris/activites/service-a-domicile-paris-17-kol.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Pas le temps de vous occuper de l'apéro ?</h2>
                        <p class="color-grey">
                            Grâce à KOL, faites vous livrer vos vins & spiritueux préférés en 30 min, nuit et jour 
                            à température de dégustation. C’est en travaillant directement avec les producteurs que 
                            Kol propose une carte évolutive variant selon les saisons, les millésimes et vos sensibilités.
                        </p>
                        <p><span> - </span>Service disponible à <strong>Paris 17</strong> ème - 7j/7 et 24h/24<span> - </span></p>
                    </div>
                </div>
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo Cook Angels réparation d'objets connectés" src="/img/paris/activites/service-a-domicile-paris-17-cookangels.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Cuisinez de bons plats sains en 20 minutes !</h2>
                        <p class="color-grey">
                            Cook Angels est le premier service à <span class="bold"> vous livrer chaque semaine 
                            tous les ingrédients pour concevoir des recettes délicieuses</span> concoctées par un 
                            chef !… Cessez de cuisiner la même chose tous les soirs et passez à une cuisine équilibrée 
                            composée d'ingrédients frais.
                        </p>
                        <p><span> - </span>Service disponible à <strong>Paris 17</strong> ème - Livré au créneau horaire voulu<span> - </span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link" class="hidden-xs">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Votre femme de ménage Nobo est disponible dans d'autres arrondissements :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage Paris 1</p></a>
                    <p>Femme de menage Paris 2</p>
                    <p>Femme de menage Paris 3</p>
                    <p>Femme de menage Paris 4</p>
                    <p>Femme de menage Paris 5</p>
                    <a href="Paris-6-eme-75006"><p>Femme de menage Paris 6</p></a>
                    <a href="Paris-7-eme-75007"><p>Femme de menage Paris 7</p></a>
                    <a href="8"><p>Femme de menage Paris 8</p></a>
                    <p>Femme de menage Paris 9</p>
                    <p>Femme de menage Paris 10</p>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <p>Femme de menage Paris 11</p>
                    <p>Femme de menage Paris 12</p>
                    <p>Femme de menage Paris 13</p>
                    <p>Femme de menage Paris 14</p>
                    <a href="15"><p>Femme de menage Paris 15</p></a>
                    <a href="16"><p>Femme de menage Paris 16</p></a>
                    <a href="17"><p>Femme de menage Paris 17</p></a>
                    <p>Femme de menage Paris 18</p>
                    <p>Femme de menage Paris 19</p>
                    <p>Femme de menage Paris 20</p>
                </div>
            </div>
        </div>
</section>

<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>