<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/Paris-7-eme-75007"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Femme de menage haut de gamme dans le 7 ème arrondissement"/>
<meta property="og:description"     content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie pour le ménage et le repassage, dans le 7 ème arrondissement. Réservation en ligne, sans engagement."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Trouver une femme de ménage à Paris 7 ème en quelques clics grâce à Nobo (75007) ! Femmes de ménage issues de l'hôtellerie. 50% de crédit d'impôts.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 7ème arrondissement de Paris (75007) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-7eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 7ème <br>arrondissement de Paris (75007)</h1>
            <p class="hidden-xs color-grey">Trouver enfin la perle rare à Paris 7!</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Ménage et repassage haut de gamme, à l'heure et sans engagement dans le 7ème arrondissement !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Qualité</h3>
                        <p class="nomarge">
                            Avec Nobo réservez en ligne une
                            <strong>femme de ménage</strong> issue de l’hôtellerie de luxe dans le 
                            <strong>7 ème arrondissement de Paris</strong>. Vous aurez enfin l'assurance 
                            de trouver une aide de confiance au savoir faire prouvé pour s'occuper de 
                            dépoussiérer votre appartement du sol au plafond et pour ne plus laisser 
                            un pli sur vos chemises !
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Flexibilité</h3>
                        <p class="nomarge">
                            <span class="bold">Facturé à l'heure, sans engagement</span> ni frais
                            d'inscription. C'est la liberté totale ! Choisissez le nombre d'heures de ménage / repassage 
                            dont vous avez besoin ainsi que la fréquence des prestations de votre <strong>femme de ménage</strong>.
                            <a class="link-gold underline" href="/reservation/besoin">Réservez</a> dès à présent en quelques 
                            clics une <strong>femme de menage dans le 7ème arrondissement</strong>.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Accompagnement personnalisé</h3>
                        <p class="nomarge">
                            Toutes les <span class="bold">démarches administratives</span> liées à l’embauche de votre 
                            <strong>femme de ménage</strong> sont balayées d'un coup de main avec Nobo.
                            Bénéficiez de surcroît d'un
                            <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">
                            crédit d'impôts de 50% </a> sur toutes vos prestations de ménage et repassage.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Effectuez votre réservation en ligne (ou par téléphone)</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.La réception vous contact pour fixer un rendez-vous à domicile</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Rencontrez votre Guest Relation pour partager vos envies et besoins </p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Testez votre future femme de ménage lors d'une première prestation</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Vous avez d'autres questions ? Rendez-vous sur notre rubrique
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez une femme de menage dans le 7 ème arrondissement de Paris (75007)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans votre quartier</h3>
                <p class="text-justify">
                    Nobo dépoussière le domaine du ménage dans tout le <strong>17ème arrondissement de Paris</strong> 
                    grâce à son offre tout en un. Ne vous occupez plus des poussières <strong>Rue du Bac</strong>, de la
                    vaisselle sale à <strong>Sèvres Babylone</strong> ni de nettoyer vos vitres à 
                    <strong> Solférino</strong> !
                    Que vous habitiez du côté de la <strong>Tour Eiffel</strong>, de l'<strong>Ecole Militaire</strong> 
                    ou encore des <strong>Invalides</strong> nous viendrons vous faire oublier à quoi ressemble une 
                    éponge et un fer à repasser !
                    <br>
                    Ne pensez plus aux tâches ménagères et allez plutôt vous balader et découvrir ou redécouvrir 
                    les merveilles du <strong>7 ème arrondissement de Paris</strong>. Dépoussiérez vos souvenirs de 
                    jeunesse et grimpez les 1665 marches de la Tour Eiffel pour admirer le Champs de Mars vu du ciel !
                    <br>
                    Et si votre vue est perçante, vous apercevrez peut être dans l'alignement du Champs de Mars une
                    femme de ménage dans le 14 ème arrondissement en train de passer l'aspirateur
                    dans un appartement de la Tour Montparnasse !
                </p>
                <h3 class="titre-seo">Profitez de votre <strong>femme de ménage</strong> et oubliez l'administratif </h3>
                <p class="text-justify">
                    Les risques liés à l'embauche non déclarée d'une <strong>femme de ménage</strong>
                    sont de plus en plus grands. Cependant, un des frein à la déclaration
                    de sa <strong>femme de ménage</strong> en tant que particulier employeur est
                    la lourdeur des démarches administratives, ainsi que la responsabilité que cela
                    entraîne. Assurance, mutuelle, congés payés, déclaration URSSAF...
                    Autant de contraintes obligatoires lié à l'embauche de votre
                    <strong>femme de ménage</strong>.
                    <br>
                    Optez pour les services de Nobo, c'est choisir l'option clé en main pour
                    votre <strong>femme de ménage à Paris 7 ème</strong>. Nous gérons pour vous
                    100% des démarches administratives et fiscales. Vous bénéficiez également
                    d'une assurance couvrant les prestations de <strong>femme de ménage</strong>
                    dans la limite de 300.000 € par sinistre.
                    <br>
                    La tranquillité d'esprit n'a pas de prix !
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre femme de menage à Paris 7 (75007)</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

 <section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Nos femmes de ménage facilitent la vie de vos voisins à <strong>Paris 7 (75007)</strong></h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Pas du genre à pousser la poussière sous le tapis ! De vrais pros de confiance !"</p>
                                <p>Justine, Ecole Militaire, <strong>Paris 7 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Justine cliente ménage nobo à Paris 7 (75007)" src="/img/paris/avis/femme-de-menage-paris-7-avis-justine.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Nous sommes très satisfaits du travail de Rama depuis maintenant un an."
                                </p>
                                <p>Magali, Champs de Mars, <strong>Paris 7 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Magali cliente ménage nobo à Paris 7 ème (75007)" src="/img/paris/avis/femme-de-menage-paris-7-avis-magali.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Le ménage est fait dans les détails et le système du planning d'entretien permet de bien suivre le travail. Je n'ai pas prévu de changer !"
                                </p>
                                <p>Jean-Baptiste, Ecole Militaire, <strong>Paris 7 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Jean-Baptiste client menage Paris 7" src="/img/paris/avis/femme-de-menage-paris-7-avis-jean-baptiste.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark hidden-xs" id="info">
    <div class="container">
        <div class="row margeTopBottom-2">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="h2-seo">Et pour finir, quelques bonnes adresses à Paris 7</h2>
            </div>
            <div class="col-xs-12 info-content">
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo frichti livreur de bonheur" src="/img/paris/activites/service-a-domicile-paris-7-grandeepicerie.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Votre épicerie de quartier va sembler poussiéreuse</h2>
                        <p class="color-grey">
                            Envie de cuisiner un festin ? Bienvenue à la grande épicerie du Bon Marché.
                            Les étalages débordants de produits frais et de mets délicieux ne vous
                            aideront certe pas à éponger vos dettes, mais certainement à prouver à
                            vos amis que votre cuisine est la meilleure !
                            Reprenez plaisir à recevoir grâce au travail de votre 
                            <strong>femme de ménage</strong> qui aura tout nettoyé dans votre appartement
                            après le passage de vos amis, et de votre utilisation intensive des plaques de
                            cuisson !
                        </p>
                        <p><span> - </span>Situé au <strong>38 Rue de Sèvres, 75007, Paris</strong> 7j/7<span> - </span></p>
                    </div>
                </div>
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo save réparation d'objets connectés" src="/img/paris/activites/service-a-domicile-paris-7-musee-d-orsay.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Une balade au musée pour éponger votre semaine</h2>
                        <p class="color-grey">
                            Notre musée préféré du <strong>7 ème arrondissement</strong> est le 
                            Musée d'Orsay. Outre les pièces uniques qu'il renferme, sa verrière
                            magnifique ne risque pas de vous lasser de sa beauté ! Saviez-vous que
                            le Musée d'Orsay compte une brigade de 9 hommes et femmes de ménage spécialisés
                            qui nettoient à plein temps la verrière centrale ainsi que toutes les fenêtres 
                            géantes. Il faut un sacré coup de raclette à vitres pour en venir à bout !
                        </p>
                        <p><span> - </span>Situé au <strong>1 Rue de la Légion d'Honneur, 75007 Paris</strong> Du Mardi au Dimanche<span> - </span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo est disponible proche du 7 ème arrondissement à Paris :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage à Paris 1 (75001)</p></a>
                    <a href="8"><p>Femme de menage à Paris 8 (75008)</p></a>
                    <a href="15"><p>Femme de menage à Paris 15 (75015)</p></a>
                    <a href="16"><p>Femme de menage à Paris 16 (75016)</p></a>
                </div>
            </div>
        </div>
</section>

<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
