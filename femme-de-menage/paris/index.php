<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>
<!-- title -->
<title>Femme de ménage haut de gamme à Paris | Nobo</title>
<!-- Google -->
<meta name="description" content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie pour le ménage et le repassage, partout à Paris. Réservation en ligne, sans engagement ni frais d'inscription">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- Facebook -->
<meta property="og:url"           content="https://nobo.life/femme-de-menage/paris"/>
<meta property="og:type"           content="website"/>
<meta property="og:title"           content="Nobo - Femme de menage haut de gamme a Paris"/>
<meta property="og:description"       content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie pour le ménage et le repassage, partout à Paris. Réservation en ligne, sans engagement ni frais d'inscription"/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="img/fb.png"/>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">
<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="index">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <h1>Femme de menage Paris</h1>
            <p class="hidden-xs">Nobo, femme de ménage haut de gamme à domicile</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Profitez d'une <strong>femme de ménage</strong> de qualité, à l'heure et sans engagement à <strong>Paris</strong> !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Qualité</h3>
                        <p class="nomarge">
                            Nobo vous propose enfin un service de <strong>femme de ménage haut de gamme</strong> à <strong>Paris</strong>.
                            <br>
                            Réservez de manière sécurisée, en ligne et en quelques clics seulement une
                            <strong>femme de menage</strong> issue d'hôtels 4 et 5 étoiles !
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Flexibilité</h3>
                        <p class="nomarge">
                            Vous êtes facturés à l'<span class="bold">heure</span> et
                            <span class="bold">sans engagement</span> !
                            <br> Pas de frais d'inscription ni de résiliation, profitez d'une <strong>femme de ménage</strong>
                            sans contrainte !
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Suivi</h3>
                        <p class="nomarge">
                            Suivez et faites évoluer vos prestations quand vous le souhaitez grâce
                            au gouvernant personnel qui vous est affecté pour encadrer le travail
                            de votre <strong>femme de menage</strong>.
                            <br>
                            <br>
                            Voir toutes les options du <a class="underline link-gold" href="/service-a-domicile">service a domicile</a> Nobo.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservation en ligne sécurisée</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.Appel d'un réceptionniste pour établir vos besoins</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                    </div>
                    <p class="color-grey">3.Visite de votre gouvernant(e)</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                    </div>
                    <p class="color-grey">4.Première prestation de votre femme de chambre</p>
                </div>
            </div>
            <p class="hidden-xs text-center margeTopBottom-1">
                Encore des interrogations ? Voir les
                <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
            </p>
        </div>
</section>

<section id="seo1">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez en quelques clics une femme de menage à Paris et en proche banlieue</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Ne vous occupez plus de trouver la perle rare !</h3>
                <p class="text-justify">
                    Les prestations de votre <strong>femme de ménage</strong> ne sont plus satisfaisantes ?
                    Vous ne voulez plus perdre de temps à chercher une <strong>femme de ménage</strong>
                    de confiance en épluchant les petites annonces ?
                    <br>
                    Nobo s'occupe de trouver une <strong>femme de ménage</strong> qui correspondra parfaitement
                    à vos attentes. Pour cela, un premier entretien téléphonique nous permet de
                    comprendre vos besoins et vos attentes en termes de menage, de repassage
                    ou même de savoir-vivre !
                    <br>
                    Notre gouvernant recherche alors le profil le plus adapté parmi son équipe.
                    Une fois sélectionnée votre future <strong>femme de ménage</strong> sera également prévenue
                    de vos attentes en amont pour commencer à s'imprégner de sa mission.
                </p>
                <h3 class="titre-seo">Profitez d'une femme de ménage issue de l'hôtellerie</h3>
                <p class="text-justify">
                    Faire le ménage est une tâche bien plus technique que l'on veut
                    bien l'imaginer. Afin d'obtenir une qualité de service satisfaisante, nous avons
                    fait le choix de sélectionner nos <strong>femmes de ménage</strong> uniquement parmi
                    d'ancienne femmes de chambre de l'hôtellerie de luxe. Notre processus de
                    recrutement comprend également une mise en situation dans un appartement témoin,
                    deux appels de référence ainsi que la vérification du casier judiciaire. Une
                    fois ces étapes passées, une <strong>femme de ménage</strong> Nobo entre
                    en formation afin de revoir avec nous les essentiels d'un ménage et repassage de
                    qualité ainsi que nos outils de travail. Rien n'est laissé au hasard !
                </p>
                <h3 class="titre-seo">Une femme de ménage - 0 engagement</h3>
                <p class="text-justify">
                    Recourir aux services d'une <strong>femme de ménage à Paris</strong> nécessite
                    d'établir un lien de confiance fort. Aussi bien avec votre <strong>femme de ménage</strong>
                    qu'avec l'agence et son personnel encadrant. Pour vous aider à passer
                    le cap, nous avons décider de créer un service 100% sans engagement. Pour faire
                    simple, vous n'avez ni frais d'inscription, ni frais de résiliation, ni durée
                    d'engagement minimale. Cela vous permet de tester plus sereinement
                    votre <strong>femme de ménage</strong> et la qualité globale du service Nobo.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre <strong>femme de ménage</strong> à <strong>Paris</strong></h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots"> crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-outline-blue">réserver</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="seo2">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Les avantages d'une femme de ménage, sans les complications administratives !</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Faites le choix d'une femme de ménage déclarée et assurée</h3>
                <p class="text-justify">
                    Vous ne souhaitez pas gérer les démarches administratives liées à l'embauche d'une
                    <strong>femme de ménage</strong> en tant que particulier employeur ?
                    Vous ne comptez pas prendre le risque d'embaucher une
                    <strong>femme de ménage</strong> sans la déclarer ?<br>
                    Il est temps de tester les services de Nobo ! La gestion administrative et fiscale
                    globale est incluse dans notre offre sans surcoût. Plus aucun papier à envoyer ni
                    démarche à effectuer. Vous achetez simplement des heures de service auprès de votre
                    <strong>femme de ménage</strong>.
                    <br>
                    Notre service comprend également une assurance couvrant les sinistres que
                    pourrait accidentellement causé votre <strong>femme de ménage</strong> à hauteur
                    de 300.000 euros
                </p>
                <h3 class="titre-seo">Réservez votre femme de ménage en ligne</h3>
                <p class="text-justify">
                    Vous avez déjà l'habitude de réserver toutes sortes de services sur internet,
                    et pourtant le menage à domicile reste un des domaines les moins en avance sur
                    son temps ! Nobo propose enfin une solution digitale de réservation de
                    <strong>femmes de ménage à Paris</strong> pour vous faciliter l'accès à ce service.
                    La réservation de votre <strong>femme de ménage</strong> se fait en moins d'une minute, et le
                    paiement en ligne est sécurisé grâce à notre partenaire Stripe. Le premier
                    paiement n'intervient qu'après votre première prestation test. La facturation se
                    fait ensuite 24h après chacune de vos prestations suivantes.
                </p>
                <h3 class="titre-seo">Oubliez la mise en relation </h3>
                <p class="text-justify">
                     Toutes nos <strong>femmes de ménage</strong> sont employées en CDI à temps plein.
                    Il ne s'agit donc pas d'un service de mise en relation avec une
                    <strong>femme de ménage</strong> mais bien d'un service contrôlé de bout en
                    bout par nos soins. Ce que cela change contrairement pour vous est notre responsabilité
                    vis à vis des prestations de votre <strong>femme de ménage</strong>. En effet,
                    les services de mise en relation ne peuvent pas garantir le travail de leurs
                    prestataires ni même légalement le contrôler. La formation continue
                    des <strong>femmes de ménage</strong> et les inspections surprises ne sont
                    étalements possibles que pour les sociétés prestataires de services comme Nobo, cela
                    étant également interdit aux services de mise en relation.
                </p>
                <h3 class="titre-seo">Ne supervisez plus vous même votre femme de ménage ! </h3>
                <p class="text-justify">
                    En plus d'accéder à une <strong>femmes de ménage</strong> au savoir faire prouvé,
                    un gouvernant référant vous sera attribué. Il est le manager direct de
                    votre future <strong>femmes de ménage</strong> et encadre son travail.
                    Vous le rencontrerez lors d’un premier rendez vous à votre domicile.
                    Il créera à cette occasion <span class="bold">votre planning d'entretien</span> en
                    fonction de vos besoins.
                    Il sera ensuite votre point de contact, <span class="bold">disponible en permanence</span>
                    pour répondre à vos besoins.
                </p>
            </div>
        </div>
    </div>
</section>

<section id="photo-banner-50">
</section>

<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Votre femme de ménage Nobo est disponible dans tous les arrondissements de <strong>Paris</strong> :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="réservez votre femme de menage a Paris avec Nobo !">
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage Paris 1</p></a>
                    <a href="Paris-2-eme-75002"><p>Femme de menage Paris 2</p></a>
                    <p>Femme de menage Paris 3</p>
                    <p>Femme de menage Paris 4</p>
                    <a href="Paris-5-eme-75005"><p>Femme de menage Paris 5</p></a>
                    <a href="Paris-6-eme-75006"><p>Femme de menage Paris 6</p></a>
                    <a href="Paris-7-eme-75007"><p>Femme de menage Paris 7</p></a>
                    <a href="8"><p>Femme de menage Paris 8</p></a>
                    <a href="Paris-9-eme-75009"><p>Femme de menage Paris 9</p></a>
                    <p>Femme de menage Paris 10</p>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <a href="Paris-11-eme-75011"><p>Femme de menage Paris 11</p></a>
                    <p>Femme de menage Paris 12</p>
                    <p>Femme de menage Paris 13</p>
                    <p>Femme de menage Paris 14</p>
                    <a href="15"><p>Femme de menage Paris 15</p></a>
                    <a href="16"><p>Femme de menage Paris 16</p></a>
                    <a href="17"><p>Femme de menage Paris 17</p></a>
                    <p>Femme de menage Paris 18</p>
                    <p>Femme de menage Paris 19</p>
                    <p>Femme de menage Paris 20</p>
                </div>
            </div>
        </div>
</section>

<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>
<?php include_once ('../../inc/footer_end.php'); ?>