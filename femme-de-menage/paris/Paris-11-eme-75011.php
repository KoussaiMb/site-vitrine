<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/Paris-11-eme-75011"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Femme de menage haut de gamme dans le 11 ème arrondissement"/>
<meta property="og:description"     content="Aide ménagère issue de l'hôtellerie de luxe pour en finir avec le ménage et le repassage, dans le 11 ème arrondissement."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Réservez une femme de ménage en ligne à Paris 11 ème. Personnel exclusivement issu de l'hotellerie, service à domicile sans engagement, crédit d'impôts de 50%.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage | Paris 11ème arrondissement (75011) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-11eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Réservez votre femme de menage dans le 11 ème <br>arrondissement de Paris (75011)</h1>
            <p class="hidden-xs color-grey">Service d'entretien 5 étoiles à domicile, oubliez le ménage !</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Aide ménagère haut de gamme, à l'heure et sans engagement dans le 11 ème arrondissement de Paris!</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Ménage et repassage de qualité</h3>
                        <p class="nomarge">
                            Avec Nobo réservez en ligne une
                            <strong>femme de ménage</strong> issue de l’hôtellerie de luxe dans le 
                            <strong>11 ème arrondissement de Paris</strong>.
                            <br>
                            Optez pour une aide au savoir faire prouvé pour entretnir et nettoyer votre 
                            domicile de fond en comble. Nous allons enfin répondre à vos besoins !
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Un service complet</h3>
                        <p class="nomarge">
                            Nous profitons des heures d'intervention de votre <strong>femme de ménage</strong> 
                            pour synchroniser gratuitement d'autres services à domicile. Pressing, cordonnerie, courses,ne 
                            passez plus des heures à vous en occuper grâce à notre aide.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Un entretien flexible</h3>
                        <p class="nomarge">
                            <span class="bold">Facturé à l'heure et sans engagement</span> vous choisissez 
                            simplement le nombre d'heures de service dont vous avez besoin ainsi que la fréquence 
                            des prestations de votre <strong>femme de ménage</strong>.
                            Alors <a class="link-gold underline" href="/reservation/besoin">réservez</a> en ligne 
                            une <strong>aide ménagère à Paris 11 (75011)</strong>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservez votre aide en ligne (ou par téléphone)</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.La réception vous contact pour fixer un rendez-vous à domicile</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Rencontrez votre Guest Relation pour partager vos besoins </p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Votre femme de ménage réalise un premier test de ménage et/ou de repassage</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Besoin de plus d'informations sur notre service de nettoyage ?! Rendez-vous sur la page 
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Trouvez votre femme de menage à Paris 11 ème (75011)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans le 11 ème arrondissement</h3>
                <p class="text-justify">
                    Nobo astique et fait briller tout le <strong>11 ème arrondissement de Paris</strong> !
                    On dépoussière tout de l'<strong>Avenue de la République</strong> à la 
                    <strong>Place de la nation</strong> en passant par <strong>Oberkampf</strong>, 
                    <strong>Saint Ambroise</strong>, <strong>Richard Lenoir</strong> et <strong>Charonne</strong>.
                    Que vous habitiez du côté de l'<strong>Opéra Bastille</strong>, ou à la limite du 
                    <strong>Père Lachaise</strong> nous viendrons vous faire oublier à quoi ressemble une 
                    éponge et un balais !
                    <br>
                    Notre service d'entretien va vous libérer quelques heures afin de découvrir les meilleurs restaurants 
                    du 11ème arrondissement, on vous conseille en particulier <strong>East Mamma</strong>, 
                    <strong>Septime</strong> ou <strong>Aux Bons Crus</strong>.
                    <br>
                    <br>
                    Vous souhaitez déménager ? Sachez que Nobo vous proposera également une 
                    <a href="Paris-9-eme-75009">femme de ménage dans le 9 ème arrondissement de Paris</a> si vous 
                    en aviez besoin !
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre femme de menage à Paris 11 (75011)</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo est disponible proche du 11 ème arrondissement à Paris :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage à Paris 1 er (75001)</p></a>
                    <a href="Paris-5-eme-75005"><p>Femme de menage à Paris 5 (75005)</p></a>
                    <a href="Paris-6-eme-75006"><p>Femme de menage à Paris 6 (75006)</p></a>

                </div>
            </div>
        </div>
</section>

<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
