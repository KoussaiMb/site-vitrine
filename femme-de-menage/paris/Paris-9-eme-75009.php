<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/Paris-9-eme-75009"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Ménage, repassage, pressing et courses dans le 9 ème arrondissement de Paris"/>
<meta property="og:description"     content="Nobo vous permet de réserver une aide ménagère issue de l'hôtellerie de luxe, dans le 9 ème arrondissement. Profitez également gratuitement de services annexes comme le pressing à dmoicile ou la livraison de courses !.."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Dans le 9ème arrondissement, Nobo vous propose un homme ou une femme de ménage issue de l'hôtellerie de luxe. Service à domicile sans engagement, crédit d'impôts de 50%.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 9 ème arrondissement de Paris (75009) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-9eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Services de ménage et repassage <br>dans le 9ème à Paris (75009)</h1>
            <p class="hidden-xs color-grey">Le service d'entretien qui répond enfin à tous vos besoins !</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Un service à domicile de qualité et de confiance <br> dans le 9 ème arrondissement de Paris!</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Un service de qualité</h3>
                        <p class="nomarge">
                            Nobo vous propose du personnel de maison issu de l’hôtellerie pour entretenir votre domicile 
                            du sol au plafond dans le 9 ème arrondissement de Paris.
                            Aspirateurs, poussières, sols, vaisselle, machine de linge, repassage... Nous nous occupons
                            de tout pour vous faire gagner des heures de vie !<br>
                            Alors <a class="link-gold underline" href="/reservation/besoin">Réservez</a> vite votre future 
                            <strong>femme de ménage dans 9 ème</strong> et goûtez au luxe de vivre à la maison comme dans 
                            un hôtel !..
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Un service complet</h3>
                        <p class="nomarge">
                            Nous profitons des heures d'intervention de votre <strong>femme de ménage</strong> 
                            pour synchroniser gratuitement d'autres services à domicile. Pressing, cordonnerie, courses,ne 
                            passez plus des heures à vous en occuper grâce à notre aide.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Liberté d'esprit</h3>
                        <p class="nomarge">
                            Toutes les <span class="bold">obligations du particulier employeur</span> liées à l’embauche de votre 
                            <strong>femme de ménage</strong> sont balayées avec Nobo. Une assurance à hauteur de 300.000 euros 
                            couvre également votre domicile pendant nos heures de service. Enfin, notre service d'entretien est 
                            sans engagement et peut donc s'adapter à vos besoins fluctuants.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservez des heures de service en ligne</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.Un réceptionniste vous joint pour discuter de vos besoins</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Un(e) gouvernant(e) se déplace pour découvrir votre domicile</p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Nous vous présentons votre femme ou homme de ménage</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Vous avez d'autres questions sur notre service d'entretien ? Rendez-vous sur notre rubrique
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez votre homme ou femme de menage - Paris 9 ème (75009)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans le 9 ème arrondissement de Paris</h3>
                <p class="text-justify">
                    Nous proposons l'aide de nos hommes et femmes de ménage dans tout le 9 ème !
                    Barbès - Rochechouart, Anvers, Pigalle, Blanche et Place de Clichy).
                    Du <strong>Grand Rex</strong> à la <strong>place de Clichy</strong> en passant 
                    par <strong>Cadet</strong>, <strong>Poissonière</strong> ou <strong>Pigalle</strong>, nous 
                    dépoussiérons votre quartier ! Nous entretenons aussi les <strong>Grands Boulevards</strong>, 
                    <strong>Havre Caumartin</strong> et le quartier de <strong>Saint-Georges</strong>. Profitez 
                    de notre aide précieuse <strong>Boulevard Hausmann</strong> et oubliez les tâches ménagères 
                    <strong>Rue notre-dame-de-lorette</strong>
                    <br>
                    Pendant que votre aide ménagère lustre votre appartement, quittez la maison pour faire des 
                    courses à l'épicerie Italienne de Paris située Rue Fléchier ou pour aller voir une pièce 
                    au <strong>théâtre Fontaine</strong>.
                    <br>
                    Vous vous êtes trompé de page et cherchiez une 
                    <a href="8">femme de ménage pour le 2ème arrondissement</a> ?
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre femme de ménage dans le 9 ème arrondissement de Paris</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo propose aussi ses services aux voisins du 9 ème arrondissement :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage à Paris 1 (75001)</p></a>
                    <a href="Paris-2-eme-75002"><p>Femme de menage à Paris 2 (75002)</p></a>
                    <a href="8"><p>Femme de menage à Paris 8 (75008)</p></a>
                    <a href="17"><p>Femme de menage à Paris 17 (75017)</p></a>                    
                </div>
            </div>
        </div>
</section>

<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
