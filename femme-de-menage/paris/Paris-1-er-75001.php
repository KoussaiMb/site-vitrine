<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/Paris-1-er-75001"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Hommes et Femmes de menage en un clic dans le 1 er arrondissement"/>
<meta property="og:description"     content="Avec Nobo, réservez un homme ou une femme de ménage issue de l'hôtellerie de luxe, dans le 1 er arrondissement. Réservation en ligne, sans engagement, crédit d'impôts de 50%."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie de luxe, dans le 1 er arrondissement. Réservez en ligne, sans engagement, crédit d'impôts de 50%.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 1 er arrondissement de Paris (75001) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-1er">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 1er <br>arrondissement de Paris (75001)</h1>
            <p class="hidden-xs color-grey">Débarassez-vous de toutes vos tâches ménagères en 3 clics !</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Une femme de ménage de qualité et de confiance, <br>à l'heure et sans engagement dans le 1er arrondissement de Paris!</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Qualité</h3>
                        <p class="nomarge">
                            Avec Nobo réservez en ligne une <strong>aide ménagère à Paris 1er</strong>.
                            Tous nos hommes et femmes de ménage sont issus de l'hôtellerie 4 et 5 étoiles afin d'assurer 
                            la qualité de notre entretien.

                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Flexibilité</h3>
                        <p class="nomarge">
                            <span class="bold">Facturé à l'heure, sans engagement</span> ni frais
                            d'inscription. C'est la liberté totale ! Choisissez le nombre d'heures de ménage et de repassage 
                            dont vous avez besoin ainsi que la fréquence des prestations de votre <strong>femme de ménage</strong>.
                            <a class="link-gold underline" href="/reservation/besoin">Réservez</a> dès à présent en quelques 
                            clics une <strong>aide ménagère dans le 1 er arrondissement de Paris</strong>.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Accompagnement personnalisé</h3>
                        <p class="nomarge">
                            Toutes les <span class="bold">démarches administratives</span> liées à l’embauche de votre 
                            <strong>femme de ménage</strong> sont balayées d'un coup de main avec Nobo.
                            Bénéficiez de surcroît d'un
                            <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">
                            crédit d'impôts de 50% </a> sur toutes vos prestations d'entretien du domicile et du linge.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservez en ligne en quelques clics</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.Un réceptionniste vous appel pour qualifier votre besoin</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Un(e) gouvernant(e) vous rend visite pour vous rencontrer et faire le bilan de vos besoins</p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Votre femme de ménage réalise un premier test de ménage et de repassage</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Vous avez d'autres questions ? Rendez-vous sur notre rubrique
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez votre homme ou femme de menage - Paris 1 er (75001)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans le 1 er arrondissement</h3>
                <p class="text-justify">
                    Nous vous proposons notre service d'entretien dans tout votre quartier ! 
                    De <strong>Chatelet</strong> à la <strong>place Vendôme</strong> en passant 
                    par <strong>Le Louvre</strong>, la <strong>Rue de Rivoli</strong>, le <strong>Palais Royal</strong>, 
                    le <strong>Pont Neuf</strong> et même l'entame de la <strong>Rue Etienne Marcel</strong>; Nobo 
                    dépoussière votre quartier et le désinfecte du sol au plafond ! Vous pouvez jeter vos éponges 
                    si vous habitez près de <strong>La Comédie Française</strong> !
                    <br>
                    Ne pensez plus aux tâches ménagères et quittez la maison pour allez vous balader aux 
                    <strong>Jardins des Tuileries</strong>, prendre un thé au <strong>café Marly</strong> 
                    pour nettoyer votre corps, ou redécouvrir le travail des plus grands artistes au 
                    <strong>Musée du Louvre</strong>.
                    <br>
                    Si vous êtes tombés chez vos voisins de quartier et que vous cherchiez en fait une 
                    <a href="8">femme de ménage dans le 8 ème arrondissement</a>, cliquez simplement sur le lien !
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre femme de menage dans le 1 er arrondissement de Paris</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo est disponible proche du 1 er arrondissement à Paris :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-2-eme-75002"><p>Femme de menage à Paris 2 (75002)</p></a>
                    <a href="Paris-5-eme-75005"><p>Femme de menage à Paris 5 (75005)</p></a>
                    <a href="Paris-7-eme-75007"><p>Femme de menage à Paris 7 (75007)</p></a>
                    <a href="8"><p>Femme de menage à Paris 8 (75008)</p></a>
                </div>
            </div>
        </div>
</section>
<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
