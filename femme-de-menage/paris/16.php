<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

    <meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/16"/>
    <meta property="og:type"            content="website"/>
    <meta property="og:title"           content="Nobo - Femme de menage haut de gamme dans le 16 ème arrondissement"/>
    <meta property="og:description"     content="Nobo est le premier service de réservation haut de gamme pour le ménage et le repassage à domicile dans le 16 ème arrondissement. Personnel hôtelier déclaré et assuré."/>
    <meta property="fb:app_id"          content="430915960574732"/>
    <meta property="og:image"           content="/img/paris/fb.png"/>
    <!-- Google -->
    <meta name="description" content="Nobo est le premier service de réservation haut de gamme pour le ménage et le repassage à domicile dans le 16 ème arrondissement. Personnel hôtelier déclaré et assuré."/>
    <meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
    <!-- title -->
    <title>Femme de menage | Paris 16 ème arrondissement (75016) | Nobo</title>
    <link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-16eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 16 ème <br>arrondissement de Paris (75016)</h1>
            <p class="hidden-xs">Un service de ménage effectué par du personnel hôtelier à Paris 16</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Des prestations de ménage et repassage qui répondent enfin à vos besoins à <strong>Paris 16</strong> !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Un service de qualité</h3>
                        <p class="nomarge">
                            Une <strong>femme de ménage </strong> issue de l'hôtellerie de luxe pour chez vous ? 
                            Profitez de <strong>personnel de maison</strong> formé à l'excellence à <strong>Paris 16</strong> 
                            pour s'occuper de <strong>l'entretien de votre domicile</strong>.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Prestations de ménage sur mesure</h3>
                        <p class="nomarge">
                            N'épluchez plus les petites annonces pour trouver la <strong>perle rare</strong> 
                            pour votre ménage à domicile, <a class="link-gold underline" href="/reservation/besoin"> réservez </a>
                            en ligne en quelques clics un ancien valet ou femme de chambre pour astiquer votre appartement 
                            de fond en comble.
                            Convaincu de la qualité de nos prestations de ménage, notre service est 
                            <span class="bold">sans engagement</span> ! Alors craquez vite pour une 
                            <strong>femme de ménage</strong> Nobo pour entretenir 
                            votre appartement dans le 16ème arrondissement.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Nettoyage suivi</h3>
                        <p class="nomarge">
                            En fonction de vos besoins nous sélectionnerons la <strong>femme de menage</strong> 
                            idéale pour votre domicile. Vous la garderez pour chaque prestation de ménage.
                            Un gouvernant vous sera également dédié et inspectera régulièrement son travail 
                            pour s'assurer que le ménage et le repassage soit à la hauteur du service attendu !
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </div>
</section>

<section class="section-dark" id="fonctionnement">
        <div class="container">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2 class="fix-h2-small">Notre fonctionnement</h2>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                        </div>
                        <p class="col-xs-12 text-sticker">1.Réservez des heures d'entretien en ligne</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                        </div>
                        <p class="col-xs-12 text-sticker">2.Nous vous appelons pour comprendre vos besoins et fixer un rendez-vous</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                        </div>
                        <p class="col-xs-12 text-sticker">3.Rencontrez votre gouvernant(e) à domicile pour sélectionner votre femme de ménage</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                        </div>
                        <p class="col-xs-12 text-sticker">4.Profitez d'une prestation de ménage test pour juger de notre service !</p>
                    </div>
                </div>
            </div>
             <p class="hidden-xs text-center" style="margin-top: 20px;">Si notre service ne vous semble pas parfaitement clair, ou que vous avez des 
                questions spécifiques au ménage et au repassage, rendez-vous à la section 
                <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes"> questions fréquentes</a> de notre site !
        </div>
    </section>

<section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Nos femmes de ménage font déjà briller <strong>Paris 16</strong></h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Service de nettoyage impeccable. L'hôtel à la maison c'est vraiment le pied ! Je le recommande !!"</p>
                                <p>Hannah, Trocadéro, Paris 16</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Hannah cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-16-avis-hannah.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Bluffé par la qualité du ménage ! Digne d'un grand hôtel 
                                Hâte d'essayer leurs autres services !!"</p>
                                <p>Jonathan, La Muette, Paris 16</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Jonathan client ménage nobo" src="/img/paris/avis/femme-de-menage-paris-16-avis-jonathan.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Un service de très grande qualité, notamment pour le ménage. J'ai pu tisser de vrais liens de confiance avec l'équipe et les recommande chaleureusement."</p>
                                <p>Danièle, Jasmin, Paris 16</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Daniele cliente menage repassage paris 16" src="/img/paris/avis/femme-de-menage-paris-16-avis-daniele.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Votre femme de menage a Paris 16 (75016) dès 14€/h</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots"> crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou nous <a class="link-gold underline" href="/contact">contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Les services Nobo dans le <strong>16 ème arrondissement de Paris</strong> :</h2>
                </div>

                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h3 class="titre-seo">Zone d'intervention dans votre quartier</h3>
                        <p class="text-justify">
                        Tout le quartier du <strong>16ème arrondissement de Paris</strong> est couvert par notre service. 
                        D'<strong>Exelmans</strong> au <strong>Trocadéro</strong> en passant 
                        par la <strong>Porte d'Auteuil</strong>, <strong>la villa montmorency</strong>, ou <strong>Passy</strong> 
                        nous nettoyons votre domicile de fond en comble ! Oubliez l'aspirateur <strong>Avenue Victor Hugo</strong> 
                        et la vaisselle qui traine <strong>Avenue Foch</strong> !
                        <br>
                        Des heures de liberté pour vous balader en famille au <strong>Parc Sainte Perrine</strong>,
                        prendre un verre au nouveau bar de la <strong>Maison de la radio</strong> ou encore aller au 
                        <strong>cinéma MK2 Beaugrenelle</strong>.
                        <br>
                        Si vous habitez de l'autre côté du <strong>Pont de Grenelle</strong> nous proposons également une 
                        <a href="15">aide ménagère dans le 15 ème arrondissement</a> ?
                         </p>
                    <h3 class="titre-seo">Bien plus que du ménage, ne vous occupez plus d'aucune tâche ménagère à la maison !</h3> 
                        <p class="text-justify">
                        L'entretien de votre domicile est le cœur de notre travail. Mais en plus du ménage et du repassage, votre 
                        <strong>femme de menage</strong> Nobo peut s'occuper d'autres services à domicile.
                        <br>
                        Déposer vos robes et chemises au pressing ? Facile grâce à notre service de pressing à domicile. 
                        Votre gouvernant planifiera le retrait à domicile des vêtements à nettoyer lors de la prestation de votre 
                        <strong>aide ménagère</strong> !
                        <br>
                        Pas envie de faire les courses ? Faites vous livrer à domicile, votre <strong>femme de ménage</strong> 
                        s'occupera de tout ranger à l'arrivée du livreur !
                        <br>
                        Une paire de chaussure à entretenir ? Un partenaire local du <strong>16</strong> ème arrondissement de <strong>Paris</strong> 
                        viendra les récupérer lors du nettoyage de votre domicile.
                        <br>
                        <br>
                        </p>
                    <br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark hidden-xs" id="info">
    <div class="container">
        <div class="row margeTopBottom-2">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2>Pour compléter les services de votre <strong>femme de ménage</strong> Nobo, voici d'autres services 
                disponibles à <strong>Paris 16</strong> :</h2>
            </div>
            <div class="col-xs-12 info-content">
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo Nestor livreur de bonheur" src="/img/paris/activites/service-a-domicile-paris-16-nestor.png" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Une cuisine de qualité livrée à domicile ?</h2>
                        <p class="color-grey">
                            Grâce à Nestor, commandez en ligne un déjeuner dans <strong>16</strong> ème arrondissement de <strong>Paris</strong>. 
                            Nestor dépoussière les services de livraison de repas en proposant chaque jour un menu composé par un chef.
                            Plus besoin de salir la cuisine que nous aurons parfaitement nettoyée !
                        </p>
                        <p><span> - </span>Service disponible à <strong>Paris 16 (75016)</strong><span> - </span></p>
                    </div>
                </div>
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo Zenest réparation d'objets connectés" src="/img/paris/activites/service-a-domicile-paris-16-zenest.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Un service pour se ménager à domicile?</h2>
                        <p class="color-grey">
                            Zenest est un service innovant de réservation de massage pour la maison. Avec un appartement qui 
                            brille, sans une trace de poussière et le dos décontracté, vous serez au paradis !
                        </p>
                        <p><span> - </span>Service disponible dans le <strong>16 ème arrondissement de Paris</strong><span> - </span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link" class="hidden-xs">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Votre femme de ménage Nobo est disponible dans d'autres arrondissements de Paris:</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="15"><p>Femme de menage à Paris 15 (75015)</p></a>
                    <a href="17"><p>Femme de menage à Paris 17 (75017)</p></a>
                    <a href="8"><p>Femme de menage à Paris 8 (75008)</p></a>
                </div>
            </div>
        </div>
</section>
<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>