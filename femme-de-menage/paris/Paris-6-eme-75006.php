<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/Paris-6-eme-75006"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Hommes et Femmes de menage en un clic dans le 6 ème arrondissement"/>
<meta property="og:description"     content="Avec Nobo, réservez un homme ou une femme de ménage issue de l'hôtellerie de luxe, dans le 6 ème arrondissement. Réservation en ligne, sans engagement, crédit d'impôts de 50%."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie de luxe, dans le 6 ème arrondissement. Réservez en ligne, sans engagement, crédit d'impôts de 50%.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 6ème arrondissement de Paris (75006) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-6eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 6ème <br>arrondissement de Paris (75006)</h1>
            <p class="hidden-xs color-grey">Nobo dépoussière le secteur du ménage à domicile à Paris 6 !</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Un homme ou une femme de ménage issu de l’hôtellerie, <br>à l'heure et sans engagement dans le 6ème (75006) !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Qualité</h3>
                        <p class="nomarge">
                            Avec Nobo réservez en ligne une <strong>femme de ménage à Paris 6</strong>.
                            Tous nos intervenants sont issus de l'hôtellerie 4 et 5 étoiles afin d'assurer 
                            un ménage et un repassage parfait.

                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Sur mesure</h3>
                        <p class="nomarge">
                            N'épluchez plus les petites annonces pour trouver la <strong>perle rare</strong> 
                            pour votre ménage à domicile, <a class="link-gold underline" href="/reservation/besoin"> réservez </a>
                            en ligne en quelques clics un ancien valet ou femme de chambre pour astiquer votre appartement 
                            de fond en comble.
                            Convaincu de la qualité de nos prestations de ménage, notre service est 
                            <span class="bold">sans engagement</span> ! Alors craquez vite pour une 
                            <strong>femme de ménage</strong> Nobo pour entretenir 
                            votre appartement dans le 6ème arrondissement.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Accompagnement personnalisé</h3>
                        <p class="nomarge">
                            Toutes les <span class="bold">démarches administratives</span> liées à l’embauche de votre 
                            <strong>femme de ménage</strong> sont balayées d'un coup de main avec Nobo.
                            Bénéficiez de surcroît d'un
                            <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">
                            crédit d'impôts de 50% </a> sur toutes vos prestations de ménage et repassage.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservez en ligne en quelques clics</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.Un réceptionniste vous appel pour qualifier votre besoin</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Un(e) gouvernant(e) vous rend visite pour vous rencontrer et faire le bilan de vos besoins</p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Votre femme de ménage réalise un premier test de ménage / repassage</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Vous avez d'autres questions ? Rendez-vous sur notre rubrique
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez votre homme ou femme de menage - Paris 6 ème (75006)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans votre quartier</h3>
                <p class="text-justify">
                    Tout le quartier du <strong>6ème arrondissement de Paris</strong> est couvert par notre service. 
                    De <strong>Saint Germain des Prés</strong> aux <strong>jardins du Luxembourg</strong> en passant 
                    par <strong>Saint Sulpice</strong>, <strong>Odéon</strong>, <strong>Mabillon</strong> et même la fameuse 
                    <strong>Rue Princesse</strong>. Ne passez plus l'aspirateur à <strong>Saint Michel</strong> ni la 
                    serpillière à <strong>Montparnasse</strong>
                    <br>
                    Ne pensez plus aux tâches ménagères et allez plutôt vous balader <strong>aux jardins du Luxembourg</strong>,
                    prendre un verre au <strong>café de Flore</strong> ou encore aller au <strong>cinéma MK2 d'Odéon</strong>.
                    La culture et l’art de vivre à la parisienne sont à l’honneur dans ce quartier du 6e arrondissement. 
                    L’atmosphère et le décor y sont propices pour se laisser flâner et ne surtout pas s'occuper du ménage !
                    <br>
                    Et pour les fins gourmets, pourquoi ne pas se laisser tenter par un dîner au Jules Verne au cœur de 
                    la tour Eiffel chez vos voisins qui ont une <a href="Paris-7-eme-75007">femme de ménage dans le 7 ème arrondissement</a> ?
                </p>
                <h3 class="titre-seo">Profitez de service additionnels pour vous faciliter la vie</h3>
                <p class="text-justify"> Vous proposer une <strong>femme de ménage</strong> pour s'occuper de
                    votre ménage et repassage dans le <strong>6 ème arrondissement de Paris</strong> est notre mission principale, mais
                    Nobo, c'est bien plus que ça ! Nous proposons un ensemble de services à domicile
                    pour transformer votre appartement en hôtel 5 étoiles !
                    <br>
                    Nous proposons de gérer pour vous la mise au pressing de vos vêtements. Un livreur
                    récupère vos vêtements lors du passage de votre <strong>femme de ménage</strong>,
                    les dépose à un pressing partenaire, puis vous les retourne lors de votre prochaine
                    prestation.
                    <br>
                    Envie d’égailler votre intérieur ? Nous proposons de vous faire livrer de
                    somptueux bouquets de fleur toutes les semaines ou deux fois par mois.
                    Votre <strong>femme de ménage</strong> s'occupe de réceptionner le bouquet puis
                    de l'entretenir d'une semaine sur l'autre. Découvrez tous nos services additionnels
                    à la rubrique <a href="/service-a-domicile">service a domicile</a> de notre site.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre femme de menage dans le 6ème arrondissement de Paris</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

 <section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Dans votre quartier du <strong>6 ème arrondissement de Paris</strong>, vos voisins nous recommandent déjà !</h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Je suis très maniaque avec la poussière mais j'ai rien à redire (plinthes, sous le lit, étagères, etc..!)"</p>
                                <p>Anaïs, Saint Germain des Prés, <strong>Paris 6 (75006)</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo d'Anais cliente ménage nobo Paris 6" src="/img/paris/avis/femme-de-menage-paris-6-anais.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Super agence bravo !"
                                </p>
                                <p>Michel, Odéon, <strong>Paris 6 (75006)</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Michel client ménage nobo à Paris 6 ème" src="/img/paris/avis/femme-de-menage-paris-6-michel.jpeg" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Anita est top je suis très satisfaite."
                                </p>
                                <p>Jessica, Saint Sulpice, <strong>Paris 6 (75006)</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Jessica cliente menage paris 6 75006" src="/img/paris/avis/femme-de-menage-paris-6-jessica.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark hidden-xs" id="info">
    <div class="container">
        <div class="row margeTopBottom-2">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="h2-seo">Et pour finir, quelques recommandations à Paris 6</h2>
            </div>
            <div class="col-xs-12 info-content">
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo frichti livreur de bonheur" src="/img/paris/activites/service-a-domicile-paris-6-aux-pres.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">On dépoussière la cuisine bistrot aux Prés</h2>
                        <p class="color-grey">
                            Envie d'un diner entre amis sans salir la cuisine tout juste nettoyée 
                            par nos soins ?! Optez pour la nouvelle adresse de Cyril Lignac dans le 
                            <strong>6ème arrondissement</strong>. Aux prés, la cuisine est simple 
                            mais de grande qualité. Le charme du bistrot, le punch d'une cuisine de 
                            chef.
                        </p>
                        <p><span> - </span>Situé au <strong>27 Rue du Dragon, 75006, Paris</strong> 7j/7 midi et soir<span> - </span></p>
                    </div>
                </div>
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo save réparation d'objets connectés" src="/img/paris/activites/service-a-domicile-paris-6-aroma-zone.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Un appartement désinfecté et qui sent booooon !</h2>
                        <p class="color-grey">
                            Quelle chance d'habiter dans le quartier qui abrite le plus grand magasin 
                            d'huiles essentiels et produits d'entretiens naturels de tout Paris !
                            <br>
                            Chez Aroma Zone, trouvez de quoi vivre dans un appartement parfaitement sain.
                            Des huiles essentiels pour mélanger à vos détergents, du vinaigre blanc bio 
                            pour détartrer sans agresser, des épices concentrées pour désodoriser votre 
                            réfrigérateur.
                        </p>
                        <p><span> - </span>Situé au <strong>25 Rue de l'école de Médecine, 75006, Paris</strong> du Mardi au Samedi<span> - </span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo est disponible proche du 6 ème arrondissement à Paris :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-2-eme-75002"><p>Femme de menage à Paris 2 (75002)</p></a>
                    <a href="Paris-5-eme-75005"><p>Femme de menage à Paris 5 (75005)</p></a>
                    <a href="Paris-7-eme-75007"><p>Femme de menage à Paris 7 (75007)</p></a>
                    <p>Femme de menage à Paris 14 (75014)</p>
                    <a href="15"><p>Femme de menage à Paris 15 (75015)</p></a>
                </div>
            </div>
        </div>
</section>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
