<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/15"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Femme de menage haut de gamme dans le 15 ème arrondissement"/>
<meta property="og:description"     content="Avec Nobo, réservez une femme de ménage issue de l'hôtellerie pour le ménage et le repassage, dans le 15 ème arrondissement. Réservation en ligne, sans engagement."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Trouver une femme de ménage à Paris 15 ème en quelques clics grâce à Nobo ! Personnel de maison issu de l'hôtellerie. 50% de crédit d'impôts.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 15ème arrondissement de Paris (75015) | Nobo</title>
<!-- CSS -->
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-15eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 15ème <br>arrondissement de Paris (75015)</h1>
            <p class="hidden-xs color-grey">Trouver une aide de qualité pour répondre à vos besoins à Paris 15!</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez une femme de ménage de qualité, à l'heure et sans engagement à Paris 15 !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Entretien de qualité</h3>
                        <p class="nomarge">
                            Avec Nobo réservez en ligne une
                            <strong>femme de ménage dans le 15 ème arrondissement de Paris</strong>.
                            Toutes nos <strong>aides ménagères</strong> sont issues de l'hôtellerie 4 et 5
                            étoiles afin d'assurer un service de qualité.
                            <br>
                            Fini la poussière sur les plinthes et les traces de doigts sur les vitres grâce à 
                            nos experts de l'entretien à domicile !

                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Service flexible</h3>
                        <p class="nomarge">
                            <span class="bold">Facturé à l'heure, sans engagement</span> ni frais
                            d'inscription, choisissez simplement le nombre d'heures de ménage et repassage 
                            dont vous avez besoin.
                            Alors <a class="link-gold underline" href="/reservation/besoin">réservez</a> dès 
                            maintenant votre <strong>femme de menage à Paris 15 ème</strong>.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Bien plus que du nettoyage !..</h3>
                        <p class="nomarge">
                            En plus de vous apporter du <strong>personnel de maison</strong> compétent,
                            Nobo vous propose des services hôteliers à la maison ! Service de pressing ou
                            de cordonnerie à domicile, livraison de fleurs, courses...<br>
                            Nobo dépoussière le secteur avec une offre tout en un !
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="fonctionnement">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="fix-h2-small">Notre fonctionnement</h2>
            </div>
            <div class="col-xs-12 margeTopBottom-3">
                <div class="col-xs-12 col-sm-3 text-center fadeUp">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                    </div>
                    <p class="color-grey">1.Réservez des heures en ligne en quelques clics</p>
                </div>
                <div class="col-xs-12 col-sm-3 text-center fadeDown">
                    <div class="func-sticker valign">
                        <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="établissez vos besoins au téléphone">
                    </div>
                    <p class="color-grey">2.La réception vous appel pour comprendre vos besoins</p>
                </div>
            <div class="col-xs-12 col-sm-3 text-center fadeDown">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="créez le planning d'entretien de votre femme de ménage">
                </div>
                <p class="color-grey">3.Un(e) gouvernant(e) vous rend visite pour créer votre planning d'entretien</p>
            </div>
            <div class="col-xs-12 col-sm-3 text-center fadeUp">
                <div class="func-sticker valign">
                    <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="faites un premier test avec votre femme de ménage">
                </div>
                <p class="color-grey">4.Votre femme de ménage réalise sa première prestation d'entretien</p>
            </div>
        </div>
        <p class="hidden-xs text-center margeTopBottom-1">
            Vous avez d'autres questions sur notre service ? Trouvez les réponses à vos besoins : 
            <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a>
        </p>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row margeTopBottom-1">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Réservez une femme de menage dans le 15 ème arrondissement de Paris (75015)</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h3 class="titre-seo">Zone d'intervention dans votre quartier</h3>
                <p class="text-justify">
                    Nobo dépoussière le domaine du ménage dans tout le <strong>15ème arrondissement de Paris</strong>.
                    Que vous habitiez du côté de <strong>Convention</strong>, <strong>Commerce</strong>, <strong>Charles Michels</strong>,
                    du centre commercial <strong>Beaugrenelle</strong>, de la Gare <strong>Montparnasse</strong>, de la Rue de 
                    <strong>Vaugirard</strong> ou du côté de <strong>Sèvres Lecourbe</strong> nous viendrons vous délivrer des corvées !
                    Plus de vaisselle à <strong>la Motte Piquet Grenelle</strong>, fini de balayer à <strong>Balard</strong>, 
                    adieu les éponges à la <strong>Porte de Versailles</strong>.
                    <br>
                    Alors laissez nous vous libérer quelques heures et sortez de la maison pour profitez des 
                    nombreux restaurants, espaces verts et musées de votre quartier du 15 ème à Paris. 
                    Nobo s’engage à vous aider dans la recherche de la perle rare qui saura faire briller 
                    votre appartement du sol au plafond !
                    <br>
                    Vous n'habitez pas le 15 ème mais à proximité du Bois de boulogne ? Trouvez une 
                    <a href="16">femme de ménage dans le 16 ème arrondissement de Paris</a> !
                </p>
                <h3 class="titre-seo">Recrutez votre femme de ménage en un clic</h3>
                <p class="text-justify">
                    Trouver une <strong>femme de ménage à Paris 15</strong> nécessite de
                    rencontrer 4 profils en moyenne avant de trouver la
                    <strong>perle rare</strong> adaptée à vos besoins. Cela représente une 15 aine d'heures 
                    et ne garanti pas toujours des prestations de ménage et de repassage de qualité.
                    <br>
                    Avec Nobo, trouvez immédiatement une <strong>aide ménagère</strong> qui vous proposera 
                    une service de ménage digne des palaces Parisiens dont elle est issue !
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Les tarifs pour votre femme de menage à Paris 75015</h2>
                    <p>Bénéficiez de 50% de <a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">crédit d'impôts</a> sur toutes vos prestations</p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou <a class="link-gold underline" href="/contact">nous contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Nos femmes de ménage facilitent la vie de vos voisins à <strong>Paris 15 (75015)</strong></h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Une vraie aide ! Le ménage est parfait et les petites attentions font qu’on
                                    se sent vraiment traité comme à l’hôtel, c’est top bravo!"</p>
                                <p>Emmanuelle, Beaugrenelle, <strong>Paris 15 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo d'Emmanuelle cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-emmanuelle.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Nettoyage toujours aussi bien après plusieurs mois de service,
                                    ce qui est très rare malheureusement.
                                    Je recommande, surtout pour les allergiques à la poussière!.."
                                </p>
                                <p>Margaux, Convention, <strong>Paris 15 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Margaux cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-margaux.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Très bon service de repassage, facilement joignable
                                    au téléphone + un réel suivi. Je recommande !"
                                </p>
                                <p>Xavier, Commerce, <strong>Paris 15 ème</strong></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Xavier client menage paris 15" src="/img/paris/avis/femme-de-menage-paris-15-avis-xavier.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark hidden-xs" id="info">
    <div class="container">
        <div class="row margeTopBottom-2">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="h2-seo">Et pour finir, quelques services utiles à Paris 15 (75015)</h2>
            </div>
            <div class="col-xs-12 info-content">
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo frichti livreur de bonheur" src="/img/paris/activites/service-a-domicile-paris-15-frichti.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Un repas équilibré livré à domicile</h2>
                        <p class="color-grey">
                            Vous êtes du genre à salir toute la maison quand vous cuisinez ?!
                            Découvrez Frichti, un restaurant connecté qui vous
                            livrera un repas équilibré à domicile partout dans votre quartier du 
                            <strong>15ème arrondissement</strong>. Nobo s'occupe de passer l'aspirateur, 
                            Frichti de combler votre estomac ! Le paradis n'est plus très loin, surtout si on en 
                            profite pour aller faire un picnic au parc André Citroën !
                        </p>
                        <p><span> - </span>Service disponible à <strong>Paris 15 eme</strong> 7j/7<span> - </span></p>
                    </div>
                </div>
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo save réparation d'objets connectés" src="/img/paris/activites/service-a-domicile-paris-15-save.png" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Un iPhone tout propre en moins de deux !</h2>
                        <p class="color-grey">
                            Votre smartphone semble attiré par le sol comme un plumeau par la poussière ? 
                            Un corner Save a ouvert à <strong>Paris 15 dans le centre commercial Beaugrenelle</strong>.
                            Vous pouvez le déposer sur place ou opter pour le retrait et livraison à domicile.
                        </p>
                        <p><span> - </span>Service disponible à <strong>Paris 75015</strong> du Lundi au Samedi de 10h à 21h<span> - </span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Votre femme de ménage Nobo est disponible dans d'autres quartiers proches du 15 ème arrondissement à Paris :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <a href="Paris-6-eme-75006"><p>Femme de menage à Paris 6 (75006)</p></a>
                    <a href="Paris-7-eme-75007"><p>Femme de menage à Paris 7 (75007)</p></a>
                    <a href="16"><p>Femme de menage à Paris 16 (75016)</p></a>
                </div>
            </div>
        </div>
</section>
<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>
