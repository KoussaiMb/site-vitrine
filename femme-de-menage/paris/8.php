<?php include_once('../../inc/bootstrap.php'); ?>
<?php include_once('../../inc/header_start.php'); ?>

<meta property="og:url"             content="https://nobo.life/femme-de-menage/paris/8"/>
<meta property="og:type"            content="website"/>
<meta property="og:title"           content="Nobo - Femme de menage & repassage haut de gamme à domicile - Paris 8"/>
<meta property="og:description"     content="Trouvez en quelques clics une femme de ménage issue de l'hôtellerie. Dès 14€/h. Sans engagement. Assurance et gestion administrative incluses."/>
<meta property="fb:app_id"          content="430915960574732"/>
<meta property="og:image"           content="/img/paris/fb.png"/>
<!-- Google -->
<meta name="description" content="Trouvez en quelques clics une femme de ménage issue de l'hôtellerie. Dès 14€/h. Sans engagement. Assurance et gestion administrative incluses."/>
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Femme de menage dans le 8ème arrondissement de Paris (75008) | Nobo</title>
<link rel="stylesheet" href="/css/localization_style.css">

<?php include_once('../../inc/header_end.php'); ?>
<?php include_once('../../inc/navbar.php'); ?>
<?php include_once('../../inc/navbar_phone.php'); ?>

<section id="arrondissement-8eme">
    <div class="landing-wrapper landing-wrapper-dark">
        <div class="col-xs-12 title-white">
            <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png" alt="logo société nobo femme de ménage haut de gamme à domicile">
            <h1>Femme de menage dans le 8ème arrondissement de Paris (75008)</h1>
            <p class="hidden-xs">Dès 14€/h, votre femme de ménage issue d'hôtels 4 et 5 étoiles à Paris 8 ème</p>
        </div>
        <a href="/reservation/besoin"><button type="button" class="btn-stage btn-arr">Réserver maintenant</button></a>
    </div>
</section>

<div>
    <?php include_once ('../../inc/breadcrumbs.php'); ?>
</div>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Une <strong>femme de ménage</strong> alliant savoir-faire et savoir-être à <strong>Paris 8</strong> !</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Expérience hôtelière</h3>
                        <p class="nomarge">
                            Nobo est le premier service de réservation en ligne à vous proposer une 
                            <strong>femme de ménage</strong> ayant travaillée dans l'hôtellerie haut 
                            de gamme.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Sans engagement</h3>
                        <p class="nomarge">
                            Avec notre formule sans engagement et facturées à la carte, trouvez enfin la 
                            <strong>femme de ménage</strong> qu'il vous faut !
                            <br>
                            Notre service est accessible dès 14€/heure après crédit d'impôts de 50%. Il 
                            comprend la sélection de votre <strong>femme de menage</strong>, la gestion 
                            administrative, une assurance couvrant 300 000 euros par événement et la remise 
                            de votre attestation fiscale.                        
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Service client dédié</h3>
                        <p class="nomarge">
                            Un(e) gouvernant(e) supervise votre <strong>femme de ménage</strong> pour garantir 
                            un travail soigné à long terme. À votre écoute, il sera votre interlocuteur direct prêt à 
                            vous faciliter la vie à chaque instant !
                            <br>
                            Enfin, pour que votre <strong>femme de menage</strong> puisse être autonome, 
                            nous assurons la gardiennage gratuit de vos clés.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </div>
</section>

<section class="section-dark" id="fonctionnement">
        <div class="container">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2 class="fix-h2-small">Réservez facilement une <strong>femme de ménage</strong> à <strong>Paris 8</strong></h2>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/reservation-femme-de-menage.svg" alt="réservez votre femme de menage en ligne">
                        </div>
                        <p class="col-xs-12 text-sticker">1.Vous choisissez le créneau et la fréquence de votre <strong>femme de ménage</strong> en ligne</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/appel-femme-de-menage.svg" alt="Appel de votre gouvernant pour détailler vos envies">
                        </div>
                        <p class="col-xs-12 text-sticker">2.Nous détaillons vos besoins, puis sélectionnons avec vous la <strong>femme de ménage</strong> adéquate</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/planning-femme-de-menage.svg" alt="Rencontrez votre gouvernant et votre femme de ménage">
                        </div>
                        <p class="col-xs-12 text-sticker">3.Vous rencontrez votre gouvernant(e) et votre <strong>femme de ménage</strong> à domicile</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/paris/sticker/fonctionnement/premiere-prestation-femme-de-menage.svg" alt="1er essai de votre femme de ménage">
                        </div>
                        <p class="col-xs-12 text-sticker">4.Nous réalisons alors une première mission test de votre <strong>femme de ménage</strong></p>
                    </div>
                </div>
            </div>
             <p class="hidden-xs text-center" style="margin-top: 20px;">Toutes vos questions et leurs réponses se trouvent à la section
                <a class="link-gold underline" href="/menage-a-domicile/questions-frequentes"> questions fréquentes</a> de notre site internet !
        </div>
    </section>

<section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Vos voisins aiment déjà nos <strong>femmes de ménage</strong> - <strong>Paris 8</strong></h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Expérience incroyable. <br>Comme à l'hôtel !"</p>
                                <p>Damien, Miromesnil, Paris 8</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Damien client femme de ménage nobo paris 8" src="/img/paris/avis/femme-de-menage-paris-8-avis-damien.jpg" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"J'ai pu tester 2 personnes afin de trouver la mieux pour nous. Le ménage se passe maintenant très bien avec Éloïsa nous sommes ravis."</p>
                                <p>Agnès, Saint-Philippe-du-Roule, Paris 8</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo d'Agnes client femme de ménage et repassage nobo paris 8" src="/img/paris/avis/femme-de-menage-paris-8-avis-agnes.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Un service super innovant et une équipe très à l'écoute en cas de problème. On est fan de la première heure !"</p>
                                <p>Justine, Saint-Augustin, Paris 8</p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo Justine cliente femme de menage Paris 8" src="/img/paris/avis/femme-de-menage-paris-8-avis-justine.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2><strong>Femme de menage</strong> haut de gamme, tarif tout doux - <strong>Paris 8</strong></h2>
                    <p><a class="link-gold underline" href="/menage-a-domicile/femme-de-menage-deductible-des-impots">Crédit d'impôts</a> de 50% sur les prestations de votre <strong>femme de ménage</strong></p>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-stage">réserver maintenant</button></a>
                    <p>ou nous <a class="link-gold underline" href="/contact">contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="seo">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Les avantages d'une <strong>femme de menage</strong> Nobo dans le <strong>8</strong> ème à <strong>Paris</strong></h2>
                </div>

                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h3 class="titre-seo">Une qualité de service inégalée</h3>          
                    <p class="text-justify">
                        Bien plus que de vous proposer les services d'une <strong>femme de ménage</strong>, Nobo s'appuie sur un système qualité complet.
                        Nous analysons chaque CV pour valider l'expérience hôtelière du candidat, en recevons ensuite environ 3% en entretien, pour finalement en 
                        embaucher moins de 1% en moyenne à l'issue d'un exercice de mise en situation.
                        <br>
                        Cela nous assure que votre future <strong>femme de ménage</strong> aura déjà un niveau de compétences et de savoir-être répondant à de réelles 
                        exigences. Notre nouvelle recrue complète ce processus par une formation revoyant tous les standards du ménage complet d'un appartement, 
                        notamment l'entretien des matériaux précieux comme le marbre, le corian, le cuir ou l'argenterie. Notre nouvelle <strong>femme de ménage</strong>
                        apprend enfin certaines technique de repassage pour parfaire ses connaissances. Elle devient alors officiellement <strong>femme de ménage</strong>
                         Nobo et peut à ce titre faire partie de notre brigade !
                        <br>
                        <br>
                    </p>
                    <h3 class="titre-seo">Un accompagnement sur mesure</h3> 
                    <p class="text-justify">
                        Nous pensons que le service à la "personne" s'est largement déshumanisé ses 10 dernières années et comptons bien ne pas succomber à cette tendance.
                        Pour cela, nous avons construis notre service client de manière à être le plus proche de vous.
                        <br>
                        Pas de hotline téléphonique, mais un gouvernant à votre écoute et joignable en permanence pour suite votre <strong>femme de ménage</strong>. 
                        Il connaît personnellement votre domicile et vos petites habitudes, et effectuera régulièrement des inspections qualité de votre 
                        <strong>femme de menage</strong>.
                        <br>
                        Enfin, il permettra aussi de faire l'interface avec votre <strong>femme de ménage</strong> pour effectuer des remontées d'informations ou 
                        prévenir de changements à votre domicile. Le tout, pas sms, email ou téléphone !
                        <br>
                        <br>
                    </p>
                    <h3 class="titre-seo">Bien plus qu'une <strong>femme de ménage</strong></h3> 
                    <p class="text-justify">
                        Notre brigade de <strong>femmes de ménage</strong> est l'essence primaire de notre offre de service, mais cela s'étend bien plus loin et à pour
                        vocation de couvrir tous les pans de votre vie quotidienne.
                        <br>
                        <br>
                        Nous proposons en plus du <strong>ménage</strong>, de <span class="bold">l'entretien du linge</span>  et du <span class="bold">repassage à 
                        domicile :
                    </p>
                    <ul>
                        <li>Un service de pressing à domicile sans engagement. Retrait de vos vêtements lors de l'intervention de votre <strong>femme de ménage</strong>, puis dépôt
                        lors du passage suivant</li>
                        <li>Un service de cordonnerie à domicile à la carte. Retrait de vos chaussures lors de l'intervention de votre <strong>femme de ménage</strong>, puis dépôt
                        lors du passage suivant</li>
                        <li>Un service de livraison de fleurs pour embaumer votre appartement. Choisissez de 2 à 4 bouquets par mois, votre <strong>femme de ménage</strong> le
                        réceptionne, le met en eau, puis l'entretien lors de ces prochaines prestations</li>
                        <li>Un service de préparation et d'entretien ponctuel de votre appartement lors de location courtes durées (départ en vacances notamment)</li>
                        <li>Un service de conciergerie de vos clés dans des consignes électronique à proximité de chez vous à <strong>Paris 8</strong>. Valable pour vos besoins ponctuels ou prolongés.
                    </ul>
                    <p>
                        Avec Nobo, vous n'avez plus qu'à trouver de quoi occuper votre temps libre ! 
                        <br>
                        <br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark hidden-xs" id="info">
    <div class="container">
        <div class="row margeTopBottom-2">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2>Votre <strong>ménage</strong> entre de bonnes mains, découvrez d'autres services innovants 
                dans votre quartier de <strong>Paris 8</strong>:</h2>
            </div>
            <div class="col-xs-12 info-content">
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo Urb-it livreur de bonheur" src="/img/paris/activites/service-a-domicile-paris-8-urbit.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Achetez ou offrez ce que vous voulez en quelques clics</h2>
                        <p class="color-grey">
                            Grâce à Urb-it, vous êtes à quelques clics de vos boutiques et produits préférés à 
                            <strong>Paris 8</strong>. Avec votre application shopping Urb-it, toutes vos envies 
                            et besoins sont livrables, sans inconvénient, selon vos conditions, et en contribuant 
                            à un projet éco et socio responsable (livraison en vélo).
                        </p>
                        <p><span> - </span>Service disponible à <strong>Paris 8</strong> ème 7j/7 de 6h à minuit<span> - </span></p>
                    </div>
                </div>
                <div class="col-xs-12 info-row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="logo Epicery réparation d'objets connectés" src="/img/paris/activites/service-a-domicile-paris-8-epicery.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="color-gold">Les produits frais de vos commerçants enfin livrés !</h2>
                        <p class="color-grey">
                            Qui ne rêve pas de faire ses courses au marché ? Mais soyons réaliste, qui en a encore 
                            le temps ?! Grâce à Epicery, faites-vous livrer dans l'heure les produits frais de vos 
                            commerçants et artisans de quartier ! Le tout en ligne et au même prix qu'en boutique !
                        </p>
                        <p><span> - </span>Service disponible à <strong>Paris 8</strong> ème, livré dans l'heure<span> - </span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="link" class="hidden-xs">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Votre femme de ménage Nobo est disponible dans d'autres arrondissements :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <a href="Paris-1-er-75001"><p>Femme de menage Paris 1</p></a>
                    <p>Femme de menage Paris 2</p>
                    <p>Femme de menage Paris 3</p>
                    <p>Femme de menage Paris 4</p>
                    <p>Femme de menage Paris 5</p>
                    <a href="Paris-6-eme-75006"><p>Femme de menage Paris 6</p></a>
                    <a href="Paris-7-eme-75007"><p>Femme de menage Paris 7</p></a>
                    <a href="8"><p>Femme de menage Paris 8</p></a>
                    <p>Femme de menage Paris 9</p>
                    <p>Femme de menage Paris 10</p>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <p>Femme de menage Paris 11</p>
                    <p>Femme de menage Paris 12</p>
                    <p>Femme de menage Paris 13</p>
                    <p>Femme de menage Paris 14</p>
                    <a href="15"><p>Femme de menage Paris 15</p></a>
                    <a href="16"><p>Femme de menage Paris 16</p></a>
                    <a href="17"><p>Femme de menage Paris 17</p></a>
                    <p>Femme de menage Paris 18</p>
                    <p>Femme de menage Paris 19</p>
                    <p>Femme de menage Paris 20</p>
                </div>
            </div>
        </div>
</section>

<?php include_once ('../../inc/footer_start.php'); ?>
<?php include_once ('../../inc/exit_popup.php'); ?>
<?php include_once('../../inc/analyticstracking.php'); ?>
<?php include_once ('../../inc/footer_end.php'); ?>