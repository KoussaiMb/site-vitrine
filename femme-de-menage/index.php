<?php include_once('../inc/bootstrap.php'); ?>
<?php include_once('../inc/header_start.php'); ?>
    <!-- facebook preview -->
    <meta property="og:url"                 content="https://nobo.life/femme-de-menage"/>
    <meta property="og:type"                content="website"/>
    <meta property="og:title"               content="Nobo - Femme de ménage"/>
    <meta property="og:description"         content="Nobo - Plateforme de réservation en ligne de femme de ménage issues de l'hôtellerie de luxe"/>
    <meta property="fb:app_id"              content="430915960574732"/>
    <meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
    <!-- Google -->
    <meta name="description" content="Nobo - Réservez en ligne une femme de ménage issue de l'hôtellerie de luxe. Sans engagement, assurance et gestion administrative comprises.">
    <meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
    <!-- title -->
    <title>Nobo - Femme de ménage haut de gamme à domicile</title>
    <!-- CSS -->
    <!--<link rel="stylesheet" href="/css/lib/magnific-popup.css">-->
    <link rel="stylesheet" href="/css/public_style.css">
    <link rel="stylesheet" href="/css/lib/t-scroll.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<?php include_once('../inc/header_end.php'); ?>
<?php include_once('../inc/navbar.php'); ?>
<?php include_once('../inc/navbar_phone.php'); ?>

<section class="first-section section-dark">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 title-white text-center spaceTop">
                <h1>Femme de ménage</h1>
            </div>
        </div>
    </div>
</section>

<!-- paris -->
<section class="section-dark section-flyer">
    <div class="container">
        <div class="row section-chapeau">
            <div class="col-xs-12 text-center">
                <?php include_once('../inc/breadcrumbs.php'); ?>
            </div>
            <div class="chapeau-part col-xs-12">
                <div class="info-row">
                    <div class="col-xs-12 col-sm-6">
                        <h2>Femme de ménage dans Paris</h2>
                        <p>
                            Nobo assure l'entretien de votre appartement et de votre linge dans Paris.
                            <br>
                            <br>
                            Nos <strong>femmes de ménage</strong> sont exclusivement issues de l'hôtellerie avec des 
                            expériences en tant que femme de chambre dans des hôtels 4 et 5 étoiles.
                            Notre service est actuellement disponible dans tous les arrondissements de 
                            <strong>Paris</strong>.
                            <br>
                            <br>
                            <a href="/reservation/besoin">Réserver</a>
                            dès maintenant votre femme de ménage en ligne en quelques clics.
                            La gestion administrative de votre <strong>femmes de ménage</strong> est entièrement 
                            assurée par Nobo, et une assurance est également incluse !
                            <br>
                            <br>
                            Enfin, vous bénéficiez d'un <a href="/menage-a-domicile/femme-de-menage-deductible-des-impots"> crédit d'impôts</a>
                            de 50% sur l'ensemble de nos prestations grâce à notre déclaration service à la personne.
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 flex-col flex-center">
                        <a href="paris/"><button class="service-btn-blue">Femme de ménage Paris</button></a>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-sm-6 col-md-3 box-service hidden-xs">
                        <a href="paris/8">
                            <img class="img-responsive" src="/img/nobo/gallery/miniature-8eme.jpg" alt="photo miniature page femme de menage paris 8">
                            <div class="bg-h3">
                                <h3>Femme de ménage Paris 8</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 box-service">
                        <a href="paris/15">
                            <img class="img-responsive" src="/img/nobo/gallery/miniature-15eme.jpg" alt="photo miniature page femme de menage paris 15">
                            <div class="bg-h3">
                                <h3>Femme de ménage Paris 15</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 box-service">
                        <a href="paris/16">
                            <img class="img-responsive" src="/img/nobo/gallery/miniature-16eme.jpg" alt="photo miniature page femme de menage paris 16">
                            <div class="bg-h3">
                                <h3>Femme de ménage Paris 16</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 box-service hidden-xs">
                        <a href="paris/17">
                            <img class="img-responsive" src="/img/nobo/gallery/miniature-17eme.jpg" alt="photo miniature page femme de menage paris 17">
                            <div class="bg-h3">
                                <h3>Femme de ménage Paris 17</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- autres villes -->
<?php include_once ('../inc/footer_start.php'); ?>
<?php include_once ('../inc/exit_popup.php'); ?>
<?php include_once('../inc/analyticstracking.php'); ?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>
<?php include_once ('../inc/footer_end.php'); ?>
