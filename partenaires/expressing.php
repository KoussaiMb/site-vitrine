<?php
include_once ('../inc/bootstrap.php');

if (Session::getInstance()->hasFlash())
    $notif = Session::getInstance()->getFlash();

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken'))) {
    ApiCaller::addGuestApiTokenToSession('/', false);
}


//If Expressing give us some infos, we put it in recapInfo
$queryString = ['firstname', 'lastname', 'phone', 'email', 'code'];

foreach($queryString as $query) {
    if (isset($_GET[$query]))
        $res[$query] = $_GET[$query];
}

if (isset($res))
    Session::getInstance()->write('recapInfo', (object)$res);

include_once('../inc/header_start.php');
?>
<!-- title -->
<title>Nobo - patenaire expressing</title>
<!-- Google -->
<meta name="description" content="Nobo vous présente son partenaire expressing">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- facebook preview -->
<meta property="og:url"                 content="https://nobo.life/partenaires/expressing"/>
<meta property="og:type"                content="website"/>
<meta property="og:title"               content="Nobo - patenaire expressing"/>
<meta property="og:description"         content="Nobo vous présente son partenaire expressing"/>
<meta property="fb:app_id"              content="430915960574732"/>
<meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
<!-- CSS -->
<link rel="canonical" href="https://nobo.life" />
<link rel="stylesheet" href="/css/lib/magnific-popup.css">
<link rel="stylesheet" href="/css/public_style.css">
<link rel="stylesheet" href="/css/lib/t-scroll.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<?php include_once ('../inc/header_end.php'); ?>

    <div id="stage">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 intro-box text-center">
                    <div class="col-xs-12">
                        <img src="/img/nobo/logo/nobo-brand-logo-marque-white.png">
                        <p class="p-stage">L'hôtel à la maison</p>
                        <h1 class="h1-stage">Femme de ménage haut de gamme à domicile</h1>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <a href="/reservation/besoin"><button type="button" class="btn-stage-v2 btn-arr reservation">réserver maintenant</button></a>
                <div class="hidden-xs col-xs-12" id="scroller">
                    <p class="glyphicon glyphicon-chevron-down"></p>
                </div>
            </div>
        </div>
    </div>

    <section id="mission">
        <div class="container">
            <div class="row">
                <div class="section">
                    <div class="landing-wrapper">
                        <h2>Notre mission</h2>
                        <p class="hidden-xs">Moderniser le service de ménage et repassage de votre appartement</p>
                    </div>
                    <div class="col-xs-12 col-sm-10 col-xs-offset-0 col-sm-offset-1 pad-top text-center all-mission-wrap">
                        <div class="col-xs-12 col-sm-4 fadeUp mission-wrap">
                            <div class="col-xs-4 col-sm-12">
                                <img class="mission-img" src="/img/nobo/sticker/star-one-blue.svg" alt="star-one-blue">
                            </div>
                            <div class="col-xs-8 col-sm-12">
                                <h3 class="gold">Qualité</h3>
                                <p class="below-sticker valign">
                                    Du personnel d'exception, issu de l'hôtellerie de luxe, recruté en CDI
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 fadeUp mission-wrap">
                            <div class="col-xs-4 col-sm-12">
                                <img class="mission-img" src="/img/nobo/sticker/clock-blue-150.svg" alt="clock-blue">
                            </div>
                            <div class="col-xs-8 col-sm-12">
                                <h3 class="gold">Flexibilité</h3>
                                <p class="below-sticker valign">
                                    Facturé à l'heure, sans engagement et sans gestion administrative
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 fadeUp mission-wrap">
                            <div class="col-xs-4 col-sm-12">
                                <img class=" mission-img" src="/img/nobo/sticker/eye-blue.svg" alt="star-one-blue">
                            </div>
                            <div class="col-xs-8 col-sm-12">
                                <h3 class="gold">Suivi</h3>
                                <p class="below-sticker valign">
                                    Travail supervisé par un(e) gouvernant(e) personnel(le)
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="visible-xs fadeLeft" id="inter-link">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a href="/menage-a-domicile/fonctionnement" class="spaceTop btn-important">
                        Notre fonctionnement<span class="glyphicon glyphicon-chevron-right pull-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="section-transition section-transition-white"><div></div></div>

    <section class="section-dark" id="team">
        <div class="container">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Notre équipe</h2>
                    <p class="hidden-xs">Du directeur des opérations aux femmes de chambre</p>
                </div>
                <div class="col-xs-12 pad-top team-wrap-all">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-4 fadeUp">
                        <div class="col-xs-12 team-box team-box-main text-center">
                            <div class="col-xs-6 col-sm-12">
                                <img class="img-circle img-gouv" src="/img/nobo/gallery/directeur-des-operations-paris.png" alt="photo-COO-Sebastien">
                            </div>
                            <div class="col-xs-6 col-sm-12 spaceTop">
                                <h4>Sébastien</h4>
                                <span class="nopadding-xs-left-right seperate-name">-</span>
                                <a class="hidden" href="https://fr.linkedin.com/in/sébastien-mouton-9308605a" target="_blank"><img src="/img/nobo/sticker/linkedin-logo-circle-gold.png" alt="linkedin-circle-logo"></a>
                                <p class="bold nomarge">Gouvernant général <a class="hidden" href="https://fr.linkedin.com/in/sébastien-mouton-9308605a" target="_blank"><img src="/img/nobo/sticker/linkedin-logo-circle-gold.png" alt="linkedin-circle-logo"></a></p>
                            </div>
                            <div class="col-xs-12 spaceTop">
                                <p class="underline hidden-xs">Expérience</p>
                                <p class="col-xs-6">
                                    George V<br>
                                    Seabourn Sojourn
                                </p>
                                <p class="col-xs-6">
                                    Royal Monceau<br>
                                    Le Meurice
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 team-fdc">
                        <div class="col-xs-12 text-center fadeIn">
                            <p class="team-subtitle">Quelques unes de nos Femmes de chambre</p>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="1">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Soraya</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">200</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>George V</p>
                            </div>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="2">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Sylvie</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">30</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>Château de la tour</p>
                            </div>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="3">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Anita</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">150</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>Sofitel</p>
                            </div>
                        </div>
                        <div class="col-sm-12 team-box flex-row flex-arround-stretch text-center fadeLeft" data-t-show="4">
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <h4>Camelia</h4>
                            </div>
                            <div class="hidden-xs col-sm-4 flex-col flex-center">
                                <p>Déjà <span class="bold">80</span> prestations</p>
                            </div>
                            <div class="col-xs-6 col-sm-4 flex-col flex-center">
                                <p class="underline hidden-xs">Expérience notable</p>
                                <p>Hôtel de Bourbon</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden-xs section-transition section-transition-blue" style="margin-bottom: -1em;"><div></div></div>

    <section id="pricing">
        <div class="container">
            <div class="row">
                <div class="section">
                    <div class="landing-wrapper">
                        <h2>Nos tarifs</h2>
                    </div>
                    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                        <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                            <div class="cnrflash">
                                <div class="cnrflash-inner">
                                    <span class="cnrflash-label">Populaire</span>
                                </div>
                            </div>
                            <div class="n_t-top bg-gold">
                                <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                            </div>
                            <div class="n_t-bottom n_t-bottom-wrap">
                                <p class="n_t-detail-price gold">28 €/h</p>
                                <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                            </div>
                        </div>
                        <div class="col-sm-1 hidden-xs"><p></p></div>
                        <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                            <div class="n_t-top bg-blue-light">
                                <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                            </div>
                            <div class="n_t-bottom n_t-bottom-wrap">
                                <p class="n_t-detail-price blue">32 €/h</p>
                                <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                            </div>
                        </div>
                    </div>
                    <!--TODO: PRIX EXPRESSING -->
                    <style>
                        .table-expressing th{
                            width: 33%;
                        }
                        .table-container {
                            border: 3px dotted rgba(0,0,0,0.3);
                        }
                        .table-container .table-expressing {
                            border: none;
                        }
                    </style>
                    <div class="text-center col-xs-12">
                        <h2>Vos avantages Expressing</h2>
                        <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 text-center table-container">
                            <table class="table table-responsive table-expressing">
                                <thead class="text-center">
                                <tr>
                                    <th class="text-center">Vous réservez</th>
                                    <th class="text-center">On vous offre</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>2h30</td>
                                    <td>10 minutes</td>
                                    <td>2h40</td>
                                </tr>
                                <tr>
                                    <td>3h00</td>
                                    <td>20 minutes</td>
                                    <td>3h20</td>
                                </tr>
                                <tr>
                                    <td>4h00</td>
                                    <td>30 minutes</td>
                                    <td>4h30</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center spaceTop">
                        <a href="/reservation/besoin"><button class="btn-outline-blue">réserver</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="visible-xs border-transition-blue"></div>
    <div class="hidden-xs section-transition section-transition-white"><div></div></div>

    <section class="hidden-xs section-dark" id="fonctionnement">
        <div class="container">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2 class="fix-h2-small">Notre fonctionnement</h2>
                </div>
                <div class="col-xs-12 pad-top">
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/reservation-gold-120.svg" alt="phone-call-gold">
                        </div>
                        <p class="col-xs-12">Réservation en ligne sécurisée</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/phone-call-gold.svg" alt="phone-call-gold">
                        </div>
                        <p class="col-xs-12">Appel d'un réceptionniste pour établir vos besoins</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeDown">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/gouvernant-premiere-visite-gold.svg" alt="gouvernant-gold">
                        </div>
                        <p class="col-xs-12">Visite de votre gouvernant(e) pour créer votre planning d'entretien</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center fadeUp">
                        <div class="func-sticker valign">
                            <img src="/img/nobo/sticker/fonctionnement/house-cleaned-gold.svg" alt="phone-call-gold">
                        </div>
                        <p class="col-xs-12">Première prestation de votre femme de chambre</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden-xs section-transition section-transition-blue"><div></div></div>

    <section id="voisin">
        <div class="container-fluid">
            <div class="row">
                <div class="section">
                    <div class="landing-wrapper">
                        <h2>Ils nous ont fait confiance</h2>
                    </div>
                    <div class="col-xs-12 voisin-section">
                        <div id="feedback-carousel-sm" class="carousel slide hidden-xs" data-ride="carousel">
                            <div class="carousel-inner feedback-carousel">
                                <div class="item item-space active">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                            "Toujours aussi bien après plusieurs mois de prestations,
                                            ce qui est très rare malheureusement.
                                            Je recommande pour leur sérieux et l’attention portée aux clients."
                                            </p>
                                            <p>Margaux, Convention, <strong>Paris 15</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo de Margaux cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-margaux.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Nobo, c'est un service à la carte et on ne peut plus professionnel. Eric et son équipe sont formidables et mettront tout en œuvre pour vous satisfaire. L'hôtel à la maison, une promesse tenue!!"</p>
                                            <p>Marc, Ternes, <strong>Paris 17</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Marc client femme de menage et repassage paris 17" src="/img/paris/avis/femme-de-menage-paris-17-avis-marc.jpg" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item item-space">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Super service, le ménage est parfait et les petites attentions font qu’on
                                            se sent vraiment traité comme à l’hôtel, c’est top bravo!"</p>
                                            <p>Emmanuelle, Beaugrenelle, <strong>Paris 15</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo d'Emmanuelle cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-emmanuelle.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Un service super innovant et une équipe très à l'écoute en cas de problème. On est fan de la première heure !"</p>
                                            <p>Justine, Saint-Augustin, <strong>Paris 8</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Justine cliente femme de menage Paris 8" src="/img/paris/avis/femme-de-menage-paris-8-avis-justine.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item item-space">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Un service de très grande qualité. J'ai pu tisser de vrais liens de confiance avec l'équipe et les recommande chaleureusement."</p>
                                            <p>Danièle, Jasmin, <strong>Paris 16</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Daniele cliente menage repassage paris 16" src="/img/paris/avis/femme-de-menage-paris-16-avis-daniele.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Bluffé par la qualité du ménage ! Digne d'un grand hôtel
                                            Hâte d'essayer leurs autres service !!"</p>
                                            <p>Jonathan, La Muette, <strong>Paris 16</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo de Jonathan client ménage nobo" src="/img/paris/avis/femme-de-menage-paris-16-avis-jonathan.jpg" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item item-space">
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"J'utilise Nobo 4h par semaine depuis 1 an. J'ai pu garder la même <strong>femme de menage</strong> depuis le début et le travail est nickel."</p>
                                            <p>Carole, Villiers, <strong>Paris 17</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Carole cliente femme de ménage nobo paris 17" src="/img/paris/avis/femme-de-menage-paris-17-avis-carole.png" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="voisin-wrap col-xs-6">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                                "Très bon service, facilement joignable
                                                au téléphone + un réel suivi. Je recommande !"
                                            </p>
                                            <p>Xavier, Sèvres-Lecourbes, <strong>Paris 15</strong></p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo Xavier client menage paris 15" src="/img/paris/avis/femme-de-menage-paris-15-avis-xavier.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a data-slide="prev" href="#feedback-carousel-sm" class="left carousel-control"><i class="glyphicon glyphicon-chevron-left"></i></a>
                            <a data-slide="next" href="#feedback-carousel-sm" class="right carousel-control"><i class="glyphicon glyphicon-chevron-right"></i></a>
                        </div>
                        <div id="feedback-carousel-xs" class="carousel slide visible-xs" data-ride="carousel">
                            <div class="carousel-inner feedback-carousel">
                                <div class="item active">
                                    <div class="voisin-wrap col-xs-12">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>"Le bonheur de rentrer chez soi.
                                                Tout est dans le détail et Nobo l'a bien compris !"</p>
                                            <p>Aurélia D.S. – Cliente depuis Aout 2016</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo-profil-avis-client-aureliaDS" src="/img/nobo/misc/avis-client-aureliaDS.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="voisin-wrap col-xs-12">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                                "Le service est de qualité et sur mesure. Il n'y a
                                                rien à ajouter, c'est parfait ! Merci Nobo."
                                            </p>
                                            <p>Alice R. – Cliente depuis Janvier 2017</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img src="/img/nobo/misc/avis-client-aliceR.png" alt="photo-profil-avis-client-aliceR" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-xs-12 voisin-wrap">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>
                                                "Service impeccable. L'hôtel à la maison c'est
                                                vraiment le pied ! Je le recommande !"
                                            </p>
                                            <p>Hannah V. – Cliente depuis Décembre 2016</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo-profil-avis-client-hannahV" src="/img/nobo/misc/avis-client-hannahV.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-xs-12 voisin-wrap">
                                        <div class="voisin-box flex-col flex-between">
                                            <p>Service excellent, de qualité et petite surprise à notre retour !</p>
                                            <p>Tino I. – Client depuis Septembre 2016</p>
                                        </div>
                                        <div class="voisin-arrow"><div></div></div>
                                        <div>
                                            <img alt="photo-profil-avis-client-tinoI" src="/img/nobo/misc/avis-client-tinoI.png" class="img-circle">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a data-slide="prev" href="#feedback-carousel-xs" class="left carousel-control"><i class="glyphicon glyphicon-chevron-left"></i></a>
                            <a data-slide="next" href="#feedback-carousel-xs" class="right carousel-control"><i class="glyphicon glyphicon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .ci-boxes > div > div {
            border: 2px solid grey;
            background-color: #EEF2F6;
            color: #0b0b0b;
            padding: 10px 10px 0 10px;
        }
        .ci-boxes p{
            flex: 1;
        }
        .ci-boxes > div > div{
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-align-content: space-between;
            -ms-flex-line-pack: justify;
            align-content: space-between;
            -webkit-align-items: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
        }
        .ci-boxes h5{
            color: #C5A547;
            font-weight: bold;
        }
        .ci-boxes img {
            width: 30%;
        }
        @media only screen and (max-width: 767px) {
            .ci-boxes > div {
                margin-bottom: 1.5em;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-flex-direction: row;
                -ms-flex-direction: row;
                flex-direction: row;
                -webkit-flex-wrap: nowrap;
                -ms-flex-wrap: nowrap;
                flex-wrap: nowrap;
                -webkit-justify-content: space-between;
                justify-content: space-between;
                -webkit-align-content: center;
                -ms-flex-line-pack: center;
                align-content: center;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
            }
            .ci-boxes img {
                padding-right: 6px;
            }
        }
        @media only screen and (min-width: 768px) {
            .ci-boxes {
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-flex-direction: row;
                -ms-flex-direction: row;
                flex-direction: row;
                -webkit-flex-wrap: nowrap;
                -ms-flex-wrap: nowrap;
                flex-wrap: nowrap;
                -webkit-justify-content: space-around;
                justify-content: space-around;
                -webkit-align-content: stretch;
                -ms-flex-line-pack: stretch;
                align-content: stretch;
                -webkit-align-items: stretch;
                -ms-flex-align: stretch;
                align-items: stretch;
            }
            .ci-boxes > div > div:last-child{
                height: 120px;
            }
            .ci-boxes > div {
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column;
                -webkit-flex-wrap: nowrap;
                -ms-flex-wrap: nowrap;
                flex-wrap: nowrap;
                -webkit-justify-content: space-between;
                justify-content: space-between;
                -webkit-align-content: center;
                -ms-flex-line-pack: center;
                align-content: center;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
            }
            .ci-boxes img {
                padding-bottom: 30px;
            }
        }
    </style>
    <section class="section-dark">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h2>Le crédit d'impôt</h2>
                    </div>
                    <div class="ci-boxes section">
                        <div class="col-xs-12 col-sm-4">
                            <img class="img-responsive" src="/img/nobo/sticker/credit-impot-nobo.png" alt="sticker avec le chiffre 1">
                            <div class="col-xs-12">
                                <h5>50% de crédit d’impôt</h5>
                                <p>Avec Nobo, vous bénéficiez donc de 50% de crédit d’impôt sur nos prestations</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <img class="img-responsive" src="/img/nobo/sticker/credit-impot-menage.png" alt="sticker avec le chiffre 2">
                            <div class="col-xs-12">
                                <h5>Nobo s’occupe de la paperasse</h5>
                                <p>Nobo vous envoie votre attestation fiscale</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <img class="img-responsive" src="/img/nobo/sticker/credit-impot-paris.png" alt="sticker avec le chiffre 3">
                            <div class="col-xs-12">
                                <h5>Votre feuille d’impôt</h5>
                                <p>Vous n’avez plus qu’à reporter le montant dans la case mentionnée dans votre attestation</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<style>
    .faq-carousel {
        height: 430px;
    }
    @media only screen and (min-width: 768px) {
        .faq-carousel {
            height: 330px;
        }
    }
    .faq-carousel .carousel-control .glyphicon-chevron-left,
    .faq-carousel .carousel-control .glyphicon-chevron-right {
        color: #27374D;
    }
    ol.carousel-indicators > li.active{
        background-color: #27374D;
    }
    ol.carousel-indicators > li {
        border-color: #27374D;
    }
    .triangle-border {
        position:relative;
        padding:15px;
        margin:1em 0 3em;
        border:2px solid #27374D;
        color:#333;
        background:#fff;
        /* css3 */
        -webkit-border-radius:10px;
        -moz-border-radius:10px;
        border-radius:10px;
    }
    .triangle-border:before {
        content:"";
        position:absolute;
        bottom:-20px; /* value = - border-top-width - border-bottom-width */
        left:40px; /* controls horizontal position */
        border-width:20px 20px 0;
        border-style:solid;
        border-color: #27374D transparent;
        /* reduce the damage in FF3.0 */
        display:block;
        width:0;
    }
    /* creates the smaller  triangle */
    .triangle-border:after {
        content:"";
        position:absolute;
        bottom:-17px; /* value = - border-top-width - border-bottom-width */
        left:42px; /* value = (:before left) + (:before border-left) - (:after border-left) */
        border-width:18px 18px 0;
        border-style:solid;
        border-color:#fff transparent;
        /* reduce the damage in FF3.0 */
        display:block;
        width:0;
    }
    .triangle-border.top:before {
        top:-20px; /* value = - border-top-width - border-bottom-width */
        bottom:auto;
        left:auto;
        right:40px; /* controls horizontal position */
        border-width:0 20px 20px;
    }
    .triangle-border.top:after {
        top:-17px; /* value = - border-top-width - border-bottom-width */
        bottom:auto;
        left:auto;
        right:42px; /* value = (:before right) + (:before border-right) - (:after border-right) */
        border-width:0 18px 18px;
    }
</style>
    <section class="" id="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h2>FAQ</h2>
                    </div>
                    <div class="col-xs-12">
                        <div id="faq-carousel-sm" class="carousel slide faq-carousel" data-ride="carousel" data-interval="false">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#faq-carousel-sm" data-slide-to="0" class="active"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="1"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="2"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="3"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="4"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="5"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="6"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="7"></li>
                                <li data-target="#faq-carousel-sm" data-slide-to="8"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Aurais-je toujours le même prestataire à la maison ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Oui, nous vous affectons une femme de ménage qui restera la même
                                                dans le temps, sauf demande de votre part.
                                                <span class="hidden-xs">
                                                    Cependant, en cas de congés ou de maladie, nous vous enverrons
                                                    exceptionnellement une autre femme de ménage avec votre consentement.
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Dois-je fournir les produits d'entretien ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Oui ! Afin de nous adapter à vos habitudes, nos femmes de ménage
                                                utiliseront vos produits d'entretien.
                                                <span class="hidden-xs">
                                                    Cependant, nous pourrons vous transmettre une liste élaborée
                                                    par notre gouvernant général de produits essentiels pour
                                                    l'entretien de votre domicile.
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Peut-on résilier le service Nobo ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Oui ! Notre service est sans engagement et vous êtes donc
                                                libres de suspendre les prestations de votre femme de ménage<span class="hidden-xs"> quand vous le souhaitez</span>.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Quels sont les frais d'inscription/résiliation ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Il n'y en a aucun !
                                                <br>
                                                Chez Nobo, nous considérons que l'accès
                                                au service aussi bien que sa résiliation n'ont
                                                pas de raison d'être facturé.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Comment sont sélectionnées vos femmes de ménage ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                <span class="hidden-xs">Notre processus de recrutement est un de nos atouts.</span>
                                                Nous recevons des centaines de CV par mois, on en sélectionne environ
                                                3% pour un entretien, puis 1% pour un test grandeur nature.
                                                <span class="hidden-xs">Un appel de référence est systématiquement fait afin de valider les
                                                qualités de chaque femme de ménage. Et pour finir, la femme de ménage
                                                suivra une de nos formations.</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Suis-je le particulier employeur de ma femme de ménage ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Non ! Nobo est prestataire de services, ce qui signifie que
                                                vous n'achetez que des heures de service.
                                                <span class="hidden-xs">
                                                    Cela vous dédouane
                                                    totalement de toutes démarches administratives, ainsi que des
                                                    responsabilités qui y sont liées.
                                                </span>
                                                Nos femmes de ménage sont toutes employées en CDI à temps plein.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Puis-je annuler mes prestations pendant mes vacances ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Oui, vous pouvez <span class="hidden-xs"> sans problème </span>suspendre les interventions
                                                de votre femme de ménage <span class="hidden-xs"> pendant vos congés </span>si vous le souhaitez !
                                                Nous exigeons cependant 7 jours de délai pour une annulation sans frais.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Dois-je être sur place pendant les missions ?</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Non, notre service de gardiennage des clés vous permet de ne
                                                plus vous préoccuper de cette problématique. Votre femme de
                                                ménage sera ainsi parfaitement autonome.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                     <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-6 text-left">
                                         <div class="triangle-border">
                                             <p>Et si je ne veux pas laisser mes clés ?!</p>
                                         </div>
                                     </div>
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-left">
                                        <div class="triangle-border top">
                                            <p>
                                                Si vous le souhaitez, vous pouvez laisser vos clés à
                                                la gardienne ou systématiquement être sur place pour nous ouvrir.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#faq-carousel-sm" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#faq-carousel-sm" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include_once ('../inc/footer_start.php'); ?>
<?php include_once('../inc/analyticstracking.php'); ?>
<script src="/js/lib/t-scroll.min.js"></script>
<script>
    $(document).ready(function () {
        // API call newsletter
        $('#newsletterValidation').on('click', function () {
            var name = $('#name');
            var email = $('#email');

            $.ajax({
                beforeSend: function(request) {
                    var apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/newsletters'; ?>",
                type: 'POST',
                data: {name: name.val(), email: email.val()},
                dataType: 'json',
                success: function (ans) {
                    var messages = ans.message;

                    messages.forEach(function (message) {
                        $.notify({message: message},{type: 'success'});
                    });
                    name.val("");
                    email.val("");
                },
                error: function (jqXHR) {
                    var messages = $.parseJSON(jqXHR.responseText).message;

                    messages.forEach(function (message) {
                        $.notify({message: message},{type: 'danger'});
                    });
                }
            });
        });

        var notifs = <?= empty($notif) ? json_encode(null) : json_encode($notif); ?>;

        if (notifs) {
            var keys = Object.keys(notifs);
            var values = Object.values(notifs);

            for (var i = 0, len = keys.length; i < len; i++) {
                $.notify({message: values[i]},{type: keys[i]});
            }
        }
        $('#feedback-carousel-sm').carousel({
            pause: 'hover',
            interval: 10000
        });
        $('#feedback-carousel-xs').carousel({
            pause: 'hover',
            interval: 10000
        });
        //Listeners
        $("#scroller").click(function() {
            $('html,body').animate({scrollTop: $("#mission").offset().top}, 'slow');
        });
        Tu.tScroll({'t-element': '.fadeUp'});
        Tu.tScroll({'t-element': '.fadeIn'});
        Tu.tScroll({'t-element': '.fadeLeft'});
        Tu.tScroll({'t-element': '.fadeDown'});
    });
</script>
<div class="col-xs-12 footer">
    <div class="col-xs-12 col-sm-3">
        <div class="log-icon">
            <a href="tel:<?= $_SERVER['HTTP_PHONE_NOBO'] ?>">
                <img src="/img/nobo/logo/social/bell-blue.svg" alt="logo-bell-blue">
                <img src="/img/nobo/logo/social/bell-gold.svg" alt="logo-bell-gold">
            </a>
            <a href="mailto:contact@nobo.life">
                <img src="/img/nobo/logo/social/email-blue.svg" alt="logo-email-blue">
                <img src="/img/nobo/logo/social/email-gold.svg" alt="logo-email-gold">
            </a>
            <a href="http://www.instagram.com/nobofrance">
                <img src="/img/nobo/logo/social/instagram-blue.svg" alt="logo-instagram-blue">
                <img src="/img/nobo/logo/social/instagram-gold.svg" alt="logo-instagram-gold">
            </a>
            <a href="http://www.facebook.com/nobofrance">
                <img src="/img/nobo/logo/social/facebook-blue.svg" alt="logo-facebook-blue">
                <img src="/img/nobo/logo/social/facebook-gold.svg" alt="logo-facebook-gold">
            </a>
        </div>
        <div class="col-xs-12 footer-logo">
            <a href="#"><img class="img-responsive" src="/img/nobo/logo/nobo-logo-paris-blue-darker.png" alt="logo nobo bleu sombre"></a>
        </div>
        <p><a href="/mentions-legales">Conditions générales d'utilisation</a></p>
    </div>
    <div class="col-xs-12 col-sm-3 right-aligned">
    </div>
    <div class="col-xs-12 col-sm-3 left-aligned">
    </div>
    <div class="col-xs-12 col-sm-3 footer-awards">
        <div class="col-xs-12 text-center">
            <a href="http://reseau-entreprendre.org" rel="follow" target="_blank"><img src="/img/nobo/logo/awards/laureat-reseau-entreprendre-paris.png" alt="logo des lauréats du réseau entreprendre paris"></a>
        </div>
    </div>
    <div class="col-xs-12">
        <p>© 2017</p>
    </div>
</div>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <!-- Hotjar Tracking Code for https://www.nobo.life -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:666727,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
<?php endif; ?>
</body>
</html>
