<?php include_once ('inc/bootstrap.php'); ?>
<?php include_once('inc/header_start.php'); ?>
    <title>Nobo - mentions legales</title>
<?php include_once('inc/header_end.php'); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <b>Conditions Générales d’Utilisation de la plateforme NOBO pour l’Utilisateur
            Ou « CGU Utilisateur »</b>
        </div>
        <div class="col-xs-12">
            <p>
                <u>I. Généralités</u>
            </p>
            <p>
                Les prestations de la société D&L Services sont soumises à la fiche Tarifs ainsi qu’aux présentes conditions générales, qui prévalent sur tout autre accord préalable écrit ou oral. Le client faisant appel à nos prestations accepte sans réserve l’intégralité des clauses et conditions des présentes et reconnait avoir reçu la fiche tarifaire à jour.
                La société D&L Services SAS, dont le siège social est situé au 18 boulevard Montmartre, 75009, enregistrée au Registre du Commerce et des Sociétés de Paris sous le numéro 820 890 739, ci-après dénommée (« NOBO ») exploitant la plateforme de mise en relation www.nobo.life. (« Plateforme » ou « Site (internet) »).
            </p>
            <p class="sideStep">
                A. Agrément :
            </p>
            <p>
                La société D&L Services a obtenu pour la région Ile de France une autorisation permettant à ses clients d’obtenir une réduction ou un crédit d’impôts, sous réserve de la modification de la législation. Cette réduction ou ce crédit d’impôts sont équivalents à 50% des sommes versées dans les limites fixées par l’article 199.sexdecies du CGI.
            </p>
            <p class="sideStep">
                B. Attestation fiscale :
            </p>
            <p>
                La société Nobo, conformément à la législation en vigueur sur le principe de la déduction fiscale associée aux services à domicile, s’engage à adresser au client une attestation fiscale lui permettant de bénéficier de la réduction ou du crédit d’impôts. Il est expressément convenu entre les parties que, conformément à l’article 199.sexdecies du CGI, seules les factures effectivement encaissées par la société Nobo ouvrent droit à la réduction ou au crédit d’impôts précités et que seules les sommes correspondant aux heures de prestations ouvrent droit à réductions d’impôts.
            </p>
            <p>
                <u>II. Modalités d’exécution</u>
            </p>
            <p class="sideStep">
                A. Intervenant :
            </p>
            <p>
                Durant l’exécution de son travail, ou lors du rendez-vous préalable l’intervenant(e) reste sous la responsabilité exclusive de la société Nobo. L’intervenant(s) ne peut recevoir du client une quelconque délégation de pouvoir relative aux avoirs, biens ou droits du client, quelle qu’en soit la nature, y compris fonds, bijoux ou autres valeurs. Il est formellement interdit au client d’engager comme salarié un intervenant présenté par la société Nobo. Au cas où le client ne respecterait pas cette obligation, la société se réserve le droit de réclamer une indemnité pour le préjudice subi. Cette indemnité pourra être d’un montant de 5 000 €.
            </p>
            <p class="sideStep">
                B. Fourniture du matériel et des produits d’entretien :
            </p>
            <p>
                Le matériel ainsi que les produits d’entretien indispensables à l’exécution des services sollicités auprès de la société Nobo sont exclusivement fournis par le client. Celui-ci s’engage à fournir des matériels et produits d’entretien conformes à la législation en vigueur. Une liste de produits constituant un panier d’entretien pourra être transmis au client afin de guider le client dans ses achats de produits et matériel sans pour autant l’en obliger.
            </p>
            <p class="sideStep">
                C. Durée de l’intervention :
            </p>
            <p>
                La durée minimum d'intervention est de 3 heures.
            </p>
            <p class="sideStep">
                D. Début des prestations :
            </p>
            <p>
                La première prestation ne peut avoir lieu sans que la commande n’ait été réalisée sur notre site internet, et réglée par carte bleue en ligne. Le fait de bénéficier de la première prestation entraîne l’acceptation des présentes conditions générales par le client.
            </p>
            <p class="sideStep">
                E. Planification des interventions :
            </p>
            <p>
                Les services sont commandés au moins 48 heures à l’avance et planifiés à la réception de la commande, sous réserve de personnel disponible. Les dates et les horaires d’intervention sont convenus par téléphone ou par mail entre le client et la société.
            </p>
            <p class="sideStep">
                F. Fonctionnement des prestations d’entretien en récurrent et en ponctuel :
            </p>
            <p>
                Lors de la commande d’une ou plusieurs prestations sur la plateforme web, le client se voit recevoir un appel téléphonique du support afin de répondre aux questions éventuelles puis de convenir d’un premier rendez-vous avec la gouvernante. Lors de ce premier rendez-vous, la gouvernante expliquera en détail le fonctionnement du service puis établira un diagnostic de l’appartement, des besoins du client ainsi que de ses habitudes. A la suite du premier rendez-vous une femme ou un valet de chambre sera attribué au client en fonction de la localisation géographique et de l’emploi du temps des femmes et valets dans la même zone géographique. Un suivi client sera mis en place par la gouvernante afin de bien vérifier la qualité du service rendu. A la fin de chaque prestation le client aura la possibilité de noter la prestation.
            </p>
            <p>
                Les clients ayant souscrit à un service récurrent recevront des prestations de services de conforts en fonction de leurs attentes cernées lors du premier rendez-vous avec la gouvernante à intervalle convenu lors de la réservation de la commande.
            </p>
            <p class="sideStep">
                G. Annulation ou modification d’une intervention :
            </p>
            <p>
                La commande d’une prestation peut être modifiée ou annulée sans frais 7 jours calendaires
                avant la prochaine prestation. En cas d’annulation entre 7 jours calendaires et 72 heures
                avant la prochaine prestation, des frais d’annulation seront appliqués à hauteur de 50%
                du prix de la prestation.
                En cas d’annulation de la prestation dans un délai inférieur à 72 heures ou si
                l’intervenant ne peut effectuer le(s) service(s) du fait du client, pour quelque raison
                que ce soit, la prestation est considérée comme due, au tarif habituel du client.
                La société Nobo s’engage à fournir un intervenant sous réserve de personnel disponible aux dates
                et heures convenues au téléphone. En cas d’impossibilité de satisfaire le client aux dates
                et heures initialement convenues, la société Nobo s’engage à contacter le client dans les
                plus brefs délais afin de convenir d’un horaire différent.
            </p>
            <p>
                <u>III. Prix et modalités de paiement</u>
            </p>
            <p class="sideStep">
                A. Contenu des factures :
            </p>
            <p>
                Toute facture précise la nature exacte des services fournis, le décompte du temps passé,
                le taux horaire, le montant des sommes effectivement encaissées, ou à acquitter et le
                numéro de l’autorisation de la société Nobo et le nom et l’adresse du client. Le prix de
                toute heure de prestation est net pour le client. Il tient compte du salaire de
                l’intervenant (charges sociales incluses), de la TVA et des frais administratifs.
            </p>
            <p class="sideStep">
                B. Révision :
            </p>
            <p>
                Les tarifs sont révisables annuellement par la société Nobo qui s’engage à communiquer
                ses nouveaux tarifs au moins 1 mois avant leur entrée en vigueur, notamment en
                cas d’évolution de la législation sociale ou fiscale.
            </p>
            <p class="sideStep">
                C. Délai de paiement :
            </p>
            <p>
                Le règlement des prestations s’effectue par débit des coordonnées bancaires
                enregistrées sur la plateforme Stripe, société spécialisée dans le paiement en ligne sécurisé.
                Le paiement des prestations est automatiquement débité pour les clients récurrents directement
                après la prestation. Pour les clients ponctuels, le paiement de la prestation sera du
                avant la prestation au moins 24 heures à l’avance. Tout retard de paiement à la
                charge du client (moyen de paiement ou oubli) entrainera le paiement
                d’intérêt de 20% par an de retard.
            </p>
            <p class="sideStep">
                D. Modes de règlement :
            </p>
            <p>
                Tout règlement ne peut être opéré que par carte bleue. Tout défaut de paiement entraînera la suspension immédiate de toute nouvelle prestation, les sommes versées à la société Nobo lui restant acquises jusqu’à parfait paiement par le client des sommes dues.
            </p>
            <p class="sideStep">
                E. Remboursement :
            </p>
            <p>
                Les prestations peuvent être remboursées en cas de faute grave de l’intervenant. Nobo effectue les remboursements en utilisant le même moyen de paiement que celui utilisé par le consommateur pour la transaction initiale, sauf accord exprès du consommateur pour qu'il utilise un autre moyen de paiement et dans la mesure où le remboursement n'occasionne pas de frais pour le consommateur.
            </p>
            <p>
                <u>IV. Contestation</u>
            </p>
            <p>
                Malgré tous nos efforts pour satisfaire le désir et les exigences du client, dans l’hypothèse où la prestation effectuée ne répondrait pas entièrement à ses attentes, celui-ci peut faire état de sa réclamation par téléphone ou par email dans un délai de 48 heures maximum suivant l’exécution de la prestation.
            </p>
            <p>
                <u>V. Assurance - Responsabilité</u>
            </p>
            <p>
                La société Nobo déclare être assurée pour les dommages causés par ses salariés au domicile des clients sous réserve de la franchise qui reste à la charge du client. La société Nobo ne saurait être tenue pour responsable des dommages dus à la défectuosité du matériel ou des produits d’entretien fournis par le client. Le client déclare formellement être assuré pour les dommages causés aux tiers. Tout dommage constaté devra nous être signalé par courrier recommandé avec accusé de réception dans les 72 heures suivant la prestation lors de laquelle il a eu lieu.
            </p>
            <p>
                <u>VI. Fin du contrat</u>
            </p>
            <p>
                Suspension temporaire et arrêt définitif des prestations régulières : Si le client souhaite
                interrompre de manière définitive ou temporaire les prestations, il lui faut faire
                parvenir un courrier, un fax ou un e-mail, Les mêmes délais que
                les conditions d’annulation sont applicabes.
            </p>
            <p>
                Délai de rétractation : Conformément aux dispositions de l’article L. 121-21 du code de
                la consommation, en cas de souscription au service via l’utilisation d’une
                ou plusieurs techniques de commercialisation à distance, sans la présence physique
                simultanée des parties, le client dispose d’un délai de 14 jours pour exercer son
                droit de rétractation sans avoir à justifier de motif ni à payer de pénalités.
                Dans le cas où le client exercerait son droit de rétractation, celui-ci accepte
                d’être facturé des prestations exécutées jusqu’à la date d’exercice de celui-ci.
            </p>
            <p>
                Note : Les informations recueillies sont nécessaires à l’ouverture de tout dossier.
                Elles font l’objet d’un traitement informatique. Conformément à la loi
                « Informatique et libertés » du 6 janvier 1978, vous bénéficiez d’un droit d’accès et
                de rectification aux informations qui vous concernent.
            </p>
        </div>
    </div>
</div>
<style>
    .sideStep {
        margin-left: 2em;
    }
</style>
<?php
include_once('inc/footer_start.php');
include_once('inc/analyticstracking.php');
?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>
<?php
include_once('inc/footer_end.php');
