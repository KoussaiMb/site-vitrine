<?php require('inc/bootstrap.php'); ?>
<?php include_once('inc/header_start.php');?>

<!-- facebook preview -->
<meta property="og:url"                 content="https://nobo.life/plan-du-site"/>
<meta property="og:type"                content="website"/>
<meta property="og:title"               content="Nobo - plan du site, ménage et du repassage chez vous"/>
<meta property="og:description"         content="Nobo est la seule plateforme à proposer les services de personnel hôtelier à domicile. Entretien 5 étoiles garanti à chaque passage."/>
<meta property="fb:app_id"              content="430915960574732"/>
<meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
<!-- Google -->
<meta name="description" content="Ménage haut de gamme pour appartement exigeant.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Nobo - plan du site, ménage et du repassage chez vous</title>
<!-- CSS -->
<link rel="stylesheet" href="css/public_style.css">

<?php include_once ('inc/header_end.php'); ?>
<?php include_once ('inc/navbar.php'); ?>

<section class="section-dark">
    <div class="container">
        <div class="col-xs-12 planSite-header">
            <h1 class="gold">Plan du site</h1>
        </div>
        <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 planSite-content">
        <?php
        /*
         * Directory
         */

        $nobo = scandir('.');

        echo "<h2>Nobo</h2>";
        echo "<p><a href='/'>Accueil</a></p>";

        foreach ($nobo as $k => $v) {
            $len = strlen($v);

            if ($len > 4 && preg_match("/.php$/", $v)) {
                $temp = substr($v, 0, $len - 4);

                if ($temp !== 'plan-du-site' &&
                    $temp !== 'robots' &&
                    $temp !== 'index' &&
                    $temp !== 'satisfaction')
                    echo "<p><a href=" . $temp . ">" . $temp. "</a></p>";
            }
        }

        /*
         * femme-de-menage
         */

        $nobo = scandir('femme-de-menage');

        echo "<h2>Femme de ménage</h2>";
        echo "<p><a href=femme-de-menage>femme de ménage</a></p>";

        foreach ($nobo as $k => $v) {
            $len = strlen($v);

            if ($len > 4 && preg_match("/.php$/", $v) && $v !== 'index.php') {
                $temp = substr($v, 0, $len - 4);

                echo "<p><a href=femme-de-menage/" . $temp . ">" . $temp. "</a></p>";
            }
        }

        /*
         * femme-de-menage/paris
         */

        $nobo = scandir('femme-de-menage/paris');

        echo "<h3>Paris</h3>";
        echo "<p><a href=femme-de-menage/paris>Paris</a></p>";

        foreach ($nobo as $k => $v) {
            $len = strlen($v);

            if ($len > 4 && preg_match("/.php$/", $v) && $v !== 'index.php') {
                $temp = substr($v, 0, $len - 4);

                echo "<p><a href=femme-de-menage/paris/" . $temp . ">Paris " . $temp . "</a></p>";
            }
        }

        /*
         * menage-a-domicile
         */

        $nobo = scandir('menage-a-domicile');

        echo "<h2>Ménage à domicile</h2>";
        echo "<p><a href=menage-a-domicile/>menage a domicile</a></p>";

        foreach ($nobo as $k => $v) {
            $len = strlen($v);

            if ($len > 4 && preg_match("/.php$/", $v) && $v !== 'index.php') {
                $temp = substr($v, 0, $len - 4);

                echo "<p><a href=menage-a-domicile/" . $temp . ">" . $temp. "</a></p>";
            }
        }

        /*
         * Réservation
         */

        $nobo = scandir('reservation');

        echo "<h2>Réservation</h2>";
        echo "<p><a href='reservation'>réserver</a></p>";

        /*
        * menage
         */

        $nobo = scandir('menage');

        echo "<h2>Ménage</h2>";

        foreach ($nobo as $k => $v) {
            $len = strlen($v);

            if ($len > 4 && preg_match("/.php$/", $v) && $v !== 'index.php') {
                $temp = substr($v, 0, $len - 4);

                echo "<p><a href=menage/" . $temp . ">" . $temp. "</a></p>";
            }
        }
        ?>
        </div>
    </div>
</section>
<?php include_once('inc/footer_start.php'); ?>
<?php include_once('inc/analyticstracking.php'); ?>
<?php include_once('inc/footer_end.php'); ?>