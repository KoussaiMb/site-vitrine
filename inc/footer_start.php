<?php if($_SERVER['HTTP_STAGE'] === 'local'): ?>
    <script src="/js/lib/jquery-3.1.1.min.js"></script>
    <script async defer src="/js/bootstrap.min.js"></script>
<?php else: ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" ?></script>
    <script async src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php endif; ?>
<script src="/js/navbar_public.js"></script>
<script defer src="/js/lib/bootstrap-notify.min.js"></script>
<script src="/js/js.cookie.js"></script>
<script src="/js/app.js"></script>