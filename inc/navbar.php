<nav class="navbar navbar-default navbar-transparent navbar-fixed-top" id="navbar-fixed">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-sm" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img alt="logo nobo blanc" src="/img/nobo/logo/nobo-logo-icon-blanc-cropped.png"></a>
        </div>

        <div class="collapse navbar-collapse" id="collapse-sm">
            <ul class="nav navbar-nav navbar-letft">
                <li><a href="/mon-espace">Votre espace client</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Accueil</a></li>
                <li class="visible-xs"><a href="/menage-a-domicile/fonctionnement">Fonctionnement</a></li>
                <li class="visible-xs"><a href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a></li>
                <li class="visible-xs"><a href="/menage-a-domicile/femme-de-menage-deductible-des-impots">Crédit d'impôt</a></li>
                <li class="dropdown hidden-xs">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Informations <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/menage-a-domicile/fonctionnement">Fonctionnement</a></li>
                        <li><a href="/menage-a-domicile/questions-frequentes">Questions fréquentes</a></li>
                        <li><a href="/menage-a-domicile/femme-de-menage-deductible-des-impots">Crédit d'impôt</a></li>
                    </ul>
                </li>
                <li><a class="cursor-pointer navNumber" href="tel:<?= $_SERVER['HTTP_PHONE_NOBO']; ?>"><?php App::parse_phone(' ', $_SERVER['HTTP_PHONE_NOBO']) ?></a></li>
            </ul>
        </div>
    </div>
</nav>