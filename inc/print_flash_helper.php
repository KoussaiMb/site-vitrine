<?php if(Session::getInstance()->hasFlash()): ?>
    <?php foreach(Session::getInstance()->getFlash() as $type => $message): ?>
        <div class="ad bg-<?= $type; ?> ad-<?= $type; ?> bg-flash">
            <?= $message;?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>