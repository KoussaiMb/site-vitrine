<?php

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken'))) {
    ApiCaller::addGuestApiTokenToSession('/', false);
}
?>

<div class="lightbox" id="text">
    <div class="box pink height-phone">
        <div class="popup-box height-phone">
            <div class="right height-phone col-lg-offset-4 col-lg-8 col-sm-12 col-xs-12">
                <div class="header-popup">
                    <div class="header-group">
                        <a class="close-popup close-cross" href="#"><span class="glyphicon glyphicon-remove"></span></a>
                        <div class="logo"><img src="/img/nobo/logo/logo-popup-250x150.png" alt="Nobo, femme de ménage Paris"></div>
                        <p class="title-modal">VOUS HÉSITEZ ENCORE ?</p>
                        <p class="subtitle-modal visible-xs">Faites-vous rappeler pour le meilleur :)</p>
                        <p class="subtitle-modal hidden-xs">Faites-vous rappeler par la réception pour comprendre pourquoi nous sommes les meilleurs :)</p>
                    </div>
                </div>
                <div class="content">
                <form class="flew-row flex-center">
                    <div class="input-group">
                        <input type="tel" class="form-control" placeholder="Téléphone" name="phone" id="popup-phone">
                        <span class="input-group-btn">
                            <button class="btn btn-callback callback" type="button">Rappelez-moi !</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="/js/nobo_popup.js"></script>
<script>
    $(document).ready(function () {
        let exitPhonePopUp = Cookies.get("noboExitPhonePopUp");
        let scroll_down = false;
        let trigger = $(document).height() * 0.25;
        let bounce = trigger * 0.9;

         // API call "call me back"
         $('.callback').on('click', function () {
            $.ajax({
                beforeSend: function(request) {
                    let apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/callback'; ?>",
                type: 'POST',
                data: {
                    phone: $('#popup-phone').val()
                },
                dataType: 'json',
                success: function (ans) {
                    let messages = ans.message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'success'
                        });
                    });
                },
                error: function (jqXHR) {
                    let messages = $.parseJSON(jqXHR.responseText).message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'danger'
                        });
                    });
                }
            });
            $('.lightbox').slideUp();
         });

        //Cookie management
        if (exitPhonePopUp === undefined) {
            if ($(window).width() <= 967) {
                let method = "scroll";

                $(document).on(method, function() {
                    scroll_down = scrollEvent('scroll', "noboExitPhonePopUp", trigger, bounce, scroll_down);
                });
            }
            else
            {
                let method = "mouseleave keydown scroll";

                $(document).on(method, function(e){
                    if (e.type === "mouseleave" || (e.type === 'keydown' && (e.which === 91 || e.which === 17))){
                        setCookieAndDropModalAndEvent(method, "noboExitPhonePopUp")
                    } else if (e.type === "scroll") {
                        scroll_down = scrollEvent('scroll', "noboExitPhonePopUp", trigger, bounce, scroll_down);
                    }
                });
            }
        }
    });
</script>