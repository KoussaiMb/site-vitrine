<?php
//Boostrap classes
spl_autoload_register('app_autoload');
$str = deepFiles();

function deepFiles()
{
    $i = mb_substr_count($_SERVER['PHP_SELF'], '/') - 1;
    $str = "";

    while ($i-- > 0)
        $str .= '../';

    return $str;
}

function app_autoload($class){
    $str = deepFiles();

    if ($class != 'Spyc')
        require $str . "class/$class.php";
}

//Redirect 404 on .php
$url = $_SERVER['REQUEST_URI'];
if (strlen($url) > 4 && preg_match('/.php$/', $url))
    include($str . "error/404.php");