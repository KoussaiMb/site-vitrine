<div class="visible-xs" id="phone-xs">
    <a href="tel:<?= $_SERVER['HTTP_PHONE_NOBO'] ?>">
        <div class="">
            <span class="glyphicon glyphicon-phone"></span>
            <span>Appelez-nous</span>
        </div>
    </a>
</div>