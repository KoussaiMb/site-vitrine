<!doctype html>
<html>
<head>
    <?php if($_SERVER['HTTP_STAGE'] != 'prod'): ?>
        <meta name="robots" content="noindex, nofollow">
    <?php endif; ?>
    <!-- encodage -->
    <meta charset="utf-8" lang="fr">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- mobile -->
    <meta name="viewport" content="width=device-width, shrink-to-fit=no">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=0.5, maximum-scale=3.0">
    <!-- compatibility -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- font -->
    <link rel="preload" href="https://fonts.googleapis.com/css?family=Cardo|Josefin+Slab|Nunito" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cardo|Josefin+Slab|Nunito"></noscript>
    <!-- CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="preload" href="/css/bootstrap-theme.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="/css/bootstrap-theme.min.css"></noscript>
<!--
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="preload" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <noscript><link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"></noscript>
-->
    <link rel="preload" href="/css/nobo_app.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="preload" href="/css/nobo_navbar.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="preload" href="/css/nobo_footer.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <link rel="preload" href="/css/nobo_popup.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">

    <noscript><link rel="stylesheet" href="/css/nobo_app.min.css"></noscript>
    <noscript><link rel="stylesheet" href="/css/nobo_navbar.min.css"></noscript>
    <noscript><link rel="stylesheet" href="/css/nobo_footer.min.css"></noscript>
    <noscript><link rel="stylesheet" href="/css/nobo_popup.min.css"></noscript>