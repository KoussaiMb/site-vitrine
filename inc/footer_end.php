<div class="col-xs-12 footer">
    <div class="col-xs-12 col-sm-3">
        <div class="log-icon">
            <a href="tel:<?= $_SERVER['HTTP_PHONE_NOBO'] ?>">
                <img src="/img/nobo/logo/social/bell-blue.svg" alt="logo-bell-blue">
                <img src="/img/nobo/logo/social/bell-gold.svg" alt="logo-bell-gold">
            </a>
            <a href="mailto:contact@nobo.life">
                <img src="/img/nobo/logo/social/email-blue.svg" alt="logo-email-blue">
                <img src="/img/nobo/logo/social/email-gold.svg" alt="logo-email-gold">
            </a>
            <a href="http://www.instagram.com/nobofrance">
                <img src="/img/nobo/logo/social/instagram-blue.svg" alt="logo-instagram-blue">
                <img src="/img/nobo/logo/social/instagram-gold.svg" alt="logo-instagram-gold">
            </a>
            <a href="http://www.facebook.com/nobofrance">
                <img src="/img/nobo/logo/social/facebook-blue.svg" alt="logo-facebook-blue">
                <img src="/img/nobo/logo/social/facebook-gold.svg" alt="logo-facebook-gold">
            </a>
        </div>
        <div class="col-xs-12 footer-logo">
            <a href="/"><img class="img-responsive" src="/img/nobo/logo/nobo-logo-paris-blue-darker.png" alt="logo nobo bleu sombre"></a>
        </div>
        <p><a href="/plan-du-site">Plan du site</a></p>
        <p><a href="/mentions-legales">Conditions générales d'utilisation</a></p>
    </div>
    <div class="col-xs-12 col-sm-3 right-aligned">
        <p><a href="/menage-a-domicile/" class="bold">Ménage à domicile</a></p>
        <p><a href="/menage-a-domicile/fonctionnement">Fonctionnement</a></p>
        <p><a href="/menage-a-domicile/femme-de-menage-deductible-des-impots">Crédit d'impôt</a></p>
        <p><a href="/menage-a-domicile/questions-frequentes" class="">Questions fréquentes</a></p>
        <p><a href="/contact">Contact</a></p>
    </div>
    <div class="col-xs-12 col-sm-3 left-aligned">
        <p><a href="/femme-de-menage/" class="bold">Femme de ménage</a></p>
        <p><a href="/femme-de-menage/paris">Femme de ménage à Paris</a></p>
        <p><a href="/emploi-femme-de-menage">Emploi femme de ménage</a></p>
    </div>
    <div class="col-xs-12 col-sm-3 footer-awards">
        <div class="col-xs-12 text-center">
            <img src="/img/nobo/logo/ticket-cesu-edenred.png" alt="logo des lauréats du réseau entreprendre paris">
        </div>
    </div>
    <div class="col-xs-12 text-center">
        <p>Nobo © <?= date_create()->format('Y'); ?></p>
    </div>
</div>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <!-- Hotjar Tracking Code for https://www.nobo.life -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:666727,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!--<script type="text/javascript">
        $(function () {
            (function(a,e,c,f,g,h,b,d){var k={ak:"873879215",cl:"iEYLCKX5i3EQr63ZoAM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
            _googWcmGet('numberContact', <?= App::parse_phone('-', $_SERVER['HTTP_PHONE_NOBO']) ?>);
        });
    </script>
    -->
<?php endif; ?>


</body>
</html>