<nav class="navbar navbar-default navbar-transparent navbar-fixed-top" id="navbar-fixed">
    <a class="navbar-brand"><img alt="logo nobo" src="/img/nobo/logo/nobo_logo_regular_400x240.png"></a>
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="collapse-sm">
            <ul class="nav navbar-nav navbar-right">
                <p class="navbar-text">Besoin d'une réponse rapide ?</p>
                <li><a class="cursor-pointer numberAd" href="tel:<?= $_SERVER['HTTP_PHONE_NOBO']; ?>"><span><?php App::parse_phone(' ', $_SERVER['HTTP_PHONE_NOBO']) ?></span></a></li>
            </ul>
        </div>
    </div>
</nav>