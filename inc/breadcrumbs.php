<?php
$url = $_SERVER['REQUEST_URI'];
$url_without_query_string = preg_replace('/\?.*/', '', $url);
$url_without_trailing_slash = rtrim($url_without_query_string, '/');
$arr = explode('/', $url_without_trailing_slash);
$active = array_pop($arr);
$max = count($arr);
$visible = [];
$links = [];
$i = 0;

while (--$max >= 0) {
    $links[$max] = implode('/', $arr);
    $visible[$max] = array_pop($arr);
}

echo "<ol class='breadcrumb'>";
echo "<li><a href='/'>Accueil</a></li>";
while (isset($visible[++$i]))
    echo "<li><a href='". $links[$i] ."'>". $visible[$i] ."</a></li>";
echo "<li class='active'>" . $active . "</li>";
echo "</ol>";
