<?php

// No apiToken ? -> Guest
if (empty(Session::getInstance()->read('apiToken')) ||
    !ApiCaller::isApiTokenValid(Session::getInstance()->read('apiToken'))) {
    ApiCaller::addGuestApiTokenToSession('/', false);
}
?>

<div class="lightbox" id="text">
    <div class="box pink">
        <div class="popup-box">
            <div class="right col-lg-offset-4 col-lg-8 col-sm-12 col-xs-12">
                <div class="header-popup">
                    <div class="header-group">
                        <a class="close-popup close-cross" href="#"><span class="glyphicon glyphicon-remove"></span></a>
                        <div class="logo"><img src="/img/nobo/logo/logo-popup-250x150.png" alt="Nobo, femme de ménage Paris"></div>
                        <p class="title-modal">VOUS NOUS QUITTEZ DÉJÀ ?</p>
                        <p class="subtitle-modal">Recevez nos astuces pratiques et nos offres exclusives :)</p>
                    </div>
                </div>
                <div class="content">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."#merci";?>" id="form-newsletter-popup">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" name="ns-name" placeholder="Nom, Prénom" id="ns-name"/>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control input-lg" name="ns-email" placeholder="Email" id="ns-email"/>
                        </div>
                        <div  class="form-group">
                            <button type="button" class="btn btn-pink" id="submit-newsletter">Recevoir des nouvelles de Nobo</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/js/nobo_popup.js"></script>
<script>
    $(document).ready(function () {
        let exitNewsletterPopUp = Cookies.get("noboExitNewsletterPopUp");
        let scroll_down = false;
        let trigger = $(document).height() * 0.20;
        let bounce = trigger * 0.9;

        // API call newsletter
        $('#submit-newsletter').on('click', function () {
            let name = $('#ns-name');
            let email = $('#ns-email');

            $.ajax({
                beforeSend: function(request) {
                    let apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/newsletters'; ?>",
                type: 'POST',
                data: {name: name.val(), email: email.val()},
                dataType: 'json',
                success: function (ans) {
                    let messages = ans.message;

                    messages.forEach(function (message) {
                        $.notify({message: message},{type: 'success'});
                    });
                },
                error: function (jqXHR) {
                    let messages = $.parseJSON(jqXHR.responseText).message;

                    messages.forEach(function (message) {
                        $.notify({message: message},{type: 'danger'});
                    });
                }
            });
            $('.lightbox').slideUp();
        });

        //Cookie management
        if (exitNewsletterPopUp === undefined) {
            if ($(window).width() <= 967) {
                let method = "scroll";

                $(document).on(method, function() {
                    scroll_down = scrollEvent('scroll', "noboExitNewsletterPopUp", trigger, bounce, scroll_down);
                });
            }
            else
            {
                let method = "mouseleave keydown scroll";

                $(document).on(method, function(e){
                    if (e.type === "mouseleave" || (e.type === 'keydown' && (e.which === 91 || e.which === 17))){
                        setCookieAndDropModalAndEvent(method, "noboExitNewsletterPopUp")
                    } else if (e.type === "scroll") {
                        scroll_down = scrollEvent(method, "noboExitNewsletterPopUp", trigger, bounce, scroll_down);
                    }
                });
            }
        }
    });
</script>