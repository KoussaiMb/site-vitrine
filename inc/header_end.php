<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <!-- Post script -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '624759454345400');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img
                height = "1"
                width = "1"
                style = "display: none"
                src = "https://www.facebook.com/tr?id=624759454345400&ev=PageView&noscript=1"
        />
    </noscript>
<?php endif; ?>
<script>
    /* loadCSS. [c]2017 Filament Group, Inc. MIT License */
    /* Standalone workflow */
    (function( w ){
        "use strict";
        if( !w.loadCSS ){
            w.loadCSS = function(){};
        }
        var rp = loadCSS.relpreload = {};
        rp.support = (function(){
            var ret;
            try {
                ret = w.document.createElement( "link" ).relList.supports( "preload" );
            } catch (e) {
                ret = false;
            }
            return function(){
                return ret;
            };
        })();

        rp.bindMediaToggle = function( link ){
            var finalMedia = link.media || "all";

            function enableStylesheet(){
                link.media = finalMedia;
            }
            if( link.addEventListener ){
                link.addEventListener( "load", enableStylesheet );
            } else if( link.attachEvent ){
                link.attachEvent( "onload", enableStylesheet );
            }
            setTimeout(function(){
                link.rel = "stylesheet";
                link.media = "only x";
            });
            setTimeout( enableStylesheet, 3000 );
        };

        rp.poly = function(){
            if( rp.support() ){return;}
            var links = w.document.getElementsByTagName( "link" );
            for( var i = 0; i < links.length; i++ ){
                var link = links[i];
                if( link.rel === "preload" && link.getAttribute( "as" ) === "style" && !link.getAttribute( "data-loadcss" ) ){
                    link.setAttribute( "data-loadcss", true );
                    rp.bindMediaToggle( link );
                }
            }
        };
        if( !rp.support() ){
            rp.poly();

            var run = w.setInterval( rp.poly, 500 );
            if( w.addEventListener ){
                w.addEventListener( "load", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            } else if( w.attachEvent ){
                w.attachEvent( "onload", function(){
                    rp.poly();
                    w.clearInterval( run );
                } );
            }
        }
        if( typeof exports !== "undefined" ){
            exports.loadCSS = loadCSS;
        }
        else {
            w.loadCSS = loadCSS;
        }
    }( typeof global !== "undefined" ? global : this ) );
</script>
</head>
<body>