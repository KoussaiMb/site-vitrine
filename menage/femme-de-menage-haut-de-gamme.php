<?php
include_once('../inc/bootstrap.php');

$url = $_SERVER['REQUEST_URI'];

if (Session::getInstance()->hasFlash())
    $notif = Session::getInstance()->getFlash();

$months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
if (date('d') > 15)
    $day = date('t');
else
    $day = '15';
$month = $months[date('m') - 1];

include_once('../inc/header_start.php');
?>
    <!-- facebook preview -->
    <meta property="og:url"                 content="https://nobo.life/menage/femme-de-menage-haut-de-gamme"/>
    <meta property="og:type"                content="website"/>
    <meta property="og:title"               content="Nobo - Femme de ménage haut de gamme à Paris"/>
    <meta property="og:description"         content="Réservez une femme de ménage haut de gamme en quelques clics. Personnel issu de l'hôtellerie. Service sans engagement. 50% de crédit d'impôts automatique !"/>
    <meta property="fb:app_id"              content="430915960574732"/>
    <meta property="og:image"               content="https://nobo.life/img/paris/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
    <!-- Google -->
    <meta name="description" content="Réservez une femme de ménage haut de gamme en quelques clics. Personnel issu de l'hôtellerie. Service sans engagement. 50% de crédit d'impôts automatique !">
    <meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
    <!-- title -->
    <title>Nobo - Femme de ménage haut de gamme à Paris</title>
    <!-- CSS -->
    <link rel="stylesheet" href="/css/localization_style.css">
    <link rel="stylesheet" href="/css/lib/bootstrap-datepicker.min.css">

<?php include_once('../inc/header_end.php'); ?>
<?php include_once('../inc/navbar.php'); ?>
<?php include_once('../inc/navbar_phone.php'); ?>

<section id="fond-haut-de-gamme">
    <div class="container" style="padding-top: 50px">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-0 intro-box">
                <div class="col-xs-12">
                    <h1 class="fix-h1-small">Femme de ménage haut de gamme à Paris</h1>
                    <p class="h-small">
                        <strong>Femme de ménage haut de gamme</strong> issue de l'hôtellerie de luxe pour appartements exigeants !
                    </p>
                </div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-2 intro-form">
                <form name="sentMessage" id="contactForm" action="/reservation/envoie_mail" method="post">
                    <input type="hidden" name="origine" value="<?= htmlspecialchars($_SERVER['REQUEST_URI']); ?>">
                    <div class="col-xs-12" style="margin-top: 10px;margin-bottom: 20px;">
                        <p class="center form-title">
                            Passez au haut de gamme : contactez vite <span class="gold">votre gouvernant !</span>
                        </p>
                        <div class="form-group">
                            <input type="hidden" name="code_promotion" value="" class="form-control" placeholder="Entrez votre code de promotion" id="code_promotion">
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input type="text" name="date" class="form-control pickDate" placeholder="Date de votre besoin" id="date">
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input type="text" name="cp" class="form-control" placeholder="Code Postal" id="cp">
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input type="text" name="lastname" class="form-control" placeholder="Nom *" id="lastname" required>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input type="text" name="firstname" class="form-control" placeholder="Prénom" id="firstname">
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Votre Email" id="email">
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input type="tel" name="tel" class="form-control" placeholder="Votre numéro de téléphone *" id="tel" required>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" placeholder="Message" id="message"></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 text-center">
                            <div id="success"></div>
                            <button id="reserver" type="submit" class="btn-stage" name="action" value="post" style="margin-top: 0!important;" onclick="gtag_report_conversion()">ME RECONTACTER</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper">
                <h2 class="h2-seo">Nobo, la référence des <strong>femmes de ménage haut de gamme</strong> à Paris</h2>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 mission-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified mission-tabs" role="tablist">
                    <li role="presentation" class="active text-center">
                        <a href="#qualite" aria-controls="qualite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo étoile dorée" src="/img/paris/sticker/femme-de-menage-qualité.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#flexibilite" aria-controls="flexibilite" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo pendule" src="/img/paris/sticker/femme-de-menage-flexible.svg">
                        </a>
                    </li>
                    <li role="presentation" class="text-center">
                        <a href="#suivi" aria-controls="suivi" role="tab" data-toggle="tab">
                            <img class="mission-sticker" alt="logo oeil" src="/img/paris/sticker/femme-de-menage-suivi.svg">
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content mission-content">
                    <div role="tabpanel" class="tab-pane active text-center" id="qualite">
                        <h3 class="gold">Qualité</h3>
                        <p class="nomarge">
                            Vous êtes déçu des compétences et du savoir-être de votre <strong>femme de ménage</strong> actuelle ?
                            Nobo a été créé pour vous !
                            <br>
                            Profitez enfin d'une <strong>femme de ménage haut de gamme</strong> pour entretenir votre
                            appartement et s'occuper de votre repassage. Votre <strong>femme de ménage</strong> est
                            issue de l'hôtellerie <strong>haut de gamme</strong> pour garantit notre qualité de service.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="flexibilite">
                        <h3 class="gold">Flexibilité</h3>
                        <p class="nomarge">
                            Nobo est la seule agence à vous proposer les service d'une
                            <strong>femme de ménage haut de gamme</strong>, sans engagement et facturé à l'heure.
                            <br>
                            Vous bénéficier ainsi de la qualité <strong>haut de gamme</strong> avec la flexibilité
                            des services modernes.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="suivi">
                        <h3 class="gold">Suivi</h3>
                        <p class="nomarge">
                            Un <span class="bold">gouvernant référant</span> vous est également attribué lors
                            d’un <span class="bold">premier rendez vous gratuit</span> à votre appartement.
                            Il dressera une fiche détaillée de <span class="bold">vos besoins et de vos habitudes</span>
                            puis vous proposera un <span class="bold">planning d’entretien adapté</span>. Cela guidera
                            le travail de votre <strong>femme de ménage</strong> et assurera un service
                            <strong>haut de gamme</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-dark" id="pricing">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>nos tarifs</h2>
                </div>
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 n_t-wrap">
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">Populaire</span>
                            </div>
                        </div>
                        <div class="n_t-top bg-gold">
                            <h5 class="bold n_t-title blue">Un passage par semaine ou plus</h5>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price gold">28 €/h</p>
                            <p>Soit <span class="bold">14 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"><p></p></div>
                    <div class="col-sm-5 n_t-price text-center fadeUp spaceTop">
                        <div class="n_t-top bg-blue-light">
                            <p class="bold n_t-title blue">Deux passages par mois ou ponctuel</p>
                        </div>
                        <div class="n_t-bottom n_t-bottom-wrap">
                            <p class="n_t-detail-price blue">32 €/h</p>
                            <p>Soit <span class="bold">16 €/h</span> après crédit d'impôt</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center spaceTop">
                    <a href="/reservation/besoin"><button class="btn-outline-blue">resérver</button></a>
                    <p>ou nous <a href="/contact">contacter</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="voisin">
    <div class="container">
        <div class="row">
            <div class="section">
                <div class="landing-wrapper">
                    <h2 class="h2-seo">Plus de 500 clients nous ont déja confié leur <strong>ménage et repassage à Paris</strong></h2>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 voisin-section">
                    <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>"Super service, le ménage est parfait et les petites attentions font qu’on
                                    se sent vraiment traité comme à l’hôtel, c’est top bravo!"</p>
                                <br>
                                <p>Emmanuelle, Beaugrenelle, <strong>Paris 15</strong><br></p>

                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo d'Emmanuelle cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-emmanuelle.png" class="img-circle">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                            <div class="voisin-wrap">
                                <div class="voisin-box">
                                    <p>"Un service de très grande qualité. J'ai pu tisser de vrais liens de confiance avec l'équipe et les recommande chaleureusement."</p>
                                    <br>
                                    <p>Danièle, Jasmin, Paris 16<br></p>
                                </div>
                                <div class="voisin-arrow"><div></div></div>
                                <div>
                                    <img alt="photo Daniele cliente menage repassage paris 16" src="/img/paris/avis/femme-de-menage-paris-16-avis-daniele.png" class="img-circle">
                                </div>
                            </div>
                    </div>
                   <div class="col-xs-12 col-sm-4">
                        <div class="voisin-wrap">
                            <div class="voisin-box">
                                <p>
                                    "Toujours aussi bien après plusieurs mois de prestations,
                                    ce qui est très rare malheureusement.
                                    Je recommande pour leur sérieux et l’attention portée aux clients."
                                    <br>
                                </p>
                                <p>Margaux, Convention, <strong>Paris 15</strong><br></p>
                            </div>
                            <div class="voisin-arrow"><div></div></div>
                            <div>
                                <img alt="photo de Margaux cliente ménage nobo" src="/img/paris/avis/femme-de-menage-paris-15-avis-margaux.png" class="img-circle">
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section id="link" class="section-dark hidden-xs">
    <div class="container">
        <div class="row">
            <div class="landing-wrapper landing-wrapper-dark">
                <h2 class="h2-seo">Votre femme de ménage Nobo est disponible dans d'autres arrondissements :</h2>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 link-section">
                <div class="col-xs-12 col-md-6 text-center fadeUp hidden-xs">
                    <div class="valign">
                        <img src="/img/paris/sticker/femme-de-menage-paris.png" alt="plan de paris 15">
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <a href="/femme-de-menage/paris/Paris-1-er-75001" <p>Femme de menage Paris 1</p>
                    <p href="/femme-de-menage/paris/Paris-2-eme-75002">Femme de menage Paris 2</p>
                    <p>Femme de menage Paris 3</p>
                    <p>Femme de menage Paris 4</p>
                    <p href="/femme-de-menage/paris/Paris-5-eme-75005">Femme de menage Paris 5</p>
                    <p href="/femme-de-menage/paris/Paris-6-eme-75006">Femme de menage Paris 6</p>
                    <p href="/femme-de-menage/paris/Paris-7-eme-75007">Femme de menage Paris 7</p>
                    <a href="/femme-de-menage/paris/8"><p>Femme de menage Paris 8</p></a>
                    <p>Femme de menage Paris 9</p>
                    <p>Femme de menage Paris 10</p>
                </div>
                <div class="col-xs-12 col-md-3 text-center">
                    <p>Femme de menage Paris 11</p>
                    <p>Femme de menage Paris 12</p>
                    <p>Femme de menage Paris 13</p>
                    <p>Femme de menage Paris 14</p>
                    <a href="/femme-de-menage/paris/15"><p>Femme de menage Paris 15</p></a>
                    <a href="/femme-de-menage/paris/16"><p>Femme de menage Paris 16</p></a>
                    <a href="/femme-de-menage/paris/17"><p>Femme de menage Paris 17</p></a>
                    <p>Femme de menage Paris 18</p>
                    <p>Femme de menage Paris 19</p>
                    <p>Femme de menage Paris 20</p>
                </div>
            </div>
        </div>
</section>


<?php include_once('../inc/footer_start.php'); ?>
<?php include_once ('../inc/exit_popup.php'); ?>
<script src="/js/lib/bootstrap-datepicker.min.js"></script>
<script src="/js/locale/bootstrap-datepicker.fr.min.js"></script>
<script src="/js/lib/jquery.validate.min.js"></script>
<script src="/js/lib/jquery.validate.add.min.js"></script>
<script>
    $(function () {
        var notifs = <?= empty($notif) ? json_encode(null) : json_encode($notif); ?>;

        if (notifs) {
            var keys = Object.keys(notifs);
            var values = Object.values(notifs);

            for (var i = 0, len = keys.length; i < len; i++) {
                $.notify({
                    message: values[i]
                },{
                    type: keys[i]
                });
            }
        }

        $(".pickDate").datepicker({
            language : "fr",
            startDate: "tomorrow",
            autoclose: 'true',
            format: "dd/mm/yyyy",
            disableTouchKeyboard: true,
            zIndexOffset: 1150
        });

        $('#contactForm').validate( {
            ignore: ":not(:visible)",
            rules: {
                date: {
                    required: false
                },
                lastname: {
                    required: true
                },
                tel: {
                    required: true,
                    minlength: 8,
                    pattern: /^[\d \+]+$/
                },
                email: {
                    required: false,
                    email: true
                }
            },
            messages: {
                tel: {
                    required: "Pour vous rappeler, un numéro de téléphone est nécessaire",
                    minlength: "Votre numéro de téléphone ne semble pas valide",
                    pattern: "Votre numéro de téléphone ne semble pas valide"
                },
                lastname: {
                    required: "Afin de pouvoir vous rappeler un nom est nécessaire"
                },
                email: {
                    email: "Votre email n'est pas valide"
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `help-block` class to the error element
                error.addClass( "help-block red" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                } else {
                    error.insertAfter( element.parent() );
                }
            },
            highlight: function ( element ) {
                $( element ).parents( ".form-control" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element) {
                $( element ).parents( ".form-control" ).addClass( "has-success" ).removeClass( "has-error" );
            }
        });
    });
</script>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>

        function gtag_report_conversion(url) {
            var callback = function () {
                if (typeof(url) != 'undefined') {
                    window.location = url;
                }
            };
            gtag('event', 'conversion', {
                'send_to': 'AW-873879215/kc2UCPXGrncQr63ZoAM',
                'event_callback': callback
            });
            return false;
        }

        $(document).ready(function () {
            $( '#reserver' ).on('click', function() {
                fbq('track', 'Lead', {
                    value: 750.00,
                    currency: 'USD'
                });
            });
        });
    </script>
<?php endif; ?>

<?php include_once('../inc/analyticstracking.php'); ?>
<?php include_once('../inc/footer_end.php'); ?>
