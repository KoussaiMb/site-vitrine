<?php include_once('../inc/bootstrap.php'); ?>
<?php include_once('../inc/header_start.php'); ?>
<!-- facebook preview -->
<meta property="og:url"                 content="https://nobo.life/menage-a-domicile/"/>
<meta property="og:type"                content="website"/>
<meta property="og:title"               content="Nobo - ménage haut de gamme à domicile"/>
<meta property="og:description"         content="Nobo - Ménage haut de gamme pour appartement exigeant. Personnel issu de l'hôtellerie de luxe, facturé à l'heure et sans engagement."/>
<meta property="fb:app_id"              content="430915960574732"/>
<meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
<!-- Google -->
<meta name="description" content="Nobo - Ménage haut de gamme pour appartement exigeant. Personnel issu de l'hôtellerie de luxe, facturé à l'heure et sans engagement.">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Nobo - Ménage haut de gamme à domicile</title>
<!-- CSS -->
<!--<link rel="stylesheet" href="/css/lib/magnific-popup.css">-->
<link rel="stylesheet" href="/css/public_style.css">
<link rel="stylesheet" href="/css/lib/t-scroll.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<?php include_once('../inc/header_end.php'); ?>
<?php include_once('../inc/navbar.php'); ?>
<?php include_once('../inc/navbar_phone.php'); ?>

<section class="first-section section-dark">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 title-white text-center spaceTop">
                <h1>Informations sur notre service de ménage à domicile</h1>
            </div>
        </div>
    </div>
</section>

<!-- paris -->
<section class="section-dark section-flyer">
    <div class="container">
        <div class="row section-chapeau">
            <div class="col-xs-12 text-center">
                <?php include_once('../inc/breadcrumbs.php'); ?>
            </div>
            <div class="chapeau-part col-xs-12">
                <div class="flex-row flex-arround-stretch info-row">
                    <div class="col-xs-12">
                        <h2>Ménage à domicile</h2>
                        <p>
                            Vous trouverez ici des réponses aux questions que vous vous posez certainement si vous cherchez 
                            un service de <strong>ménage à domicile</strong>.
                            <br>
                            Comment mettre en place notre service de <strong>ménage à domicile</strong>, comment fonctionne
                            la déduction d'impôts de 50%, qui sont les femmes de <strong>ménage à domicile</strong> Nobo...
                            Tout y est !
                        </p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-sm-4 col-md-4 box-service">
                        <a href="fonctionnement">
                            <img class="img-responsive" src="/img/nobo/gallery/fonctionnement.svg" alt="icone fonctionnement">
                            <div class="bg-h3">
                                <h3>Fonctionnement</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 box-service">
                        <a href="questions-frequentes">
                            <img class="img-responsive" src="/img/nobo/gallery/questions-frequentes.svg" alt="icone questions fréquentes">
                            <div class="bg-h3">
                                <h3>Questions Fréquentes</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 box-service">
                        <a href="femme-de-menage-deductible-des-impots">
                            <img class="img-responsive" src="/img/nobo/gallery/femme-de-menage-deductible-des-impots.svg" alt="icone crédit d'impots">
                            <div class="bg-h3">
                                <h3>Crédit d'impôts</h3>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-xs-12 col-sm-6 col-md-3 box-service">
                        <a href="equipe">
                            <img class="img-responsive" src="/img/nobo/gallery/equipe.svg" alt="icone équipe">
                            <div class="bg-h3">
                                <h3>Notre équipe</h3>
                            </div>
                        </a>
                    </div> -->
                </div>
                <div class="col-xs-12">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- autres villes -->

<?php include_once ('../inc/footer_start.php'); ?>
<?php include_once ('../inc/exit_popup_phone.php'); ?>
<?php include_once('../inc/analyticstracking.php'); ?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>
<?php include_once ('../inc/footer_end.php'); ?>
