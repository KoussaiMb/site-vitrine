<?php include_once ('../inc/bootstrap.php'); ?>
<?php include_once('../inc/header_start.php'); ?>

    <!-- facebook preview -->
    <meta property="og:url"                 content="https://nobo.life/menage-a-domicile/femme-de-menage-deductible-des-impots"/>
    <meta property="og:type"                content="website"/>
    <meta property="og:title"               content="Nobo - Votre femme de ménage déductible des impôts !"/>
    <meta property="og:description"         content="Avec Nobo, profitez de 50% de crédit d'impots sur les prestations de votre femme de ménage !"/>
    <meta property="fb:app_id"              content="430915960574732"/>
    <meta property="og:image"               content="https://nobo.life//img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
    <!-- Google -->
    <meta name="description" content="Avec Nobo, profitez automatiquement de 50% de crédit d'impôts sur les prestations de votre femme de ménage !">
    <meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
    <!-- title -->
    <title>Nobo - Une femme de ménage déductible des impôts</title>
    <link rel="stylesheet" href="/css/public_style.css">

<?php include_once('../inc/header_end.php'); ?>
<?php include_once('../inc/navbar.php'); ?>

    <section class="section-dark" id="ci">
        <div class="container">
            <div class="row section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h1 class="gold h1-fix">Une femme de ménage déductible des impots !</h1>
                    <p>Bénéficiez automatiquement de 50% de <strong>crédit d'impôts</strong> sur les prestations de votre <strong>femme de ménage</strong></p>
                </div>
                <div class="text-center" style="margin-top: 20px">
                    <?php include_once('../inc/breadcrumbs.php'); ?>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 ci-row">
                    <div class="col-xs-12 col-md-4 sticker">
                        <img class="img-responsive" src="/img/nobo/sticker/credit-impot-nobo.png" alt="sticker avec le chiffre 1">
                    </div>
                    <div class="col-xs-12 col-md-8 ci-box">
                        <h3>Réservez sur Nobo.life</h3>
                        <p>
                            Nobo, en tant que société déclarée de services à la personne, vous fait bénéficier
                            automatiquement d'un crédit d'impôts de 50% sur les prestations de votre <strong>femme de ménage</strong>
                        </p>
                        <p><strong>En clair, 10€ dépensés sur le site, c'est 5€ de crédit d'impôts !</strong></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 ci-row">
                    <div class="col-xs-12 col-md-4 sticker">
                        <img class="img-responsive" src="/img/nobo/sticker/credit-impot-menage.png" alt="sticker avec le chiffre 2">
                    </div>
                    <div class="col-xs-12 col-md-8 ci-box">
                        <h3>Recevez votre attestation fiscale</h3>
                        <p>
                            Chaque année en Janvier, recevez automatiquement votre attestation fiscale. Elle mentionne
                            la déduction fiscale à laquelle votre femme de ménage vous a donné droit. Elle indique également
                            où reporter le montant du crédit d'impôts sur votre feuille d'imposition.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 ci-row">
                    <div class="col-xs-12 col-md-4 sticker">
                        <img class="img-responsive" src="/img/nobo/sticker/credit-impot-paris.png" alt="sticker avec le chiffre 3">
                    </div>
                    <div class="col-xs-12 col-md-8 ci-box">
                        <h3>Remplissez votre feuille d'imposition</h3>
                        <p>
                            Reportez le montant de votre crédit d'impôts dans la case mentionnée sur notre
                            attestation. S'agissant d'un crédit d'impôts, les sommes dépassant le montant de votre
                            impôts vous seront directement remboursées par le trésor public.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="sub-button">
        <div class="section-transition section-transition-blue"><div></div></div>
        <div class="container-fluid section">
            <div class="row">
                <div class="col-xs-12">
                    <a href="/reservation/besoin" class="btn btn-lg btn-gold btn-gold-fix">réserver maintenant</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section-dark" id="ask">
        <div class="container-fluid">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Une autre question</h2>
                    <p class="gold">contactez-nous !</p>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 ask-content">
                    <form id="ask-form">
                        <div class="col-xs-12 col-md-6">
                            <label for="name"></label>
                            <input type="text" class="form-control input-lg" name="name" placeholder="Nom, Prénom" id="name" required/>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <label for="email"></label>
                            <input type="email" class="form-control input-lg" name="email" placeholder="Email" id="email" required/>
                        </div>
                        <div class="col-xs-12">
                            <label for="message"></label>
                            <textarea class="form-control input-lg" form="ask-form" maxlength="200" rows="4" name="message" placeholder="Message" id="message" required></textarea>
                        </div>
                        <button type="button" class="btn btn-gold" id="questionValidation">envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include_once ('../inc/footer_start.php'); ?>
<?php include_once ('../inc/exit_popup.php'); ?>
    <script>
        // API call contact
        $('#questionValidation').on('click', function () {
            $.ajax({
                beforeSend: function(request) {
                    var apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/contact'; ?>",
                type: 'POST',
                data: {
                    subject: 'Une nouvelle question a été posée!',
                    name: $('#name').val(),
                    email: $('#email').val(),
                    message: $('#message').val()
                },
                dataType: 'json',
                success: function (ans) {
                    var messages = ans.message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'success'
                        });
                    });
                    $('#name').val("");
                    $('#email').val("");
                    $('#message').val("");
                },
                error: function (jqXHR) {
                    var messages = $.parseJSON(jqXHR.responseText).message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'danger'
                        });
                    });
                }
            });
        });

    </script>

<?php include_once ('../inc/analyticstracking.php'); ?>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>

<?php include_once ('../inc/footer_end.php'); ?>
