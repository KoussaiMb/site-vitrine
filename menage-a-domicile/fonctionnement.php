<?php include_once ('../inc/bootstrap.php'); ?>
<?php include_once ('../inc/header_start.php'); ?>

    <!-- facebook preview -->
    <meta property="og:url"                 content="https://nobo.life/fonctionnement"/>
    <meta property="og:type"                content="website"/>
    <meta property="og:title"               content="Nobo - Comment avoir du ménage et du repassage chez vous"/>
    <meta property="og:description"         content="Nobo est la seule plateforme à proposer les services de personnel hôtelier à domicile. Entretien 5 étoiles garanti à chaque passage."/>
    <meta property="fb:app_id"              content="430915960574732"/>
    <meta property="og:image"               content="https://nobo.life/img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
    <!-- Google -->
    <meta name="description" content="Ménage haut de gamme pour appartement exigeant.">
    <meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
    <!-- title -->
    <title>Nobo - Comment avoir du ménage et du repassage chez vous</title>
    <link rel="stylesheet" href="/css/public_style.css">

<?php include_once ('../inc/header_end.php'); ?>
<?php include_once ('../inc/navbar.php'); ?>

    <section class="first-section section-dark" id="fonctionnement">
        <div class="container">
            <div class="row section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h1 class="gold h1-fix"><span class="long-h1-mobile">Fonctionnement</span><br><span class="small" style="text-transform: lowercase">ménage à domicile</span></h1>
                </div>
                <div class="text-center">
                    <?php include_once('../inc/breadcrumbs.php'); ?>
                </div>
            </div>
            <div class="row fonctionnement-content">
                <div class="col-xs-12">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-2 poincon flex-row flex-arround-stretch">
                        <div class="col-xs-2 valign">
                            <span class="pastille-nobo">1</span>
                        </div>
                        <div class="col-xs-8 col-sm-6 valign">
                            <p class="nomarge"><span class="bold">Réservation</span> en ligne <span class="bold">sécurisée</span></p>
                        </div>
                        <div class="col-xs-2 col-sm-4 valign">
                            <img src="/img/nobo/sticker/fonctionnement/reservation-gold-120.svg" alt="réservation sécurisée">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-6 col-md-5 col-md-offset-6 col-lg-offset-6 col-lg-4 poincon flex-row flex-arround-stretch">
                        <div class="col-xs-2 valign">
                            <span class="pastille-nobo">2</span>
                        </div>
                        <div class="col-xs-8 col-sm-6 valign">
                            <p class="nomarge"><span class="bold">Appel</span> d'un réceptionniste pour établir vos besoins</p>
                        </div>
                        <div class="col-xs-2 col-sm-4 valign">
                            <img src="/img/nobo/sticker/fonctionnement/phone-call-gold.svg" alt="téléphone">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-2 poincon flex-row flex-arround-stretch">
                        <div class="col-xs-2 valign">
                            <span class="pastille-nobo">3</span>
                        </div>
                        <div class="col-xs-8 col-sm-6 valign">
                            <p class="nomarge"><span class="bold">Visite</span> de votre gouvernant(e) pour créer votre planning d'entretien</p>
                        </div>
                        <div class="col-xs-2 col-sm-4 valign">
                            <img src="/img/nobo/sticker/fonctionnement/gouvernant-premiere-visite-gold.svg" alt="gouvernant">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-6 col-md-5 col-md-offset-6 col-lg-offset-6 col-lg-4 poincon flex-row flex-arround-stretch">
                        <div class="col-xs-2 valign">
                            <span class="pastille-nobo">4</span>
                        </div>
                        <div class="col-xs-8 col-sm-6 valign">
                            <p class="nomarge"><span class="bold">Première prestation</span> de votre femme de ménage</p>
                        </div>
                        <div class="col-xs-2 col-sm-4 valign">
                            <img src="/img/nobo/sticker/fonctionnement/house-cleaned-gold.svg" alt="maison après le ménage">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="fonctionnement-button col-sm-4 col-sm-offset-4 flex-col flex-between">
                        <button type="button" class="btn-outline-blue" onclick="window.location='/reservation/besoin'">Réserver</button>
                        <p class="spaceTopBottom">ou</p>
                        <form class="flew-row flex-center">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="ex: 0608192019" name="phone" id="phone">
                                <span class="input-group-btn">
                                <button class="btn btn-success callback" type="button">Se faire rappeler</button>
                            </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="visible-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 poincon poincon-xs-link flex-row flex-center-stretch">
                        <div class="col-xs-6 flex-col flex-center">
                            <a href="/" class="col-xs-12">
                                <p class="col-xs-12 text-right"><span class="glyphicon glyphicon-chevron-left pull-left"></span>Accueil</p>
                            </a>
                        </div>
                        <div class="col-xs-6 flex-col flex-center">
                            <a href="/menage-a-domicile/questions-frequentes" class="col-xs-12">
                                <p class="col-xs-12 text-left">F.A.Q<span class="glyphicon glyphicon-chevron-right pull-right"></span></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include_once ('../inc/footer_start.php'); ?>
<?php include_once ('../inc/exit_popup_phone.php'); ?>
    <script>
        $(document).ready(function () {
            $('.navbar.visible-xs').remove();
            $('body').append("<section class='visible-xs mobile-menu'>-</section>");
        });
    </script>

    <script>
        // API call "call me back"
        $('.callback').on('click', function () {
            $.ajax({
                beforeSend: function(request) {
                    var apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/callback'; ?>",
                type: 'POST',
                data: {
                    phone: $('#phone').val()
                },
                dataType: 'json',
                success: function (ans) {
                    var messages = ans.message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'success'
                        });
                    });
                    $('#phone').val("");
                },
                error: function (jqXHR) {
                    var messages = $.parseJSON(jqXHR.responseText).message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'danger'
                        });
                    });
                }
            });
        });

    </script>

<?php include_once ('../inc/analyticstracking.php'); ?>
<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
            $('.navbar.visible-xs').remove();
        });
    </script>
<?php endif; ?>

<?php include_once ('../inc/footer_end.php'); ?>