<?php include_once ('../inc/bootstrap.php'); ?>
<?php include_once('../inc/header_start.php'); ?>

<!-- facebook preview -->
<meta property="og:url"                 content="https://nobo.life/menage-a-domicile/questions-frequentes"/>
<meta property="og:type"                content="website"/>
<meta property="og:title"               content="Les questions fréquentes avant l'emploi d'une femme de ménage à domicile"/>
<meta property="og:description"         content="Nobo répond à vos questions concernant l'emploi d'une femme de ménage à domicile. Gestion des clés, congés, impôts..."/>
<meta property="fb:app_id"              content="430915960574732"/>
<meta property="og:image"               content="https://nobo.life//img/nobo/gallery/nobo-menage-paris-repassage-hotel-luxe-entretien-domicile.jpg"/>
<!-- Google -->
<meta name="description" content="Nobo répond à vos questions concernant l'emploi d'une femme de ménage à domicile. Gestion des clés, congés, impôts...">
<meta name="google-site-verification" content="Qsgt-k44j2d_gwgV0tWvYGY6tKlAgNiATGEA7th86qU"/>
<!-- title -->
<title>Nobo - Questions fréquentes - femme de menage a domicile</title>
<link rel="stylesheet" href="/css/public_style.css">

<?php include_once('../inc/header_end.php'); ?>
<?php include_once('../inc/navbar.php'); ?>

    <section class="section-dark" id="faq">
        <div class="container">
            <div class="row section">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h1 class="gold h1-fix">questions fréquentes<br><span class="small" style="text-transform: lowercase">ménage à domicile</span></h1>
                </div>
                <div class="text-center">
                    <?php include_once('../inc/breadcrumbs.php'); ?>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Aurais-je toujours le même prestataire à la maison ?</h3>
                    <p>
                        Oui, nous vous affectons une <strong>femme de ménage</strong> qui restera la même dans le temps,
                        sauf demande de votre part. Cependant, en cas de congés ou de maladie, nous vous enverrons 
                        exceptionnellement une autre <strong>femme de ménage</strong>.
                        <br>
                        Le remplaçant aura le même degré de formation et de compétences, et il aura accès à la fiche 
                        créée par notre gouvernant lors de votre première mission afin de pouvoir parfaitement s'adapter 
                        à votre domicile et vos besoins.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Dois-je fournir les produits d'entretien ?</h3>
                    <p>
                        Oui ! Afin de nous adapter à vos habitudes, nos <strong>femmes de ménage</strong> utiliseront 
                        vos produits d'entretien.
                        <br>
                        Cependant, nous pourrons vous transmettre une liste élaborée par notre gouvernant général de
                        produits essentiels pour l'entretien de votre domicile.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Peut-on résilier le service Nobo ?</h3>
                    <p>
                        Oui ! Notre service est <strong>sans engagement</strong> et vous êtes donc libres de suspendre 
                        les prestations de votre <strong>femme de ménage</strong> quand vous le souhaitez.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Quels sont les frais d'inscription/résiliation ?</h3>
                    <p>
                        Il n'y en a aucun ! Chez Nobo, nous considérons que l'accès au service aussi bien que sa 
                        résiliation n'ont pas de raison d'être facturé.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Y a-t-il des frais de gestion ?</h3>
                    <p>
                        Non ! Le taux horaire de votre <strong>femme de ménage</strong> comprend tout. Vous ne payez
                        <strong>aucun</strong> supplément !
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Comment sont sélectionnées vos <strong>femmes de ménage</strong> ?</h3>
                    <p>
                        Notre processus de recrutement est un de nos atouts. Nous recevons des centaines de CV par mois, 
                        en choisissons environ 3% pour un entretien, puis 1% pour un test grandeur nature. Un appel de 
                        référence est systématiquement fait avant d'aller plus loin dans le processus afin de valider 
                        les qualités de chaque <strong>femme de ménage</strong>. Une fois ces étapes validées, notre 
                        nouvelle <strong>femme de ménage</strong> passe en formation puis est prête à rejoindre l'équipe !
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Suis-je le particulier employeur de ma <strong>femme de ménage</strong> ?</h3>
                    <p>
                        Non ! Nobo est prestataire de services, ce qui signifie que vous n'achetez que des heures de 
                        service. Cela vous dédouane totalement de toutes démarches administratives, ainsi que des 
                        responsabilités qui y sont liées.
                        <br>
                        Nos <strong>femmes de ménage</strong> sont toutes employées en CDI à temps plein.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Puis-je annuler mes prestations pendant mes vacances ?</h3>
                    <p>
                        Oui, vous pouvez sans problème suspendre les interventions de votre 
                        <strong>femme de ménage</strong> pendant vos congés si vous le souhaitez !
                        Nous exigeons cependant 7 jours de délai pour une annulation sans frais.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Dois-je être sur place pendant les missions ?</h3>
                    <p>
                        Non, notre service de gardiennage des clés vous permet de ne plus vous préoccuper 
                        de cette problématique. Votre <strong>femme de ménage</strong> sera ainsi parfaitement 
                        autonome.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Et si je ne veux pas laisser mes clés ?!</h3>
                    <p>
                        Si vous le souhaitez, vous pouvez laisser vos clés à la gardienne ou systématiquement
                        être sur place pour nous ouvrir.
                    </p>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 question">
                    <h3>Que se passe t-il si j'ai besoin d'un passage supplémentaire ponctuellement ?</h3>
                    <p>
                        Pas de souci ! Sous réserve de disponibilité, vous pouvez ajouter un/des passages de manière
                        ponctuelle.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="sub-button">
        <div class="section-transition section-transition-blue"><div></div></div>
        <div class="container-fluid section">
            <div class="row">
                <div class="col-xs-12">
                    <a href="/reservation/besoin" class="btn btn-lg btn-gold btn-gold-fix">réserver maintenant</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section-dark" id="ask">
        <div class="container-fluid">
            <div class="row">
                <div class="landing-wrapper landing-wrapper-dark">
                    <h2>Une autre question</h2>
                    <p class="gold">contactez-nous !</p>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 ask-content">
                    <form id="ask-form">
                        <div class="col-xs-12 col-md-6">
                            <label for="name"></label>
                            <input type="text" class="form-control input-lg" name="name" placeholder="Nom, Prénom" id="name" required/>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <label for="email"></label>
                            <input type="email" class="form-control input-lg" name="email" placeholder="Email" id="email" required/>
                        </div>
                        <div class="col-xs-12">
                            <label for="message"></label>
                            <textarea class="form-control input-lg" form="ask-form" maxlength="200" rows="4" name="message" placeholder="Message" id="message" required></textarea>
                        </div>
                        <button type="button" class="btn btn-gold" id="questionValidation">envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include_once ('../inc/footer_start.php'); ?>
<?php include_once ('../inc/exit_popup_phone.php'); ?>

    <script>
        // API call contact
        $('#questionValidation').on('click', function () {
            $.ajax({
                beforeSend: function(request) {
                    var apiToken = "<?php echo Session::getInstance()->read('apiToken'); ?>";

                    request.setRequestHeader("apiToken", apiToken);
                },
                url: "<?php echo $_SERVER['HTTP_URL_API_NOBO'] . '/v1/mailJet/contact'; ?>",
                type: 'POST',
                data: {
                    subject: 'Une nouvelle question a été posée!',
                    name: $('#name').val(),
                    email: $('#email').val(),
                    message: $('#message').val()
                },
                dataType: 'json',
                success: function (ans) {
                    var messages = ans.message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'success'
                        });
                    });
                    $('#name').val("");
                    $('#email').val("");
                    $('#message').val("");
                },
                error: function (jqXHR) {
                    var messages = $.parseJSON(jqXHR.responseText).message;
                    messages.forEach(function (message) {
                        $.notify({
                            // options
                            message: message
                        },{
                            // settings
                            type: 'danger'
                        });
                    });
                }
            });
        });

    </script>

<?php if (strtolower($_SERVER['HTTP_STAGE']) === 'prod'): ?>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                fbq('track', 'ViewContent');
            });
        });
    </script>
<?php endif; ?>

<?php include_once ('../inc/analyticstracking.php'); ?>
<?php include_once ('../inc/footer_end.php'); ?>
